using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class AOUGuides : PluginBase
    {
        private string AOURL="http://{0}/mobile/parser.php";
        private string WPURL="http://{0}/mobile/waypoints.php";
        private string aouhost="www.ao-universe.com";
        private string thisbot="vhabot";
        private int[] tradeskill_sections =
            new int[6] { 13, 25, 30, 47, 55, 62  };
        private int[] quest_sections =
            new int[7] { 10, 28, 45, 69,  // quest folders
                         11, 54, 74 };    // encounter folders

        public AOUGuides()
        {
            this.Name = "AO-Universe Guide Lookup";
            this.InternalName = "llAOUGuides";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("aou", true, UserLevel.Guest),
                new Command("recipe", true, UserLevel.Guest),
                new Command("quest", true, UserLevel.Guest),
                new Command("locate", true, UserLevel.Guest),
                new Command("aou_guide", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
        }

        public override void OnUnload(BotShell bot)
        {
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
        }

        private void LoadConfiguration(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {
            case "aou_guide":
                int id = 0;
                try
                {
                    id = Convert.ToInt32( e.Args[0] );
                }
                catch
                {
                    bot.SendReply(e, "Error.  Unknown guide ID number.");
                }
                DisplayGuide ( bot, e, id );
                break;

            case "aou":
            case "recipe":
            case "quest":
                SearchGuides ( bot, e );
                break;

            case "locate":
                SearchWaypoints ( bot, e );
                break;

            }

        }

        private void DisplayGuide ( BotShell bot, CommandArgs e, int id )
        {

            bot.SendReply(e, "Attempting to parse and display guide " +
                          Convert.ToString( id ) + ".  Please be patient...");

            string url=string.Format(AOURL,aouhost);
            url = url + "?mode=view&id=" + Convert.ToString(id) + "&bot=" +
                thisbot + "&fullitem=true";

            string xml = HTML.GetHtml(url, 60000);
            if (string.IsNullOrEmpty(xml))
            {
                bot.SendReply(e, "Unable to connect to AO-Universe.");
                return;
            }

            // Fix results to prevent XMLSerializer from crashing
            xml = xml.Replace( "&nbsp;", " " );

            MemoryStream stream = null;
            AOUGuidesViewResults search_results = null;

            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(AOUGuidesViewResults));
                search_results = (AOUGuidesViewResults)serializer.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                bot.SendReply(e, "Unable parse results from AO-Universe.");  
                if (stream != null)
                    stream.Close();
                return;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            if ( ( search_results.Section == null ) ||
                 ( search_results.Section.Length == 0 ) )
            {
                bot.SendReply(e, "Specified guide not found on AO-Universe.");
                return;
            }
            else
            {
                // debugging purposes:
                // Console.WriteLine( search_results.Section[0].Content.Text );

                RichTextWindow window = new RichTextWindow(bot);

                window.AppendTitle( "AO-Universe Guide: " +
                                    search_results.Section[0].Content.Name );
                window.AppendLineBreak();
                window.AppendHighlight("Updated: ");
                window.AppendNormal(search_results.Section[0].Content.Update);
                window.AppendLineBreak();
                window.AppendHighlight("Class: ");
                window.AppendNormal(search_results.Section[0].Content.Class);
                window.AppendLineBreak();
                window.AppendHighlight("Faction: ");
                window.AppendNormal(search_results.Section[0].Content.Faction);
                window.AppendLineBreak();
                window.AppendHighlight("Level(s): ");
                window.AppendNormal(search_results.Section[0].Content.Level);
                window.AppendLineBreak();
                window.AppendHighlight("Author: ");
                window.AppendNormal(StripBBCode(search_results.Section[0].Content.Author));
                window.AppendLineBreak(2);

                try
                {
                    AppendGuideText( bot, e, window,
                                     search_results.Section[0].Content.Text );

                    bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex,
                                                            "AO-Universe Guide: " +
                                                            search_results.Section[0].Content.Name  ) + " »» ", window);
                }
                catch
                {
                    bot.SendReply(e, "An error occurred trying to parse this guide.  Please post a comment for this guide on the AOU website indicating there is a problem with the guide." );
                }

            }

        }

        private void ParseItem( string substr, ref Int32 LowID,
                                ref Int32 IconID, ref string ItemName )
        {
            Regex tag = new Regex("[[][^]]+[]]");
            substr = tag.Replace( substr, "" );
            string[] item_array = substr.Split(';');
            LowID = 0;
            IconID = 0;
            ItemName = "[Item]";

            if ( item_array.Length == 3 )
            {
                try
                {
                    LowID = Convert.ToInt32( item_array[0] );
                    IconID = Convert.ToInt32( item_array[1] );
                    ItemName = item_array[2];
                }
                catch
                {
                }
            }
            
        }

        private string StripBBCode( string buffer )
        {
            while ( buffer.IndexOf( '[' ) >= 0 )
                buffer = buffer.Remove( buffer.IndexOf( '[' ),
                                        buffer.IndexOf( ']' ) - buffer.IndexOf( '[' ) + 1 );
            return buffer;
        }

        private void AppendGuideText( BotShell bot, CommandArgs e,
                                      RichTextWindow window, string text )
        {
            string buffer = text;
            string substr = string.Empty;
            string url = string.Empty;
            string bbtag = string.Empty;

            Int32 LowID = 0;
            Int32 IconID = 0;
            string ItemName = "[Item]";

            while ( buffer.Length > 0 )
            {

                // grab a section of text up to the next BBCode tag
                substr = string.Empty;
                if ( buffer.IndexOf( '[' ) > 0 )
                {
                    substr = buffer.Substring( 0, buffer.IndexOf( '[' ) );
                    buffer = buffer.Remove( 0, buffer.IndexOf( '[' ) );
                }
                else if ( buffer.IndexOf( '[' ) < 0 )
                {
                    substr = buffer;
                    buffer = string.Empty;
                }
                if ( substr != string.Empty )
                    window.AppendString( substr );

                if ( buffer.IndexOf( '[') == 0 )
                {
                    // handle BBCode tags
                    bbtag = buffer.Substring( 1, buffer.IndexOfAny( new char[]{ ']', '=', ' ' } ) - 1 );
                    string closetag = "[/" + bbtag + "]";

                    switch (bbtag)
                    {

                        // tags not yet handled...
                        // case "anchor":
                        // case "ct":
                        // case "quote":
                        // case "list":
                        // case "spoiler":
                        //     break;

                    case "localurl":
                        if ( buffer.IndexOf('=') < buffer.IndexOf(']') )
                        {
                            url = buffer.Substring( buffer.IndexOf('=') + 1,
                                                    buffer.IndexOf(']') -
                                                    buffer.IndexOf('=') - 1);
                            substr = buffer.Substring( buffer.IndexOf(']')+1,
                                                       buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)-
                                                       buffer.IndexOf(']')-1 );
                        }
                        else
                        {
                            url = buffer.Substring( buffer.IndexOf(']')+1,
                                                    buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)-
                                                    buffer.IndexOf(']')-1 );
                            substr = "[link]";
                        }
                        if ( url.IndexOf("pid=") >= 0 ) 
                        {
                            // link points to another AOU guide
                            int pid = url.IndexOf("pid=") + 4;
                            int idx = url.IndexOfAny( new char[]{ ']', '&', '#' },
                                                      pid );
                            if ( idx >= 0 )
                                url = url.Substring( pid , idx - pid );
                            else
                                url = url.Substring( pid );
                            window.AppendBotCommand( substr,
                                                     "aou_guide " + url );
                        }
                        else
                        {
                            if ( substr.IndexOf( "[itemicon" ) >= 0 )
                            {
                                ParseItem(substr, ref LowID, ref IconID, ref ItemName);
                                Regex tag = new Regex("[[]itemicon[^]]*[]][^[]+[[]/itemicon[]]");
                                substr = tag.Replace( substr, string.Format("<img src=rdb://{0}>", IconID ) );
                            }
                            PluginShared.AppendLink( window, "/start http://" +
                                                     aouhost + "/" + url,
                                                     substr );
                        }
                        buffer = buffer.Remove( 0, buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase) + closetag.Length );
                        break;

                    case "url":
                        if ( buffer.IndexOf('=') < buffer.IndexOf(']') )
                        {
                            url = buffer.Substring( buffer.IndexOf('=') + 1,
                                                    buffer.IndexOf(']') -
                                                    buffer.IndexOf('=') - 1);
                            substr = buffer.Substring( buffer.IndexOf(']')+1,
                                                       buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)-
                                                       buffer.IndexOf(']')-1 );
                        }
                        else
                        {
                            url = buffer.Substring( buffer.IndexOf(']')+1,
                                                    buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)-
                                                    buffer.IndexOf(']')-1 );
                            substr = "[link]";
                        }
                        if (( ! url.StartsWith( "http://" ) &&
                              ( url.IndexOf("pid=") >= 0 ) ) ||
                            ( url.StartsWith( "http://www.ao-universe.com/" ) &&
                              ( url.IndexOf("pid=") >= 0 )) )
                        {
                            // link points to another AOU guide
                            int pid = url.IndexOf("pid=") + 4;
                            int idx = url.IndexOfAny( new char[]{ ']', '&', '#' },
                                                      pid );
                            if ( ( pid >=0 ) && ( idx >= 0 ) )
                                url = url.Substring( pid , idx - pid );
                            else if ( pid >=0 )
                                url = url.Substring( pid );
                            window.AppendBotCommand( substr, 
                                                     "aou_guide " + url );
                        }
                        else
                        {
                            if ( substr.IndexOf( "[itemicon" ) >= 0 )
                            {
                                ParseItem(substr, ref LowID, ref IconID, ref ItemName);
                                Regex tag = new Regex("[[]itemicon[^]]*[]][^[]+[[]/itemicon[]]");
                                substr = tag.Replace( substr, string.Format("<img src=rdb://{0}>", IconID ) );
                            }
                            PluginShared.AppendLink( window, url, substr );
                        }
                        buffer = buffer.Remove( 0, buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase) +
                                                closetag.Length );
                        break;

                    case "color":
                        substr = buffer.Substring( buffer.IndexOf('=') + 1,
                                                   buffer.IndexOf(']') -
                                                   buffer.IndexOf('=') - 1);
                        buffer = buffer.Remove( 0, buffer.IndexOf(']') + 1);
                        if ( substr[0] != '#' )
                        {
                            switch( substr )
                            {
                            case "orange":
                                substr = "DD6600";
                                break;
                            case "blue":
                                substr = "00BBFF";
                                break;
                            case "red":
                                substr = "DD0000";
                                break;
                            case "purple":
                                substr = "DD00DD";
                                break;
                            case "yellow":
                                substr = "DDDD00";
                                break;
                            case "gray":
                                substr = "DDDDDD";
                                break;
                            case "green":
                                substr = "00DD00";
                                break;
                            }
                        }
                        window.AppendColorStart(substr);
                        break;

                    case "/color":
                        buffer = buffer.Remove( 0, buffer.IndexOf(']')+1 );
                        window.AppendColorEnd();
                        break;

                    // handle bbtags that are the same as HTML that work in AO
                    case "center": 
                    case "/center": 
                    case "u":
                    case "/u":
                        buffer = buffer.Remove( 0, buffer.IndexOf(']') + 1);
                        window.AppendRawString("<" + bbtag + ">");
                        break;

                    case "b":
                    case "i":
                    case "size":
                        // we handle all other formatting with "highlight"

                        // remove open and close tags
                        buffer = buffer.Remove( 0, buffer.IndexOf(']')+1 );
                        if ( buffer[0] != '[' )
                        {
                            substr = buffer.Substring( 0, buffer.IndexOf('[') );
                            buffer = buffer.Remove( 0, substr.Length );
                            window.AppendHighlight( substr );
                        }
                        if ( buffer.IndexOf( closetag,StringComparison.InvariantCultureIgnoreCase ) >= 0 )
                            buffer=buffer.Remove(buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase),
                                                 closetag.Length);
                    break;

                    case "br":
                    case "cttr":
                    case "/tr":  // break at table row ends
                        buffer = buffer.Remove( 0, buffer.IndexOf(']')+1 );
                        window.AppendLineBreak();
                        break;

                    case "img":
                        // we can't display images in AO
                        buffer = buffer.Remove( 0, buffer.IndexOf("[/img]")+6 );
                        window.AppendString("[Image]");
                        break;

                    case "item":
                    case "itemname":
                    case "itemicon":
                        substr = buffer.Substring( buffer.IndexOf( ']' )+1,
                                                   buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)-
                                                   buffer.IndexOf( ']' )-1);
                        buffer = buffer.Remove( 0, buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)+
                                                closetag.Length );
                        ParseItem(substr, ref LowID, ref IconID, ref ItemName);

                        switch ( bbtag )
                        {

                        case "item":
                            window.AppendIcon( IconID );
                            window.AppendLineBreak();
                            window.AppendItem( ItemName, LowID, LowID, 1 );
                            window.AppendLineBreak();
                            break;

                        case "itemicon":
                            window.AppendIcon( IconID );
                            break;

                        case "itemname":
                            window.AppendItem( ItemName, LowID, LowID, 1 );
                            break;

                        }
                        break;

                    case "ts_t":
                        // tradeskill formula
                        // ts_ts / ts_ts2
                        substr = buffer.Substring( buffer.IndexOf( ']' )+1,
                                                   buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)-
                                                   buffer.IndexOf( ']' )-1);
                        buffer = buffer.Remove( 0, buffer.IndexOf(closetag,StringComparison.InvariantCultureIgnoreCase)+
                                                closetag.Length );

                        List<Int32> LowID1 = new List<Int32>();
                        List<Int32> LowID2 = new List<Int32>();
                        List<Int32> LowID3 = new List<Int32>();
                        List<Int32> IconID1 = new List<Int32>();
                        List<Int32> IconID2 = new List<Int32>();
                        List<Int32> IconID3 = new List<Int32>();
                        List<string> ItemName1 = new List<string>();
                        List<string> ItemName2 = new List<string>();
                        List<string> ItemName3 = new List<string>();

                        // parse first section of ts_t block
                        int section = 0;
                        int idx1 = 0;
                        int idx2 = 0;
                        int idxc = 0;
                        string item_to_parse;
                        while ( substr.IndexOf( "[item]" ) >= 0 )
                        {
                            idx1 = substr.IndexOf( "[item]" );
                            idx2 = substr.IndexOf( "[/item]" );
                            if ( section == 0 )
                            {
                                idxc = substr.IndexOf( "[ts_ts]" );
                                if ( ( idxc >= 0 ) && ( idx1 > idxc ) )
                                {
                                    substr = substr.Remove( idxc, idxc+7 );
                                    idx1 = substr.IndexOf( "[item]" );
                                    idx2 = substr.IndexOf( "[/item]" );
                                    section ++;
                                }
                            }
                            if ( section == 1 )
                            {
                                idxc = substr.IndexOf( "[ts_ts2]" );
                                if ( ( idxc >= 0 ) && ( idx1 > idxc ) )
                                {
                                    substr = substr.Remove( idxc, idxc+8 );
                                    idx1 = substr.IndexOf( "[item]" );
                                    idx2 = substr.IndexOf( "[/item]" );
                                    section ++;
                                }
                            }
                            item_to_parse = substr.Substring( idx1+6,
                                                              idx2-idx1-6 );
                            Int32 parse_id = 0;
                            Int32 parse_icon = 0;
                            string parse_name  = string.Empty;

                            ParseItem( item_to_parse, ref parse_id,
                                       ref parse_icon, ref parse_name);
                            switch ( section )
                            {
                            case 0:
                                LowID1.Add(parse_id);
                                IconID1.Add(parse_icon);
                                ItemName1.Add(parse_name);
                                break;
                            case 1:
                                LowID2.Add(parse_id);
                                IconID2.Add(parse_icon);
                                ItemName2.Add(parse_name);
                                break;
                            case 2:
                                LowID3.Add(parse_id);
                                IconID3.Add(parse_icon);
                                ItemName3.Add(parse_name);
                                break;
                            }
                            substr = substr.Substring( idx2+7 );
                        }

                        int nitems = 0;
                        foreach ( Int32 icon in IconID1 )
                            window.AppendIcon( icon );
                        window.AppendHighlight( " + " );
                        nitems = 0;
                        foreach ( Int32 icon in IconID2 )
                            window.AppendIcon( icon );
                        window.AppendHighlight( " = " );
                        foreach ( Int32 icon in IconID3 )
                            window.AppendIcon( icon );

                        window.AppendLineBreak( );
                        window.AppendNormal( "Add " );
                        nitems = 0;
                        foreach ( Int32 id in LowID1 )
                        {
                            if ( nitems > 0 )
                                window.AppendNormal( " or " );
                            window.AppendItem( ItemName1[nitems], id, id, 1 );
                            nitems ++;
                        }
                        window.AppendLineBreak( );
                        window.AppendNormal( " to " );
                        nitems = 0;
                        foreach ( Int32 id in LowID2 )
                        {
                            if ( nitems > 0 )
                                window.AppendNormal( " or " );
                            window.AppendItem( ItemName2[nitems], id, id, 1 );
                            nitems ++;
                        }
                        window.AppendLineBreak( );
                        window.AppendNormal( "to obtain " );
                        nitems = 0;
                        foreach ( Int32 id in LowID3 )
                        {
                            if ( nitems > 0 )
                                window.AppendNormal( " or " );
                            window.AppendItem( ItemName3[nitems], id, id, 1 );
                            nitems ++;
                        }
                        window.AppendLineBreak( );

                        break;

                    case "waypoint":
                        // waypoints
                        substr = buffer.Substring( buffer.IndexOf( ']' )+1,
                                                   buffer.IndexOf(closetag)-
                                                   buffer.IndexOf( ']' )-1);
                        Int32 start, length;
                        string x, y, pfid;

                        start = buffer.IndexOf("x=")+2;
                        length = buffer.IndexOfAny( new char[]{ ']', ' ' }, start ) - start;
                        x = buffer.Substring( start, length );

                        start = buffer.IndexOf("y=")+2;
                        length = buffer.IndexOfAny( new char[]{ ']', ' ' }, start ) - start;
                        y = buffer.Substring( start, length );

                        start = buffer.IndexOf("pf=")+3;
                        length = buffer.IndexOfAny( new char[]{ ']', ' ' }, start ) - start;
                        pfid = buffer.Substring( start, length );
                        
                        window.AppendCommand( substr, "/waypoint " + x + " " + y + " " + pfid );
                        buffer = buffer.Remove( 0, buffer.IndexOf(closetag)+
                                                closetag.Length );
                        break;

                    case "cttd":
                        // remove tag
                        buffer = buffer.Remove( 0, buffer.IndexOf(']')+1 );
                        // add a space
                        window.AppendRawString( " " );
                        // cttd usually has no close tag
                        break;
                                                
                    default:
                        if ( bbtag[0] == '/' ) // Orphan close tag. Delete & move on
                            buffer = buffer.Remove( 0, buffer.IndexOf(']')+1 );
                        else
                        {
                            // Don't know what to do about a tag? Delete & move on.
                            // remove current tag and matching close tag
                            buffer = buffer.Remove( 0, buffer.IndexOf(']')+1 );
                            if ( buffer.IndexOf( closetag ) >= 0 )
                                buffer=buffer.Remove(buffer.IndexOf(closetag),
                                                     closetag.Length);
                        }
                        break;
                    }

                }

            }
        }

        private void SearchGuides ( BotShell bot, CommandArgs e )
        {
            string urlTemplate=string.Format(AOURL,aouhost);
            urlTemplate = urlTemplate + "?mode=search&search={0}&bot={1}";

            if ( e.Args.Length == 0 )
            {
                bot.SendReply(e, "Please specify a search term.");
                return;
            }

            // assemble search terms
            string search_string = HttpUtility.UrlEncode( e.Words[0] );

            string url = string.Format( urlTemplate, search_string, thisbot );
            
            string xml = HTML.GetHtml(url, 60000);
            if (string.IsNullOrEmpty(xml))
            {
                bot.SendReply(e, "Unable to connect to AO-Universe.");
                return;
            }

            MemoryStream stream = null;
            AOUGuidesSearchResults search_results = null;

            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(AOUGuidesSearchResults));
                search_results = (AOUGuidesSearchResults)serializer.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                bot.SendReply(e, "Unable parse results from AO-Universe.");
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            if ( ( search_results.Sections == null ) ||
                 ( search_results.Sections.Length == 0 ) )
            {
                bot.SendReply(e, "No guides found on AO-Universe matching your search terms.");
                return;
            }
            else
            {

                // count total results:
                int n_guides = 0;
                for ( int i=0; i<search_results.Sections.Length; i++ )
                    for( int j=0;
                         j<search_results.Sections[i].GuideList.Guides.Length;
                         j++ )
                    {

                        // find offset = 2
                        int offset_idx = 1;
                        for( int k=0; k<search_results.Sections[i].FolderList.Folders.Length; k++ )
                            if ( search_results.Sections[i].FolderList.Folders[k].Offset == 2 )
                                offset_idx = k;

                        // filtered count
                        if( ( e.Command == "recipe" ) &&
                            ( ( search_results.Sections[i].FolderList.Folders.Length > 1 ) &&
                              ( Array.IndexOf( tradeskill_sections, search_results.Sections[i].FolderList.Folders[offset_idx].ID ) < 0 ) || ( search_results.Sections[i].FolderList.Folders.Length< 2 ) ) )
                            continue;

                        if( ( e.Command == "quest" ) &&
                            ( ( search_results.Sections[i].FolderList.Folders.Length > 1 ) &&
                              ( Array.IndexOf( quest_sections, search_results.Sections[i].FolderList.Folders[offset_idx].ID ) < 0 ) || ( search_results.Sections[i].FolderList.Folders.Length< 2 ) ) )
                            continue;

                        n_guides ++;
                    }

                RichTextWindow window = new RichTextWindow(bot);

                window.AppendTitle("AO-Universe Guide Search");
                window.AppendHighlight("Search String: ");
                window.AppendNormal( "\"" + e.Words[0] + "\"" );
                window.AppendLineBreak();
                window.AppendHighlight("Results: ");
                window.AppendNormal( Convert.ToString( n_guides ) );
                window.AppendLineBreak(2);
                window.AppendHeader("Search Results");

                for ( int i=0; i<search_results.Sections.Length; i++ )
                {

                    // find offset = 2
                    int offset_idx = 1;
                    for( int k=0; k<search_results.Sections[i].FolderList.Folders.Length; k++ )
                        if ( search_results.Sections[i].FolderList.Folders[k].Offset == 2 )
                            offset_idx = k;

                    // filtered count
                    if( ( e.Command == "recipe" ) &&
                        ( ( search_results.Sections[i].FolderList.Folders.Length > 1 ) &&
                          ( Array.IndexOf( tradeskill_sections, search_results.Sections[i].FolderList.Folders[offset_idx].ID ) < 0 ) || ( search_results.Sections[i].FolderList.Folders.Length< 2 ) ) )
                        continue;

                    if( ( e.Command == "quest" ) &&
                        ( ( search_results.Sections[i].FolderList.Folders.Length > 1 ) &&
                          ( Array.IndexOf( quest_sections, search_results.Sections[i].FolderList.Folders[offset_idx].ID ) < 0 ) || ( search_results.Sections[i].FolderList.Folders.Length< 2 ) ) )
                        continue;


                    window.AppendLineBreak();
                    for( int j=search_results.Sections[i].FolderList.Folders.Length-1;
                         j>=0; j-- )
                    {
                        window.AppendHighlight( search_results.Sections[i].FolderList.Folders[j].Name );
                        if ( j > 0 ) window.AppendHighlight( " : " );
                    }
                    window.AppendLineBreak();
                    for( int j=0;
                         j<search_results.Sections[i].GuideList.Guides.Length;
                         j++ )
                    {
                        window.AppendBotCommand(
                            search_results.Sections[i].GuideList.Guides[j].Name,
                            "aou_guide " +
                            search_results.Sections[i].GuideList.Guides[j]._ID);
                        window.AppendLineBreak();
                        window.AppendNormal("    " );
                        window.AppendNormal(search_results.Sections[i].GuideList.Guides[j].Description);
                        window.AppendLineBreak();
                    }
                }

                bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, Convert.ToString( n_guides ) ) + " Results »» ", window);
            }

        }

        private void SearchWaypoints ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length == 0 )
            {
                bot.SendReply(e, "Please specify a search term.");
                return;
            }

            string urlTemplate=string.Format(WPURL,aouhost);
            urlTemplate = urlTemplate + "?mode=search&search={0}&bot={1}";

            // assemble search terms
            string search_string = HttpUtility.UrlEncode( e.Words[0] );

            string url = string.Format( urlTemplate, search_string, thisbot );
            
            string xml = HTML.GetHtml(url, 60000);
            if (string.IsNullOrEmpty(xml))
            {
                bot.SendReply(e, "Unable to connect to AO-Universe.");
                return;
            }

            MemoryStream stream = null;
            AOUWaypointSearchResults search_results = null;

            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(AOUWaypointSearchResults));
                search_results = (AOUWaypointSearchResults)serializer.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                bot.SendReply(e, "Unable parse results from AO-Universe.");
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            if ( ( search_results == null ) ||
                 ( search_results.Waypoints == null ) ||
                 ( search_results.Waypoints.Waypoint.Length < 1 ) )
            {
                
                string result = string.Empty;
                List<string> other_commands = new List<string>();
                if ( bot.Plugins.IsLoaded("llNPCs") )
                    other_commands.Add("!npc");
                if ( bot.Plugins.IsLoaded("llWhereis") )
                    other_commands.Add("!whereis");
                if ( bot.Plugins.IsLoaded("llUniqueMobs") )
                    other_commands.Add("!boss");
                if ( other_commands.Count > 0 )
                {
                    result =  "  Perhaps the location you are looking for might be found in one of the other location databases this bot has access to.  You could try one of these other commands: ";
                    for ( int i=0; i<other_commands.Count; i++ )
                    {
                        result = result + other_commands[i];
                        if ( i < other_commands.Count - 1 )
                            result = result + ", ";
                    }
                }

                bot.SendReply(e, "No locations matching \"" +
                              e.Words[0] + "\"." + result );
                return;
            }

            // eliminate duplicates
            List<AOUWaypoints> Waypoints = new List<AOUWaypoints>();
            if ( search_results.Waypoints.Waypoint != null )
                foreach ( AOUWaypoints rp in search_results.Waypoints.Waypoint )
                {
                    bool exists = false;
                    foreach( AOUWaypoints wp in Waypoints )
                    {
                        if ( ( rp.PFID == wp.PFID ) &&
                             ( rp.X == wp.X ) &&
                             ( rp.Y == wp.Y ) &&
                             ( rp.Name == wp.Name ) )
                            exists = true;
                    }
                    if ( ( Waypoints.Count == 0 ) || ! exists )
                        Waypoints.Add(rp);
                }

            RichTextWindow window = new RichTextWindow(bot);

            window.AppendTitle("AO-Universe Waypoint Search");
            window.AppendHighlight("Search String: ");
            window.AppendNormal( "\"" + e.Words[0] + "\"" );
            window.AppendLineBreak();
            window.AppendHighlight("Results: ");
            window.AppendNormal( Waypoints.Count.ToString() );
            window.AppendLineBreak(2);
            window.AppendHeader("Search Results");

            Playfields PFs = PluginShared.GetPlayfields();

            foreach ( AOUWaypoints wp in Waypoints )
            {
                window.AppendNormal("[");
                PluginShared.AppendWaypoint( window, wp.X, wp.Y, wp.PFID,
                                             "Waypoint" );
                window.AppendNormal("] [");
                window.AppendBotCommand("Guide", "!aou_guide " + wp._id );
                window.AppendNormal("] ");
                string name = wp.Name;
                if ( name.IndexOf( " in " ) > 0 )
                    name = name.Substring(0, name.IndexOf( " in " ) );
                if ( name.IndexOf( " at " ) > 0 )
                    name = name.Substring(0, name.IndexOf( " at " ) );
                window.AppendHighlight(name);
                window.AppendNormal(" (");
                window.AppendHighlight( PluginShared.GetPFName ( wp.PFID,
                                                                 PFs ) );
                window.AppendNormal(")");
                window.AppendLineBreak();
            }

            bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, Convert.ToString( Waypoints.Count ) ) + " Results »» ", window);

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "aou":
                return "Searches all of AO-Universe guides returns matching guides.\n\n" +
                    "Usage: /tell " + bot.Character + " aou [search string]";

            case "quest":
                return "Searches only quests and encounter guides on AO-Universe.\n\n" +
                    "Usage: /tell " + bot.Character + " quest [search string]";

            case "recipe":
                return "Searches only tradeskill guides on AO-Universe.\n\n" +
                    "Usage: /tell " + bot.Character + " recipe [search string]";

            case "locate":
                return "Searches waypoints listed in AO-Universe guides.\n\n" +
                    "Usage: /tell " + bot.Character + " locate [search string]";

            case "aou_guide":
                return "Display a specific AO-Universe guide.\n\n" +
                    "Usage: /tell " + bot.Character + " aou_guide [ID]";

            }
            return null;
        }

    }

    [XmlRoot("result")]
    public class AOUGuidesSearchResults
    {
        [XmlElement("section")]
        public AOUGuidesSearchSection[] Sections;
    }

    [XmlRoot("section")]
    public class AOUGuidesSearchSection
    {
        [XmlElement("folderlist")]
        public AOUGuidesFolderList FolderList;
        [XmlElement("guidelist")]
        public AOUGuidesGuideList GuideList;
    }

    [XmlRoot("result")]
    public class AOUGuidesViewResults
    {
        [XmlElement("section")]
        public AOUGuidesViewSection[] Section;
    }

    [XmlRoot("section")]
    public class AOUGuidesViewSection
    {
        [XmlElement("folderlist")]
        public AOUGuidesFolderList FolderList;
        [XmlElement("content")]
        public AOUGuidesContent Content;
    }

    [XmlRoot("folderlist")]
    public class AOUGuidesFolderList
    {
        [XmlElement("folder")]
        public AOUGuidesFolder[] Folders;
    }

    [XmlRoot("folder")]
    public class AOUGuidesFolder
    {
        [XmlElement("id")]
        public string _ID;
        [XmlElement("name")]
        public string Name;
        [XmlElement("offset")]
        public string _Offset;
        [XmlElement("items")]
        public string _Items;
        [XmlElement("parent")]
        public string _Parent;

        public int ID { get { try { return Convert.ToInt32(this._ID); } catch { return 0; } } }
        public int Offset { get { try { return Convert.ToInt32(this._Offset); } catch { return 0; } } }
        public int Items { get { try { return Convert.ToInt32(this._Items); } catch { return 0; } } }
        public int Parent { get { try { return Convert.ToInt32(this._Parent); } catch { return 0; } } }
        
    }
    
    [XmlRoot("guidelist")]
    public class AOUGuidesGuideList
    {
        [XmlElement("guide")]
        public AOUGuidesGuide[] Guides;
    }

    [XmlRoot("guide")]
    public class AOUGuidesGuide
    {
        [XmlElement("id")]
        public string _ID;
        [XmlElement("name")]
        public string Name;
        [XmlElement("desc")]
        public string Description;

        public int ID { get { try { return Convert.ToInt32(this._ID); } catch { return 0; } } }
    }

    [XmlRoot("content")]
    public class AOUGuidesContent
    {
        [XmlElement("id")]
        public string _ID;
        [XmlElement("name")]
        public string Name;
        [XmlElement("update")]
        public string Update;
        [XmlElement("class")]
        public string Class;
        [XmlElement("faction")]
        public string Faction;
        [XmlElement("level")]
        public string Level;
        [XmlElement("author")]
        public string Author;
        [XmlElement("text")]
        public string Text;

        public int ID { get { try { return Convert.ToInt32(this._ID); } catch { return 0; } } }
    }

    [XmlRoot("result")]
    public class AOUWaypointSearchResults
    {
        [XmlElement("waypoints")]
        public AOUWaypointsSection Waypoints;
    }

    [XmlRoot("waypoints")]
    public class AOUWaypointsSection
    {
        [XmlElement("waypoint")]
        public AOUWaypoints[] Waypoint;
    }

    [XmlRoot("waypoint")]
    public class AOUWaypoints
    {
        [XmlElement("guide_id")]
        public string _id;
        [XmlElement("xcoord")]
        public string _xcoord;
        [XmlElement("ycoord")]
        public string _ycoord;
        [XmlElement("pfid")]
        public string _pfid;
        [XmlElement("coordname")]
        public string Name;
        [XmlElement("parent")]
        public AOUWaypointParent Parent;

        public float Xcoord { get { try { float v; float.TryParse(this._xcoord, out v); return v; } catch { return 0; } } }
        public float Ycoord { get { try { float v; float.TryParse(this._ycoord, out v); return v; } catch { return 0; } } }

        public int ID { get { try { return Convert.ToInt32(this._id); } catch { return 0; } } }
        public int X { get { try { return Convert.ToInt32(this.Xcoord); } catch { return 0; } } }
        public int Y { get { try { return Convert.ToInt32(this.Ycoord); } catch { return 0; } } }
        public int PFID { get { try { return Convert.ToInt32(this._pfid); } catch { return 0; } } }

    }

    [XmlRoot("parent")]
    public class AOUWaypointParent
    {
        [XmlElement("folder")]
        public AOUWaypointFolder[] Folders;
    }

    [XmlRoot("folder")]
    public class AOUWaypointFolder
    {
        [XmlElement("id")]
        public string _id;
        [XmlElement("name")]
        public string Name;

        public int ID { get { try { return Convert.ToInt32(this._id); } catch { return 0; } } }
    }

}
