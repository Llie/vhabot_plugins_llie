using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Threading;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class AltMsg : PluginBase
    {
        private int _mailboxsize = 21;
        private Config _database;
        private BotShell _bot;
        private System.Timers.Timer _welcome;

        public AltMsg()
        {
            this.Name = "Alt Message Service";
            this.InternalName = "llAltMsg";
            this.Author = "Llie";
            this.Description = "Send and receive messages to players independant of what alt they log.";
            this.Version = 100;
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("msgs", true, UserLevel.Guest),
                new Command("msg", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);
            bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            bot.Events.ConfigurationChangedEvent +=new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "mailboxsize", "Mailbox Size", this._mailboxsize );

            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS altmessages ( Username VARCHAR(14), Sender VARCHAR(14), MessageID INTEGER, Message TEXT, Read BOOLEAN, PRIMARY KEY ( Username, MessageID ) )");

            this._bot = bot;

            this._welcome = new System.Timers.Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;
        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= AltMsgWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += AltMsgWindow;
                this._bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);
            this._bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= AltMsgWindow;
            bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
            bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);

            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            int total_messages = 0;
            int unread_messages = 0;
            int max_id = 0;
            string user = e.Sender;
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Alt Messaging System");
            window.AppendLineBreak(2);

            switch (e.Command)
            {

            case "msgs":
                UserCheckMessages( bot, user );
                break;

            case "msg":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: msg username|read|list|delete [message|message id]");
                    return;
                }

                switch ( e.Args[0] )
                {
                case "list":
                    
                    using (IDbCommand command=this._database.Connection.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM altmessages WHERE Username = '" + user + "'";
                        IDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            total_messages ++;
                            if ( reader.GetBoolean(4) )
                                window.AppendNormal("[");
                            else
                                window.AppendHighlight("[");
                            window.AppendCommand( Convert.ToString( total_messages ),
                                                  "/tell " + bot.Character + " msg read " + Convert.ToString( reader.GetInt32(2) ) );
                            if ( reader.GetBoolean(4) )
                                window.AppendNormal("] ");
                            else
                                window.AppendHighlight("] ");
                            window.AppendNormal( "From: " );
                            window.AppendHighlight( reader.GetString(1) );
                            String brief = reader.GetString(3);
                            if ( brief.Length > 10 )
                            {
                                brief = brief.Substring(0, 10);
                                brief = brief + "...";
                            }
                            window.AppendNormal( " >> " + brief + " [" );
                            window.AppendCommand( "delete",
                                                  "/tell " + bot.Character + " msg delete " + Convert.ToString( reader.GetInt32(2) ) );
                            window.AppendNormal( "]" );
                            window.AppendLineBreak();
                        }
                        reader.Close();
                    }
                    if ( total_messages > 0 )
                        bot.SendReply( e, "Messages »» ", window);
                    else
                        bot.SendReply( e, "You have no messagse." );
                    break;

                case "read":
                    if (e.Args.Length < 2)
                    {
                        bot.SendReply(e, "Correct Usage: msg read message_id");
                        return;
                    }

                    using (IDbCommand command=this._database.Connection.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM altmessages WHERE Username = '" + user + "' and MessageID = " + e.Args[1];
                        IDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            total_messages ++;
                            bot.SendReply( e, "[" + reader.GetString(1) + "] " + reader.GetString(3) );
                            this._database.ExecuteNonQuery("UPDATE altmessages SET Read = 'true' WHERE Username = '" + e.Sender + "' AND MessageID = " + e.Args[1] );
                        }
                        reader.Close();
                    }
                    if ( total_messages == 0 )
                        bot.SendReply( e, "Error: No such message." );
                    break;

                case "delete":

                    if (e.Args.Length < 2)
                    {
                        bot.SendReply(e, "Correct Usage: msg delete message_id");
                        return;
                    }

                    this._database.ExecuteNonQuery("DELETE FROM altmessages WHERE Username = '" + e.Sender + "' AND MessageID = " + e.Args[1] );
                    bot.SendReply( e, "Message deleted." );
                    break;

                default:

                    if (e.Args.Length < 2)
                    {
                        bot.SendReply(e, "Correct Usage: msg username message");
                        return;
                    }

                    // check if User's mailbox size
                    user = Format.UppercaseFirst(e.Args[0]);

                    if (bot.GetUserID(user) < 1)
                    {
                        bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, user));
                        return;
                    }

                    if ( bot.Users.IsAlt( user ) )
                    {
                        user = bot.Users.GetMain( user );
                    }

                    UserMessages( bot, user, ref total_messages,
                                  ref unread_messages, ref max_id );

                    if ( total_messages == this._mailboxsize )
                    {
                        bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, user) + "'s mailbox is full.  Not sent.");
                        break;
                    }

                    string insert_command =
                        string.Format("INSERT INTO altmessages ( Username, Sender, MessageID , Message, Read ) VALUES ( '{0}', '{1}', {2}, '{3}', 'false' )",
                                      user, e.Sender, max_id + 1, e.Words[1] );
                    if ( this._database.ExecuteNonQuery( insert_command ) == 0 )
                        bot.SendReply(e, "Message sent.");
                    else
                        bot.SendReply(e, "Error: Message not sent.");
                    break;
                }
                break;
            }
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._mailboxsize = bot.Configuration.GetInteger(this.InternalName, "mailboxsize", this._mailboxsize );
        }

        private void UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            UserCheckMessages( bot, e.Sender );
        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return; 
            UserCheckMessages( bot, e.Sender );
        }

        private void UserMessages( BotShell bot, string user,
                                   ref int total_messages,
                                   ref int unread_messages,
                                   ref int max_id )
        {
            total_messages = 0;
            unread_messages = 0;
            max_id = 0;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM altmessages WHERE Username = '" + user + "'";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    total_messages ++;
                    if ( ! reader.GetBoolean(4) )
                        unread_messages ++;
                    if ( max_id < reader.GetInt32(2) )
                        max_id = reader.GetInt32(2);
                }
                reader.Close();
            }
        }

        private void UserCheckMessages( BotShell bot, string user )
        {
            int total_messages = 0;
            int unread_messages = 0;
            int max_id = 0;

            if ( bot.Users.IsAlt( user ) )
            {
                user = bot.Users.GetMain(user);
            }

            UserMessages( bot, user, ref total_messages,
                          ref unread_messages, ref max_id );

            uint UserID = bot.GetUserID( user );

            if ( total_messages == 0 )
                bot.SendPrivateMessage( UserID, "You have no messages.");
            else
                bot.SendPrivateMessage( UserID, "You have " +
                                        Convert.ToString(unread_messages) +
                                        " unread messages. ( Your Mailbox is " +
                                        Convert.ToString((int)((((float)total_messages)/this._mailboxsize)*100.0)) + "% full )" );

        }

        public void AltMsgWindow( ref RichTextWindow window, string user )
        {
            window.AppendHeader( "Alt Messages");
            window.AppendLineBreak();

            int total_messages = 0;
            int unread_messages = 0;
            int max_id = 0;

            if ( this._bot.Users.IsAlt( user ) )
            {
                user = this._bot.Users.GetMain(user);
            }

            UserMessages( this._bot, user, ref total_messages,
                          ref unread_messages, ref max_id );

            if ( total_messages == 0 )
                window.AppendNormal( "You have no messages.");
            else
                window.AppendNormal( "You have " +
                                     Convert.ToString(unread_messages) +
                                     " unread messages. ( Your Mailbox is " +
                                     Convert.ToString((int)((((float)total_messages)/this._mailboxsize)*100.0)) + "% full )" );
            window.AppendLineBreak();
            
            window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            window.AppendLineBreak();
            
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "msgs":
                return "Checks to see if you have any unread messages\n" +
                    "Usage: /tell " + bot.Character + " msgs";

            case "msg":
                return "Sends or reads messages.\n" +
                    "Usage: /tell " + bot.Character + " msg username message\n" +
                    "       /tell " + bot.Character + " msg list\n" +
                    "       /tell " + bot.Character + " msg read messageID\n" +
                    "       /tell " + bot.Character + " msg delete messageID\n";

            }
            return null;
        }
    }
}
