using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class Applicants : PluginBase
    {
        private Config _applicants;

        private BotShell _bot;

        private string _add_to = "Both";
        private string _side_requirement = "None";
        private int _level_requirement = 0;

        public Applicants()
        {
            this.Name = "Handle applications";
            this.InternalName = "llApplicants";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Description = "A system to allow applications to bot or org.";
            this.Version = 100;
            this.Commands = new Command[] {
                new Command("apply", true, UserLevel.Guest),
                new Command("applicants", true, UserLevel.Admin),
                new Command("applicant", true, UserLevel.Admin)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._bot = bot;

            this._applicants = new Config(bot.ToString(), "applications");
            this._applicants.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Applications ( Username VARCHAR(14) NOT NULL PRIMARY KEY )");

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "level", "Minimum level to apply.", this._level_requirement);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "side", "Required faction to apply.", this._side_requirement, "None", "Omni", "Neutral", "Clan" );
            bot.Configuration.Register(ConfigType.String, this.InternalName, "list", "Add new members to guest list or member?", this._add_to, "Guest", "Member", "Both" );
            LoadConfiguration(bot);

        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            LoadConfiguration( bot );
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._level_requirement = bot.Configuration.GetInteger(this.InternalName, "level", this._level_requirement);
            this._side_requirement = bot.Configuration.GetString(this.InternalName, "side", this._side_requirement );
            this._add_to = bot.Configuration.GetString(this.InternalName, "list", this._add_to );
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "apply":
                this.OnApplyCommand(bot, e);
                break;

            case "applicants":
                this.OnShowApplicantsCommand(bot, e);
                break;

            case "applicant":
                this.OnProcessApplicantCommand(bot, e);
                break;

            }
        }

        public void OnApplyCommand(BotShell bot, CommandArgs e)
        {

            string[] guestlist = _bot.FriendList.List("guestlist");
            if ( ( Array.IndexOf( guestlist, e.Sender.ToLower() ) >= 0 ) ||
                 ( bot.Users.UserExists( e.Sender, bot.GetUserID(e.Sender) ) ) )
            {
                bot.SendReply(e, "You are already a member.");
                return;
            }

            // if there are requirements -- check applicant against them
            if ( ( this._side_requirement != "None" ) ||
                 ( this._level_requirement > 1 ) )
            {
                WhoisResult whois = XML.GetWhois( e.Sender.ToLower(),
                                                  bot.Dimension );

                if (whois == null || !whois.Success)
                {
                    bot.SendReply(e, "Sorry, unable to verify your identity at the moment.  Cannot submit your application.  Please try again later.");
                    return;
                }

                if ( whois.Stats.Level < this._level_requirement )
                {
                    bot.SendReply(e, "Sorry, You do not meet the level requirements to apply.");
                    return;
                }

                if ( ( this._side_requirement != "None" ) &&
                     ( whois.Stats.Faction != this._side_requirement ) )
                {
                    bot.SendReply(e, "Sorry, You are not aligned with " + this._side_requirement + " and cannot apply.");
                    return;
                }
                    
            }

            if ( this._applicants.ExecuteNonQuery("INSERT INTO Applications (Username ) VALUES ('" + Format.UppercaseFirst(Config.EscapeString(e.Sender)) + "' )") < 0 )
                bot.SendReply(e, "Sorry, an error occurred attempting to submit your application.  You will need to contact an adimin." );
            else
                bot.SendReply(e, "Your application has been submitted." );

        }

        public void OnShowApplicantsCommand(BotShell bot, CommandArgs e)
        {

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Pending applications");
            window.AppendLineBreak();

            int count = 0;

            using (IDbCommand command =
                   this._applicants.Connection.CreateCommand())
            {
                command.CommandText = "Select * from Applications";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        count ++;
                        string applicant = reader.GetString(0);

                        UInt32 userid = bot.GetUserID(applicant);
                        OnlineState state = bot.FriendList.IsOnline(userid);

                        switch ( state )
                        {

                        case OnlineState.Online:
                            window.AppendRawString( HTML.CreateColorString(RichTextWindow.ColorGreen, applicant ) );
                            break;

                        case OnlineState.Offline:
                            window.AppendRawString( HTML.CreateColorString(RichTextWindow.ColorRed, applicant ) );
                            break;

                        default:
                            window.AppendHighlight( applicant );
                            break;

                        }

                        window.AppendNormal( " [" );
                        window.AppendCommand( "»»", "/tell " + applicant );
                        window.AppendNormal( "] [" );
                        window.AppendBotCommand( "?", "!whois " + applicant );
                        window.AppendNormal( "] [" );
                        window.AppendBotCommand( "+", "!applicant approve " + applicant );
                        window.AppendNormal( "] [" );
                        window.AppendBotCommand( "x", "!applicant reject " + applicant );
                        window.AppendNormal( "]" );
                        window.AppendLineBreak();

                    }
                    catch { }
                    reader.Close();
                }
            }

            if ( count >  0 )
                bot.SendReply( e, count.ToString() + " Applicants »» ",
                               window );
            else
                bot.SendReply( e, "There are no current applications." );

        }

        public void OnProcessApplicantCommand(BotShell bot, CommandArgs e)
        {
            if ( e.Args.Length != 2 )
            {
                bot.SendReply( e, "Please use the \"applicants\" command to process applications." );
                return;
            }

            if ( e.Args[0] == "approve" )
            {
                if ( ( this._add_to == "Member" ) ||
                     ( this._add_to == "Both" ) )
                    bot.Users.AddUser( e.Args[1], UserLevel.Member );
                if ( ( this._add_to == "Guest" ) ||
                     ( this._add_to == "Both" ) )
                    bot.FriendList.Add("guestlist", e.Args[1]);
            }

            if ( this._applicants.ExecuteNonQuery("DELETE FROM Applications WHERE Username = '" + e.Args[1] + "';") < 0 )
                bot.SendReply(e, "An error has occurred attempting to remove " +e.Args[1]  + "'s application" );
            else
                bot.SendReply(e, e.Args[1] + "'s application has been removed" );

        }

        public override string OnHelp(BotShell bot, string command)
        {

            switch (command)
            {

            case "apply":
                    return "Submit an application to join.\n" +
                        "Usage: /tell " + bot.Character + " apply";

            case "applicants":
                    return "Display a list of all pending applications.\n" +
                        "Usage: /tell " + bot.Character + " applicants";

            }

            return null;
        }

    }

}
