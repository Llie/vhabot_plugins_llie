using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using AoLib.Utils;

namespace VhaBot.Plugins
{

    public class ArmorEval : PluginBase
    {

        public ArmorEval()
        {
            this.Name = "Armor Evaluator";
            this.InternalName = "llArmorEval";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Dependencies = new string[] { "llXMLItems" };
            this.Version = 100;
            this.Commands = new Command[] {
                new Command("armor", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
        }

        public override void OnUnload(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "armor":
                CompareArmors ( bot, e );
                break;

            }

        }

        private void UsageMessages(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "armor":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [drop armor piece #1 here] [drop armor piece #2 here]" );
                break;

            }

        }

        public static bool IsArmor( Item item )
        {
            if ( item.Attributes.GetAttributeByName( "EquipmentPage" ) != 2 )
                return false;
            return true;
        }

        public static int Score ( Item armor )
        {
            double sum = 0.0;
            foreach ( FunctionEl onWearModify in
                      armor.Events.GetEventByName("OnWear").Functions.Function )
                if ( onWearModify.Func.Name == "Modify" )
                {
                    int attribute = 0;
                    int value = 0;
                    int.TryParse( onWearModify.Parameters.Param[0],
                                  out attribute );
                    int.TryParse( onWearModify.Parameters.Param[1],
                                  out value );

                    // AC
                    if ( ( attribute >= 90 ) && ( attribute <= 97 ) )
                        sum += value / 505.0;
                    // max health or max nano
                    else if ( ( attribute == 1 ) || ( attribute == 221 ) )
                        sum += value / 405.0;
                    else if ( ( attribute >= 16 ) && ( attribute <= 21 ) )
                        sum += value / 55.0;
                    else
                        sum += value / 105.0;

                }

            return (int)Math.Round(sum * 5.0);
        }

        public static Dictionary< int, int > GetACs( Item armor )
        {

            Dictionary< int, int > AC = new Dictionary< int, int >();
            foreach ( FunctionEl onWearModify in
                      armor.Events.GetEventByName("OnWear").Functions.Function )
                if ( onWearModify.Func.Name == "Modify" )
                {

                    int attribute;
                    int.TryParse( onWearModify.Parameters.Param[0],
                                  out attribute );
                    // this is an AC
                    if ( ( attribute >= 90 ) && ( attribute <= 97 ) )
                    {
                        int value;
                        int.TryParse( onWearModify.Parameters.Param[1],
                                      out value );
                        AC.Add( attribute, value );
                    }
                }

            return AC;
        }

        public static Dictionary< int, int > GetBuffs( Item armor )
        {

            Dictionary< int, int > Buffs = new Dictionary< int, int >();
            foreach ( FunctionEl onWearModify in
                      armor.Events.GetEventByName("OnWear").Functions.Function )
                if ( onWearModify.Func.Name == "Modify" )
                {

                    int attribute;
                    int.TryParse( onWearModify.Parameters.Param[0],
                                  out attribute );
                    // this is not an AC
                    if ( ( attribute < 90 ) || ( attribute > 97 ) )
                    {
                        int value;
                        int.TryParse( onWearModify.Parameters.Param[1],
                                      out value );

                        if( ( attribute == 1 ) || // max health
                            ( attribute == 221 ) ) // max nano 
                            value /= 7;
                        else if ( ( attribute < 16 ) || // not an base skill
                                  ( attribute > 21 ) )
                            value /= 2;

                        Buffs.Add( attribute, value );
                    }
                }

            return Buffs;
        }

        public static Int32 DictionaryTotal( Dictionary< int, int > d )
        {
            Int32 sum = 0;
            foreach( KeyValuePair<int, int> pair in d )
                sum += pair.Value;
            return sum;
        }

        public static double DictionaryAverage( Dictionary< int, int > d )
        {
            double sum = (double)ArmorEval.DictionaryTotal(d);
            return sum / d.Count;
        }
                                            
        private void CompareArmors ( BotShell bot, CommandArgs e )
        {
            
            if ( e.Items.Length !=  2 )
            {
                UsageMessages( bot, e );
                return;
            }

            Item a = XMLItems.GetItem( bot, e,
                                       e.Items[0].LowID, e.Items[0].QL );
            Item b = XMLItems.GetItem( bot, e,
                                       e.Items[1].LowID, e.Items[1].QL );

            if ( a == null )
            {
                bot.SendReply(e, "Error: " + e.Items[0].Name +
                              " not found on Xyphos." );
                return;
            }

            if ( b == null )
            {
                bot.SendReply(e, "Error: " + e.Items[1].Name +
                              " not found on Xyphos." );
                return;
            }

            if ( ! ArmorEval.IsArmor( a ) )
            {
                bot.SendReply(e, "Error: " + e.Items[0].Name +
                              " is not armor." );
                return;
            }

            if ( ! ArmorEval.IsArmor( b ) )
            {
                bot.SendReply(e, "Error: " + e.Items[1].Name +
                              " is not armor." );
                return;
            }

            // Verify that we're comparing two armors for the same slot
            if ( a.Attributes.GetAttributeByName ( "Slot" ) !=
                 b.Attributes.GetAttributeByName ( "Slot" ) )
            {
                bot.SendReply(e, "Error: The two pieces of armor you have indicated do not occupy the same slot." );
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Armor Comparison Tool");
            window.AppendLineBreak();

            // Compare ACs
            Dictionary< int, int > AC1 = ArmorEval.GetACs( a );
            Dictionary< int, int > AC2 = ArmorEval.GetACs( b );

            double avgAC1 = DictionaryAverage( AC1 );
            double avgAC2 = DictionaryAverage( AC2 );
            int diff = (int)Math.Round(Math.Abs( avgAC1 - avgAC2 ));

            window.AppendHighlight( "Comparison based on ACs:" );
            window.AppendLineBreak();

            if ( avgAC1 > avgAC2 )
            {
                window.AppendNormal ( "On average, " );
                window.AppendHighlight( a.Name );
                window.AppendNormal ( " is better than " );
                window.AppendHighlight( b.Name );
                window.AppendNormal ( " by " );
                window.AppendHighlight( diff.ToString() );
                window.AppendNormal ( " AC points" );
            }
            else if ( avgAC1 < avgAC2 )
            {
                window.AppendNormal ( "On average, " );
                window.AppendHighlight( b.Name );
                window.AppendNormal ( " is better than " );
                window.AppendHighlight( a.Name );
                window.AppendNormal ( " by " );
                window.AppendHighlight( diff.ToString() );
                window.AppendNormal ( " AC points" );
            }
            else
            {
                window.AppendHighlight( a.Name );
                window.AppendNormal ( " and " );
                window.AppendHighlight( b.Name );
                window.AppendNormal ( " are rouughly comparable in terms of AC." );
            }

            window.AppendLineBreak(2);
            window.AppendHighlight( "Comparison based on buffs:" );
            window.AppendLineBreak();
            
            // Compare Buffs
            Dictionary< int, int > Buffs1 = ArmorEval.GetBuffs( a );
            Dictionary< int, int > Buffs2 = ArmorEval.GetBuffs( b );
            int totBuff1 = DictionaryTotal( Buffs1 );
            int totBuff2 = DictionaryTotal( Buffs2 );

            if ( totBuff1 > totBuff2 )
            {
                window.AppendHighlight( a.Name );
                window.AppendNormal ( " provides more buffs than " );
                window.AppendHighlight( b.Name );
            }
            else if ( totBuff1 < totBuff2 )
            {
                window.AppendHighlight( b.Name );
                window.AppendNormal ( " provides more buffs than " );
                window.AppendHighlight( a.Name );
            }
            else
            {
                window.AppendHighlight( a.Name );
                window.AppendNormal ( " and " );
                window.AppendHighlight( b.Name );
                window.AppendNormal (" are roughly comparable in terms of buffs." );
            }

            window.AppendLineBreak(2);
            window.AppendHighlight( "Summary:" );
            window.AppendLineBreak();

            int Score1 =  ArmorEval.Score ( a );
            int Score2 =  ArmorEval.Score ( b );

            window.AppendNormal ( "We give each of the following armor pieces the following scores(*):" );
            window.AppendLineBreak(1);
            window.AppendHighlight( Score1.ToString() );
            window.AppendNormal ( ": " );
            window.AppendHighlight( a.Name );
            window.AppendLineBreak(1);
            window.AppendHighlight( Score2.ToString() );
            window.AppendNormal ( ": " );
            window.AppendHighlight( b.Name );
            window.AppendLineBreak(2);
            if ( Score1 > Score2 )
            {
                window.AppendNormal ( "Making the " );
                window.AppendHighlight( a.Name );
                window.AppendNormal ( " the overall better choice." );
            }
            else if ( Score1 < Score2 )
            {
                window.AppendNormal ( "Making the " );
                window.AppendHighlight( b.Name );
                window.AppendNormal ( " the overall better choice." );
            }
            else
                window.AppendNormal( "Making the two armors roughly equal." );

            window.AppendLineBreak(2);

            window.AppendNormal( "*This scoring system is an arbitrary measurement of AC+buffs used by this plug-in to score each armor piece.  Item look up is courtesy of Xyphos." );

            window.AppendLineBreak(2);

            bot.SendReply(e, "Armor Comparison »» ", window );

        }

        public override string OnHelp( BotShell bot, string command )
        {
            switch (command)
            {

            case "armor":
                return "Provides a rough comparison of two pieces of armor.  " +
                    "Both pieces of armor should be for the same slot.\n\n" +
                    "Usage: /tell " + bot.Character +
                    " armor [drop armor piece #1 in chat] [drop armor piece #2 in chat]\n";

            }

            return null;

        }

    }

}
