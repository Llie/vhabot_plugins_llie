using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
	public class BotNet : PluginBase
	{

		private Config _database;
        private string[] _factions = new string[] { "Omni", "Clan", "Neutral",
                                                    "All" };

        private bool _enable_botnet = false;
        private bool _enable_relay = false;

        private bool _relay_org = true;
        private bool _relay_pc = true;
        private bool _relay_irc = true;

        private int _max_channel_digest = 10;

        private string _master = string.Empty;

        public static string ToUpperFirst( string input )
        {
            if ( input == string.Empty )
                return string.Empty;
            return  input.Substring(0,1).ToUpper() + input.Substring(1);
        }

		private List<string> GetChannels()
        {

            List<string> Channels = new List<string>();
            string query_channels = "SELECT Name FROM Channels";
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = query_channels;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        Channels.Add( reader.GetString(0) );
                    }
                    catch { }
                }
                reader.Close();
            }

            return Channels;
        }

		private string CachedFaction ( string username )
        {

            string Faction = string.Empty;
            string query_cache =
                "SELECT Faction FROM UserCache WHERE Name = '{0}'";
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = string.Format(query_cache, username );
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        Faction = reader.GetString(0);
                    }
                    catch { }
                }
                reader.Close();
            }

            return Faction;
        }

		private List<string> GetSubscribers( BotShell bot,
                                             string Channel, string Faction )
        {

            List<string> Subscribers = new List<string>();

            string query_subs = "SELECT Name FROM Subscriptions";
            string count_subs = "SELECT COUNT(*) FROM Subscriptions";

            if ( Channel != string.Empty )
            {
                query_subs += string.Format( " WHERE Channel = '{0}'",
                                             Channel );
                count_subs += string.Format( " WHERE Channel = '{0}'",
                                             Channel );

                if ( ( Faction != string.Empty ) &&
                     ( Faction != "All" ) )
                {
                    query_subs += string.Format(
                        "AND ( Faction = 'All' OR Faction = '{0}' )",
                        Faction );
                    count_subs += string.Format(
                        "AND ( Faction = 'All' OR Faction = '{0}' )",
                        Faction );
                }
            }

            Config Database = null;
            if ( this._master == string.Empty )
                Database = this._database;
            else
            {
                string master = this._master + "@" + bot.Dimension;
                Database = new Config( master.ToLower(), this.InternalName);
            }

            int slaves = 0;
            int slave_index = -1;
            using ( IDbCommand command =
                    Database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT Name FROM Slaves";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() )
                {
                    if ( bot.Character == reader.GetString(0) )
                        slave_index = slaves;
                    slaves++;
                }
            }

            if ( slaves > 0 )
            {
                int total_subscribers = 0;
                using ( IDbCommand command =
                        Database.Connection.CreateCommand())
                {
                    command.CommandText = count_subs;
                    IDataReader reader = command.ExecuteReader();
                    if ( reader.Read() )
                        total_subscribers = reader.GetInt32(0);
                }

                int limit = (int)Math.Ceiling((double)total_subscribers/
                                              (double)(slaves+1));
                int offset = 0;
                if ( this._master != string.Empty )
                    offset = limit * (slave_index + 1);

                if ( total_subscribers > slaves )
                {
                    query_subs += string.Format(
                        " ORDER BY Name ASC LIMIT {0} OFFSET {1}",
                        limit, offset );
                }
            }

            using ( IDbCommand command = Database.Connection.CreateCommand())
            {
                command.CommandText = query_subs;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        Subscribers.Add(reader.GetString(0));
                    }
                    catch { }
                }
                reader.Close();
            }

            return Subscribers;
        }

        public BotNet ()
		{
			this.Name = "BotNet functions and relay";
			this.InternalName = "llBotNet";
			this.Version = 100;
			this.Author = "Llie";
            this.Description = "Provides botnet service or relays botnet spam to org or guest channels.  Suitable for use to relay spam from NeutNet, DNet, etc..";
            this.Dependencies = new string[] { "vhMembersManager",
                                               "vhBansManager" };
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("relay", true, UserLevel.SuperAdmin),
                new Command("channel", true, UserLevel.SuperAdmin),
                new Command("master", true, UserLevel.SuperAdmin),
                new Command("register", true, UserLevel.Guest),
                new Command("unregister", true, UserLevel.Member),
                new Command("subscribe", true, UserLevel.Member),
                new Command("unsubscribe", true, UserLevel.Member),
                new Command("spam", true, UserLevel.Member),
                new Command("digest", true, UserLevel.Member),
                new Command("rules", true, UserLevel.Guest),
            };
		}

		public override void OnLoad(BotShell bot)
		{
			bot.Events.PrivateMessageEvent +=
                new PrivateMessageHandler(PrivateMessageEventRelay);

            bot.Timers.Hour += new EventHandler(PruneDigest);

            this._database = new Config(bot.ID, this.InternalName);

            // tables for BotNet
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS UserCache ( Name VARCHAR(14) NOT NULL PRIMARY KEY, Faction VARCHAR(14) NOT NULL )");
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Channels ( Name VARCHAR(14) NOT NULL PRIMARY KEY )");
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Subscriptions ( Name VARCHAR(14) NOT NULL, Channel VARCHAR(14) NOT NULL, Faction VARCHAR(14) NOT NULL, PRIMARY KEY( Name, Channel ) )");
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Rules ( Rules VARCHAR(1024) NOT NULL )");
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Digest ( TimeStamp INTEGER, Channel VARCHAR(14) NOT NULL, Faction VARCHAR(14) NOT NULL, Sender VARCHAR(14) NOT NULL, Message VARCHAR(256) )");
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Slaves ( Name VARCHAR(14) NOT NULL Primary Key )");

            // tables for BotNet relay
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Relays ( Name VARCHAR(14) NOT NULL Primary Key, RelaySlaves INTEGER )");

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "enable_botnet", "Enable BotNet functions", this._enable_botnet );

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "enable_relay", "Enable BotNet relay", this._enable_relay );

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "relay_org", "Relay BotNet to organization channel", this._relay_org );
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "relay_pc", "Relay BotNet to private channel", this._relay_pc );
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "relay_irc", "Relay BotNet to irc", this._relay_irc );

            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "max_digest", "Maximum number of historical messages to keep and display", this._max_channel_digest, 10, 25, 50 );

            this.LoadConfiguration(bot);

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);

		}

		public override void OnUnload(BotShell bot)
		{
			bot.Events.PrivateMessageEvent -=
                new PrivateMessageHandler(PrivateMessageEventRelay);

            bot.Timers.Hour -= new EventHandler(PruneDigest);

            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);

            if ( this._master != string.Empty )
            {
                string master = this._master + "@" + bot.Dimension;
                Config master_db = new Config( master.ToLower(),
                                               this.InternalName);
                master_db.ExecuteNonQuery(
                    string.Format( "DELETE FROM Slaves WHERE Name='{0}'",
                                   bot.Character ) );
            }
            
		}

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            bot.Events.PrivateMessageEvent -=
                new PrivateMessageHandler(PrivateMessageEventRelay);
            bot.Timers.Hour -= new EventHandler(PruneDigest);

            this._enable_botnet = bot.Configuration.GetBoolean(this.InternalName, "enable_botnet", this._enable_botnet );

            this._enable_relay = bot.Configuration.GetBoolean(this.InternalName, "enable_relay", this._enable_relay );
                        
            this._relay_org = bot.Configuration.GetBoolean(this.InternalName, "relay_org", this._relay_org );
            this._relay_pc = bot.Configuration.GetBoolean(this.InternalName, "relay_pc", this._relay_pc );
            this._relay_irc = bot.Configuration.GetBoolean(this.InternalName, "relay_irc", this._relay_irc );

            this._max_channel_digest = bot.Configuration.GetInteger(this.InternalName, "max_digest", this._max_channel_digest );

            if ( this._enable_botnet )
                bot.Timers.Hour += new EventHandler(PruneDigest);

            if ( this._enable_relay )
                bot.Events.PrivateMessageEvent +=
                    new PrivateMessageHandler(PrivateMessageEventRelay);

        }

		private void PrivateMessageEventRelay( BotShell bot,
                                               PrivateMessageArgs e )
		{

            if ( e.Self || ! this._enable_relay )
                return;

            List<string> Relays = new List<string>();
            List<string> Match = new List<string>();
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Relays";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        Relays.Add(reader.GetString(0));
                        if ( reader.GetInt32(1) != 0 )
                            Match.Add( reader.GetString(0) + "[0-9]*" );
                        else
                            Match.Add( reader.GetString(0) );
                    }
                    catch { }
                }
                reader.Close();
            }

            for( int i=0; i<Match.Count; i++ )
            {
                Regex regex = new Regex( Match[i] );
                if ( regex.IsMatch ( e.Sender ) )
                {
                    e.DisableAutoMessage = true;
                    if (this._relay_org)
                        bot.SendOrganizationMessage( "[" + Relays[i] + "] " +
                                                     bot.ColorHighlight +
                                                     e.Message);
                    if (this._relay_pc)
                        bot.SendPrivateChannelMessage( "[" + Relays[i] + "] " +
                                                       bot.ColorHighlight +
                                                       e.Message);
                    if (this._relay_irc)
                        bot.SendIrcMessage( "[" + Relays[i] + "] " + e.Message);
                    return;
                }
            }
		}

        private void PruneDigest(object sender, EventArgs e)
        {

            if ( ! this._enable_botnet )
                return;

            List<string> Channels = GetChannels();
            List<string> Factions = new List<string>(this._factions);

            foreach ( string Channel in Channels )
            {
                foreach ( string Faction in Factions )
                {

                    bool pruned_needed = false;
                    using ( IDbCommand command =
                            this._database.Connection.CreateCommand() )
                    {
                        command.CommandText = string.Format( "SELECT count(*) FROM Digest WHERE Channel = '{0}' AND Faction = '{1}'", Channel, Faction );
                        IDataReader reader = command.ExecuteReader();
                        if ( reader.Read() && !reader.IsDBNull(0) )
                            if ( reader.GetInt64(0)>this._max_channel_digest )
                                pruned_needed = true;
                        reader.Close();
                    }
                    if ( pruned_needed )
                        using ( IDbCommand command =
                                this._database.Connection.CreateCommand() )
                        {
                            command.CommandText = string.Format( "SELECT TimeStamp FROM Digest WHERE Channel = '{0}' AND Faction = '{1}' ORDER BY TimeStamp DESC", Channel, Faction );
                            IDataReader reader = command.ExecuteReader();
                            for ( int i=0; i<=this._max_channel_digest; i++ )
                            
                                reader.Read();
                            this._database.ExecuteNonQuery( string.Format( "DELETE FROM Digest WHERE Channel = '{0}' AND Faction = '{1}' AND TimeStamp < {2}", Channel, Faction, reader.GetInt64(0) ) );
                            reader.Close();
                        }

                }
            }
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "relay":
                this.OnRelayCommand(bot, e);
                break;

            case "channel":
                this.OnChannelCommand(bot, e);
                break;

            case "master":
                this.OnMasterCommand(bot, e);
                break;
                
            case "rules":
                this.OnRulesCommand(bot, e);
                break;

            case "register":
                this.OnRegisterCommand(bot, e);
                break;

            case "unregister":
                this.OnUnregisterCommand(bot, e);
                break;

            case "subscribe":
                this.OnSubscribeCommand(bot, e);
                break;

            case "unsubscribe":
                this.OnUnsubscribeCommand(bot, e);
                break;

            case "spam":
                this.OnSpamCommand(bot, e);
                break;

            case "digest":
                this.OnDigestCommand(bot, e);
                break;
                
            }
        }

        private void OnRelayCommand(BotShell bot, CommandArgs e)
        {

            if ( bot.Users.GetUser(e.Sender) != UserLevel.SuperAdmin )
            {
                bot.SendReply( e, "Only SuperAdmin is allowed to modify relay." );
                return;
            }

            if ( e.Args.Length == 0 )
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle( bot.Character + " Relays" );
                window.AppendLineBreak();

                int n_relays = 0;
                string query_relay = "SELECT * FROM Relays";
                using ( IDbCommand command =
                        this._database.Connection.CreateCommand())
                {
                    command.CommandText = query_relay;
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        try
                        {
                            window.AppendNormal( "[" );
                            window.AppendBotCommand( "X",
                                                     "!relay delete " +
                                                     reader.GetString(0) );
                            window.AppendNormal( "] " );
                            window.AppendNormal(reader.GetString(0));
                            if ( reader.GetInt32(1) != 0 )
                                window.AppendNormal( " (Will relay slave bots)" );
                            n_relays ++;
                        }
                        catch { }
                    }
                    reader.Close();

                    if ( n_relays == 0 )
                        window.AppendNormal("No relays configured.");
                }
                bot.SendReply( e, bot.Character + " relays »» ", window);
                return;
            }

            if ( e.Args.Length < 2 )
            {
                bot.SendReply( e, "Usage: !relay [add|delete] Bot [slaves]");
                return;
            }

            string BotName = Format.UppercaseFirst(e.Args[1]);

            switch ( e.Args[0].ToLower() )
            {

            case "add":
                WhoisResult info = XML.GetWhois( BotName.ToLower(),
                                                 bot.Dimension );
                if ( info == null )
                {
                    bot.SendReply( e, "No such user: " + BotName );
                    return;
                }
                if ( ! info.Success )
                    bot.SendReply( e, "Warning: unable to verify " + BotName );
                int slave = 0;
                if ( e.Args.Length > 2 )
                    slave = 1;
                if ( this._database.ExecuteNonQuery( string.Format( "REPLACE INTO Relays ( Name, RelaySlaves ) VALUES ( '{0}', {1} )", BotName, slave.ToString() ) ) > 0 )
                    bot.SendReply( e, "Relay " + BotName + " added." );
                else
                    bot.SendReply( e, "An error occurred attempting to add " + BotName + " relay." );
                break;

            case "delete":
                if ( this._database.ExecuteNonQuery( string.Format( "DELETE FROM Relays where Name = '{0}'", BotName ) ) > 0 )
                    bot.SendReply( e, "Relay " + BotName + " removed." );
                else
                    bot.SendReply( e, "An error occurred attempting to remove " + BotName + " relay." );
                break;

            default:
                
                List<string> Relays = new List<string>();
                using ( IDbCommand command =
                        this._database.Connection.CreateCommand())
                {
                    command.CommandText = "SELECT Name FROM Relays";
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        try
                        {
                            Relays.Add(reader.GetString(0));
                        }
                        catch { }
                    }
                    reader.Close();
                }
                BotName = Format.UppercaseFirst(e.Args[0]);
                if ( Relays.Contains(BotName) )
                    bot.SendPrivateMessage( bot.GetUserID(BotName),
                                            e.Words[1] );
                else
                    bot.SendReply( e, "Usage: !relay [add|delete] Bot [slaves]\n!relay Bot [!command]" );
                break;
            }

        }

        private void OnChannelCommand(BotShell bot, CommandArgs e)
        {

            if ( bot.Users.GetUser(e.Sender) != UserLevel.SuperAdmin )
            {
                bot.SendReply( e, "Only SuperAdmin is allowed to modify channels." );
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( bot.Character + " Channels" );
            window.AppendLineBreak();

            List<string> Channels = GetChannels();

            if ( e.Args.Length == 0 )
            {
                window.AppendHighlight( "Delete the following channel(s):" );
                window.AppendLineBreak();
                foreach ( string AChannel in Channels.ToArray() )
                {
                    window.AppendBotCommand( AChannel, "channel delete " +
                                             AChannel );
                    window.AppendLineBreak();
                }
                bot.SendReply( e, bot.Character + " channels »» ", window);
                return;
            }

            string Channel = BotNet.ToUpperFirst(e.Args[1]);

            if ( e.Args[0].ToLower() == "add" )
            {
                if ( Channel.ToLower() != "default" )
                {
                    if ( ! Channels.Contains( Channel ) )
                    {
                        this._database.ExecuteNonQuery( string.Format( "INSERT INTO Channels ( Name ) VALUES ( '{0}' )", Channel ) );
                        bot.SendReply( e, Channel + " channel added." );
                    }
                }
                else
                {
                    this._database.ExecuteNonQuery( string.Format( "INSERT INTO Channels ( Name ) VALUES ( 'Events' )" ) );
                    this._database.ExecuteNonQuery( string.Format( "INSERT INTO Channels ( Name ) VALUES ( 'PvM' )" ) );
                    this._database.ExecuteNonQuery( string.Format( "INSERT INTO Channels ( Name ) VALUES ( 'PvP' )" ) );
                    this._database.ExecuteNonQuery( string.Format( "INSERT INTO Channels ( Name ) VALUES ( 'Lootrights' )" ) );
                    this._database.ExecuteNonQuery( string.Format( "INSERT INTO Channels ( Name ) VALUES ( 'Shopping' )" ) );
                        bot.SendReply( e, "Events, PvM, PvP, Lootrights, and Shopping channels added." );
                }
            }
            else if ( e.Args[0].ToLower() == "delete" )
            {
                if ( Channels.Contains( Channel ) )
                {
                    this._database.ExecuteNonQuery( string.Format( "DELETE FROM Channels WHERE Name  = '{0}'", Channel ) );
                }                
            }

        }

        private void OnMasterCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            if ( bot.Users.GetUser(e.Sender) != UserLevel.SuperAdmin )
            {
                bot.SendReply( e, "Only SuperAdmin is allowed to set the BotNet master bot." );
                return;
            }

            string master = string.Empty;
            Config master_db = null;

            if ( ( e.Args.Length == 0 ) && ( this._master == string.Empty ) )
            {
                bot.SendReply( e, "Usage: /tell " + bot.Character +
                               " !master [master_bot_name]" );
                return;
            }
            else if ( ( e.Args.Length == 0 ) &&
                      ( this._master != string.Empty ) )
            {
                bot.SendReply( e, "Disconnecting from " + this._master );
                master = this._master + "@" + bot.Dimension;
                master_db = new Config( master.ToLower(),
                                        this.InternalName);
                master_db.ExecuteNonQuery(
                    string.Format( "DELETE FROM Slaves WHERE Name='{0}'",
                                   bot.Character ) );
                this._master = string.Empty;
                return;
            }

            this._master = Format.UppercaseFirst(e.Args[0]);

            master = this._master + "@" + bot.Dimension;
            master_db = new Config( master.ToLower(), this.InternalName);
            master_db.ExecuteNonQuery(
                string.Format( "REPLACE INTO Slaves ( Name ) VALUES ( '{0}' )",
                               bot.Character ) );
            bot.SendReply( e, "BotNet master set to " + this._master );
            
        }

        private void OnRulesCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( bot.Character + " Rules" );
            window.AppendLineBreak();

            if ( ( bot.Users.GetUser(e.Sender) == UserLevel.SuperAdmin) &&
                 ( e.Args.Length > 0 ) )
            {
                if ( e.Args[0] == "Clear" )
                    this._database.ExecuteNonQuery( "DELETE FROM Rules" );
                else
                    this._database.ExecuteNonQuery( string.Format( "INSERT INTO Rules ( Rules ) VALUES ( '{0}' )", e.Words[0] ) );
            }

            string query_rules = "SELECT * FROM Rules";
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = query_rules;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        window.AppendNormal(reader.GetString(0));
                        window.AppendLineBreak(2);
                    }
                    catch { }
                }
                reader.Close();
            }
            bot.SendReply( e, bot.Character + " rules »» ", window);

        }

		private bool AddUser( BotShell bot, string username, string mainname )
        {

            if ( bot.Users.UserExists( username ) )
                return false;

            WhoisResult info = XML.GetWhois( username.ToLower(), bot.Dimension);

            this._database.ExecuteNonQuery( string.Format ( "REPLACE INTO UserCache ( Name, Faction ) Values ( '{0}', '{1}' )", username, info.Stats.Faction ) );

            if ( mainname == string.Empty )
                return bot.Users.AddUser( username, UserLevel.Member,
                                          username );
            else
                return bot.Users.AddAlt( mainname, username );

        }

		private bool RemoveUser( BotShell bot, string username )
        {

            if ( ! bot.Users.UserExists( username ) )
                return false;

            this._database.ExecuteNonQuery( string.Format ( "DELETE FROM UserCache WHERE Name = '{0}' )", username ) );

            if ( bot.Users.GetMain( username ) == username )
                try
                {
                    bot.Users.RemoveUser( username );
                    return true;
                }
                catch
                {
                    return false;
                }
            else
                try
                {
                    bot.Users.RemoveAlt( username );
                    return true;
                }
                catch
                {
                    return false;
                }
        }

        private void OnRegisterCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            if ( this._master != string.Empty )
            {
                bot.SendReply( e, "Please register with " + this._master );
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( "Welcome to " + bot.Character );
            window.AppendLineBreak();
            window.AppendHighlight( "The following commands are available:" );
            window.AppendLineBreak(2);

            window.AppendNormal( "[" );
            window.AppendBotCommand( "Rules", "rules" );
            window.AppendNormal( "] Display rules." );
            window.AppendLineBreak();

            window.AppendNormal( "[" );
            window.AppendBotCommand( "Unregister", "unregister" );
            window.AppendNormal( "] Unregister from " + bot.Character );
            window.AppendLineBreak();
            window.AppendNormal( "[" );
            window.AppendBotCommand( "Subscribe", "subscribe" );
            window.AppendNormal( "] Subscribe to channels." );
            window.AppendLineBreak();
            window.AppendNormal( "[" );
            window.AppendBotCommand( "Unsubscribe", "unsubscribe" );
            window.AppendNormal( "] Unsubscribe to channels." );
            window.AppendLineBreak();
            window.AppendNormal( "[" );
            window.AppendBotCommand( "Digest", "digest" );
            window.AppendNormal( "] Display recent messages." );

            string username = string.Empty;
            string mainname = string.Empty;
            if ( e.Args.Length == 0 )
                username = e.Sender;
            else
            {
                mainname = e.Sender;
                username = Format.UppercaseFirst(e.Args[0]);
            }
                
            if ( bot.Bans.IsBanned(username) )
            {
                bot.SendReply( e, username + " is banned from " + bot.Character + ".  Please contact an administrator for assistance." );
                return;
            }

            if ( bot.Users.UserExists( username ) )
            {
                WhoisResult info = XML.GetWhois( username.ToLower(), bot.Dimension);
                this._database.ExecuteNonQuery( string.Format ( "REPLACE INTO UserCache ( Name, Faction ) Values ( '{0}', '{1}' )", username, info.Stats.Faction ) );
                bot.SendReply( e, username + " is already registered on " + bot.Character + ".  Faction information updated." );
                return;
            }

            if ( mainname == string.Empty )
                if( this.AddUser( bot, username, mainname ) )
                    bot.SendReply( e, "Welcome to " + bot.Character +
                                   " »» ", window);
                else
                    bot.SendReply( e, "An error occurred attempting to add you to " + bot.Character + ".  Please contact an administrator for assistance." );
            else 
                if ( this.AddUser( bot, username, mainname ) )
                    bot.SendReply( e, username + " has been added as your alt." );
                else
                    bot.SendReply( e, "An error occurred attempting to add " + username + " to " + bot.Character + ".  Please contact an administrator for assistance." );

        }

        private void OnUnregisterCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            if ( this._master != string.Empty )
            {
                bot.SendReply( e, "Please unregister with " + this._master );
                return;
            }

            string username = string.Empty;
            string mainname = string.Empty;
            if ( e.Args.Length == 0 )
                if ( bot.Users.GetMain( e.Sender ) == e.Sender )
                    username = e.Sender;
                else
                {
                    mainname = bot.Users.GetMain( e.Sender );
                    username = e.Sender;
                }
            else
            {
                mainname = e.Sender;
                username = Format.UppercaseFirst(e.Args[0]);
            }

            if ( ! bot.Users.UserExists( username ) )
            {
                bot.SendReply( e, "Error: " + username +
                               " is not registered on " + bot.Character + "." );
                return;
            }

            if ( mainname == string.Empty )
                if ( this.RemoveUser( bot, username ) )
                    bot.SendReply( e, "You have been removed from " +
                                   bot.Character );
                else
                    bot.SendReply( e, "An error occurred attempting to remove you from " + bot.Character + ".  Please contact an administrator for assistance." );
            else
                if ( this.RemoveUser( bot, username ) )
                    bot.SendReply( e, username + " has been removed from " +
                                   bot.Character );
                else
                    bot.SendReply( e, "An error occurred attempting to remove " + username + " from " + bot.Character + ".  Please contact an administrator for assistance." );

        }

        private void OnSubscribeCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            if ( this._master != string.Empty )
            {
                bot.SendReply( e, "Please subscribe with " + this._master );
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( bot.Character + " Channels" );
            window.AppendLineBreak();
            window.AppendHighlight( "The following channels are available:" );
            window.AppendLineBreak(2);

            List<string> Channels = GetChannels();
            foreach ( string AChannel in Channels )
            {
                window.AppendBotCommand( AChannel, "subscribe " + AChannel );
                window.AppendLineBreak();
            }

            if ( e.Args.Length == 0 )
            {
                bot.SendReply( e, bot.Character + " channels »» ", window);
                return;
            }

            string Channel = BotNet.ToUpperFirst(e.Args[0]);
            if ( ! Channels.Contains( Channel ) )
            {
                bot.SendReply( e, "Error: Invalid channel specified.  Try again without any arguments to see the list of channels." );
                return;
            }
                
            string Faction = "All";
            if ( e.Args.Length > 1 )
                Faction = Format.UppercaseFirst(e.Args[1]);
            List<string> Factions = new List<string>(this._factions);
            if ( ! Factions.Contains(Faction) )
            {
                bot.SendReply( e, "Error: Invalid faction specified." );
                return;
            }

            if ( this._database.ExecuteNonQuery( string.Format( "REPLACE INTO Subscriptions ( Name, Channel, Faction ) VALUES ( '{0}', '{1}', '{2}' )", bot.Users.GetMain( e.Sender ), Channel, Faction ) ) == 1 )
                bot.SendReply( e, "You are now subscribed to " +
                               Channel + " (" + Faction + ")." );
            else
                bot.SendReply( e, "An error occurred attempting to subscribe you to " + Channel + " (" + Faction + ").  Please contact an administrator for assistance." );

        }

        private void OnUnsubscribeCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            if ( this._master != string.Empty )
            {
                bot.SendReply( e, "Please unsubscribe with " + this._master );
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( bot.Character + " Channels" );
            window.AppendLineBreak();
            window.AppendHighlight( "You are subscribed to the following channels:" );
            window.AppendLineBreak(2);

            List<string> Channels = new List<string>();

            string query_subs = "SELECT * FROM Subscriptions WHERE Name = '{0}'";
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand() )
            {
                command.CommandText = string.Format(query_subs, bot.Users.GetMain( e.Sender ) );
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        Channels.Add( reader.GetString(1) );
                        window.AppendBotCommand( reader.GetString(1),
                                                 "unsubscribe " +
                                                 reader.GetString(1) );
                        window.AppendLineBreak();
                    }
                    catch { }
                }
                reader.Close();
            }

            if ( e.Args.Length == 0 )
            {
                bot.SendReply( e, bot.Character + " channels »» ", window);
                return;
            }

            string Channel = BotNet.ToUpperFirst(e.Args[0]);
            if ( ! Channels.Contains( Channel ) )
            {
                bot.SendReply( e, "Error: Invalid channel specified.  Try again without any arguments to see the list of channels." );
                return;
            }

            if ( this._database.ExecuteNonQuery( string.Format( "DELETE FROM Subscriptions WHERE  Name = '{0}' AND  Channel = '{1}'", bot.Users.GetMain( e.Sender ), Channel ) ) == 1 )
                bot.SendReply( e, "You have unsubscribed to " + Channel + "." );
            else
                bot.SendReply( e, "An error occurred attempting to unsubscribe you to " + Channel + " .  Please contact an administrator for assistance." );

        }

        private void OnSpamCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            if ( e.Args.Length < 2 )
                return;

            if ( ( bot.Users.GetUser(e.Sender) < UserLevel.Member ) &&
                 ( e.Sender != this._master ) )
            {
                bot.SendReply( e, "You are not registered on " + bot.Character);
                return;
            }

            int word = 1;
            string Channel = BotNet.ToUpperFirst( e.Args[0] );
            string Faction = CachedFaction( bot.Users.GetMain( e.Sender ) );

            List<string> Channels = this.GetChannels();
            if ( ! Channels.Contains( Channel ) )
            {
                bot.SendReply( e, "No such channel: " + Channel );
                return;
            }

            if ( e.Args.Length < 2 )
            {
                List<string> Factions = new List<string>(this._factions);
                if ( Factions.Contains(Format.UppercaseFirst(e.Args[1])) )
                {
                    Faction = Format.UppercaseFirst(e.Args[1]);
                    word = word + 1;
                }
            }

            if ( Faction == string.Empty )
                Faction = "All";

            List<string> Subscribers = GetSubscribers( bot, Channel, Faction );

            long now = TimeStamp.FromDateTime( DateTime.UtcNow );
            string Sender = e.Sender + ": ";
            if ( e.Sender == this._master )
                Sender = "";
            else
                this._database.ExecuteNonQuery( string.Format( "INSERT INTO Digest ( TimeStamp, Channel, Faction, Sender, Message ) VALUES ( {0}, '{1}', '{2}', '{3}', '{4}')", now, Channel, Faction, e.Sender, e.Words[word] ) );

            foreach ( string username in Subscribers )
            {
                UInt32 userid = bot.GetUserID(username);
                if ( ( bot.FriendList.IsOnline(userid) ==
                       OnlineState.Online ) && ( username != e.Sender ) )
                    bot.SendPrivateMessage( bot.GetUserID(username), "[" +
                                            Channel + " " + Faction + "] " +
                                            Sender + bot.ColorHighlight +
                                            e.Words[word] );
            }

            if ( this._master == string.Empty )
                using ( IDbCommand command =
                        this._database.Connection.CreateCommand())
                {
                    command.CommandText = "SELECT Name FROM Slaves";
                    IDataReader reader = command.ExecuteReader();
                    while ( reader.Read() )
                    {
                        try
                        {
                            bot.SendPrivateMessage(
                                bot.GetUserID(reader.GetString(0)),
                                "!spam " + e.Words[0] );
                        }
                        catch { }
                    }
                }

            bot.SendReply( e, "Message sent to " + bot.Character );

        }

        private void OnDigestCommand(BotShell bot, CommandArgs e)
        {

            if ( ! this._enable_botnet )
            {
                bot.SendReply( e, bot.Character + " is disabled.");
                return;
            }

            string Channel = string.Empty;

            if ( e.Args.Length > 0 )
                Channel = BotNet.ToUpperFirst( e.Args[0] );

            string Faction = CachedFaction( bot.Users.GetMain( e.Sender ) );

            if ( e.Args.Length > 1 )
            {
                List<string> Factions = new List<string>(this._factions);
                if ( Factions.Contains(Format.UppercaseFirst(e.Args[1])) )
                    Faction = Format.UppercaseFirst(e.Args[1]);
            }

            if ( Faction == string.Empty )
                Faction = "All";

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle(  bot.Character + " "  + Channel +
                                 " " + Faction + " Digest" );
            window.AppendLineBreak();

            string query_digest = "SELECT * FROM Digest";

            if ( ( Channel != string.Empty ) || ( Faction != "All" ) )
                query_digest +=  " WHERE";

            if ( Channel != string.Empty )
                query_digest += string.Format( " Channel = '{0}'",
                                                Channel );
            if ( Faction != "All" )
                query_digest += string.Format( " Faction = '{0}'",
                                                Faction );
            query_digest += string.Format( " ORDER BY TimeStamp DESC Limit {0}",
                                            this._max_channel_digest );

            using ( IDbCommand command =
                    this._database.Connection.CreateCommand() )
            {
                command.CommandText = query_digest;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        window.AppendHighlight( "[" + reader.GetString(1) +
                                                " " + reader.GetString(2) +
                                                "] " + reader.GetString(3) +
                                                ": "  );
                        window.AppendNormal ( reader.GetString(4) );
                    }
                    catch { }
                }
                reader.Close();
            }

            bot.SendReply( e, bot.Character + " digest »» ", window);

        }

        public override string OnHelp(BotShell bot, string command)
        {
            string c = bot.Character;
        
            switch (command)
            {

            case "relay":
                return "Configure a BotNet relay.\n\n" +
                    "/tell " + c + " relay\n" +
                    "/tell " + c + " relay add [BotNet]\n" +
                    "/tell " + c + " relay delete [BotNet]\n" +
                    "/tell " + c + " relay [BotNet] [!command]\n";

            case "channel":
                return "Configure BotNet channels.\n\n" +
                    "/tell " + c + " channel add [ChannelName]\n" +
                    "/tell " + c + " channel delete [ChannelName]\n";

            case "rules":
                if ( this._enable_botnet )
                    return "Display " + c +" rules. (Superadmin can use this command to set the rules.)\n\n" +
                        "/tell " + c + " rules\n" +
                        "/tell " + c + " rules clear\n" +
                        "/tell " + c + " rules [rule paragraph]\n";
                break;

            case "register":
                if ( this._enable_botnet )
                    return "Register with " + c + ".\n\n" +
                        "/tell " + c + " register\n" +
                        "/tell " + c + " register [alt]\n";
                break;

            case "unregister":
                if ( this._enable_botnet )
                    return "Unregister with " + c + ".\n\n" +
                        "/tell " + c + " unregister\n" +
                        "/tell " + c + " unregister [alt]\n";
                break;

            case "subscribe":
                if ( this._enable_botnet )
                    return "Subscribe to a channel on " + c + ".\n\n" +
                        "/tell " + c + " subscribe\n" +
                        "/tell " + c + " subscribe [channel] [faction]\n";
                break;

            case "unsubscribe":
                if ( this._enable_botnet )
                    return "Unubscribe to a channel on " + c + ".\n\n" +
                        "/tell " + c + " unsubscribe\n" +
                        "/tell " + c + " unsubscribe [channel]\n";
                break;

            case "spam":
                if ( this._enable_botnet )
                    return "Send message to all subscribers on " + c + ".\n\n" +
                        "/tell " + c + " spam [channel] [faction] [message]\n\n" +
                        "Your message must adhere to the current rules or you risk being banned!\n";
                break;

            case "digest":
                if ( this._enable_botnet )
                    return "View recent messages on " + c + ".\n\n" +
                        "/tell " + c + " digest [channel] [faction]\n";
                break;

            }

            return c + " is disabled";

        }
	}
}
