using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class ChatTools : PluginBase
    {

        private Config _database;
        List<string> _args;

        public ChatTools()
        {
            this.Name = "Various Chat Tools";
            this.InternalName = "llChatTools";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;

            this.Commands = new Command[] {
                new Command("spam", true, UserLevel.Leader),
                new Command("chanspam", true, UserLevel.Leader),
                new Command("macro add", true, UserLevel.Member),
                new Command("macro remove", true, UserLevel.Member),
                new Command("macros", true, UserLevel.Member),
                new Command("macro", true, UserLevel.Member),
                new Command("mac", "macro" )
            };

        }

        public override void OnLoad(BotShell bot)
        {

            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS macros ( Name VARCHAR(80) PRIMARY KEY, Macro VARCHAR(256) );");

            // useful macros always to add include in database
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'Assist', '/assist %t' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'AssistHeal', '/assist %t \\n /assist' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'MultiAssist', '/assist %r \\n ' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'Attack', '/pet %t attack' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'Guard', '/pet %t guard' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'Heal', '/pet %t heal' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'Follow', '/pet %t follow' )");
            this._database.ExecuteNonQuery("REPLACE INTO macros ( Name, Macro ) VALUES ( 'Terminate', '/pet %t terminate' )");

            _args = new List<string>();

        }

        public override void OnUnload(BotShell bot)
        {
        }

        public string Usage( string command )
        {

            switch (command)
            {

            case "spam":
                return "Usage: !spam [message]";

            case "chanspam":
                return "Usage: !chanspam [message]";

            case "macro add":
                return "Usage: !macro add [name] [macro]";

            case "macro remove":
                return "Usage: !macro remove [name]";

            case "macros":
                return "Usage: !macros";

            case "macro":
                return "Usage: !macro [name] [last|arg1 arg2 ...]";

            }

            return string.Empty;
        }

        public static List<string> ParseArgs( CommandArgs e )
        {
            List<string> args = new List<string>();
            bool open = false;
            foreach ( string arg in e.Args )
            {
                if ( !open )
                {
                    args.Add( HTML.UnescapeString(arg) );
                    if ( arg.Contains("&quot;") || arg.Contains("\"") )
                        open = true;
                }
                else
                {
                    args[args.Count-1] = args[args.Count-1] + " " +
                        HTML.UnescapeString(arg);
                    if ( arg.Contains("&quot;") || arg.Contains("\"") )
                        open = false;
                }
            }
            return args;
        }

        private static void AppendRawCommand( ref RichTextWindow window,
                                              string Name, string Command )
        {
            Name=HTML.EscapeString(Name);
            Command=HTML.EscapeString(Command);
            window.AppendRawString( string.Format( HTML.CommandLink, "'",
                                                   Command, "" ) +
                                    Name +
                                    string.Format( HTML.LinkEnd ) );
        }

        public void OnMacroAddCommand(BotShell bot, CommandArgs e)
        {
            
            if ( e.Args.Length < 2 )
            {
                bot.SendReply(e, Usage(e.Command) );
                return;
            }

            string name = Format.UppercaseFirst(e.Args[0]);
            string macro = e.Words[1];

            if ( macro.Contains("&lt;no target&gt;") )
                macro = macro.Replace( "&lt;no target&gt;", "%t" );

            if ( this._database.ExecuteNonQuery(string.Format("REPLACE INTO macros ( Name, Macro ) VALUES ( '{0}', '{1}' )", name, macro ) ) == 1 )
                bot.SendReply(e, "Macro \"" + name + "\" successfully added." );
            else
                bot.SendReply(e, "An error occurred attempting to add the macro: \"" + name + "\"." );

        }

        public void OnMacroRemoveCommand(BotShell bot, CommandArgs e)
        {
            
            if ( e.Args.Length != 1 )
            {
                bot.SendReply(e, Usage(e.Command) );
                return;
            }

            string name = Format.UppercaseFirst(e.Args[0]);

            if ( this._database.ExecuteNonQuery(string.Format("DELETE FROM macros WHERE Name = '{0}';", name ) ) == 1 )
                bot.SendReply(e, "Macro \"" + name + "\" successfully removed." );
            else
                bot.SendReply(e, "An error occurred attempting to remove the macro: \"" + name + "\"." );

        }

        public void OnMacrosCommand(BotShell bot, CommandArgs e)
        {

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( "Useful Macros" );
            window.AppendLineBreak();

            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM macros";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                {
                    string name = reader.GetString(0);
                    window.AppendNormal( " [" );
                    window.AppendBotCommand( "X", "!macro remove " +
                                             name );
                    window.AppendNormal( "] " );
                    ChatTools.AppendRawCommand( ref window, name,
                                                "/macro " +
                                                name  + " " +
                                                reader.GetString(1) );
                    window.AppendLineBreak();
                }
            }

            bot.SendReply( e, "Macros »» ", window );
            
        }

        public void OnMacroCommand(BotShell bot, CommandArgs e)
        {
            
            RichTextWindow window = new RichTextWindow(bot);

            List<string> args = ChatTools.ParseArgs( e );
                
            if ( args.Count < 1 )
            {
                bot.SendReply(e, Usage(e.Command) );
                return;
            }

            if ( ( args[1].ToLower() == "last" ) &&
                 ( _args.Count > 0 ) )
            {
                args.RemoveAt(1);
                args.AddRange(_args);
            }

            string name = string.Empty;
            string macro = string.Empty;

            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = string.Format("SELECT * FROM macros WHERE UPPER(Name)=UPPER('{0}')", args[0] );
                IDataReader reader = command.ExecuteReader();
                if ( reader.Read() && !reader.IsDBNull(0) )
                {
                    name = reader.GetString(0);
                    macro = reader.GetString(1);
                }
            }

            if ( macro == string.Empty )
            {
                bot.SendReply(e, "The macro " + args[0] + " was not found.  Try the !macros command to see available macros." );
                return;
            }

            window.AppendTitle( name + " Macro:" );
            window.AppendHighlight( macro );
            window.AppendLineBreak( 2 );

            int n_input_args = args.Count-1; 
            int n_macro_targ = Regex.Matches( macro, "%t" ).Count;
            int n_macro_args = 0;
            for ( int i=0; i<10; i++ )
                if( Regex.Matches( macro, "%" + i.ToString() ).Count > 0 )
                    n_macro_args++;
            int n_macro_rep =  Regex.Matches( macro, "%r" ).Count;

            int arg = 1;
            string out_macro = string.Empty;
            if ( ( n_macro_rep > 0 ) && ( n_input_args > 0 ) )
            {
                for ( int i=0; i<n_input_args; i++ )
                    out_macro += macro.Replace( "%r", args[arg++] );
                Regex endnl = new Regex(@"[ ]*[\\]n[ ]*$");
                out_macro = endnl.Replace( out_macro, "" );
                if ( ! out_macro.Contains(@"\n") )
                    ChatTools.AppendRawCommand( ref window, 
                                                name,
                                                "/macro " + name + " " +
                                                out_macro );
                else
                {
                    window.AppendHighlight("Please cut and paste the following line into your chat window:" );
                    window.AppendLineBreak();
                    window.AppendNormal( out_macro );
                }
                window.AppendLineBreak();
            }
            else if ( ( n_macro_targ > 0 ) && ( n_macro_args == 0 ) &&
                      ( n_input_args > 0 ) )
            {
                for ( int i=0; i<n_input_args; i++ )
                {
                    out_macro = macro.Replace( "%t", args[arg] );
                    if ( ! out_macro.Contains(@"\n") )
                        ChatTools.AppendRawCommand( ref window, 
                                                    name + " " + args[arg],
                                                    "/macro " + name + " " +
                                                    out_macro );
                    else
                    {
                        window.AppendHighlight("Please cut and paste the following line into your chat window:" );
                        window.AppendLineBreak();
                        window.AppendNormal( out_macro );
                    }
                    window.AppendLineBreak();
                    arg ++;
                }
            }
            else if ( ( n_macro_args > 0 ) &&
                      ( n_input_args % n_macro_args == 0 ) )
            {

                int n_macs = n_input_args / n_macro_args;
                for( int i=0; i<n_macs; i++ )
                {
                    string name_append = string.Empty;
                    out_macro = macro;
                    for( int j=1; j<=n_macro_args; j++ )
                    {
                        string sub = "%" + j.ToString();
                        out_macro = out_macro.Replace( sub, args[arg++] );
                    }
                    if ( ! out_macro.Contains(@"\n") )
                        ChatTools.AppendRawCommand( ref window,
                                                    name + name_append, "/macro " +
                                                    name + " " +
                                                    out_macro );
                    else
                    {
                        window.AppendHighlight("Please cut and paste the following line into your chat window:" );
                        window.AppendLineBreak();
                        window.AppendNormal( out_macro );
                    }
                    window.AppendLineBreak();
                }

            }
            else
            {
                window.AppendNormal( "Sorry, you supplied an incorrect number of arguments to the specified macro.  Please check the usage of the macro." );
            }

            if ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader )
            {
                args.RemoveAt(0);
                _args = args;
            }
            bot.SendReply(e, name + " Macro »» ", window );
            return;

        }

        public void OnSpamCommand(BotShell bot, CommandArgs e)
        {
            string[] online = bot.FriendList.Online("notify");
            foreach ( string player in online )
            {
                bot.SendPrivateMessage( player, HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
                bot.SendPrivateMessage( player, HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
                bot.SendPrivateMessage( player, HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
            }

        }

        public void OnChanSpamCommand(BotShell bot, CommandArgs e)
        {
            bot.SendPrivateChannelMessage( HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
            if ( ! string.IsNullOrEmpty( bot.Organization ) )
                bot.SendOrganizationMessage( HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
            bot.SendPrivateChannelMessage( HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
            if ( ! string.IsNullOrEmpty( bot.Organization ) )
                bot.SendOrganizationMessage( HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
            bot.SendPrivateChannelMessage( HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
            if ( ! string.IsNullOrEmpty( bot.Organization ) )
                bot.SendOrganizationMessage( HTML.CreateColorString( "#FFFF00", e.Words[0] ) );
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "spam":
                OnSpamCommand( bot, e );
                break;

            case "chanspam":
                OnChanSpamCommand( bot, e );
                break;

            case "macro add":
                OnMacroAddCommand( bot, e );
                break;

            case "macro remove":
                OnMacroRemoveCommand( bot, e );
                break;

            case "macros":
                OnMacrosCommand( bot, e );
                break;

            case "macro":
                OnMacroCommand( bot, e );               
                break;

            }
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "spam":
                return "Send highly visible tells to everyone who is online.\n" + Usage(command);

            case "chanspam":
                return "Sends a highly visible message to organization and private channels.\n" + Usage(command);

            case "macro add":
                return "Add a macro to the database.\n" + Usage(command);

            case "macro remove":
                return "Remove a macro from the database.\n" + Usage(command);

            case "macros":
                return "List stored macros.\n" + Usage(command);

            case "macro":
                return "Generate macros.\n" + Usage(command);

            }
            return null;
        }
    }

}
