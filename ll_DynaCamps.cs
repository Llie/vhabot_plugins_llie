using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class DynaCamps : PluginBase
    {
        // private string Server = "nano.byethost12.com/services";
        // private string Server = "nano.exofire.net/services";
        private string Server = string.Empty;
        private string UrlTemplate = "http://{0}/dyna.php";
        private string filename = "data/dynacamps.xml";
        private int Max = 100;

        public DynaCamps()
        {
            this.Name = "Dyna Camp Database Lookup";
            this.InternalName = "llDynaCamps";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("dyna", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "dynacamps.xml" );
        }

        public override void OnUnload(BotShell bot) { }

        private string Remainders( string[] list, Int32 startAt )
        {
            if ( startAt >= list.Length )
                return string.Empty;
            string remainder = list[startAt];
            Int32 i;
            for ( i=startAt+1; i<list.Length; i++)
            {
                remainder = remainder + "+" + list[i];
            }
            remainder = remainder.ToLower();
            return remainder;
        }

        private void DisplayResults ( BotShell bot, CommandArgs e,
                                      DynaCampsResults search_results )
        {
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Dyna Camp Database");

            if ( ! File.Exists ( filename ) )
            {
                window.AppendHighlight("Server: ");
                window.AppendNormal(this.Server);
                window.AppendLineBreak();
            }

            window.AppendHighlight("Version: ");
            window.AppendNormal(search_results.Version);
            window.AppendLineBreak();
            window.AppendHighlight("Results: ");
            window.AppendNormal(search_results.Camps.Length.ToString() +
                                " / " + search_results.Max);
            window.AppendLineBreak(2);
            window.AppendHeader("Search Results:");
            window.AppendHighlight("Species ( Level / Mob Levels ) Location ");
            window.AppendLineBreak();
            foreach (DynaCamp camp in search_results.Camps)
            {
                window.AppendHighlight(camp.name + " ");
                window.AppendNormalStart();
                window.AppendString("(" + camp.level + "/" );
                window.AppendString( camp.mob_levels + ") " );
                int pfid = 0;
                Int32.TryParse ( camp.pfid, out pfid );
                if ( pfid > 0 )
                {
                    string coordstr = camp.coords;
                    coordstr = coordstr.Replace ( "x", " " );
                    window.AppendCommand( camp.location +
                                          " (" + camp.coords + ")",
                                          "/waypoint " +
                                          coordstr + " " +
                                          camp.pfid );
                }
                else
                {
                    window.AppendString(" " + camp.location +
                                        " (" + camp.coords + ")");
                    
                }
                window.AppendLineBreak();
            }
            bot.SendReply(e, "Results »» ", window);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " minlevel [maxlevel] [location]");
                return;
            }

            string result = string.Empty;
            MemoryStream stream = null;
            DynaCampsResults search_results = null;
            string xml = string.Empty;
            Regex isNumber = new Regex("^[0-9]+$");
            Int32 minlevel = -1, maxlevel = -1;
            string location = string.Empty;

            // populate arguments
            Int32 i = 0;
            Int32 n = 0;
            while ( ( i < e.Args.Length ) &&
                    ( isNumber.IsMatch(e.Args[i]) ) &&
                    ( n < 2 ) )
            {
                if ( n == 0 )
                {
                    minlevel = Convert.ToInt32(e.Args[i]);
                    n++;
                }
                else if ( n == 1 )
                {
                    maxlevel = Convert.ToInt32(e.Args[i]);
                    n++;
                }
                i++;
            }

            location = Remainders ( e.Args, i );
            if ( ( minlevel > 0 ) && ( maxlevel < 0 ) )
            {
                minlevel -= 5;
                maxlevel = minlevel + 10;
            }

            if ( ! File.Exists ( filename ) )
            {

                // no local file -- search over the internet
                string url = string.Format(this.UrlTemplate, this.Server);
                if ( n == 1 )
                {
                    url = url + "?minlevel=" + Convert.ToString(minlevel) +
                        "&maxlevel=" + Convert.ToString(minlevel);
                    if ( e.Args.Length > 1 )
                        url = url + "&location=" + location;
                }
                else if ( n == 0 )
                {
                    url = url + "?location=" + location;
                }
                else
                {
                    url = url + "?minlevel=" + minlevel +
                        "&maxlevel=" + maxlevel;
                    if ( e.Args.Length > i )
                        url = url + "&location=" + location;
                }

                xml = HTML.GetHtml(url, 60000);
                if (string.IsNullOrEmpty(xml))
                {
                    bot.SendReply(e, "Unable to query the dyna camp database");
                    return;
                }

                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(DynaCampsResults));
                    search_results = (DynaCampsResults)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to query the dyna camp database";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

            }
            else  // read local dynacamps.xml file
            {
                using(StreamReader rdr = File.OpenText(filename))
                {
                    xml = rdr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to read file: " + filename );
                    return;
                }

                DynaCampsResults all_camps = null;
                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(DynaCampsResults));
                    all_camps = (DynaCampsResults)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to query the dyna camp database";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                search_results = all_camps;
                search_results.Version = all_camps.Version;
                search_results.Max = Convert.ToString ( Max );
                search_results.Credits = all_camps.Credits;

                bool is_match;
                i = 0;
                foreach (DynaCamp camp in all_camps.Camps)
                {
                    is_match = true;
                    if ( ( minlevel > 0 ) &&
                         ( ( Convert.ToInt32( camp.level ) < minlevel ) ||
                           ( Convert.ToInt32( camp.level ) > maxlevel ) ) )
                        is_match = false;
                    if ( ( location != string.Empty ) &&
                         ( camp.location.IndexOf( location, StringComparison.CurrentCultureIgnoreCase ) < 0 ) )
                        is_match = false;
                    if ( is_match )
                        search_results.Camps[i++] = camp;
                }
                Array.Resize( ref search_results.Camps, i );
                search_results.Results = Convert.ToString( search_results.Camps.Length );
                
            }

            if ( search_results.Camps == null ||
                 search_results.Camps.Length == 0)
                result = "No camps were found";
            else
            {
                DisplayResults ( bot, e, search_results );
                return;
            }
            bot.SendReply(e, result);

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "dyna":
                    return "Allows you to search the vhabot central dyna camp database.\n" +
                        "To search the items database use: /tell " + bot.Character + " dyna level [maxlevel] [location].\n";
            }
            return null;
        }
    }

    [XmlRoot("dynas")]
    public class DynaCampsResults
    {
        [XmlElement("version")]
        public string Version;
        [XmlElement("results")]
        public string Results;
        [XmlElement("max")]
        public string Max;
        [XmlElement("camp")]
        public DynaCamp[] Camps;
        [XmlElement("credits")]
        public string Credits;
    }

    [XmlRoot("camp")]
    public class DynaCamp
    {
        [XmlElement("species")]
        public string name;
        [XmlElement("level")]
        public string level;
        [XmlElement("mob_levels")]
        public string mob_levels;
        [XmlElement("location")]
        public string location;
        [XmlElement("pfid")]
        public string pfid;
        [XmlElement("coords")]
        public string coords;
        [XmlElement("pf")]
        public string pf;
    }

}
