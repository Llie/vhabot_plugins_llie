using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Timers;
using System.Globalization;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class EventAnnouncer : PluginBase
    {

        private Config _database;
        private bool _sendLogon = true;      // Send announcement on logon
        private bool _collectstats = false;  // Collect online statistics
        private int _pruneMonths = 1;        // Prune events older than this
        private BotShell _bot;
        private Timer _welcome;
        private TimeZones _AllTimeZones;

        public EventAnnouncer()
        {
            this.Name = "Organize, Plan and Announce Events";
            this.InternalName = "llEvents";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 105;
            this.Commands = new Command[] {
                new Command("event add", true, UserLevel.Leader),
                new Command("event clear", true, UserLevel.Leader),
                new Command("events", true, UserLevel.Member),
                new Command("daystats", true, UserLevel.Member),
                new Command("hourstats", true, UserLevel.Member),
                new Command("calendar", true, UserLevel.Member),
                new Command("lft", true, UserLevel.Member),
                new Command("convtime", true, UserLevel.Member),
                new Command("cal", "calendar")
            };
        }

        public override void OnLoad(BotShell bot)
        {
            _AllTimeZones = PluginShared.GetTimeZones();

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "send_logon", "Announce events on logon.", this._sendLogon);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "prune_months", "Delete events older than this many months. (Zero means, never prune)", this._pruneMonths, 1, 2, 3, 6, 12, 0 );
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "collect_stats", "Collect user statistics.", this._collectstats);
            this.LoadConfiguration(bot);

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(Events_ConfigurationChangedEvent);
            bot.Events.UserLogonEvent += new UserLogonHandler(Events_UserLogonEvent);
            bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(Events_UserJoinChannelEvent);

            if ( this._collectstats )
                bot.Timers.Hour += new EventHandler(CollectUserStatsEvent);

            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS events (EventTitle VARCHAR(80), EventDateTime DATETIME PRIMARY KEY, RallyX INTEGER, RallyY INTEGER, RallyPF INTEGER)");

            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS stats (WeekDay INTEGER, Hour INTEGER, Total REAL, Samples REAL, PRIMARY KEY ( WeekDay, Hour ) )");
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS LastUpdate ( DayTime REAL, Users INTEGER )");

            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Activity (Title VARCHAR(80), ID INTEGER  PRIMARY KEY AUTOINCREMENT )");
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS ActivitySignup ( Name VARCHAR(14), ID INTEGER  NOT NULL, FOREIGN KEY (ID) REFERENCES Activity(ID) )");

            this.PruneEvents();

            Updater.Update( "tz.xml" );

            this._bot = bot;

            this._welcome = new Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;
        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= EventsWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += EventsWindow;
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(Events_UserLogonEvent);
                this._bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(Events_UserJoinChannelEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserLogonEvent += new UserLogonHandler(Events_UserLogonEvent);
            this._bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(Events_UserJoinChannelEvent);
        }

        private void Events_ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);

            bot.Timers.Hour -= new EventHandler(CollectUserStatsEvent);
            if ( this._collectstats )
                bot.Timers.Hour += new EventHandler(CollectUserStatsEvent);

        }

        private void LoadConfiguration(BotShell bot)
        {
            this._sendLogon = bot.Configuration.GetBoolean(this.InternalName, "send_logon", this._sendLogon);
            this._pruneMonths = bot.Configuration.GetInteger(this.InternalName, "prune_months", this._pruneMonths);
            this._collectstats = bot.Configuration.GetBoolean(this.InternalName, "collect_stats", this._collectstats);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= EventsWindow;
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(Events_ConfigurationChangedEvent);

            bot.Events.UserLogonEvent -= new UserLogonHandler(Events_UserLogonEvent);
            bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(Events_UserJoinChannelEvent);

            bot.Timers.Hour -= new EventHandler(CollectUserStatsEvent);
        }

        private void Events_UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (!this._sendLogon) return;

            if( ! e.First )
            {

                RichTextWindow window = new RichTextWindow(bot);

                CommandArgs c = new CommandArgs( CommandType.PrivateChannel,
                                                 0, 0, e.Sender, null,
                                                 string.Empty, string.Empty,
                                                 false, null );

                int NEvents = UpcomingEvents( bot, c, ref window );
            
                if ( NEvents > 0 )
                    bot.SendPrivateMessage( e.SenderID, "Upcoming Events »» "
                                            + window.ToString() );
            }            

        }

        private void Events_UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            if (!this._sendLogon) return;

            RichTextWindow window = new RichTextWindow(bot);

            CommandArgs c = new CommandArgs( CommandType.PrivateChannel,
                                             0, 0, e.Sender, null,
                                             string.Empty, string.Empty,
                                             false, null );

            int NEvents = UpcomingEvents( bot, c, ref window );

            if ( NEvents > 0 )
                bot.SendPrivateMessage( e.SenderID,
                                        "Upcoming Events »» " +
                                        window.ToString() );
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "event add":
                this.OnEventAdd(bot, e);
                break;

            case "event clear":
                this.OnEventClear(bot, e);
                break;

            case "events":
                this.OnEvents(bot, e);
                break;

            case "calendar":
                this.OnCalendar(bot, e);
                break;

            case "lft":
                this.OnTeamActivity(bot, e);
                break;

            case "convtime":
                this.OnConvertTime(bot, e);
                break;

            case "daystats":
            case "hourstats":
                this.OnStats(bot, e);
                break;

            }
        }

        public bool ParseTimeArgs( BotShell bot, CommandArgs e,
                                   ref int ArgCursor, ref int Hour,
                                   ref int Mins, ref string MeridianStr,
                                   ref TimeZone SpecZone,
                                   ref bool DaylightTime )
        {

            if ( ArgCursor >= e.Args.Length )
            {
                bot.SendReply(e,
                              String.Format(
                                  "Error: Unable to parse time ({0}).\n",
                                  e.Args[ArgCursor] ) +
                              OnHelp(bot, e.Command) );
                return false;
            }
            
            Regex TimeRegex = new Regex( "[0-9]:[0-9][0-9]" );
            if (  TimeRegex.IsMatch( e.Args[ArgCursor] ) )
            {
                try
                {
                    string[] Tokens = e.Args[ArgCursor].Split( new Char[] {
                            ' ', ':' } );
                    Hour = Convert.ToInt32(Tokens[0]);
                    int LastIndex = Tokens[1].LastIndexOfAny(
                        new Char[] { '0', '1', '2', '3', '4',
                                '5', '6', '7', '8', '9' } );
                    Mins = Convert.ToInt32(Tokens[1].Substring(0,LastIndex+1));
                    if ( Tokens[1].Length - LastIndex > 1 )
                    {
                        MeridianStr=Tokens[1].Substring(LastIndex+1).ToUpper();
                    }
                    ArgCursor++;
                }
                catch
                {
                    bot.SendReply(e,
                                  String.Format(
                                      "Error: Unable to parse time ({0}).\n",
                                      e.Args[ArgCursor] ) +
                                  OnHelp(bot, e.Command) );
                    return false;
                }
            }
            else
            {
                bot.SendReply(e, String.Format(
                                  "Error: Unable to parse time ({0}).\n",
                                  e.Args[ArgCursor] ) +
                              OnHelp(bot, e.Command) );
                return false;
            }
            
            if ( ( ArgCursor < e.Args.Length ) &&
                 ( ( e.Args[ArgCursor].ToUpper() == "A" ) ||
                   ( e.Args[ArgCursor].ToUpper() == "AM" ) ||
                   ( e.Args[ArgCursor].ToUpper() == "P" ) ||
                   ( e.Args[ArgCursor].ToUpper() == "PM" ) ) )
            {
                MeridianStr = e.Args[ArgCursor++].ToUpper();
            }

            if ( ArgCursor < e.Args.Length )
            {
                foreach ( TimeZone tzi in this._AllTimeZones.List )
                    if ( e.Args[ArgCursor].ToUpper() == tzi.StandardName )
                    {
                        SpecZone = tzi;
                        ArgCursor++;
                        break;
                    }
                    else if ( e.Args[ArgCursor].ToUpper() == tzi.DaylightName )
                    {
                        SpecZone = tzi;
                        DaylightTime = true;
                        ArgCursor++;
                        break;
                    }
            }
            else
                SpecZone =this._AllTimeZones.Utc;

            return true;
        }

        public void OnEventAdd(BotShell bot, CommandArgs e)
        {

            // arbitrarily prune events whenever new events are added
            this.PruneEvents();

            if ( e.Args.Length < 3 )
                bot.SendReply(e, OnHelp(bot, e.Command) );

            DateTime Now = DateTime.UtcNow;

            int ArgCursor = 0;
            int Hour = -1;
            int Mins = -1;
            int Day = -1;
            int Mon = -1;
            int Year = -1;
            string MeridianStr = String.Empty;
            TimeZone GMTZone = this._AllTimeZones.Utc;
            TimeZone SpecZone = null;
            bool DaylightTime = false;
            string EventTitle = String.Empty;
            int RallyX = -1;
            int RallyY = -1;
            int RallyPF = -1;

            if ( ! this.ParseTimeArgs( bot, e,
                                       ref ArgCursor, ref Hour, ref Mins,
                                       ref MeridianStr, ref SpecZone,
                                       ref DaylightTime ) )
                return;

            Regex DateRegex = new Regex( "[0-9][0-9]*/[0-9][0-9]*" );
            if (  DateRegex.IsMatch( e.Args[ArgCursor] ) )
            {
                try
                {
                    string[] Tokens = e.Args[ArgCursor].Split( new Char[] {
                            ' ', '/' } );
                    Day = Convert.ToInt32(Tokens[0]);
                    Mon = Convert.ToInt32(Tokens[1]);
                    if ( Tokens.Length > 2 )
                        Year = Convert.ToInt32(Tokens[2]);
                    else
                    {
                        DateTime EventTimeTest;
                        try
                        {
                            EventTimeTest = new DateTime( Now.Year, Mon, Day,
                                                          Hour, Mins,
                                                          0 );
                        }
                        catch
                        {
                            bot.SendReply(
                                e, String.Format(
                                    "Error: Unable to parse date ({0}).\n",
                                    e.Args[ArgCursor] ) +
                                OnHelp(bot, e.Command) );
                            return;
                            
                        }
                            
                        if ( EventTimeTest < Now )
                            Year = Now.Year + 1;
                        else
                            Year = Now.Year;
                    }
                }
                catch
                {
                    bot.SendReply(e, String.Format(
                                      "Error: Unable to parse date ({0}).\n",
                                      e.Args[ArgCursor] ) +
                                  OnHelp(bot, e.Command) );
                    return;
                }
                ArgCursor++;
            }
            else
            {
                bot.SendReply(e, String.Format(
                                  "Error: Unable to parse date ({0}).\n",
                                  e.Args[ArgCursor] ) +
                              OnHelp(bot, e.Command) );
                return;
            }

            try
            {
                int x = Convert.ToInt32( e.Args[ArgCursor] );
                int y = Convert.ToInt32( e.Args[ArgCursor+1] );
                int pfid = 0;
                try
                {
                    pfid = Convert.ToInt32( e.Args[ArgCursor+2] );
                }
                catch
                {
                    pfid = PluginShared.GetPFID( e.Args[ArgCursor+2] );
                }
                RallyX = x;
                RallyY = y;
                RallyPF = pfid;
                ArgCursor += 3;
            }
            catch { }

            EventTitle = e.Words[ArgCursor];
            if ( EventTitle.Length > 80 )
                EventTitle = EventTitle.Substring(0,77) + "...";

            if( ((MeridianStr=="P")||(MeridianStr=="PM")) && (Hour < 12) )
                Hour += 12;
            else if( ((MeridianStr=="A")||(MeridianStr=="AM")) && (Hour == 12) )
                Hour = 0;

            DateTime EventTime;
            try
            {
                 EventTime = new DateTime( Year, Mon, Day, Hour, Mins, 0 );
            }
            catch
            {
                bot.SendReply(e, "Error: Unable to parse date.\n" +
                              OnHelp(bot, e.Command) );
                return;
            }            

            if ( (SpecZone != null) &&
                 (SpecZone.StandardName.ToString() != "GMT") )
            {
                EventTime = TimeZones.ConvertTime( EventTime,
                                                   SpecZone, GMTZone,
                                                   DaylightTime );
            }

            if ( EventTime < Now )
            {
                bot.SendReply(e,
                              "Error: Can not schedule events in the past.\n" );
                return;
            }

            string EventDateSpec = string.Format(
                "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                EventTime.Year, EventTime.Month, EventTime.Day,
                EventTime.Hour, EventTime.Minute );

            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText =
                    string.Format(
                        "SELECT * FROM events WHERE EventDateTime = '{0}'",
                        EventDateSpec );

                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() )
                {
                    bot.SendReply(e, "Error: Event already scheduled at that time.  Please select a different time.\n" );
                    return;
                }
            }

            string insert_command =
                string.Format( "INSERT INTO events ( EventTitle, EventDateTime, RallyX, RallyY, RallyPF ) VALUES ( '{0}', '{1}', {2}, {3}, {4} )",
                               EventTitle, EventDateSpec,
                               RallyX, RallyY, RallyPF );

            this._database.ExecuteNonQuery( insert_command );            

            RichTextWindow window = new RichTextWindow(bot);

            int NEvents = UpcomingEvents( bot, e, ref window );

            if ( NEvents > 0 )
                bot.SendReply(e, "Event \"" + EventTitle + "\" scheduled for " +
                           EventDateSpec + ".\nUpcoming Events »» ", window);
            else
                bot.SendReply(e,"Event \"" + EventTitle + "\" scheduled for " +
                           EventDateSpec + ".\nNo upcoming events." );

        }

        private int UpcomingEvents( BotShell bot, CommandArgs e,
                                    ref RichTextWindow window )
        {

            window.AppendTitle( "Upcoming Events" );
            window.AppendLineBreak();

            DateTime Now = DateTime.UtcNow;

            if ( e.Args.Length > 0 )
            {
                try
                {
                    Now =
                        DateTime.ParseExact( e.Words[0], "dd/MM/yyyy hh:mm:ss",
                                             CultureInfo.InvariantCulture );
                }
                catch
                {
                    try
                    {
                        Now =
                            DateTime.ParseExact( e.Words[0], "dd/MM/yyyy",
                                                 CultureInfo.InvariantCulture );
                        Now = Now.AddSeconds(1);
                    }
                    catch { }
                }
            }

            string NowDateSpec = string.Format(
                "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute );

            int NEvents = 0;

            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                if ( ( Now.Hour == 0 ) &&
                     ( Now.Minute == 0 ) &&
                     ( Now.Second == 1 ) )
                {
                    string EndDateSpec = string.Format(
                        "{0}/{1,2:D2}/{2,2:D2} 23:59:59",
                        Now.Year, Now.Month, Now.Day );
                    command.CommandText =
                        string.Format(
                            "SELECT * FROM events WHERE EventDateTime >= '{0}' AND EventDateTime <= '{1}'",
                            NowDateSpec, EndDateSpec );
                }
                else    
                    command.CommandText =
                        string.Format(
                            "SELECT * FROM events WHERE EventDateTime > '{0}'",
                            NowDateSpec );

                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() )
                {
                    window.AppendHighlight( reader.GetString(0) );
                    window.AppendLineBreak();
                    string DateSpec = reader.GetString(1);
                    window.AppendNormal( DateSpec + " GMT" );
                    int RallyX = reader.GetInt32(2);
                    int RallyY = reader.GetInt32(3);
                    int RallyPF = reader.GetInt32(4);
                    if ( RallyPF > 0 )
                    {
                        window.AppendNormal( " [" );
                        window.AppendCommand( "Rally Waypoint",
                                              "/waypoint " +
                                              RallyX.ToString() + " " +
                                              RallyY.ToString() + " " +
                                              RallyPF.ToString() );
                        window.AppendNormal( "]" );
                    }

                    string TimeStr = DateSpec.ToString();
                    TimeStr = TimeStr.Substring( TimeStr.IndexOf(' ')+1 );
                    window.AppendNormal( " [" );
                    window.AppendBotCommand( "TZ",
                                             "convtime " + TimeStr );
                    window.AppendNormal( "]" );
                    
                    if ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) 
                    {
                        window.AppendNormal( " [" );
                        window.AppendBotCommand( "Clear",
                                                 "event clear " + DateSpec );
                        window.AppendNormal( "]" );
                    }

                    window.AppendLineBreak(2);
                    NEvents++;
                }

            }

            return NEvents;

        }

        public void OnEventClear(BotShell bot, CommandArgs e)
        {
            if ( e.Args.Length == 0 )
            {
                bot.SendReply(e, OnHelp(bot, e.Command) );
                return;
            }
            
            DateTime EventTime;
            try
            {
                EventTime =
                    DateTime.ParseExact( e.Words[0], "M/d/yyyy h:mm:ss tt",
                                         CultureInfo.InvariantCulture );
            }
            catch
            {
                bot.SendReply(e, string.Format( "Unable to parse date/time: '{0}'", e.Words[0] ) + OnHelp(bot, e.Command) );
                return;
            }

            string EventDateSpec = string.Format(
                "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                EventTime.Year, EventTime.Month, EventTime.Day,
                EventTime.Hour, EventTime.Minute );

            this._database.ExecuteNonQuery(
                string.Format("DELETE FROM events WHERE EventDateTime = '{0}'",
                              EventDateSpec));

            RichTextWindow window = new RichTextWindow(bot);

            int NEvents = UpcomingEvents( bot, e, ref window );

            if ( NEvents > 0 )
                bot.SendReply(e, "Event originally scheduled for " +
                           EventDateSpec + " deleted.\nUpcoming Events »» ",
                              window);
            else
                bot.SendReply(e, "Event originally scheduled for " +
                           EventDateSpec + " deleted.\nNo upcoming events." );

        }

        public void OnEvents(BotShell bot, CommandArgs e)
        {
            
            RichTextWindow window = new RichTextWindow(bot);

            int NEvents = UpcomingEvents( bot, e, ref window );

            if ( NEvents > 0 )
                bot.SendReply(e, "Upcoming Events »» ", window);
            else
                bot.SendReply(e, "No upcoming events." );

        }

        public void OnCalendar(BotShell bot, CommandArgs e)
        {

            RichTextWindow window = new RichTextWindow(bot);

            window.AppendTitle( "Event Calendar" );

            window.AppendRawString( "<center>\n" );

            DateTime Now = DateTime.UtcNow;

            string NowDateSpec = string.Format(
                "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute );

            int DaysThisMonth = DateTime.DaysInMonth( Now.Year, Now.Month );
            int NextMonth = Now.Month + 1;
            int AddYear = 0;
            if ( NextMonth > 12 )
            {
                AddYear = 1;
                NextMonth = 1;
            }
            int DaysNextMonth = DateTime.DaysInMonth( Now.Year + AddYear,
                NextMonth );

            bool[] TwoMonthFlags = new bool[DaysThisMonth+DaysNextMonth];
            int NEvents = 0;

            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText =
                    string.Format(
                        "SELECT * FROM events WHERE EventDateTime > '{0}'",
                        NowDateSpec );

                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() )
                {
                    string DateSpec = reader.GetString(1);
                    DateTime EventTime;
                    try
                    {
                        EventTime =
                            DateTime.ParseExact( DateSpec, "M/d/yyyy h:mm:ss tt",
                                                 CultureInfo.InvariantCulture );
                    }
                    catch
                    {
                        bot.SendReply(e, "Event database error.");
                        return;
                    }
                    int Idx = EventTime.Day - 1;
                    if ( Now.Month != EventTime.Month )
                        Idx += DaysThisMonth;
                    TwoMonthFlags[Idx] = true;
                    NEvents ++;
                }

            }

            if ( NEvents > 0 )
            {

                DateTime FirstOfMonth = new DateTime( Now.Year, Now.Month, 1 );

                int DayOfWeekCursor = (int)FirstOfMonth.DayOfWeek;
                int ThisMonth = FirstOfMonth.Month;
                int ThisYear = FirstOfMonth.Year;

                int TotalDaysToPrint = DaysThisMonth+DaysNextMonth;
                for( int i = 0; i<TotalDaysToPrint; i++ )
                {

                    int day = i;
                    int DaysMonthCur = DaysThisMonth;
                    if ( i >= DaysThisMonth )
                    {
                        DaysMonthCur = DaysNextMonth;
                        day -= DaysThisMonth;
                    }

                    if ( day == 0 )
                    {
                        window.AppendLineBreak();
                        window.AppendColorStart("#EE9A00");
                        window.AppendRawString( 
                            string.Format("{0,2}", ThisMonth) +
                            string.Format("{0,5}", ThisYear));
                        window.AppendColorEnd();
                        window.AppendLineBreak();
                        window.AppendHighlight( " Su Mo Tu We Th Fr Sa" );
                        window.AppendLineBreak();
                        window.AppendColorStart("#000000");
                        if ( DayOfWeekCursor > 0 )
                            for( int j=0; j<DayOfWeekCursor; j++ )
                                window.AppendRawString(" 00");
                        window.AppendColorEnd();
                    }

                    if ( ! TwoMonthFlags[i] )
                        window.AppendNormal(string.Format("{0,3:D2}", day+1));
                    else
                    {
                        string DateSpec = string.Format(
                            "{0,2:D2}/{1,2:D2}/{2}",
                            day+1, ThisMonth, ThisYear );
                        window.AppendRawString(" ");
                        window.AppendCommand(string.Format("{0,2:D2}", day+1),
                                             "/tell " + bot.Character +
                                             " events " + DateSpec );
                    }

                    if ( DayOfWeekCursor == 6 )
                    {
                        window.AppendLineBreak();
                        DayOfWeekCursor = 0;
                    }
                    else
                        DayOfWeekCursor ++;

                    if ( day == DaysMonthCur - 1 )
                    {
                        if ( DayOfWeekCursor < 6 )
                        {
                            window.AppendColorStart("#000000");
                            for( int j=DayOfWeekCursor; j<=6; j++ )
                                window.AppendRawString(" 00");
                            window.AppendColorEnd();
                        }
                        ThisMonth = NextMonth;
                        ThisYear += AddYear;
                        window.AppendLineBreak();                        
                    }
                }

                window.AppendRawString( "</center>\n" );

                bot.SendReply(e, "Event calendar »» ", window);
            }
            else
                bot.SendReply(e, "No upcoming events." );

        }

		private void PruneEvents()
        {

            if ( this._pruneMonths != 0 )
            {

                DateTime Now = DateTime.UtcNow;

                DateTime PruneDate =
                    Now.AddMonths(-Math.Abs(this._pruneMonths));

                string PruneDateSpec = string.Format(
                    "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                    PruneDate.Year, PruneDate.Month, PruneDate.Day,
                    PruneDate.Hour, PruneDate.Minute );

                this._database.ExecuteNonQuery(
                    string.Format("DELETE FROM events WHERE EventDateTime < '{0}'",
                                  PruneDateSpec));
            }

        }

        public void EventsWindow( ref RichTextWindow window, string user )
        {
            window.AppendHeader( "Upcoming Events");
            window.AppendLineBreak();

            DateTime Now = DateTime.UtcNow;

            string NowDateSpec = string.Format(
                "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute );

            int NEvents = 0;

            using (IDbCommand command=this._database.Connection.CreateCommand())
            {

                command.CommandText =
                    string.Format(
                        "SELECT * FROM events WHERE EventDateTime > '{0}'",
                        NowDateSpec );

                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() )
                {
                    window.AppendHighlight( reader.GetString(0) );
                    window.AppendLineBreak();
                    string DateSpec = reader.GetString(1);
                    window.AppendNormal( DateSpec + " GMT" );
                    int RallyX = reader.GetInt32(2);
                    int RallyY = reader.GetInt32(3);
                    int RallyPF = reader.GetInt32(4);
                    if ( RallyPF > 0 )
                    {
                        window.AppendNormal( " [" );
                        window.AppendCommand( "Rally Waypoint",
                                              "/waypoint " +
                                              RallyX.ToString() + " " +
                                              RallyY.ToString() + " " +
                                              RallyPF.ToString() );
                        window.AppendNormal( "]" );
                    }

                    if ( this._bot.Users.GetUser(user) >= UserLevel.Leader ) 
                    {
                        window.AppendNormal( " [" );
                        window.AppendCommand( "Clear",
                                              "/tell " + this._bot.Character +
                                              " event clear " + DateSpec );
                        window.AppendNormal( "]" );
                    }

                    window.AppendLineBreak(2);
                    NEvents ++;
                }
            }

            if ( NEvents == 0 )
            {
                window.AppendNormal( "No upcoming events." );
                window.AppendLineBreak();
            }

            window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            window.AppendLineBreak();

        }

        private void CollectUserStatsEvent(object sender, EventArgs e)
        {

            // Check currenttime against last update
            long time = TimeStamp.FromDateTime( DateTime.UtcNow );
            double last_update = 0;

            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM LastUpdate";
                IDataReader reader = command.ExecuteReader();
                if ( reader.Read() && !reader.IsDBNull(0) )
                    last_update = reader.GetDouble(0);
            }

            if ( time >= last_update + 3600 )
            {
                int currently_online =
                    _bot.FriendList.Online("notify").Length +
                    _bot.PrivateChannel.List().Count;

                DateTime dt = TimeStamp.ToDateTime( time );
                double last_total = 0;
                double last_sample = 0;

                using (IDbCommand command =
                       this._database.Connection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM stats where WeekDay = " + Convert.ToInt32(dt.DayOfWeek).ToString() + " AND Hour = " + Convert.ToInt32(dt.Hour).ToString();
                    IDataReader reader = command.ExecuteReader();
                    if ( reader.Read() && !reader.IsDBNull(0) )
                    {
                        last_total = reader.GetDouble(2);
                        last_sample = reader.GetDouble(3);
                    }
                }

                last_total += currently_online;
                last_sample += 1.0;

                this._database.ExecuteNonQuery("DELETE FROM LastUpdate");
                this._database.ExecuteNonQuery(string.Format( "INSERT INTO LastUpdate ( DayTime, Users ) VALUES ( {0}, {1} )", time.ToString(), currently_online.ToString() ) );

                this._database.ExecuteNonQuery(string.Format("REPLACE INTO stats ( WeekDay, Hour, Total, Samples ) VALUES ( {0}, {1}, {2}, {3} )", Convert.ToInt32(dt.DayOfWeek).ToString(),  Convert.ToInt32(dt.Hour).ToString(), last_total.ToString(), last_sample.ToString()) );
            }
        }

        private string TextBarGraph ( double value, double max, string label )
        {
            int bars = (int)Math.Round( ( value / max ) * 20 );
            string bar_string = string.Empty;
            for ( int i=0; i<bars; i++ )
                bar_string += HTML.CreateColorString(RichTextWindow.ColorGreen, "|");
            for ( int i=bars; i<20; i++ )
                bar_string += HTML.CreateColorString("000000", "|");
            if ( value == max )
                bar_string += " " + HTML.CreateColorString(RichTextWindow.ColorGreen, label ) + " (" + value.ToString() + ")";
            else
                bar_string += " " + label + " (" + value.ToString() + ")";
            return bar_string;
        }

        public void OnStats(BotShell bot, CommandArgs e)
        {
            double[,] average = new double[7,24];
            double[,] day_average = new double[7,2];
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM stats";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                {
                    int day_of_week = reader.GetInt32(0);
                    int hour_of_day = reader.GetInt32(1);
                    double total = reader.GetDouble(2);
                    double samples = reader.GetDouble(3);
                    average[day_of_week,hour_of_day] =  total / samples;
                    day_average[day_of_week,0] += total;
                    day_average[day_of_week,1] += samples;
                }
            }

            RichTextWindow window = new RichTextWindow(bot);
            
            if ( e.Command == "daystats" )
            {
                window.AppendTitle( "Daily User Statistics" );
                window.AppendLineBreak();
                int max_idx = 0;
                for ( int i=0; i<7; i++ )
                {
                    day_average[i,0] /= day_average[i,1];
                    if ( day_average[i,0] > day_average[max_idx,0] )
                        max_idx = i;
                }

                for ( int i=0; i<7; i++ )
                {
                    DayOfWeek d = (DayOfWeek)i;
                    string bar = TextBarGraph ( day_average[i,0],
                                                day_average[max_idx,0],
                                                d.ToString() );
                    window.AppendRawString( bar );
                    window.AppendLineBreak();
                }
            }
            else
            {

                int day = 0;
                DayOfWeek d = DayOfWeek.Sunday;

                try
                {
                    day = Convert.ToInt32( e.Args[0] );
                }
                catch
                {
                    if ( e.Args.Length > 0 )
                        for ( int i=0; i<7; i++ )
                        {
                            d = (DayOfWeek)i;
                            string dn = d.ToString();
                            if (dn.ToLower().IndexOf(e.Args[0].ToLower()) >= 0)
                            {
                                day = i;
                                break;
                            }
                        }
                }

                d = (DayOfWeek)day;
                window.AppendTitle( "Hourly User Statistics for " + d.ToString() );
                window.AppendLineBreak();

                window.AppendNormal( "Statistics by hour of day (GMT)" );
                window.AppendLineBreak(2);

                int max_idx = 0;
                for ( int i=0; i<24; i++ )
                    if ( average[day,i] > average[day,max_idx] )
                        max_idx = i;

                for ( int i=0; i<24; i++ )
                {
                    string bar = TextBarGraph ( average[day,i],
                                                average[day,max_idx],
                                                string.Format( "{0,2:00}:00",
                                                               i) );
                    window.AppendRawString( bar );
                    window.AppendLineBreak();
                }
            }
            bot.SendReply(e, "User Statistics »» ", window);
            
        }

        public void OnTeamActivity( BotShell bot, CommandArgs e )
        {
            if ( e.Args.Length > 0 )
            {

                int testint;

                if ( ( e.Args.Length == 1 ) &&
                     ( int.TryParse( e.Args[0], out testint ) ) )
                {

                    if ( testint > 0 ) // Toggle user
                    {

                        bool found = false;
                        using (IDbCommand command =
                               this._database.Connection.CreateCommand())
                        {
                            command.CommandText = string.Format( "SELECT * FROM ActivitySignup WHERE Name = '{0}' and ID = {1};", e.Sender, testint );
                            IDataReader reader = command.ExecuteReader();
                            while ( reader.Read() && !reader.IsDBNull(0) )
                                if ( reader.GetString(0) == e.Sender )
                                    found = true;
                        }
                        if ( found )
                        {
                            bot.SendReply( e, "Removing you from activity sign-up." );
                            this._database.ExecuteNonQuery( string.Format( "DELETE FROM ActivitySignup WHERE Name = '{0}' AND ID = {1};", e.Sender, testint ) );

                        }
                        else
                        {
                            bot.SendReply( e, "Adding you to activity sign-up." );
                            this._database.ExecuteNonQuery( string.Format( "INSERT INTO ActivitySignup ( Name, ID ) VALUES ( '{0}', {1} );", e.Sender, testint ) );
                        }
                    }
                    else // Delete activity and all users
                    {
                        bot.SendReply( e, "Deleting activity and all sign-ups." );
                        this._database.ExecuteNonQuery( string.Format( "DELETE FROM Activity WHERE ID = {0};", -testint ) );
                        this._database.ExecuteNonQuery( string.Format( "DELETE FROM ActivitySignup WHERE ID = {0};", -testint ) );
                    }

                }
                else if ( ( e.Args.Length > 1 ) &&
                          ( int.TryParse( e.Args[0], out testint ) ) )
                {
                    bot.SendReply( e, "Please do not start your activity with a number." );
                    return;
                }

                else if ( this._database.ExecuteNonQuery( string.Format( "INSERT INTO Activity ( Title ) VALUES ( '{0}' )", e.Words[0] ) ) > 0 )
                {
                    bot.SendReply( e, "Activity added." );
                }
                else
                {
                    bot.SendReply( e, "Error attempting to add activity." );
                }

            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( "Looking for Teams" );
            window.AppendLineBreak();

            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Activity";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                {
                    window.AppendHighlight( reader.GetString(0) );
                    int ID = reader.GetInt32(1);
                    window.AppendNormal( " [" );
                    window.AppendBotCommand( "X", "lft -" + ID.ToString() );
                    window.AppendNormal( "]" );
                    window.AppendLineBreak( );
                    window.AppendNormal( "Sign-ups: " );

                    using (IDbCommand subcommand =
                           this._database.Connection.CreateCommand())
                    {
                        subcommand.CommandText = string.Format( "SELECT * FROM ActivitySignup WHERE ID = {0}", ID );
                        IDataReader subreader = subcommand.ExecuteReader();
                        while ( subreader.Read() && !subreader.IsDBNull(0) )
                        {
                            string name = subreader.GetString(0);
                            window.AppendNormal( name );
                            window.AppendNormal( ", " );
                        }
                    }

                    window.AppendBotCommand( "Add/Remove me",
                                             "lft " + ID.ToString() );

                    window.AppendLineBreak( 2 );
                }
            }

            bot.SendReply(e, "Looking for team »» ", window);

        }

        public void OnConvertTime(BotShell bot, CommandArgs e)
        {

            if ( e.Args.Length < 1 )
            {
                bot.SendReply( e, "Usage: !convtime HH:MM [AM/PM] [TZ]" );
                return;
            }

            int ArgCursor = 0;
            int Hour = 0;
            int Mins = 0;
            string MeridianStr = String.Empty;
            TimeZone GMTZone = this._AllTimeZones.Utc;
            TimeZone SpecZone = null;
            bool DaylightTime = false;

            if ( ! this.ParseTimeArgs( bot, e,
                                       ref ArgCursor, ref Hour, ref Mins,
                                       ref MeridianStr, ref SpecZone,
                                       ref DaylightTime ) )
                return;

            if( ((MeridianStr=="P")||(MeridianStr=="PM")) && (Hour < 12) )
                Hour += 12;
            else if( ((MeridianStr=="A")||(MeridianStr=="AM")) && (Hour == 12) )
                Hour = 0;

            DateTime Now = DateTime.UtcNow;
            DateTime EventTime = new DateTime( Now.Year, Now.Month, Now.Day,
                                               Hour, Mins, 0 );

            // first convert event time to GMT
            if ( (SpecZone != null) &&
                 (SpecZone.StandardName.ToString() != "GMT") )
            {
                EventTime = TimeZones.ConvertTime( EventTime,
                                                   SpecZone, GMTZone,
                                                   DaylightTime );
            }

            string TimeStr = EventTime.ToString();
            TimeStr = TimeStr.Substring( TimeStr.IndexOf(' ')+1 );

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle(  "The time: " + TimeStr + " GMT" );
            window.AppendLineBreak();

            window.AppendNormal( "(Use time in parenthesis if your location observes Daylight Savings Time and it is summer)" );
            window.AppendLineBreak();

            string current_continent = string.Empty;
            List<float> displayed_tz = new List<float>();
            foreach ( TimeZone tzi in this._AllTimeZones.List )
            {
                if ( current_continent !=
                     tzi.DisplayName.Substring(0,tzi.DisplayName.IndexOf('/')) )
                {
                    displayed_tz.Clear();
                    current_continent = tzi.DisplayName.Substring(0,tzi.DisplayName.IndexOf('/'));
                    window.AppendLineBreak();
                    window.AppendHeader( current_continent );
                    window.AppendLineBreak();
                }
                if ((current_continent==tzi.DisplayName.Substring(0,tzi.DisplayName.IndexOf('/'))) && ( !displayed_tz.Contains(tzi.UtcOffsetHours)))
                {
                    displayed_tz.Add(tzi.UtcOffsetHours);
                    DateTime ConvTime = TimeZones.ConvertTime( EventTime,
                                                               GMTZone, tzi );
                    string ConvTimeStr = ConvTime.ToString();
                    ConvTimeStr=ConvTimeStr.Substring( ConvTimeStr.IndexOf(' ')+1);
                    window.AppendNormal( "  " + ConvTimeStr );
                    window.AppendHighlight( " " + tzi.StandardName );
                    if ( ! string.IsNullOrEmpty( tzi.DaylightName ) )
                    {
                        ConvTime=ConvTime.AddHours(1);
                        ConvTimeStr = ConvTime.ToString();
                        ConvTimeStr=ConvTimeStr.Substring( ConvTimeStr.IndexOf(' ')+1);
                        window.AppendNormal( " (" + ConvTimeStr );
                        window.AppendHighlight( " " + tzi.DaylightName + ")" );
                    }
                    window.AppendLineBreak();
                }
            }
            
            bot.SendReply(e, "The time: " + TimeStr + " GMT »» ", window);
        }

		public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "event add":
                return "Adds a new event.\n\n" +
                    "Usage: /tell " + bot.Character +
                    " event add [time] [date] [rally coords] [title]\n\n" +
                    " Time is specified as HH:MM [AM/PM] TZ\n" +
                    " Date is specified as DD/MM/YYYY\n" +
                    " Rally location is specified as X Y PFID\n" +
                    " AM/PM is optional when specifying 24 hour time.\n" +
                    " Time zone is optional and will default to GMT.\n" +
                    " Year is optional and will default to this year.\n" +
                    " Rally coordinates are optional.\n";

            case "event clear":
                return "Clears an event.\n\n" +
                    "Usage: /tell " + bot.Character +
                    " event clear [date time]\n\n" +
                    " Date/Time is specified as m/d/yyyy h:mm:ss am.\n" +
                    " (Use the \"events\" command to easily clear events)\n";

            case "events":
                return "Displays up-coming scheduled events.\n\n" +
                    "Usage: /tell " + bot.Character + " events";

            case "calendar":
                return "Displays up-coming scheduled events calendar.\n\n" +
                    "Usage: /tell " + bot.Character + " calendar";

            case "lft":
                return "Manage and display activities members would like to do together.\n\n" +
                    "Usage: /tell " + bot.Character + " lft\n\n" +
                    "If the activity you need is not listed, you can add one with the following command:\n\n" +
                    "Usage: /tell " + bot.Character + " lft [activity]";

            case "daystats":
                return "Displays histogram of player activity by day.\n\n" +
                    "Usage: /tell " + bot.Character + " daystats";

            case "hourstats":
                return "Displays histogram of player activity by hour.\n\n" +
                    "Usage: /tell " + bot.Character + " hourstats";

            case "convtime":
                return "Displays specified time (GMT by default) in other time zones.\n\n" +
                    "Usage: /tell " + bot.Character + " convtime HH:MM" +
                    " Time is specified as HH:MM [AM/PM] TZ\n";

            }
            return null;
        }

    }
}
