using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class FriendsList : PluginBase
    {
        private Config _database;
        public FriendsList()
        {
            this.Name = "Friends List Manager";
            this.InternalName = "llFriendsList";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;
            this.Description = "Provides an ability to keep a friend list on bot and see if they are online.";
            this.Commands = new Command[] {
                new Command("friend", true, UserLevel.Member),
                new Command("friends","friend"),
                new Command("friend add", true, UserLevel.Member),
                new Command("friend remove", true, UserLevel.Member),
                new Command("friend clear", true, UserLevel.Leader)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS friend (listname VARCHAR(14) NOT NULL, friendname VARCHAR(14), PRIMARY KEY ( listname, friendname ) )");
        }
        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
                case "friend":
                    this.OnFriendsListCommand(bot, e);
                    break;
                case "friend add":
                    this.OnFriendsListAddCommand(bot, e);
                    break;
                case "friend remove":
                    this.OnFriendsListRemoveCommand(bot, e);
                    break;
                case "friend clear":
                    this.OnFriendsListClearCommand(bot, e);
                    break;
            }
        }

        private void OnFriendsListCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);
            
            string listname = bot.Users.GetMain(e.Sender);

            window.AppendTitle( Format.UppercaseFirst(listname) +
                                "'s Friends List");

            bool found = false;
            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT friendname FROM friend WHERE listname = '" + listname + "' ORDER BY friendname";
                    
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    found = true;
                    window.AppendNormal("[");
                    window.AppendBotCommand("X", "friend remove " + reader.GetString(0) );
                    window.AppendNormal("] ");
                    window.AppendNormal("[");
                    window.AppendCommand("+", "/cc addbuddy " + reader.GetString(0) );
                    window.AppendNormal("] ");

                    window.AppendCommand( reader.GetString(0), "/tell " + reader.GetString(0) );
                    OnlineState state = bot.FriendList.IsOnline(reader.GetString(0) );
                    switch (state)
                    {

                    case OnlineState.Offline:
                        window.AppendColorString(RichTextWindow.ColorRed,
                                                 " Offline" ); 
                        break;

                    case OnlineState.Timeout:
                    case OnlineState.Unknown:
                        window.AppendColorString(RichTextWindow.ColorRed,
                                                 " Unknown" ); 
                        break;
                        
                    default:
                        window.AppendColorString(RichTextWindow.ColorGreen,
                                                 " Online" );
                        break;
                    }
                    window.AppendLineBreak();
                }
                reader.Close();
            }
            if (!found)
                bot.SendReply(e, "No names in your friends list.");
            else
                bot.SendReply(e, "View your friends list »» ", window );
        }

        private void OnFriendsListAddCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: friend add [friendname]");
                return;
            }

            // rules for which list to add to:
            // by default add all names to personal private list
            string listname = bot.Users.GetMain(e.Sender);
            string friendname;

            friendname = Format.UppercaseFirst(e.Args[0]);
            if (bot.GetUserID(friendname) < 100)
            {
                bot.SendReply(e, "No such user: " +
                              HTML.CreateColorString(bot.ColorHeaderHex,
                                                     friendname));
                return;
            }

            // check if friendname is already in listname's Friends list
            bool found = false;
            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT friendname FROM friend WHERE listname = '" + listname + "' AND friendname = '" + friendname + "' ORDER BY friendname";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    // user already in this list
                    if ( reader.GetString(0) == friendname )
                        found = true;
                }
                reader.Close();
            }
            if ( ! found )
            {
                this._database.ExecuteNonQuery(string.Format("INSERT INTO friend (listname, friendname) VALUES ('{0}', '{1}')", listname, friendname ));
                bot.SendReply(e, "You have added " + HTML.CreateColorString(bot.ColorHeaderHex, friendname) + " to your Friends list." );
            }
            else
            {
                bot.SendReply(e,  HTML.CreateColorString(bot.ColorHeaderHex, friendname) + " is already in your friends list." );
            }

        }

        private void OnFriendsListRemoveCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: friend remove [username]");
                return;
            }
            
            string listname = bot.Users.GetMain(e.Sender);

            string friendname = Format.UppercaseFirst(e.Args[0]);
            if (bot.GetUserID(friendname) < 100)
            {
                bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, friendname));
                return;
            }

            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM friend WHERE listname ='" + listname + "' AND friendname = '" + friendname + "'";
                IDataReader reader = command.ExecuteReader();
                if (!reader.Read())
                {
                    reader.Close();
                    bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, friendname) + " not in your friends list");
                    return;
                }
                reader.Close();
            }
            this._database.ExecuteNonQuery("DELETE FROM friend WHERE listname = '" + listname + "' AND friendname = '" + friendname + "'");
            bot.SendReply(e, "You removed " + HTML.CreateColorString(bot.ColorHeaderHex, friendname ) + " from your friends List.");
        }

        private void OnFriendsListClearCommand(BotShell bot, CommandArgs e)
        {
            
            string listname = bot.Users.GetMain(e.Sender);

            this._database.ExecuteNonQuery("DELETE FROM friend where listname = '" + listname + "'" );
             bot.SendReply(e, "All friends have been cleared from your friends List." );

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "friend":
                    return "Displays your friends list.\n" +
                        "Usage: /tell " + bot.Character + " friend";
                case "friend add":
                    return "Allows you to add [username] to your friends list.\n" +
                        "Usage: /tell " + bot.Character + " friend add [username]";
                case "friend remove":
                    return "Allows you to remove [username] from your friends list.\n" +
                        "Usage: /tell " + bot.Character + " friend remove [username]";
                case "friend clear":
                    return "Clears your friends list.\n" +
                        "Usage: /tell " + bot.Character + " friend clear";
            }
            return null;
        }
    }
}
