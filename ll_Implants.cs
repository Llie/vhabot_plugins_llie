using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class Implants : PluginBase
    {
        private string Server = "flw.nu/tools/premadeimps.php?q=dumpdb_xml";
        private string UrlTemplate = "http://{0}";
		private string pfilename = "data/premades.xml";
		private string cfilename = "data/clusters.xml";
        private PremadesImps listing;
        private ClusterSkills build_reqs;

		public static string rw( string cs )
        {
            char[] whitespace = new char[]{'\n', '\r', '\t', ' '};
            foreach (char c in whitespace)
                cs = cs.Replace(c.ToString(), "");
            return cs;
        }

        public Implants()
        {
            this.Name = "Implant Information";
            this.InternalName = "llImplants";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;
            this.Commands = new Command[] {
                new Command("premade", true, UserLevel.Guest),
                new Command("cluster", true, UserLevel.Guest),
                new Command("impinfo", true, UserLevel.Guest),
                new Command("imploc", true, UserLevel.Guest),
                new Command("impmax", true, UserLevel.Guest),
                new Command("impbuild", true, UserLevel.Guest),
                new Command("imp", true, UserLevel.Guest),
                new Command("profimp","premade"),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "premades.xml" );
            Updater.Update( "clusters.xml" );

            string xml = string.Empty;
            if ( File.Exists ( pfilename ) )
            {
                using(StreamReader rdr = File.OpenText(pfilename))
                {
                    xml = rdr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to read file: " + pfilename );
                    return;
                }
            }
            else
            {
                string url = string.Format(this.UrlTemplate, this.Server);
                xml = HTML.GetHtml(url, 60000); 
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to connect to server " + url );
                    return;
                }
                // fix non-standard XML
                if ( xml.IndexOf ( "<?xml" ) != 0 )
                {
                    xml = xml.Replace ( "<xml>",
                                        "<?xml version='1.0' encoding='UTF-8'?>" );
                    xml = xml.Replace ( "</xml>", "" );
                }
            }
            MemoryStream stream = null;
            stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            XmlSerializer serializer = new XmlSerializer(typeof(PremadesImps));
            listing = (PremadesImps)serializer.Deserialize(stream);
            stream.Close();

            if ( File.Exists ( cfilename ) )
            {
                using(StreamReader rdr = File.OpenText(cfilename))
                {
                    xml = rdr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to read file: " + cfilename );
                    return;
                }
            }
            stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            serializer = new XmlSerializer(typeof(ClusterSkills));
            build_reqs = (ClusterSkills)serializer.Deserialize(stream);
            stream.Close();

        }

        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "premade":
                PremadeCommand(bot, e);
                break;

            case "cluster":
                ClusterCommand(bot, e);
                break;

            case "impinfo":
                ImpInfoCommand(bot, e);
                break;

            case "imploc":
                ImpLocationCommand(bot, e);
                break;

            case "impbuild":
                ImpBuildCommand(bot, e);
                break;

            case "impmax":
                ImpMaxCommand(bot, e);
                break;

            case "imp":
                ImpCommand(bot, e);
                break;

            }
        }

//             		Basic					Refined	
// 					m			b			m			b
// Treatment		4.723645091	6.276354909	10.60615662	-1130.83748
// Ability			2			4			6.757615662	-932.280748
// AbilityShiny		0.250056251	4.749943749	0.181926193	18.43283528
// AbilityBright	0.15076677	2.84923323	0.111551155	10.57821782
// AbilityFaded		0.100127503	1.899872497	0.071079208	7.713079208
// SkillShiny		0.499962499	5.500037501	0.353651425	34.91606355
// SkillBright		0.301509533	2.698490467	0.222574257	18.26257426
// SkillFaded		0.201010039	1.798989961	0.151627183	11.52293627

        private void ComputeMinQLCluster( int ql, int[] results )
        {
            results[0] = (int)Math.Round( (float)ql * 0.86);
            results[1] = (int)Math.Round( (float)ql * 0.84);
            results[2] = (int)Math.Round( (float)ql * 0.82);
        }

        private int ComputeTreatmentRequirement( int ql )
        {
            if ( ( ql > 0 ) && ( ql < 201 ) )
                return (int)Math.Round( 4.723645091*(float)ql + 6.276354909 );
            else if ( ( ql > 200 ) && ( ql < 301 ) )
                return (int)Math.Round( 10.60615662*(float)ql - 1130.83748  );
            return 0;
        }

        private void ComputeJobRequirements( int ql, ref int treatment, ref int ability )
        {
            // estimated
            // 2.27638191	8.72361809
            // 4.969849246	11.03015075
            // 7.626262626	-1056.878788
            // 10.60606061	-1130.818182
            if ( ( ql > 0 ) && ( ql < 201 ) )
            {
                ability = (int)Math.Round( 2.27638191*(float)ql + 8.72361809 );
                treatment = (int)Math.Round( 4.969849246*(float)ql + 11.03015075 );
            }
            else if ( ( ql > 200 ) && ( ql < 301 ) )
            {
                ability = (int)Math.Round( 7.626262626*(float)ql - 1056.878788 );
                // treatment = (int)Math.Round( 10.60606061*(float)ql - 1130.818182 );
                // use the treatment numbers for non-jobe implants seems
                // to do a better job
                treatment = (int)Math.Round( 10.60615662*(float)ql - 1130.83748  );
;
            }
            return;
        }

        private int ComputeAbilityRequirement( int ql )
        {
            if ( ( ql > 0 ) && ( ql < 201 ) )
                return (int)Math.Round( 2*(float)ql + 4 );
            else if ( ( ql > 200 ) && ( ql < 301 ) )
                return (int)Math.Round( 6.757615662*(float)ql - 932.280748  );
            return 0;
        }

        private void ComputeImplantQL( int treatment, int ability, ref int ql, ref int jql )
        {
            int tql=0, aql=0;

            // compute requirements for basic implants
            if ( ( ability < 426 ) && ( treatment < 1001 ) )
            {
                // treatment
                tql=(int)Math.Floor(((float)treatment-6.276354909)/4.723645091);
                // ability
                aql=(int)Math.Floor(((float)ability - 4)/2);
                ql = Math.Min( tql, aql );
            }
            else // compute requirements for refined implants
            {
                // treatment refined
                tql=(int)Math.Floor(((float)treatment+1130.83748)/10.60615662);
                // ability refined
                aql=(int)Math.Floor(((float)ability+932.280748)/6.757615662);
                ql = Math.Min( tql, aql );
            }

            // compute requirements for basic jobe implants
            if ( ( ability < 476 ) && ( treatment < 1001 ) )
            {
                // treatment jobe
                tql=(int)Math.Floor(((float)treatment-11.03015075)/4.969849246);
                // ability jobe
                aql=(int)Math.Floor(((float)ability-8.72361809)/2.27638191);
                jql = Math.Min( tql, aql );
            }
            else // compute requirements for refined implants
            {
                // treatment refined job
                tql=(int)Math.Floor(((float)treatment+1130.83748)/10.60615662);
                // ability refined jobe
                aql=(int)Math.Floor(((float)ability+1056.878788)/7.626262626);
                jql = Math.Min( tql, aql );
            }

        }

        private void ComputeAbilityBuff( int ql, int[] ability )
        {
            if ( ( ql > 0 ) && ( ql < 201 ) )
            {
                ability[0] = (int)Math.Round( 0.250056251*(float)ql + 4.749943749 );
                ability[1] = (int)Math.Round( 0.15076677*(float)ql + 2.84923323 );
                ability[2] = (int)Math.Round( 0.100127503*(float)ql + 1.899872497 );
            }
            else if ( ( ql > 200 ) && ( ql < 301 ) )
            {
                ability[0] = (int)Math.Round( 0.181926193*(float)ql + 18.43283528 );
                ability[1] = (int)Math.Round( 0.111551155*(float)ql + 10.57821782 );
                ability[2] = (int)Math.Round( 0.071079208*(float)ql + 7.713079208 );
            }
        }

        private void ComputeSkillBuff( int ql, int[] skill )
        {
            if ( ( ql > 0 ) && ( ql < 201 ) )
            {
                skill[0] = (int)Math.Round( 0.499962499*(float)ql + 5.500037501 );
                skill[1] = (int)Math.Round( 0.301509533*(float)ql + 2.698490467 );
                skill[2] = (int)Math.Round( 0.201010039*(float)ql + 1.798989961 );
            }
            else if ( ( ql > 200 ) && ( ql < 301 ) )
            {
                skill[0] = (int)Math.Round( 0.353651425*(float)ql + 34.91606355 );
                skill[1] = (int)Math.Round( 0.222574257*(float)ql + 18.26257426 );
                skill[2] = (int)Math.Round( 0.151627183*(float)ql + 11.52293627 );
            }
        }

        private int ComputeAbilityQL( int ability, int[] ql )
        {
            // ql 200 shiny gives 55
            // ql 300 shiny gives 73

            float[] factors = { 2.0f, 1.6f, 1.4f, 1.0f, 0.6f, 0.4f };
            int s, a=0;

            for ( int i=0; i<factors.Length; i++ )
            {
                s = (int)Math.Round( (float)ability / factors[i] );
                if ( s <= 55 )
                {
                    ql[i] = (int)Math.Floor(((float)s-4.749943749)/0.250056251);
                    a = 1;
                }
                else if ( s <= 73 )
                {
                    ql[i] = (int)Math.Floor(((float)s-18.43283528)/0.181926193);
                    a = 1;
                }
            }

            return a;

        }

        private int ComputeSkillQL( int skill, int[] ql )
        {
            // ql 200 gives 105
            // ql 300 gives 141

            float[] factors = { 2.0f, 1.6f, 1.4f, 1.0f, 0.6f, 0.4f };
            int s, a = 0;

            for ( int i=0; i<factors.Length; i++ )
            {
                s = (int)Math.Round( (float)skill / factors[i] );
                if ( s <= 105 )
                {
                    ql[i] = (int)Math.Floor(((float)s-5.500037501)/0.499962499);
                    a = 1;
                }
                else if ( s <= 141 )
                {
                    ql[i] = (int)Math.Floor(((float)s-34.91606355)/0.353651425);
                    a = 1;
                }
            }

            return a;

        }

        private void ImpUsageMessages(BotShell bot, CommandArgs e)
        {
            // not all messages will return windows, but in case they
            // want to we declare one here
            RichTextWindow window = new RichTextWindow(bot);

            switch (e.Command)
            {

            case "premade":
                window.AppendTitle("Premade Implant Information");
                window.AppendLineBreak(2);
                window.AppendNormal( "Premade Implants are available in the \"Professional Implants\" section of Basic Shops.  Premade implants are available in the following QLs: " );
                window.AppendNormal( "10, 20, 30, 50, 75, and 100" );
                window.AppendLineBreak(2);
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [skill name]. More Info »» ", window);
                break;

            case "cluster":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [location] [shiny|bright|faded] (jobe)");
                break;

            case "impinfo":
                window.AppendTitle("Implant Information");
                window.AppendLineBreak(2);
                window.AppendNormal( "Clean implants can be purchased from Basic, Advanced, and Superior Shops in the following QLs: " );
                window.AppendNormal( "1, 5, 10, 15, 20, 30, 50, 70, 80, 90, 110, and 125.  " );
                window.AppendNormal( "Above QL 125, you must roll for them in missions or obtain them from certain classes of mobs.  (For instance, robotic and human mobs have a high likelihood of dropping implants.)" );
                window.AppendLineBreak(2);
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [ql]. More Info »» ", window);
                break;

            case "imploc":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [skill]");
                break;

            case "impbuild":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [ql] [skill|ability]");
                break;

            case "impmax":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [ability] [treatment]");
                break;

            case "imp":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [amount] [skill]");
                break;

            }

        }
    
        private void ImpInfoCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);

            if (e.Args.Length != 1)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            int ql = 0;

            try
            {
                ql = Convert.ToInt32( e.Args[0] );
            }
            catch
            {
                ImpUsageMessages( bot, e);
                return;
            }

            if ( ( ql > 0 ) && ( ql < 301 ) )
            {
                int[] ability, skills, min_ql;
                int treatment_req = ComputeTreatmentRequirement( ql );
                int ability_req = ComputeAbilityRequirement( ql );
                min_ql = new int[3];
                ComputeMinQLCluster( ql, min_ql );
                ability = new int[3];
                ComputeAbilityBuff( ql, ability );
                skills = new int[3];
                ComputeSkillBuff( ql, skills );

                window.AppendTitle("Implant Requirements Information");
                window.AppendLineBreak();

                window.AppendHighlight("For a QL " + e.Args[0] + " implant:" );
                window.AppendLineBreak(2);

                window.AppendHighlight("For ability clusters, you will receive the following benefits: " );
                window.AppendLineBreak();
                window.AppendNormal( "     Shiny cluster: +" +
                                     Convert.ToString( ability[0] ) );
                window.AppendNormal( "    Bright cluster: +" +
                                     Convert.ToString( ability[1] ) );
                window.AppendNormal( "     Faded cluster: +" +
                                     Convert.ToString( ability[2] ) );
                window.AppendLineBreak(2);

                window.AppendHighlight("For skill clusters, you will receive the following benefits: " );
                window.AppendLineBreak();
                window.AppendNormal( "     Shiny cluster: +" +
                                     Convert.ToString( skills[0] ) );
                window.AppendNormal( "    Bright cluster: +" +
                                     Convert.ToString( skills[1] ) );
                window.AppendNormal( "     Faded cluster: +" +
                                     Convert.ToString( skills[2] ) );
                window.AppendLineBreak(2);

                window.AppendHighlight("The requirements to install the implant: " );
                window.AppendLineBreak();
                window.AppendNormal( "     Treatment: " +
                                     Convert.ToString( treatment_req ) );
                window.AppendNormal( "       Ability: " +
                                     Convert.ToString( ability_req ) );
                window.AppendLineBreak(2);

                window.AppendHighlight( "The following are the minimum QL for clusters that can be installed in a QL " + e.Args[0] + " implant:" );
                window.AppendLineBreak();
                // refined implants h
                if ( ql > 200 )
                    for( int i=0; i<3; i++ ) if ( min_ql[i] < 201 ) min_ql[i] = 201;
                window.AppendNormal( "     Shiny cluster: " +
                                     Convert.ToString( min_ql[0] ) );
                window.AppendNormal( "    Bright cluster: " +
                                     Convert.ToString( min_ql[1] ) );
                window.AppendNormal( "     Faded cluster: " +
                                     Convert.ToString( min_ql[2] ) );
                window.AppendLineBreak(2);

                if ( ql > 250 )
                    window.AppendNormal( "The implant will also require you to be Title Level 6.  " );
                else if ( ql > 200 )
                    window.AppendNormal( "The implant will also require you to be Title Level 5.  " );
                else if ( ql > 100 )
                    window.AppendNormal( "If this implant has jobe clusters, it will require you to be Title Level 4.  " );
                else
                    window.AppendNormal( "If this implant has jobe clusters, it will require you to be Title Level 3.  " );

                if ( ql <= 200 )
                    window.AppendNormal( "You will need " + Convert.ToString( ql * 4.75 ) + " in Breaking & Entering, and " + Convert.ToString( ql ) + " in Nano Programming to clean a QL " + Convert.ToString( ql ) + " implant.  " );

                window.AppendNormal( "The above information is only accurate for Basic and Refined Implants.  " );
                if ( ( ql > 99 ) && ( ql < 301 ) )
                {
                    int jobe_treat = 0, jobe_abil = 0;
                    ComputeJobRequirements( ql, ref jobe_treat, ref jobe_abil );
                    window.AppendNormal( "You will need approximately " + Convert.ToString( jobe_abil ) + " ability and " + Convert.ToString( jobe_treat ) + " in treatment to install a Jobe implant at the specified QL." );
                    window.AppendLineBreak(2);
                }
                bot.SendReply(e, "For QL " + e.Args[0] + " implants.  More Info »» ", window);
            }
            else
                bot.SendReply(e, "Invalid QL: " + e.Args[0] );

            return;
        }

        private void ImpBuildCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length != 2)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            int ql = 0;
            try
            {
                ql = Convert.ToInt32( e.Args[0] );
            }
            catch
            {
                ImpUsageMessages( bot, e);
                return;
            }
            string search = string.Empty;
            search = e.Words[1];
            search = rw(search.ToLower());

            int found = -1;
            for ( int i=0; i<build_reqs.Clusters.Length; i++ )
            {
                string matcher = rw(build_reqs.Clusters[i].Name.ToLower());
                if ( matcher.IndexOf( search ) >= 0 )
                {
                    found = i;
                    break;
                }
            }

            if ( found >= 0 )
            {
                int[] min_ql={0, 0, 0};
                int sskill = 0, bskill = 0, fskill = 0;
                ComputeMinQLCluster( ql, min_ql );
                sskill = (int)Math.Round(build_reqs.Clusters[found].Shiny * (float)ql);
                bskill = (int)Math.Round(build_reqs.Clusters[found].Bright * (float)ql);
                fskill = (int)Math.Round(build_reqs.Clusters[found].Faded * (float)ql);

                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Implant Assembly Information");
                window.AppendLineBreak();

                window.AppendHighlight("In order to install a " +
                                       build_reqs.Clusters[found].Name +
                                       " cluster into a  QL " +
                                       e.Args[0] +
                                       " implant you will need the following skill: " );
                window.AppendLineBreak(2);
                window.AppendNormal( build_reqs.Clusters[found].Skill );
                window.AppendLineBreak(2);
                window.AppendNormal( "Shiny Cluster: " + Convert.ToString( sskill ) );
                window.AppendNormal( " (Min QL: " + Convert.ToString( min_ql[0] ) + ")" );
                window.AppendLineBreak(1);
                window.AppendNormal( "Bright Cluster: " + Convert.ToString( bskill ) );
                window.AppendNormal( " (Min QL: " + Convert.ToString( min_ql[1] ) + ")" );
                window.AppendLineBreak(1);
                window.AppendNormal( "Faded Cluster: " + Convert.ToString( fskill ) );
                window.AppendNormal( " (Min QL: " + Convert.ToString( min_ql[2] ) + ")" );
                window.AppendLineBreak(2);
                bot.SendReply(e, "Skills required to build a QL " +
                              e.Args[0] + " " +
                              build_reqs.Clusters[found].Name +
                              " implant.  More Info »» ", window);
            }
            else
            {
                bot.SendReply(e, "Invalid ql or skill not found" );
            }

        }

        private void ImpMaxCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length != 2)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            int ability = 0;
            int treatment = 0;

            try
            {
                ability = Convert.ToInt32( e.Args[0] );
                treatment = Convert.ToInt32( e.Args[1] );
            }
            catch
            {
                ImpUsageMessages( bot, e);
                return;
            }

            if ( ( ability > 0 ) && ( treatment > 0 ) )
            {
                int ql=0, jql=0;
                ComputeImplantQL(  treatment,  ability, ref ql, ref jql );

                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Max Implant QL Information");
                window.AppendLineBreak();

                window.AppendHighlight("For an ability of " + e.Args[0] + " and a treatment skill of " + e.Args[1] + ", you will be able to install the following QL implant:" );
                window.AppendLineBreak(2);
                window.AppendNormal( "QL " + Convert.ToString( ql ) + " implant with no Jobe clusters." );
                window.AppendLineBreak(1);
                window.AppendNormal( "QL " + Convert.ToString( jql ) + " implant with Jobe clusters. (Estimated!)" );

                window.AppendLineBreak(2);
                if ( ql > 250 )
                    window.AppendNormal( "The implant will also require you to be Title Level 6.  " );
                else if ( ql > 200 )
                    window.AppendNormal( "The implant will also require you to be Title Level 5.  " );
                else if ( ql > 100 )
                    window.AppendNormal( "If the implant contains Jobe clusters, then the implant will also require you to be Title Level 4.  " );
                else if ( ql == 100 )
                    window.AppendNormal( "If the implant contains Jobe clusters, then the implant will also require you to be Title Level 3.  " );
                window.AppendNormal( "The above information is only accurate for Basic and Refined Implants.  Values are estimated for Jobe clusters." );
                bot.SendReply(e, "Implant information for " + e.Args[0] + " ability and " + e.Args[1] + " treatment.  More Info »» ", window);
            }
            else
                bot.SendReply(e, "Invalid ability or treatment specified" );
        }

        private void PremadeCommand(BotShell bot, CommandArgs e)
        {

            RichTextWindow window = new RichTextWindow(bot);

            if (e.Args.Length < 1)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            string search = string.Empty;
            search = e.Words[0];
            search = rw(search.ToLower());

            string matcher;
            int i, n = 0;
            bool found;
            window.AppendTitle("Premade Implant Search");
            window.AppendLineBreak();

            window.AppendHighlight("The skill: \"" + e.Words[0] +
                                   "\" can be found in the following implants:" );
            window.AppendLineBreak(2);
            for( i=0; i<listing.Imps.Length; i++ )
            {
                found = false;
                matcher = rw(listing.Imps[i].Shiny.ToLower());
                if ( matcher.IndexOf( search ) >= 0 )
                    found = true;
                matcher = rw(listing.Imps[i].Bright.ToLower());
                if ( matcher.IndexOf( search ) >= 0 )
                    found = true;
                matcher = rw(listing.Imps[i].Faded.ToLower());
                if ( matcher.IndexOf( search ) >= 0 )
                    found = true;
                if ( found )
                {
                    n ++;
                    window.AppendHighlight(listing.Imps[i].Prof +
                                           " terminal: " +
                                           listing.Imps[i].Slot +
                                           " implant" );
                    window.AppendNormal( " ( " +
                                         listing.Imps[i].Shiny +
                                         ", " + listing.Imps[i].Bright +
                                         ", " + listing.Imps[i].Faded +
                                         " -- Requires: " +
                                         listing.Imps[i].Req + " )" );
                    window.AppendLineBreak();
                }
            }
            if ( n == 0 )
                window.AppendHighlight("(No Premade Implants found with that skill.)");
            window.AppendLineBreak();
            bot.SendReply(e, Convert.ToString( n ) + " Results. More Info »» ",
                          window);
            return;
        }

        private void ClusterCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length < 2)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            string[] slots = { "Chest", "Ear", "Eye", "Feet", "Head", "Left-Arm", "Left-Hand", "Left-Wrist", "Legs", "Right-Arm", "Right-Hand", "Right-Wrist", "Waist" };
            string[] types = { "Shiny", "Bright", "Faded" };

            string command = "200 ";

            if ( ( e.Args.Length == 3 ) &&
                 e.Args[2].StartsWith( "Job", StringComparison.CurrentCultureIgnoreCase) )
                command = command + "Jobe ";

            command = command + "Cluster ";

            // try to match type depending on what user submitted
            int found = -1;
            for ( int i=0; i<types.Length; i++ )
                if ( types[i].StartsWith( e.Args[1], StringComparison.CurrentCultureIgnoreCase) )
                {
                    found = i;
                    command = command + " " + types[i];
                    break;
                }

            if ( found < 0 )
            {
                ImpUsageMessages( bot, e);
                return;
            }

            // try to match location depending on what user submitted
            if ( ( e.Args[0] != "Legs" ) &&
                 e.Args[0].StartsWith( "L", StringComparison.CurrentCultureIgnoreCase ) &&
                 ! e.Args[0].StartsWith( "Left", StringComparison.CurrentCultureIgnoreCase ) )
            {
                char[] carr = e.Args[0].Substring(1).ToCharArray();
                if ( carr[0] != '-' )
                    e.Args[0] = "Left-" + e.Args[0].Substring(1);
                else
                    e.Args[0] = "Left" + e.Args[0].Substring(1);
            }
            else if ( e.Args[0].StartsWith( "R", StringComparison.CurrentCultureIgnoreCase ) &&
                      ! e.Args[0].StartsWith( "Right", StringComparison.CurrentCultureIgnoreCase ) )
            {
                char[] carr = e.Args[0].Substring(1).ToCharArray();
                if ( carr[0] != '-' )
                    e.Args[0] = "Right-" + e.Args[0].Substring(1);
                else
                    e.Args[0] = "Right" + e.Args[0].Substring(1);
            }
            else if ( e.Args[0].StartsWith( "Bod", StringComparison.CurrentCultureIgnoreCase) )
                e.Args[0] = "Chest";
            else if ( e.Args[0].StartsWith( "Foo", StringComparison.CurrentCultureIgnoreCase) )
                e.Args[0] = "Feet";

            found = -1;
            for ( int i=0; i<slots.Length; i++ )
            {
                if ( slots[i].StartsWith( e.Args[0], StringComparison.CurrentCultureIgnoreCase) || slots[i].Equals( e.Args[0], StringComparison.CurrentCultureIgnoreCase) )
                {
                    found = i;
                    command = command + " " + slots[i];
                    break;
                }                
            }

            if ( found < 0 )
            {
                ImpUsageMessages( bot, e);
                return;
            }

            try
            {
                // build a cluster search command
                CommandArgs args = new CommandArgs(e.Type, 0, e.SenderID, e.Sender, e.SenderWhois, "items", command, false, null);
                ((Items)bot.Plugins.GetPlugin("vhItems")).OnCommand(bot, args);
            }
            catch
            {
                bot.SendReply(e, "Error: vhItems Plugin must be loaded to use the \"cluster\" command." );
            }

        }

        private void ImpLocationCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length < 1)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            string command = "200 ";

            command = command + e.Words[0];

            command = command + " Cluster";

            try
            {
                // build a cluster search command
                CommandArgs args = new CommandArgs(e.Type, 0, e.SenderID, e.Sender, e.SenderWhois, "items", command, false, null);
                ((Items)bot.Plugins.GetPlugin("vhItems")).OnCommand(bot, args);
            }
            catch
            {
                bot.SendReply(e, "Error: vhItems Plugin must be loaded to use the \"imploc\" command." );
            }

        }

        private void AppendCheck(ref RichTextWindow window, string character, int ql, string loc, string skill, string type )
        {
            window.AppendNormal( "  [" );
            window.AppendCommand( "Search", "/tell " + character +
                                  " !items " + Convert.ToString( ql ) + " " +
                                  loc + " implant " + skill + " " + type );
            window.AppendNormal( "]" );
        }

        private void ImpCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length < 2)
            {
                ImpUsageMessages( bot, e);
                return;
            }

            int amount = 0;
            try
            {
                amount = Convert.ToInt32( e.Args[0] );
            }
            catch
            {
                ImpUsageMessages( bot, e);
                return;
            }
            string search = string.Empty;
            search = e.Words[1];
            search = rw(search.ToLower());

            // find cluster
            int found = -1;
            for ( int i=0; i<build_reqs.Clusters.Length; i++ )
            {
                string matcher = rw(build_reqs.Clusters[i].Name.ToLower());
                if ( matcher.IndexOf( search ) >= 0 )
                {
                    found = i;
                    break;
                }
            }

            if ( found >= 0 )
            {

                // determine if this is an ability, skill, or jobe modifier
                string[] Abilities = { "Agility", "Intelligence", "Psychic",
                                       "Sense", "Stamina", "Strength" };
                int type = 0; // 0=skill, 1=ability, 2=jobe
                if ( build_reqs.Clusters[found].Shiny == 6.25 )
                    type = 2;
                else
                    for( int i=0; i<Abilities.Length; i++ )
                        if ( build_reqs.Clusters[found].Name == Abilities[i] )
                        {
                            type = 1;
                            break;
                        }

                int[] qls = { 0, 0, 0, 0, 0, 0 };
                int solution = 0;
                switch (type)
                {

                case 0:
                    solution = ComputeSkillQL( amount, qls );
                    break;

                case 1:
                    solution = ComputeAbilityQL( amount, qls );
                    break;

                case 2:
                    bot.SendReply(e, "Sorry, I'm unable to compute values for Jobe cluster skills." );
                    return;

                }

                if( solution == 0 )
                {
                    bot.SendReply(e, "Sorry, it's not possible to increase " + build_reqs.Clusters[found].Name + " by that much using just implants." );
                    return;
                }

                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Max Implant QL Information");
                window.AppendLineBreak();

                window.AppendHighlight("In order to increase " + build_reqs.Clusters[found].Name + " by " + e.Args[0] + ", you can use the following implants:" );
                window.AppendLineBreak(2);

                if( qls[0] > 0 )
                {
                    window.AppendNormal( "QL " + Convert.ToString( qls[0] ) + " shiny cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].ShinyLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[0],
                                build_reqs.Clusters[found].ShinyLoc,
                                build_reqs.Clusters[found].Name, "Shiny" );
                    window.AppendLineBreak();
                    window.AppendNormal( "QL " + Convert.ToString( qls[0] ) + " bright cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].BrightLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[0],
                                build_reqs.Clusters[found].BrightLoc,
                                build_reqs.Clusters[found].Name, "Bright" );
                    window.AppendLineBreak();
                    window.AppendNormal( "QL " + Convert.ToString( qls[0] ) + " faded cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].FadedLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[0],
                                build_reqs.Clusters[found].FadedLoc,
                                build_reqs.Clusters[found].Name, "Faded" );
                    window.AppendLineBreak(2);
                }

                if( qls[1] > 0 )
                {
                    window.AppendNormal( "QL " + Convert.ToString( qls[1] ) + " shiny cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].ShinyLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[1],
                                build_reqs.Clusters[found].ShinyLoc,
                                build_reqs.Clusters[found].Name, "Shiny" );
                    window.AppendLineBreak();
                    window.AppendNormal( "QL " + Convert.ToString( qls[1] ) + " bright cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].BrightLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[1],
                                build_reqs.Clusters[found].BrightLoc,
                                build_reqs.Clusters[found].Name, "Bright" );
                    window.AppendLineBreak(2);
                }

                if( qls[2] > 0 )
                {
                    window.AppendNormal( "QL " + Convert.ToString( qls[2] ) + " shiny cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].ShinyLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[2],
                                build_reqs.Clusters[found].ShinyLoc,
                                build_reqs.Clusters[found].Name, "Shiny" );
                    window.AppendLineBreak();
                    window.AppendNormal( "QL " + Convert.ToString( qls[2] ) + " faded cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].FadedLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[2],
                                build_reqs.Clusters[found].FadedLoc,
                                build_reqs.Clusters[found].Name, "Faded" );
                    window.AppendLineBreak(2);
                }

                if( qls[3] > 0 )
                {
                    window.AppendNormal( "QL " + Convert.ToString( qls[3] ) + " bright cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].BrightLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[3],
                                build_reqs.Clusters[found].BrightLoc,
                                build_reqs.Clusters[found].Name, "Bright" );
                    window.AppendLineBreak();
                    window.AppendNormal( "QL " + Convert.ToString( qls[3] ) + " faded cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].FadedLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[3],
                                build_reqs.Clusters[found].FadedLoc,
                                build_reqs.Clusters[found].Name, "Faded" );
                    window.AppendLineBreak(2);

                    window.AppendNormal( "QL " + Convert.ToString( qls[3] ) + " shiny cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].ShinyLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[3],
                                build_reqs.Clusters[found].ShinyLoc,
                                build_reqs.Clusters[found].Name, "Shiny" );
                    window.AppendLineBreak(2);
                }

                if( qls[4] > 0 )
                {
                    window.AppendNormal( "QL " + Convert.ToString( qls[4] ) + " bright cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].BrightLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[4],
                                build_reqs.Clusters[found].BrightLoc,
                                build_reqs.Clusters[found].Name, "Bright" );
                    window.AppendLineBreak(2);
                }

                if( qls[5] > 0 )
                {
                    window.AppendNormal( "QL " + Convert.ToString( qls[5] ) + " faded cluster" );
                    window.AppendNormal( " in " + build_reqs.Clusters[found].FadedLoc + " implant." );
                    AppendCheck(ref window, bot.Character, qls[5],
                                build_reqs.Clusters[found].FadedLoc,
                                build_reqs.Clusters[found].Name, "Faded" );
                    window.AppendLineBreak(2);
                }

                window.AppendNormal( "These values are estimated/calculated.  You are encouraged to check that you obtain the correct buff by using the \"Search\" function." );
                bot.SendReply(e, "To buff " +
                              build_reqs.Clusters[found].Name + " by " +
                              e.Args[0] + ". More Info »» ", window);

            }
            else
            {
                bot.SendReply(e, "Error: " + e.Words[1] + " not found as an implantable skill.");
            }

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "impinfo":
                return "Displays information on requirements and benefits of an implant at the specified QL. (See also: imp, imploc, impmax, impbuild, cluster, and premade)\n";

            case "imploc":
                return "Displays the location of the clusters for the specified skill or ability. (See also: imp, impinfo, impmax, impbuild, cluster, and premade)\n";

            case "impmax":
                return "Displays the maximum QL implant you can use for a specified ability and treatment skill. (See also: imp, impinfo, imploc, impbuild, cluster, and premade)\n";

            case "impbuild":
                return "Displays information tradeskills needed to install a particular cluster into an implant. (See also: imp, impinfo, imploc, impmax, cluster, and premade)\n";

            case "premade":
                return "Displays a listing of premade implants with the desired skill. (See also: imp, impinfo, imploc, impmax, impbuild, and cluster)\n";

            case "cluster":
                return "Displays a listing of skills that can be installed the specified cluster location. (See also: imp, impinfo, imploc, impmax, impbuild, and premade)\n";

            case "imp":
                return "Displays the QL of the implant needed to buff the specified skill by the specified amount. (See also: impinfo, imploc, impmax, impbuild, cluster, and premade)\n";

            }
            return null;
        }
    }

    [XmlRoot("premadeimps")]
    public class PremadesImps
    {
        [XmlElement("implant")]
        public Imp[] Imps;
    }

    [XmlRoot("implant")]
    public class Imp
    {
        [XmlElement("slot")]
        public string Slot;
        [XmlElement("booth")]
        public string Prof;
        [XmlElement("abilityreq")]
        public string Req;
        [XmlElement("shiny")]
        public string Shiny;
        [XmlElement("bright")]
        public string Bright;
        [XmlElement("faded")]
        public string Faded;
    }

    [XmlRoot("clusters")]
    public class ClusterSkills
    {
        [XmlElement("cluster")]
        public Cluster[] Clusters;
    }

    [XmlRoot("cluster")]
    public class Cluster
    {
        [XmlElement("name")]
        public string Name;
        [XmlElement("skill")]
        public string Skill;
        [XmlElement("shiny")]
        public string _Shiny;
        [XmlElement("bright")]
        public string _Bright;
        [XmlElement("faded")]
        public string _Faded;
        [XmlElement("shinyloc")]
        public string ShinyLoc;
        [XmlElement("brightloc")]
        public string BrightLoc;
        [XmlElement("fadedloc")]
        public string FadedLoc;

        public float Shiny { get { try { return Convert.ToSingle(this._Shiny); } catch { return 0.0f; } } }
        public float Bright { get { try { return Convert.ToSingle(this._Bright); } catch { return 0.0f; } } }
        public float Faded { get { try { return Convert.ToSingle(this._Faded); } catch { return 0.0f; } } }

    }

}
