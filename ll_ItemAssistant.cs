using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class ItemAssistant : PluginBase
    {

        // leverage central item database to perform item searches
        private string Server = "cidb.botsharp.net";
        private string UrlTemplate = "http://{0}/?bot=VhaBot&search={1}&ql={2}&max={3}&output=xml";

        private static Mutex mutex = new Mutex();
        private Config _database;
        private static int NonPageSize = 3;

        private static string[] Location = { "Unknown", "Bank", "Inventory" };

        // Provide dummy validator for https:// URLs
        private static bool CertValidator ( object sender,
                                            X509Certificate certificate,
                                            X509Chain chain, 
                                            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        // Handle both downloading URLS and copying local files
        private static bool DownloadFile ( string URL, string file )
        {
            
            string thePattern = "^https*://";
            Match theMatch = Regex.Match( URL, thePattern );

            if ( ( file == string.Empty ) && ( URL.IndexOf('\\') >= 0 ))
                file = URL.Substring( URL.LastIndexOf( '\\' ) + 1 );
            else if ( ( file == string.Empty ) && ( URL.IndexOf('/') >= 0 ))
                file = URL.Substring( URL.LastIndexOf( '/' ) + 1 );
            else if ( file == string.Empty )
                file = URL;

            if ( theMatch.Success )
            {

                // Download remote URL
                ServicePointManager.ServerCertificateValidationCallback =
                    Updater.CertValidator;
                HttpWebRequest request  =
                    (HttpWebRequest)WebRequest.Create( URL );
                HttpWebResponse response =
                    (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();

                byte[] read_buffer = new byte[8192];
                int count = 0;
                int total = 0;
            
                ItemAssistant.mutex.WaitOne();

                byte[] file_buffer = new byte[response.ContentLength];

                total = 0;
                do
                {
                    count = resStream.Read( read_buffer, 0,
                                            read_buffer.Length );
                    if (count != 0)
                    {
                        Array.Copy( read_buffer, 0, file_buffer, total, count );
                        total += count;
                    }
                }
                while (count > 0);

                if ( ( total > 0 ) && ( total == response.ContentLength ) )
                {
                
                    using (BinaryWriter outfile =
                           new BinaryWriter(File.Open(file, FileMode.Create)))
                        outfile.Write(file_buffer);
                    ItemAssistant.mutex.ReleaseMutex();
                    return true;
                }

                ItemAssistant.mutex.ReleaseMutex();
                return false;
            }
            else if ( File.Exists( URL ) )
            {

                // Copy local file
                try
                {
                    File.Copy( URL, file, true );
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        private bool ImportData ( BotShell bot, string fn )
        {

            Config db = new Config( bot.ID, fn );

            // Import toons
            string query = "SELECT * FROM tToons;";
            using (IDbCommand command = db.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long charid = reader.GetInt64(0);
                        string charname = reader.GetString(1);
                        long shopid = reader.GetInt64(2);
                        // Add character if not already in local database
                        this._database.ExecuteNonQuery( string.Format( "REPLACE INTO tToons VALUES( {0}, '{1}', {2} );", charid, charname, shopid) );
                        // Drop all existing items for this character
                        this._database.ExecuteNonQuery( string.Format( "DELETE FROM tItems WHERE owner = {0};", charid ) );
                    }
                    catch
                    {
                        reader.Close();
                        return false;
                    }
                }
                reader.Close();
            }

            // Import items
            Dictionary<int, string> KnownContainers = null;
            if ( bot.Plugins.IsLoaded("llXMLItems") )
                KnownContainers = this.GetContainers();
            List<int> LookedUpContainers = new List<int>();
            query = "SELECT * FROM tItems;";
            using (IDbCommand command = db.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long itemidx = reader.GetInt64(0);
                        int keylow = reader.GetInt32(1);
                        int keyhigh = reader.GetInt32(2);
                        int ql = reader.GetInt32(3);
                        int flags = reader.GetInt32(4);
                        int stack = reader.GetInt32(5);
                        long parent =  reader.GetInt32(6);
                        int slot = reader.GetInt32(7);
                        int children = reader.GetInt32(8);
                        long owner = reader.GetInt32(9);
                        this._database.ExecuteNonQuery(
                            string.Format( "REPLACE INTO tItems VALUES( {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9} );",
                                           itemidx,
                                           keylow,
                                           keyhigh,
                                           ql,
                                           flags,
                                           stack,
                                           parent,
                                           slot,
                                           children,
                                           owner) );

                        // Update Containers if XMLItems plug-in is available
                        if ( bot.Plugins.IsLoaded("llXMLItems") &&
                             ( children > 3 ) &&
                             ( LookedUpContainers.IndexOf( keylow ) < 0 ) &&
                             ( ! KnownContainers.ContainsKey( keylow ) ) )
                        {
                            LookedUpContainers.Add( keylow );
                            Item i = XMLItems.GetItem( keylow );
                            this._database.ExecuteNonQuery(
                                string.Format( "REPLACE INTO ItemAssistantContainers VALUES( {0}, '{1}' );", keylow, i.Name ) );
                        }

                    }
                    catch
                    {
                        reader.Close();
                        return false;
                    }
                }
                reader.Close();
            }            

            return true;
        }

        public List<AOIAItem> SearchDatabase( string NameIn,
                                              int LowID, int HighID,
                                              int LowQL, int HighQL )
        {

            List<AOIAItem> Items = new List<AOIAItem>();

            string query = string.Format( "SELECT * FROM tItems WHERE keylow = {0} AND keyhigh = {1};", LowID, HighID );
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long itemidx = reader.GetInt64(0);
                        int ql = reader.GetInt32(3);
                        long parent =  reader.GetInt32(6);
                        long owner = reader.GetInt32(9);

                        AOIAItem found = new AOIAItem();

                        found.Name = NameIn;
                        found._lowid = Convert.ToString(HighID);
                        found._highid =  Convert.ToString(LowID);
                        found._lowql = Convert.ToString(LowQL);
                        found._highql = Convert.ToString(HighQL);
                        found.Owner = owner;
                        found.QL = ql;
                        found.Parent = parent;
                        found.Index = itemidx;

                        Items.Add( found );
                    }
                    catch
                    {
                        reader.Close();
                        return Items;
                    }
                }
                reader.Close();
            }            

            return Items;
        }

        public List<AOIAItem> SearchDatabase( long charid, int location )
        {

            List<AOIAItem> Items = new List<AOIAItem>();

            string query = string.Format( "SELECT * FROM tItems WHERE owner = {0} AND parent = {1};", charid, location );
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long itemidx = reader.GetInt64(0);
                        int keylow = reader.GetInt32(1);
                        int keyhigh = reader.GetInt32(2);
                        int ql = reader.GetInt32(3);
                        long parent =  reader.GetInt32(6);
                        long owner = reader.GetInt32(9);

                        AOIAItem found = new AOIAItem();

                        found._lowid = Convert.ToString(keyhigh);
                        found._highid =  Convert.ToString(keylow);
                        found.Owner = owner;
                        found.QL = ql;
                        found.Parent = parent;
                        found.Index = itemidx;

                        Items.Add( found );
                    }
                    catch
                    {
                        reader.Close();
                        return Items;
                    }
                }
                reader.Close();
            }            

            return Items;
        }

        public List<AOIAItem> GetContents( BotShell bot, long index )
        {

            List<AOIAItem> Items = new List<AOIAItem>();

            string query = string.Format( "SELECT * FROM tItems WHERE itemidx = {0};", index );
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long itemidx = reader.GetInt64(0);
                        int keylow = reader.GetInt32(1);
                        int keyhigh = reader.GetInt32(2);
                        int ql = reader.GetInt32(3);
                        // long parent =  reader.GetInt32(6);
                        int children = reader.GetInt32(8);
                        long owner = reader.GetInt32(9);

                        AOIAItem found = new AOIAItem();
                        
                        found._lowid = Convert.ToString(keyhigh);
                        found._highid =  Convert.ToString(keylow);
                        found.Owner = owner;
                        found.QL = ql;
                        found.Parent = (long)children;
                        found.Index = itemidx;

                        Items.Add( found );
                    }
                    catch
                    {
                        reader.Close();
                        return Items;
                    }
                }
                reader.Close();
            }

            query = string.Format( "SELECT * FROM tItems WHERE parent = {0};",
                                   Items[0].Parent );
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long itemidx = reader.GetInt64(0);
                        int keylow = reader.GetInt32(1);
                        int keyhigh = reader.GetInt32(2);
                        int ql = reader.GetInt32(3);
                        int children = reader.GetInt32(8);
                        long owner = reader.GetInt32(9);

                        AOIAItem found = new AOIAItem();
                        
                        found._lowid = Convert.ToString(keyhigh);
                        found._highid =  Convert.ToString(keylow);
                        found.Owner = owner;
                        found.QL = ql;
                        found.Parent = (long)children;
                        found.Index = itemidx ;

                        if ( bot.Plugins.IsLoaded("llXMLItems") )
                        {
                            Item i = XMLItems.GetItem( keylow );
                            found.Name = i.Name;
                        }
                        else
                            found.Name = "(Click to View)";

                        Items.Add( found );
                    }
                    catch
                    {
                        reader.Close();
                        return Items;
                    }
                }
                reader.Close();
            }

            return Items;
        }

        private Dictionary<long, string> GetCharacters()
        {

            Dictionary<long, string> Characters =
                new Dictionary<long, string>();

            string query = "SELECT * FROM tToons;";
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        long charid = reader.GetInt64(0);
                        string charname = reader.GetString(1);
                        Characters.Add( charid, charname );
                    }
                    catch
                    {
                        reader.Close();
                        return Characters;
                    }
                }
                reader.Close();
            }

            return Characters;
        }

        
        private Dictionary<int, string> GetContainers()
        {

            Dictionary<int, string> Containers = new Dictionary<int, string>();

            string query = "SELECT * FROM ItemAssistantContainers;";
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        Containers.Add( id, name );
                    }
                    catch { }
                }
                reader.Close();
            }

            return Containers;
        }


        public void ResolveContainers ( ref List<AOIAItem> Items )
        {

            for ( int i=0; i<Items.Count; i++ )
            {

                if ( Items[i].Parent < 3 )
                {
                    Items[i].Container = (int)Items[i].Parent;
                    Items[i].Location = (int)Items[i].Parent;
                }
                else
                {
                    string query = string.Format( "SELECT * FROM tItems WHERE children = {0};", Items[i].Parent );
                    using ( IDbCommand command =
                            this._database.Connection.CreateCommand())
                    {
                        command.CommandText = query;
                        IDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            try
                            {
                                int keylow = reader.GetInt32(1);
                                long parent =  reader.GetInt32(6);

                                Items[i].Container = (int)keylow;
                                Items[i].Location = (int)parent;
                            
                            }
                            catch { }
                        }
                        reader.Close();
                    }            

                }
            }

        }

        public ItemAssistant()
        {
            this.Name = "Item Assistant Integration";
            this.InternalName = "llItemAssistant";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;

            this.Commands = new Command[] {
                new Command("aoia", true, UserLevel.Member),
                new Command("aoia browse", true, UserLevel.Member),
                new Command("aoia add", true, UserLevel.Admin),
                new Command("aoia remove", true, UserLevel.Admin),
                new Command("aoia update", true, UserLevel.Admin),
                new Command("bank", "aoia")
            };
        }

        public override void OnLoad(BotShell bot)
        {

            this._database = new Config( bot.ID, "ItemAssistant" );

            this._database.ExecuteNonQuery( "CREATE TABLE IF NOT EXISTS ItemAssistantSources ( Source VARCHAR(256) );");

            this._database.ExecuteNonQuery( "CREATE TABLE IF NOT EXISTS ItemAssistantContainers ( itemid INTEGER NOT NULL PRIMARY KEY, Source VARCHAR(80) );");

            this._database.ExecuteNonQuery( "CREATE TABLE IF NOT EXISTS tItems (itemidx INTEGER NOT NULL PRIMARY KEY ON CONFLICT REPLACE AUTOINCREMENT UNIQUE DEFAULT '1', keylow INTEGER, keyhigh INTEGER, ql INTEGER, flags INTEGER DEFAULT '0', stack INTEGER DEFAULT '1', parent INTEGER NOT NULL DEFAULT '2', slot INTEGER, children INTEGER, owner INTEGER NOT NULL);" );

            this._database.ExecuteNonQuery( "CREATE TABLE IF NOT EXISTS tToons (charid INTEGER NOT NULL PRIMARY KEY UNIQUE, charname VARCHAR, shopid INTEGER DEFAULT '0');" );

        }

        public override void OnUnload(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "aoia":
                OnSearchCommand( bot, e );
                break;

            case "aoia browse":
                OnBrowseCommand( bot, e );
                break;

            case "aoia add":
                this.OnAddCommand( bot, e );
                break;

            case "aoia remove":
                this.OnRemoveCommand( bot, e );
                break;

            case "aoia update":
                this.OnUpdateCommand( bot, e );
                break;

            }
        }

        public void OnSearchCommand(BotShell bot, CommandArgs e )
        {

            if (e.Args.Length < 1)
            {
                this.OnBrowseCommand( bot, e );
                return;
            }

            string search = e.Words[0].ToLower();
            string url = string.Format(this.UrlTemplate, this.Server, HttpUtility.UrlEncode(search), 0, 100 );
            string xml = HTML.GetHtml(url, 60000);
            if (string.IsNullOrEmpty(xml))
            {
                bot.SendReply(e, "Unable to query the central items database.");
                return;
            }
            if (xml.ToLower().StartsWith("<error>"))
            {
                if (xml.Length > 13)
                {
                    bot.SendReply(e, "Error: " + xml.Substring(7, xml.Length - 13));
                    return;
                }
                else
                {
                    bot.SendReply(e, "An unknown error has occured!");
                    return;
                }
            }

            ItemsResults items = null;
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(ItemsResults));
                items = (ItemsResults)serializer.Deserialize(stream);
                stream.Close();
                if (items.Items == null || items.Items.Length == 0)
                {
                    bot.SendReply(e, "No items found that matches your query.");
                    return;
                }
            }
            catch
            {
                bot.SendReply(e, "Unable to parse the central items database.");
                return;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            RichTextWindow window = new RichTextWindow(bot);

            // for each potential item, see if it is in the local database
            List<AOIAItem> Items = new List<AOIAItem>();
            foreach (ItemsResults_Item item in items.Items)
            {
                List<AOIAItem> FoundItems =
                    this.SearchDatabase( item.Name, item.LowID,  item.HighID,
                                         item.LowQL, item.HighQL );
                if ( FoundItems.Count > 0 )
                    Items.AddRange( FoundItems );
            }

            Dictionary<long, string> Characters = this.GetCharacters();
            Dictionary<int, string> Containers = this.GetContainers();

            // Sort items first by charid then by name
            Items.Sort();

            // Resolve where items are located
            ResolveContainers ( ref Items );

            if ( Items.Count > ItemAssistant.NonPageSize )
            {
                window.AppendTitle("Item Assistant Listing");
                window.AppendHighlight("Search String: ");
                window.AppendNormal(search);
                window.AppendLineBreak();
                window.AppendHighlight("Results: ");
                window.AppendNormal( Items.Count.ToString() );
                window.AppendLineBreak(2);
                window.AppendHeader("Search Results:");
            }
            else
            {
                window.AppendLineBreak();
            }

            string cc = string.Empty;
            foreach (AOIAItem iaitem in Items)
            {

                if ( cc != Characters[iaitem.Owner] )
                {
                    cc = Characters[iaitem.Owner];
                    if ( Items.Count <= ItemAssistant.NonPageSize )
                        window.AppendString("    ");
                    window.AppendNormal( cc + ":" );
                    window.AppendLineBreak();
                }

                if ( Items.Count <= ItemAssistant.NonPageSize )
                    window.AppendString("    ");
                window.AppendString("  ");
                window.AppendHighlight(iaitem.Name + " ");
                window.AppendNormalStart();
                window.AppendString("[");
                window.AppendItem("QL " + iaitem.QL,
                                  iaitem.LowID, iaitem.HighID, iaitem.QL );
                window.AppendString("] ");

                if ( ( iaitem.Container > 3 ) && ( Containers.Count > 0 ) )
                {
                    try
                    {
                        window.AppendString( "in " +
                                             Containers[iaitem.Container] );
                    }
                    catch
                    {
                        window.AppendString( "in Unknown Container" );
                    }
                }
                window.AppendString( " in " +
                                     ItemAssistant.Location[iaitem.Location] );

                window.AppendLineBreak( );
            }

            if ( Items.Count > ItemAssistant.NonPageSize)
                bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, Items.Count.ToString()) + " Results »» ", window);
            else
                bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, Items.Count.ToString()) + " Results »»" + window.Text.TrimEnd('\n'));

        }

        public void OnBrowseCommand(BotShell bot, CommandArgs e )
        {

            RichTextWindow window = new RichTextWindow(bot);

            window.AppendTitle("Item Assistant Listing");
            window.AppendLineBreak();

            Dictionary<long, string> Characters = this.GetCharacters();
            List<AOIAItem> Items = null;
            long charid = 0;
            int location = 0;
            long index = 0;
            Dictionary<int, string> Containers = this.GetContainers();

            switch ( e.Args.Length )
            {

            case 0:
                window.AppendHighlight("Character(s): ");
                window.AppendLineBreak(2);

                foreach( KeyValuePair<long, string> Character in Characters )
                {
                    window.AppendBotCommand( Character.Value,
                                             "!aoia browse " +
                                             Character.Key.ToString() );
                    window.AppendLineBreak();
                }
                break;

            case 1:
                charid = Convert.ToInt64(e.Args[0]);
                window.AppendHighlight( Characters[charid] + ": ");
                window.AppendLineBreak(2);
                window.AppendBotCommand( "Bank",
                                         "!aoia browse " +
                                         e.Args[0] + " 1" );
                window.AppendLineBreak();
                window.AppendBotCommand( "Inventory",
                                         "!aoia browse " +
                                         e.Args[0] + " 2" );
                break;

            case 2:
                charid = Convert.ToInt64(e.Args[0]);
                location = Convert.ToInt32(e.Args[1]);
                window.AppendHighlight( Characters[charid] + ": " );
                window.AppendNormal( "(" + ItemAssistant.Location[location] +
                                     ")" );
                window.AppendLineBreak(2);

                Items = this.SearchDatabase( charid, location );
                foreach ( AOIAItem Item in Items )
                {

                    try
                    {
                        window.AppendBotCommand( Containers[Item.LowID],
                                                 "!aoia browse " +
                                                 e.Args[0] + " " +
                                                 e.Args[1] + " " +
                                                 Item.Index.ToString() );
                    }
                    catch
                    {
                        window.AppendBotCommand( "(Unidentified Item)",
                                                 "!aoia browse " +
                                                 e.Args[0] + " " +
                                                 e.Args[1] + " " +
                                                 Item.Index.ToString() );
                    }
                    window.AppendLineBreak();
                
                }
                break;

            case 3:
                charid = Convert.ToInt64(e.Args[0]);
                location = Convert.ToInt32(e.Args[1]);
                index = Convert.ToInt64(e.Args[2]);
                window.AppendHighlight( Characters[charid] + ": " );
                window.AppendNormal( "(" + ItemAssistant.Location[location] +
                                     ")" );
                window.AppendLineBreak();

                Items = this.GetContents( bot, index );
                try
                {
                    window.AppendItem( Containers[Items[0].LowID],
                                       Items[0].LowID, Items[0].HighID,
                                       Items[0].QL );
                }
                catch
                {
                    window.AppendItem( "(Unidentified Item)",
                                       Items[0].LowID, Items[0].HighID,
                                       Items[0].QL );
                }
                window.AppendLineBreak(2);

                for( int i=1; i<Items.Count; i++ )
                {
                    window.AppendItem( Items[i].Name, Items[i].LowID,
                                       Items[i].HighID, Items[i].QL );
                    window.AppendNormal( " [QL " + Items[i].QL.ToString() +
                                         "]" );
                    window.AppendLineBreak();                    
                }

                break;

            }

            bot.SendReply(e, "Item Assistant Listing »» ", window) ;

        }

        public void OnAddCommand(BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length == 0 )
            {
                bot.SendReply( e, "Usage: \tell " + bot.Character +
                               " aoia add [URL or local path]" );
                return;
            }

            // Make sure basename is a "db" file
            string URL = e.Words[0];
            string thePattern = "[\\x5C/][^\\x5C/]*\\.db$";
            Match theMatch = Regex.Match( URL, thePattern );
            if ( ! theMatch.Success )
            {
                bot.SendReply( e, "The path or URL you specified does not appear to be a \".db\" file." );
                return;
            }

            // Attempt to download and import into the local database
            string local = Config.ConfigPath + Path.DirectorySeparatorChar +
                bot.ID + Path.DirectorySeparatorChar + "TempItemAssistant.db3";
            bool URL_test = ItemAssistant.DownloadFile( URL, local );
            if ( ! URL_test )
            {
                bot.SendReply( e, "The path or URL you specified does not exist." );
                return;
            }

            bool Status = this.ImportData( bot, "TempItemAssistant" );
            File.Delete( local );
            if ( ! Status )
            {
                bot.SendReply( e, "The path or URL you specified does not exist." );
                return;
            }

            this._database.ExecuteNonQuery( string.Format( "INSERT INTO ItemAssistantSources ( Source ) VALUES ( '{0}' ) ;", URL ) );

            bot.SendReply( e, "Path added to local database." );

        }

        public void OnRemoveCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length == 0)
            {

                RichTextWindow window = new RichTextWindow(bot);                

                window.AppendTitle("Item Assistant Data Sources");
                window.AppendLineBreak();

                string query = "SELECT * FROM ItemAssistantSources;";
                using (IDbCommand command =
                       this._database.Connection.CreateCommand())
                {
                    command.CommandText = query;
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        try
                        {
                            string URL = reader.GetString(0);
                            window.AppendNormal("[");
                            window.AppendBotCommand( "X",
                                                     "!aoia remove " +
                                                     URL );
                            window.AppendNormal("] " + URL);
                            window.AppendLineBreak();
                        }
                        catch
                        {
                            bot.SendReply( e, "Error reading item database." );
                        }
                    }
                    reader.Close();
                }

                bot.SendReply( e, "Item Assistant Data Sources »» ", window );

                return;
            }

            if ( this._database.ExecuteNonQuery( string.Format( "DELETE FROM ItemAssistantSources WHERE Source = '{0}';", e.Words[0] ) ) < 0 )
                bot.SendReply( e, "An error occurred removing the Item Assistant data source." );
            else
                bot.SendReply( e, "Item Assistant data source removed." );

        }

        public void OnUpdateCommand(BotShell bot, CommandArgs e )
        {

            bot.SendReply( e, "Starting item database update..." );
            string local = Config.ConfigPath + Path.DirectorySeparatorChar +
                bot.ID + Path.DirectorySeparatorChar + "TempItemAssistant.db3";

            string query = "SELECT * FROM ItemAssistantSources;";
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string URL = reader.GetString(0);
                        bot.SendReply( e, "Updating from  " + URL + " ..." );
                        bool Download = ItemAssistant.DownloadFile( URL,
                                                                    local );
                        if ( ! Download )
                            bot.SendReply( e, "Item database update failed to obtain " + URL );
                        bool Import = this.ImportData( bot,
                                                       "TempItemAssistant" );
                        if ( ! Import )
                            bot.SendReply( e, "Item database update failed to import " + URL );
                        File.Delete( local );
                        
                    }
                    catch
                    {
                        bot.SendReply( e, "Item database update read failure." );
                    }
                }
                reader.Close();
            }

            bot.SendReply( e, "Item database updated." );


        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "aoia":
                return "Displays AO Item Assistant databases integrated with this bot.\n\n" +
                    "Usage: /tell " + bot.Character + " !aoia [search string]";

            case "aoia browse":
                return "Browses characters in Item Assistant database.\n\n" +
                    "Usage: /tell " + bot.Character + " !aoia browse";

            case "aoia add":
                return "Add a local file or URL to an ItemAssistant.db to integrate with this bot.\n\n" +
                    "Usage: /tell " + bot.Character + " !aoia add [path or URL]";

            case "aoia remove":
                return "Remove a local file or URL to an ItemAssistant.db from this bot.\n\n" +
                    "Usage: /tell " + bot.Character + " !aoia remove [path or URL]";

            case "aoia update":
                return "Imports the latest information from local or remote Item Assistant databases.  (Warning: This command can take a long time to complete.)\n\n" +
                    "Usage: /tell " + bot.Character + " !aoia update";
            }
            return null;
        }

    }

    // serializable items taken from vhItems
    [XmlRoot("items")]
    public class CIDBResults
    {
        [XmlElement("version")]
        public string Version;
        [XmlElement("results")]
        public string Results;
        [XmlElement("max")]
        public string Max;
        [XmlElement("item")]
        public AOIAItem[] Items;
        [XmlElement("credits")]
        public string Credits;
    }

    public class AOIAItem : IComparable<AOIAItem>
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("lowid")]
        public string _lowid;
        [XmlAttribute("highid")]
        public string _highid;
        [XmlAttribute("lowql")]
        public string _lowql;
        [XmlAttribute("highql")]
        public string _highql;
        [XmlAttribute("icon")]
        public string _iconid;

        // not in CIDB results, but useful fields from AOIA db
        [XmlAttribute("owner")]
        public long Owner;
        [XmlAttribute("ql")]
        public int QL;
        [XmlAttribute("parent")]
        public long Parent;
        [XmlAttribute("container")]
        public int Container;
        [XmlAttribute("location")]
        public int Location;
        [XmlAttribute("index")]
        public long Index;

        public int CompareTo( AOIAItem other )
        {
            if ( this.Owner < other.Owner ) return 1;
            else if ( this.Owner > other.Owner ) return -1;
            else if ( this.QL > other.QL ) return 1;
            else if ( this.QL < other.QL ) return -1;
            return 0;
        }

        public int LowID { get { try { return Convert.ToInt32(this._lowid); } catch { return 0; } } }
        public int HighID { get { try { return Convert.ToInt32(this._highid); } catch { return 0; } } }
        public int LowQL { get { try { return Convert.ToInt32(this._lowql); } catch { return 0; } } }
        public int HighQL { get { try { return Convert.ToInt32(this._highql); } catch { return 0; } } }
        public int IconID { get { try { return Convert.ToInt32(this._iconid); } catch { return 0; } } }
    }

}
