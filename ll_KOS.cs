using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class KOSList : PluginBase
    {
        private Config _database;
        private string[] _visibilities = { "public", "private" } ;
        public KOSList()
        {
            this.Name = "KOSList Manager";
            this.InternalName = "llKOSList";
            this.Author = "Llie / Vhab";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;
            this.Description = "Provides an ability to keep a \"kill on sight\" list.";
            this.Commands = new Command[] {
                new Command("kos", true, UserLevel.Member),
                new Command("kos add", true, UserLevel.Member),
                new Command("kos remove", true, UserLevel.Member),
                new Command("kos clear", true, UserLevel.Leader)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS kos (listname VARCHAR(14) NOT NULL, targetname VARCHAR(14), visibility INTEGER, PRIMARY KEY ( listname, targetname, visibility ) )");
        }
        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
                case "kos":
                    this.OnKOSListCommand(bot, e);
                    break;
                case "kos add":
                    this.OnKOSListAddCommand(bot, e);
                    break;
                case "kos remove":
                    this.OnKOSListRemoveCommand(bot, e);
                    break;
                case "kos clear":
                    this.OnKOSListClearCommand(bot, e);
                    break;
            }
        }

        private void OnKOSListCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);
            
            string listname = "Org";
            UserLevel senderlevel = bot.Users.GetUser(e.Sender);
            int visibility = 0;
            int varg = 0;

            if ( (e.Args.Length > 0) &&
                 ( e.Args[varg].ToLower() != "public" ) &&
                 ( e.Args[varg].ToLower() != "private" ) )
            {
                listname = Format.UppercaseFirst(bot.Users.GetMain(e.Args[varg]));
                varg ++;

                // only leaders can look at someone else's kos list
                if ( ( listname != "Org" ) && ( listname != e.Sender ) &&
                     ( senderlevel < UserLevel.Leader ) )
                {
                    bot.SendReply(e, "Only players with a rank leader or higher can look at someone else's KOS list");
                    return;
                }

            }

            if ( ( e.Args.Length > varg) &&
                 ( e.Args[varg].ToLower() != "public" ) )
                visibility = 1;

            // there is no "private" org list
            if ( listname == "Org" )
                visibility = 0;

            window.AppendTitle( Format.UppercaseFirst(listname) +
                                "'s KOS List");

            bool found = false;
            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                if ( visibility == 1 )
                    command.CommandText = "SELECT targetname FROM kos WHERE listname = '" + listname + "' ORDER BY targetname";
                else
                    command.CommandText = "SELECT targetname FROM kos WHERE listname = '" + listname + "' AND visibility = " + visibility + " ORDER BY targetname";
                    
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    found = true;
                    window.AppendHighlight(reader.GetString(0) + ": [");
                    window.AppendBotCommand("Remove", "kos remove " + reader.GetString(0));
                    window.AppendNormal("]");
                    window.AppendLineBreak();
                }
                reader.Close();
            }
            if (!found)
                bot.SendReply(e, "No targets on " + listname + "'s KOS list.");
            else
                bot.SendReply(e, "View " + listname + "'s KOS list »» ", window);
        }

        private void OnKOSListAddCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: kos add [org|listname(optional)] [targetname] [public|private(optional)]");
                return;
            }

            // rules for which list to add to:
            // by default add all names to personal private list
            int visibility = 0;
            string listname = bot.Users.GetMain(e.Sender);
            string targetname;
            int targetarg = 0;
            int vargs = e.Args.Length;
            UserLevel senderlevel = bot.Users.GetUser(e.Sender);

            if ( vargs > 1 )
            {

                // check if last arg is "public|private"
                if ( ( e.Args[vargs-1].ToLower() == "public" ) ||
                     ( e.Args[vargs-1].ToLower() == "private" ) )
                {
                    vargs --;
                    if ( e.Args[vargs].ToLower() == "private" )
                        visibility = 1;
                }

                // still more than 2 arguments left, so the first argument
                // is list name
                if ( vargs > 1 )
                {
                    targetarg = 1;
                    listname = Format.UppercaseFirst(bot.Users.GetMain(e.Args[0]));

                    // only leaders can add to org kos list
                    if ( ( listname == "Org" ) &&
                         ( senderlevel < UserLevel.Leader ) )
                    {
                        bot.SendReply(e, "Only players with a rank leader or higher can add names to the org's KOS list");
                        return;
                    }

                    // only leaders can add to someone else's kos list
                    if ( ( listname != e.Sender ) &&
                         ( senderlevel < UserLevel.Leader ) )
                    {
                        bot.SendReply(e, "Only players with a rank leader or higher can add names to someone else's KOS list");
                        return;
                    }

                    // validate the listname if its not self
                    // TODO -- really this should only be someone in org
                    if ( ( listname != e.Sender ) && ( listname != "Org" ) &&
                         ! bot.Users.UserExists(listname,
                                                bot.GetUserID(listname))  )
                    {
                        bot.SendReply(e, "No such KOS list");
                        return;
                    }

                }

            }

            // there is no "private" org list
            if ( listname == "Org" )
                visibility = 0;

            targetname = Format.UppercaseFirst(e.Args[targetarg]);
            if (bot.GetUserID(targetname) < 100)
            {
                bot.SendReply(e, "No such user: " +
                              HTML.CreateColorString(bot.ColorHeaderHex,
                                                     targetname));
                return;
            }

            // check if targetname is already in listname's KOS list
            bool found = false;
            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT visibility FROM kos WHERE listname = '" + listname + "' AND targetname = '" + targetname + "' ORDER BY targetname";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    // user already in this list
                    if ( reader.GetInt32(0) == visibility )
                        found = true;
                    else
                    {
                        // user exists in list but with a different visibility
                        // remove from list so we can add it again with
                        // different visibility
                        this._database.ExecuteNonQuery("DELETE FROM kos WHERE listname = '" + listname + "' AND targetname = '" + targetname + "'");
                    }
                }
                reader.Close();
            }
            if ( ! found )
            {
                this._database.ExecuteNonQuery(string.Format("INSERT INTO kos (listname, targetname, visibility) VALUES ('{0}', '{1}', {2})", listname, targetname, visibility));
                
                if ( listname != e.Sender )
                    bot.SendReply(e, "You have added " + HTML.CreateColorString(bot.ColorHeaderHex, targetname) + " to " + listname + "'s " + _visibilities[visibility] + " KOS list." );
                else
                    bot.SendReply(e, "You have added " + HTML.CreateColorString(bot.ColorHeaderHex, targetname) + " to your " + _visibilities[visibility] + " KOS list." );
            }
            else
            {
                bot.SendReply(e,  HTML.CreateColorString(bot.ColorHeaderHex, targetname) + " is already in " + listname + "'s " + _visibilities[visibility] + " KOS list." );
            }
        }

        private void OnKOSListRemoveCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: kos remove [listname(optional)] [username]");
                return;
            }
            
            string listname = bot.Users.GetMain(e.Sender);
            UserLevel senderlevel = bot.Users.GetUser(e.Sender);
            int varg = 0;

            if ( e.Args.Length > 1 )
            {
                listname = Format.UppercaseFirst(bot.Users.GetMain(e.Args[varg]));
                varg ++;

                // only leaders can remove people from someone else's kos list
                if ( ( listname != "Org" ) && ( listname != e.Sender ) &&
                     ( senderlevel < UserLevel.Leader ) )
                {
                    bot.SendReply(e, "Only players with a rank leader or higher can look at someone else's KOS list");
                    return;
                }

            }

            string targetname = Format.UppercaseFirst(e.Args[varg]);
            if (bot.GetUserID(targetname) < 100)
            {
                bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, targetname));
                return;
            }

            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM kos WHERE listname ='" + listname + "' AND targetname = '" + targetname + "'";
                IDataReader reader = command.ExecuteReader();
                if (!reader.Read())
                {
                    reader.Close();
                    bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, targetname) + " not in " + listname +  "'s KOS list");
                    return;
                }
                reader.Close();
            }
            this._database.ExecuteNonQuery("DELETE FROM kos WHERE listname = '" + listname + "' AND targetname = '" + targetname + "'");
            bot.SendReply(e, "You removed " + HTML.CreateColorString(bot.ColorHeaderHex, targetname ) + " from " + listname +  "'s KOS List.");
        }

        private void OnKOSListClearCommand(BotShell bot, CommandArgs e)
        {
            
            string listname = bot.Users.GetMain(e.Sender);
            UserLevel senderlevel = bot.Users.GetUser(e.Sender);

            if ( e.Args.Length > 0 )
            {
                listname = Format.UppercaseFirst( bot.Users.GetMain(e.Args[0]) );

                // only leaders can remove people from someone else's kos list
                if ( ( listname != "Org" ) && ( listname != e.Sender ) &&
                     ( senderlevel < UserLevel.Leader ) )
                {
                    bot.SendReply(e, "Only players with a rank leader or higher can clear someone else's KOS list");
                    return;
                }

            }

            this._database.ExecuteNonQuery("DELETE FROM kos where listname = '" + listname + "'" );
             bot.SendReply(e, "All targets have been cleared from " + listname +  "'s KOS List." );
            // bot.SendPrivateChannelMessage(bot.ColorHighlight + "All kos have been cleared");
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "kos":
                return "Displays the kill on sight list.\n" +
                    "Usage: /tell " + bot.Character + " kos [listname(optional)] [visibility(optional)]";

            case "kos add":
                return "Allows you to add [username] to a kos list.\n" +
                    "Usage: /tell " + bot.Character + " kos add [listname(optional)] [username] [visibility(optional)]";

            case "kos remove":
                return "Allows you to remove [username] from a kos list.\n" +
                    "Usage: /tell " + bot.Character + " kos remove [listname(optional)] [username]";

            case "kos clear":
                return "Clears all assigned kos.\n" +
                    "Usage: /tell " + bot.Character + " kos clear [listname(optional)]";

            }
            return null;
        }
    }
}
