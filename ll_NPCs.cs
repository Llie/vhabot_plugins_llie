using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class NPCs : PluginBase
    {
        // private string Server = "nano.byethost12.com/services";
        // private string Server = "nano.exofire.net/services";
        private string Server = string.Empty;
        private string filename = "data/npcs.xml";
        private string UrlTemplate = "http://{0}/npc.php?{1}={2}";
        private int Max = 100;

        public NPCs()
        {
            this.Name = "NPC Database Lookup";
            this.InternalName = "llNPCs";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("npc", true, UserLevel.Guest),
                // only uncomment the following line if running a bot
                // without XML services such as a Guidebot
                // new Command("whois","npc"),
                new Command("questnpcs", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "npcs.xml" );
        }
        public override void OnUnload(BotShell bot) { }

        private void DisplayResults ( BotShell bot, CommandArgs e,
                                      NPCResults search_results,
                                      string search, Int32 dr  )
        {
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle(" Database");
            if ( ! File.Exists ( filename ) )
            {
                window.AppendHighlight("Server: ");
                window.AppendNormal(this.Server);
                window.AppendLineBreak();
            }
            if ( search_results.Version != null )
            {
                window.AppendHighlight("Version: ");
                window.AppendNormal(search_results.Version);
            }
            window.AppendLineBreak();
            window.AppendHighlight("Search String: ");
            window.AppendNormal(search);
            window.AppendLineBreak(2);
            window.AppendHighlight(search_results.NPCs[dr].name);
            window.AppendNormalStart();
            window.AppendNormal(" is located at ");

            int pfid = 0;
            Int32.TryParse ( search_results.NPCs[dr].pfid, out pfid );
            if ( pfid > 0 )
            {
                string coordstr = search_results.NPCs[dr].coords;
                coordstr = coordstr.Replace ( "x", " " );
                window.AppendCommand( search_results.NPCs[dr].location +
                                      " (" + search_results.NPCs[dr].coords + ")",
                                      "/waypoint " +
                                      coordstr + " " +
                                      search_results.NPCs[dr].pfid );
            }
            else
            { 
                window.AppendString( search_results.NPCs[dr].location +
                                     " (" + search_results.NPCs[dr].coords + ")" );
            }
            if ( search_results.NPCs[dr].questline != "na" )
            {
                window.AppendLineBreak();
                window.AppendHighlight("Quest line: ");
                window.AppendNormal( search_results.NPCs[dr].questline );
            }
            window.AppendLineBreak();
            window.AppendHighlight("Additional info: ");
            window.AppendLineBreak();
            window.AppendNormal( search_results.NPCs[dr].info );
            bot.SendReply(e, search_results.NPCs[dr].name + " is located at " +
                          search_results.NPCs[dr].location + " (" + search_results.NPCs[dr].coords +
                          ") More info »» ",
                          window);
            
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            string search_command = "";

            switch (e.Command)
            {
                case "npc":
                    search_command = "search";
                    break;
                case "questnpcs":
                    search_command = "questnpcs";
                    break;
            }

            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [search string]");
                return;
            }
            string exact_search = e.Words[0];
            string search = exact_search.ToLower();

            string xml;
            string result = string.Empty;
            MemoryStream stream = null;
            NPCResults search_results = null;

            if ( ! File.Exists ( filename ) )
            {

                // no local file -- search over the internet

                string url = string.Format(this.UrlTemplate, this.Server,
                                           search_command,
                                           HttpUtility.UrlEncode(search));
                xml = HTML.GetHtml(url, 60000);
                if (string.IsNullOrEmpty(xml))
                {
                    bot.SendReply(e, "Unable to query the npc database");
                    return;
                }

                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(NPCResults));
                    search_results = (NPCResults)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to query the npc database";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

            }
            else
            {

                // search local file
                using(StreamReader rdr = File.OpenText(filename))
                    xml = rdr.ReadToEnd();
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to read file: " + filename );
                    return;
                }

                NPCResults all_npcs = null;
                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(NPCResults));
                    all_npcs = (NPCResults)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to parse the npc file";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                search_results = all_npcs;
                search_results.Version = all_npcs.Version;
                search_results.Max = Convert.ToString ( Max );
                search_results.Credits = all_npcs.Credits;

                string nlower;
                Int32 i = 0;
                foreach ( NPC npc in all_npcs.NPCs)
                {
                    if ( search_command == "search" )
                        nlower = npc.name.ToLower();
                    else
                        nlower = npc.questline.ToLower();
                    if ( nlower.IndexOf( search ) >= 0 )
                        search_results.NPCs[i++] = npc;
                }
                Array.Resize( ref search_results.NPCs, i );
                search_results.Results = Convert.ToString( search_results.NPCs.Length );

            }

            Int32 dr = 0;
            if ( search_results.NPCs.Length > 1 )
                for ( int i=0; i<search_results.NPCs.Length; i++)
                    if ( search_results.NPCs[i].name == exact_search )
                        dr = i;

            if (search_results.NPCs == null ||
                search_results.NPCs.Length == 0)
            {
                result = "No matching NPCs were found.";

                List<string> other_commands = new List<string>();
                if ( bot.Plugins.IsLoaded("llAOUGuides") )
                    other_commands.Add("!locate");
                if ( bot.Plugins.IsLoaded("llWhereis") )
                    other_commands.Add("!whereis");
                if ( bot.Plugins.IsLoaded("llUniqueMobs") )
                    other_commands.Add("!boss");
                if ( other_commands.Count > 0 )
                {
                    result = result + "  Perhaps the location you are looking for might be found in one of the other location databases this bot has access to.  You could try one of these other commands: ";
                    for ( int i=0; i<other_commands.Count; i++ )
                    {
                        result = result + other_commands[i];
                        if ( i < other_commands.Count - 1 )
                            result = result + ", ";
                    }
                }
            }
            else if ( ( search_results.NPCs.Length == 1 ) ||
                      ( dr > 0 ) )
            {
                DisplayResults( bot, e, search_results, search, dr );
                return;
            }
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle(" Database");
                if ( ! File.Exists ( filename ) )
                {
                    window.AppendHighlight("Server: ");
                    window.AppendNormal(this.Server);
                    window.AppendLineBreak();
                }
                if ( search_results.Version != null )
                {
                    window.AppendHighlight("Version: ");
                    window.AppendNormal(search_results.Version);
                }
                window.AppendLineBreak();
                window.AppendHighlight("Search String: ");
                window.AppendNormal(search);
                window.AppendLineBreak();
                Int32 i;
                result = "Do you mean one of these?: ";
                for ( i=0; i<search_results.NPCs.Length; i++)
                {
                    result = result + search_results.NPCs[i].name;
                    if ( i < search_results.NPCs.Length - 2 )
                        result = result + ", ";
                    else if ( i < search_results.NPCs.Length - 1 )
                        result = result + ", or ";
                    else
                        result = result + ".";
                    window.AppendNormal( "> " );
                    window.AppendCommand( search_results.NPCs[i].name,
                                          "/tell " +  bot.Character +
                                          " npc " +
                                          search_results.NPCs[i].name);
                    window.AppendLineBreak();
                }
                bot.SendReply(e, result + "More Info »» ", window);
                return;
            }
            bot.SendReply(e, result);
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
            case "npc":
                return "Allows you to search the central npc database by name.\n" +
                    "To search the npc database by NPC name use: /tell " + bot.Character + " npc [partial name]\n" +
                    "You don't need to specify the full name of the npc, you can use multiple words in order to find an item.\n" +
                    "However, the different words are required to be in the right order to find a certain npc.\n" +
                    "For example: 'herc linc' will work but 'linc herc' won't if you want to find Dr. Hercules Lincoln";
            case "questnpcs":
                    return "Allows you to search the central npc database by quest line.\n" +
                        "To search the npc database by quest lines use: /tell " + bot.Character + " quest [string]" +
                        "You don't need to specify the full name of the quest, you can use multiple words in order to find an item.\n" +
                        "However, the different words are required to be in the right order to find a certain NPC that is involved in a quest.\n" +
                        "For example: 'token gun' will work but 'gun token' won't if you want to find NPC's associated with the Token Guns quest";
            }
            return null;
        }
    }

    [XmlRoot("npcs")]
    public class NPCResults
    {
        [XmlElement("version")]
        public string Version;
        [XmlElement("results")]
        public string Results;
        [XmlElement("max")]
        public string Max;
        [XmlElement("npc")]
        public NPC[] NPCs;
        [XmlElement("credits")]
        public string Credits;
    }

    [XmlRoot("npc")]
    public class NPC
    {
        [XmlElement("name")]
        public string name;
        [XmlElement("location")]
        public string location;
        [XmlElement("pfid")]
        public string pfid;
        [XmlElement("coords")]
        public string coords;
        [XmlElement("questline")]
        public string questline;
        [XmlElement("info")]
        public string info;
    }
}
