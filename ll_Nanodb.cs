using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class NanoDB : PluginBase
    {
        // private string Server = "nano.exofire.com/services";
        // private string Server = "nano.byethost12.com/services";
        private string Server = string.Empty;
        private string LocalFile = "data/ao-nanodb.xml";
        private string UrlTemplate = "http://{0}/nanodb.php?max={1}{2}";
        private int Max = 100;
        private int NonPageSize = 3;

        public NanoDB()
        {
            this.Name = "AO NanoDB";
            this.InternalName = "llNanoDB";
            this.Author = "Llie / Faldon";
            this.DefaultState = PluginState.Installed;
            this.Version = 105;
            this.Commands = new Command[] {
                new Command("nano", true, UserLevel.Guest),
                new Command("nanoinfo", true, UserLevel.Guest),
                new Command("nanoline", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "ao-nanodb.xml" );
            Updater.Update( "nanolines.xml" );
        }

        public override void OnUnload(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
            case "nano":
                OnNanoCommand(bot, e);
                break;

            case "nanoinfo":
                OnNanoinfoCommand(bot, e);
                break;

            case "nanoline":
                OnNanolineCommand(bot, e);
                break;
            }
        }

        public NanoDB_Results NanoSearch( BotShell bot, CommandArgs e, 
                                          string search, string avail,
                                          string prof, int minql, int maxql,
                                          int itemid )
        {

            NanoDB_Results items = null;
            MemoryStream stream = null;

            if ( File.Exists (  LocalFile ) )
            {
                // read / deserialze from local file
                string xml = string.Empty;
                using(StreamReader rdr = File.OpenText(LocalFile))
                {
                    xml = rdr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(xml))
                {
                    bot.SendReply(e, "Unable to open local nano database");
                    return null;
                }

                NanoDB_Results all_items = null;
                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(NanoDB_Results));
                    all_items = (NanoDB_Results)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch { }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                items = all_items;
                // filter results
                Regex pattern = null;
                if ( search != string.Empty )
                    pattern = new Regex( ".*" + search.Replace( " ", ".*" ) + ".*", RegexOptions.IgnoreCase );
                List<NanoDB_Nano> list = new List<NanoDB_Nano>();
                foreach (NanoDB_Nano item in all_items.Nanos)
                {
                    int reject = 0;

                    if ( ( search != string.Empty ) && ! pattern.IsMatch( item.Name ) )
                        reject += 1;
                    if ( ( prof != string.Empty ) && ( item.prof.IndexOf( prof ) < 0 ) )
                        reject += 2;
                    if ( ( avail != string.Empty ) && ( item.available.IndexOf( avail ) < 0 ) )
                        reject += 4;
                    if ( ( minql != 0 ) && ( item.LowQL < minql ) )
                        reject += 8;
                    if ( ( maxql != 0 ) && ( item.LowQL > maxql ) )
                        reject += 16;
                    if ( ( itemid != 0 ) && ( item.LowID != itemid ) )
                        reject += 32;

                    if ( ( reject == 0 ) && ( list.Count < Max ) )
                        list.Add( item );
                }

                items.Results = Convert.ToString(list.Count);
                items.Nanos = list.ToArray();
                items.Version = "Unknown";
                items.Max =  Convert.ToString(Max);
                        
            }
            else
            {
                string urlargs = string.Empty;

                if ( search != string.Empty )
                    urlargs += "&search=" + HttpUtility.UrlEncode(search);
                if ( prof != string.Empty )
                    urlargs += "&prof=" + HttpUtility.UrlEncode(prof);
                if ( avail != string.Empty )
                    urlargs += "&avail=" + HttpUtility.UrlEncode(avail);
                if ( minql != 0 )
                    urlargs += "&minql=" + HttpUtility.UrlEncode(Convert.ToString(minql));
                if ( maxql != 0 )
                    urlargs += "&maxql=" + HttpUtility.UrlEncode(Convert.ToString(maxql));

                string url = string.Format(this.UrlTemplate, this.Server, Max, urlargs );
                string xml = HTML.GetHtml(url, 60000);
                if (string.IsNullOrEmpty(xml))
                {
                    bot.SendReply(e, "Unable to query the central nano database");
                    return null;
                }
                if (xml.ToLower().StartsWith("<error>"))
                {
                    if (xml.Length > 13)
                    {
                        bot.SendReply(e, "Error: " + xml.Substring(7, xml.Length - 13));
                        return null;
                    }
                    else
                    {
                        bot.SendReply(e, "An unknown error has occured!");
                        return null;
                    }
                }

                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(NanoDB_Results));
                    items = (NanoDB_Results)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch { }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                if ( itemid != 0 )
                {
                    List<NanoDB_Nano> list = new List<NanoDB_Nano>();
                    foreach (NanoDB_Nano item in items.Nanos)
                        if ( itemid == item.LowID )
                            list.Add( item );

                    items.Results = "1";
                    items.Nanos = list.ToArray();
                    items.Version = "Unknown";
                    items.Max = "1";
                        
                }
            }

            return items;
        }

        public void OnNanoCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: nano [search parameters]");
                return;
            }

            string search = string.Empty;
            string avail = string.Empty;
            string prof = string.Empty;
            int minql = 0;
            int maxql = 0;
            int arg = 0;

            // attempt to read minql
            try
            {
                int ql = Convert.ToInt32(e.Args[arg]);
                if (ql <= 0 || ql >= 1000)
                {
                    bot.SendReply(e, "Quality level has to be between 0 and 999");
                    return;
                }
                minql = ql;
                arg ++;
            }
            catch { }

            // attempt to read maxql
            try
            {
                int ql = Convert.ToInt32(e.Args[arg]);
                if (ql <= 0 || ql >= 1000)
                {
                    bot.SendReply(e, "Quality level has to be between 0 and 999");
                    return;
                }
                maxql = ql;
                arg ++;
            }
            catch { }

            // attempt to read prof
            try
            {
                prof = PluginShared.ParseProfession( e.Args[arg].ToLower() );
                if ( prof != string.Empty )
                    arg ++;
            }
            catch {}

            // attempt to read availablity
            try
            {
                switch ( e.Args[arg].ToLower() )
                {
                case "buyable":
                case "store":
                case "buy":
                    avail = "Buyable";
                break;
                case "dyna":
                case "chest":
                    avail = "Dyna";
                break;
                case "quest":
                    avail = "Quest";
                    break;
                case "rollable":
                case "roll":
                    avail = "Rollable";
                break;
                case "tradeskill":
                case "ts":
                    avail = "Tradeskill";
                break;
                }
                if ( avail != string.Empty )
                    arg ++;
            }
            catch {}

            // nothing provided on the command line
            if ( e.Words.Length > arg )
            {
                search = e.Words[arg];
                search = search.ToLower();
            }

            NanoDB_Results items = NanoSearch( bot, e, search, avail, prof,
                                               minql, maxql, 0 );

            string result = string.Empty;

            try
            {
                if ( (items == null) || (items.Nanos == null) ||
                     (items.Nanos.Length == 0) )
                    result = "No nanos were found";
                else
                {
                    RichTextWindow window = new RichTextWindow(bot);
                    if (items.Nanos.Length > this.NonPageSize)
                    {
                        window.AppendTitle("Central Nano Database");
                        if ( this.Server != string.Empty )
                        {
                            window.AppendHighlight("Server: ");
                            window.AppendNormal(this.Server);
                            window.AppendLineBreak();
                        }
                        window.AppendHighlight("Version: ");
                        window.AppendNormal(items.Version);
                        window.AppendLineBreak();
                        window.AppendHighlight("Search Parameters:");
                        if ( avail != string.Empty )
                        {
                            window.AppendNormal( " " );
                            window.AppendNormal(avail);
                        }
                        if ( prof != string.Empty )
                        {
                            window.AppendNormal( " " );
                            window.AppendNormal(prof);
                        }
                        window.AppendNormal( " Nanos" );
                        if ( minql != 0 )
                        {
                            window.AppendNormal( " at least QL " );
                            window.AppendNormal(Convert.ToString(minql));
                        }
                        if ( maxql != 0 )
                        {
                            window.AppendNormal( " at most QL " );
                            window.AppendNormal(Convert.ToString(maxql));
                        }
                        if ( search != string.Empty )
                        {
                            window.AppendNormal( " containing the words: \"" );
                            window.AppendNormal(search);
                            window.AppendNormal( "\"" );
                        }
                        window.AppendLineBreak();
                        window.AppendHighlight("Results: ");
                        window.AppendNormal(items.Nanos.Length.ToString() + " / " + items.Max);
                        window.AppendLineBreak(2);
                        window.AppendHeader("Search Results");
                    }
                    else
                    {
                        window.AppendLineBreak();
                    }
                    foreach (NanoDB_Nano item in items.Nanos)
                    {
                        if (items.Nanos.Length <= this.NonPageSize)
                            window.AppendString("    ");

                        if (item.IconID > 0 && items.Nanos.Length > this.NonPageSize)
                            window.AppendIcon(item.IconID);

                        window.AppendString("[");
                        window.AppendItem("QL " + item.LowQL, item.LowID, item.LowID, item.LowQL);
                        window.AppendString("] ");

                        window.AppendHighlight(item.Name + " ");
                        window.AppendNormalStart();
                        window.AppendColorEnd();
                        window.AppendString( "- " + item.available );
                        if ( item.comment.Length > 0 )
                        {
                            window.AppendHighlight( " [" );
                            window.AppendBotCommand( "info", "!nanoinfo " + Convert.ToString(item.LowID) );
                            window.AppendHighlight( "]" );
                        }
                        window.AppendLineBreak();
                    }
                    if (items.Nanos.Length > this.NonPageSize)
                        bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, items.Nanos.Length.ToString()) + " Results »» ", window);
                    else
                        bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, items.Nanos.Length.ToString()) + " Results »»" + window.Text.TrimEnd('\n'));
                    return;
                }
            }
            catch
            {
                result = "Unable to query the AO Nano Database";
            }
            bot.SendReply(e, result);
        }

        public void OnNanoinfoCommand(BotShell bot, CommandArgs e)
        {

            NanoDB_Results items = null;
            Int32 item_id = 0;
            if ( (e.Args.Length == 1) &&
                 Int32.TryParse( e.Args[0], out item_id ) )
                items = NanoSearch( bot, e, string.Empty, string.Empty,
                                    string.Empty, 0, 0, item_id );
            else
                items = NanoSearch( bot, e, e.Words[0].ToLower(),
                                    string.Empty, string.Empty, 0, 0, 0 );

            if ( (items == null) || (items.Nanos == null) ||
                 (items.Nanos.Length == 0) )
            {
                bot.SendReply(e, "No Results found.");
                bot.SendReply(e, "Correct Usage: nanoinfo [item id|search parameters]");
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            if (items.Nanos.Length > this.NonPageSize)
            {
                window.AppendTitle("Central Nano Database");
                if ( this.Server != string.Empty )
                {
                    window.AppendHighlight("Server: ");
                    window.AppendNormal(this.Server);
                    window.AppendLineBreak();
                }
                window.AppendHighlight("Version: ");
                window.AppendNormal(items.Version);
                window.AppendLineBreak();

                window.AppendHighlight("Results: ");
                window.AppendNormal(items.Nanos.Length.ToString() + " / " + items.Max);
                window.AppendLineBreak();

                window.AppendHighlight("Additional Information:");
                window.AppendLineBreak(2);
                window.AppendHeader("Results:");
            }
            else
            {
                window.AppendLineBreak();
            }
            foreach (NanoDB_Nano item in items.Nanos)
            {
                if (items.Nanos.Length <= this.NonPageSize)
                    window.AppendString("    ");

                window.AppendString("[");
                window.AppendItem("QL " + item.LowQL, item.LowID, item.LowID, item.LowQL);
                window.AppendString("] ");

                window.AppendHighlight(item.Name + ": ");
                window.AppendNormalStart();
                window.AppendColorEnd();
                if ( item.comment.Length > 0 )
                {
                    window.AppendString( item.comment );
                }
                else
                    window.AppendString( "(No additional info)" );
                window.AppendLineBreak();
            }
            if (items.Nanos.Length > this.NonPageSize)
                bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, items.Nanos.Length.ToString()) + " Results »» ", window);
            else
                bot.SendReply(e, HTML.CreateColorString(bot.ColorHeaderHex, items.Nanos.Length.ToString()) + " Results »»" + window.Text.TrimEnd('\n'));
            return;
            
        }

        public static string ZeroPad( int val, int n )
        {
            string padded = string.Empty;
            int ndigits = 0;
            if ( val > 0 )
                ndigits = (int)Math.Floor(Math.Log10( val )) + 1;
            for ( int i=0; i<n-ndigits; i++ )
                padded += "0";
            padded = HTML.CreateColorString( "#000000", padded );
            if ( val > 0 )
                padded += val.ToString();
            return padded;
        }

        public void OnNanolineCommand(BotShell bot, CommandArgs e)
        {

            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: nanoline [profession] [nanoline]");
                return;
            }

            string prof = PluginShared.ParseProfession( e.Args[0].ToLower() );

            if ( prof == string.Empty )
            {
                bot.SendReply(e, "Unable to parse profession.");
                return;
            }

            string line = string.Empty;
            if ( e.Args.Length > 1 )
                line = e.Words[1];

            NanoLines db = (NanoLines)PluginShared.ParseXml( typeof(NanoLines),
                                                             "nanolines.xml" );

            if ( db == null )
            {
                bot.SendReply(e, "Error parsing nano line database.");
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("AoItems Nanoline Look-up");
            window.AppendLineBreak( );

            int nlines = 0;

            foreach ( NanoLinesProfs dbProf in db.Professions )
                if ( prof == dbProf.Name )
                {
                    window.AppendHighlight( dbProf.Name );
                    window.AppendLineBreak( 2 );

                    foreach ( NanoLinesLines dbLine in dbProf.Lines )
                    {

                        if ( string.IsNullOrEmpty(line) )
                        {
                            window.AppendNormal( "[" );
                            window.AppendBotCommand( "View",
                                                     "!nanoline " + prof +
                                                     " " + dbLine.Name );
                            window.AppendNormal( "] " );
                            window.AppendHighlight( dbLine.Name );
                            window.AppendLineBreak( );
                            continue;
                        }

                        if ( ! string.IsNullOrEmpty(line) &&
                             ( dbLine.Name.ToLower().IndexOf(line.ToLower()) < 0 ) )
                            continue;

                        if ( e.Args.Length > 1 )
                            line = " " + dbLine.Name;

                        nlines ++;

                        window.AppendHighlight( dbLine.Name );
                        window.AppendLineBreak( 2 );
                        window.AppendHighlight( "  MM    BM    PM    SI   TS    MC  QL" );
                        window.AppendLineBreak( );

                        foreach ( NanoLinesNano dbNano in dbLine.Nanos )
                        {
                            string NanoRow = string.Empty;
                            NanoRow += ZeroPad( dbNano.Requirements.MM, 4 );
                            NanoRow += " ";
                            NanoRow += ZeroPad( dbNano.Requirements.BM, 4 );
                            NanoRow += " ";
                            NanoRow += ZeroPad( dbNano.Requirements.PM, 4 );
                            NanoRow += " ";
                            NanoRow += ZeroPad( dbNano.Requirements.SI, 4 );
                            NanoRow += " ";
                            NanoRow += ZeroPad( dbNano.Requirements.TS, 4 );
                            NanoRow += " ";
                            NanoRow += ZeroPad( dbNano.Requirements.MC, 4 );
                            NanoRow += " ";
                            NanoRow += ZeroPad( dbNano.QL, 3 );
                            window.AppendRawString( NanoRow );
                            window.AppendNormal( " " );
                            if ( dbNano.ID > 0 )
                                window.AppendItem( dbNano.Name, dbNano.ID,
                                                   dbNano.ID, dbNano.QL );
                            else
                                window.AppendBotCommand( dbNano.Name,
                                                         "!items " +
                                                         dbNano.Name );
                            if ( dbNano.Requirements.Level > 0 )
                                window.AppendNormal( " (Level: " + dbNano.Requirements.Level.ToString() + ")" );
                            window.AppendLineBreak( );

                        }
                    }
                    break;
                }

            if ( string.IsNullOrEmpty(line) || ( nlines > 0 ) )
                bot.SendReply( e, HTML.CreateColorString(
                                   bot.ColorHeaderHex, prof +
                                   line ) + " Nanos »» ", window);
            else
                bot.SendReply( e, "Nanoline \"" + line +
                               "\" not found in " + prof + " nanos." );
                

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
            case "nano":
                return "Allows you to search the Faldon's AO Nano Database.\n"+
                    "To search the Nano Database use: /tell " + bot.Character + " nano [[minql] [[maxql] [[prof] [availablity]]]] [partial item name]\n" +
                    "To search for a specific quality level use: /tell " + bot.Character + " nano [min quality level] [max quality level] [partial item name]\n" +
                    "To search for a specific profession's nanos use: /tell " + bot.Character + " nano [profession] [partial item name]\n" +
                    "To limit your search to a particular availablilty use: /tell " + bot.Character + " nano [buyable|rollable|quest|dyna|tradeskill] [partial item name]\n" +
                    "You can specify multiple criteria using the following order of arguments: /tell " + bot.Character + " nano [minql] [maxql] [prof] [availablity] [partial item name]\n" +
                    "You don't need to specify the full name of the item, you can use multiple words in order to find an item.\n" +
                    "However, the different words are required to be in the right order to find a certain item.\n" +
                    "For example: 'null sphere' will work but 'sphere null' won't if you want to find Nullity Sphere";

            case "nanoinfo":
                    return "Display additional information about a particular nano.\n"+
                        "To search the Nano Database use: /tell " + bot.Character + " nanoinfo [id | search terms]\n";

            case "nanoline":
                    return "Allows you to search AoItems Professional Nano listing.\n"+
                        "To search the Nano Database use: /tell " + bot.Character + " nanoline [prof] [[nano line]]\n";

            }
            return null;
        }
    }

    [XmlRoot("ao-nanodb")]
    public class NanoDB_Results
    {
        [XmlElement("version")]
        public string Version;
        [XmlElement("results")]
        public string Results;
        [XmlElement("max")]
        public string Max;
        [XmlElement("nano")]
        public NanoDB_Nano[] Nanos;
        [XmlElement("credits")]
        public string Credits;
    }

    [XmlRoot("nano")]
    public class NanoDB_Nano
    {
        [XmlElement("lowid")]
        public string _lowid;
        [XmlElement("lowql")]
        public string _lowql;
        [XmlElement("name")]
        public string Name;
        [XmlElement("icon")]
        public string _icon;
        [XmlElement("prof")]
        public string prof;
        [XmlElement("available")]
        public string available;
        [XmlElement("comment")]
        public string comment;

        public int LowID { get { try { return Convert.ToInt32(this._lowid); } catch { return 0; } } }
        public int LowQL { get { try { return Convert.ToInt32(this._lowql); } catch { return 0; } } }
        public int IconID { get { try { return Convert.ToInt32(this._icon); } catch { return 0; } } }
    }

    [XmlRoot("NanoLines")]
    public class NanoLines
    {
        [XmlElement("Profession")]
        public NanoLinesProfs[] Professions;

        public static int GetInt( string s ) { try { return Convert.ToInt32(s); } catch { return 0; } }
    }

    [XmlRoot("Profession")]
    public class NanoLinesProfs
    {
        [XmlAttribute("Name")]
        public string Name;
        [XmlElement("Line")]
        public NanoLinesLines[] Lines;
    }

    [XmlRoot("Line")]
    public class NanoLinesLines
    {
        [XmlAttribute("Name")]
        public string Name;
        [XmlElement("Nano")]
        public NanoLinesNano[] Nanos;
    }

    [XmlRoot("Nano")]
    public class NanoLinesNano
    {
        [XmlAttribute("Name")]
        public string Name;
        [XmlAttribute("ID")]
        public string _id;
        [XmlAttribute("QL")]
        public string _ql;
        [XmlElement("Reqs")]
        public NanoLinesReqs Requirements;

        public int ID { get { return NanoLines.GetInt( this._id ); } }
        public int QL { get { return NanoLines.GetInt( this._ql ); } }

    }

    [XmlRoot("Reqs")]
    public class NanoLinesReqs
    {
        [XmlAttribute("Level")]
        public string _lvl;
        [XmlAttribute("MM")]
        public string _mm;
        [XmlAttribute("BM")]
        public string _bm;
        [XmlAttribute("PM")]
        public string _pm;
        [XmlAttribute("SI")]
        public string _si;
        [XmlAttribute("TS")]
        public string _ts;
        [XmlAttribute("MC")]
        public string _mc;

        public int Level { get { return NanoLines.GetInt( this._lvl ); } }
        public int MM { get { return NanoLines.GetInt( this._mm ); } }
        public int BM { get { return NanoLines.GetInt( this._bm ); } }
        public int PM { get { return NanoLines.GetInt( this._pm ); } }
        public int SI { get { return NanoLines.GetInt( this._si ); } }
        public int TS { get { return NanoLines.GetInt( this._ts ); } }
        public int MC { get { return NanoLines.GetInt( this._mc ); } }

    }

}
