using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Data;
using System.Security.Cryptography;
using AoLib.Utils;
using VhaBot;

namespace VhaBot.Plugins
{

    public class Raffle : PluginBase
    {

        private string ItemsPerRoll = "one";

        private bool _running = false;
        private string _admin = null;
        private string _item;
        private AoItem _realItem = null;
        private Dictionary<string, string> _joined =
            new Dictionary<string, string>();

        private List<string> _raid = new List<string>();
        private List<string> _raid_unapproved = new List<string>();
        private List<AoItem> _raid_loot = new List<AoItem>();
        private List <string> _history_names = new List<string>();
        private List <AoItem> _history_items = new List<AoItem>();

        public Raffle()
        {
            this.Name = "Raffle and Raid Loot Distribution";
            this.InternalName = "llRaffle";
            this.Version = 150;
            this.Author = "Vhab / Llie";
            this.DefaultState = PluginState.Installed;
            this.Description = "Manages raffling and distributing loot";
            this.Commands = new Command[] {

                new Command("raffle", true, UserLevel.Guest),
                new Command("raffle start", true, UserLevel.Guest),
                new Command("raffle abort", true, UserLevel.Leader),
                new Command("raffle stop", true, UserLevel.Guest),
                new Command("raffle join", true, UserLevel.Guest),
                new Command("raffle leave", true, UserLevel.Guest),

                new Command("raidroll", true, UserLevel.Guest),
                new Command("raidroll auto", true, UserLevel.Leader),
                new Command("raidroll add", true, UserLevel.Leader),
                new Command("raidroll join", true, UserLevel.Guest),
                new Command("raidroll leave", true, UserLevel.Guest),
                new Command("raidroll kick", true, UserLevel.Leader),
                new Command("raidroll loot", true, UserLevel.Leader),
                new Command("raidroll roll", true, UserLevel.Leader),
                new Command("raidroll history", true, UserLevel.Leader),
                new Command("raidroll clear", true, UserLevel.Leader),

            };
        }

        public override void OnLoad(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent +=
                new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.String, this.InternalName,
                                       "items_per_roll",
                                       "Items per player per roll",
                                       this.ItemsPerRoll, "One", "Many");

        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent -=
                new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot,
                                               ConfigurationChangedArgs e)
        {
            if (e.Section.Equals(this.InternalName,
                                 StringComparison.CurrentCultureIgnoreCase))
                switch (e.Key.ToLower())
                {
                    case "items_per_roll":
                        this.ItemsPerRoll = (string)e.Value;
                        break;
                }
        }


        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            lock (this)
            {
                switch (e.Command)
                {

                    // raffle a single item
                case "raffle":
                    this.OnRaffleStatusCommand(bot, e);
                    break;
                case "raffle start":
                    this.OnRaffleStartCommand(bot, e);
                    break;
                case "raffle join":
                    this.OnRaffleJoinCommand(bot, e);
                    break;
                case "raffle leave":
                    this.OnRaffleLeaveCommand(bot, e);
                    break;
                case "raffle stop":
                    this.OnRaffleStopCommand(bot, e);
                    break;
                case "raffle abort":
                    this.OnRaffleAbortCommand(bot, e);
                    break;

                    // distribute loot across raiders
                case "raidroll":
                    this.OnRaidStatusCommand(bot, e);
                    break;
                case "raidroll join":
                    this.OnRaidJoinCommand(bot, e);
                    break;
                case "raidroll leave":
                    this.OnRaidLeaveCommand(bot, e);
                    break;
                case "raidroll add":
                    this.OnRaidAddCommand(bot, e);
                    break;
                case "raidroll auto":
                    this.OnRaidAutoAddCommand(bot, e);
                    break;
                case "raidroll kick":
                    this.OnRaidKickCommand(bot, e);
                    break;
                case "raidroll loot":
                    this.OnRaidLootCommand(bot, e);
                    break;
                case "raidroll roll":
                    this.OnRaidRollCommand(bot, e);
                    break;
                case "raidroll history":
                    this.OnRaidHistoryCommand(bot, e);
                    break;
                case "raidroll clear":
                    this.OnRaidClearCommand(bot, e);
                    break;

                }
            }
        }

        private void OnRaffleStartCommand(BotShell bot, CommandArgs e)
        {
            if (this._running)
            {
                bot.SendReply(e, "A raffle has already been started");
                return;
            }
            if (e.Words.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: raffle start [item]");
                return;
            }
            this._item = e.Words[0];
            this._realItem = null;
            this._admin = e.Sender;
            if (e.Items.Length > 0)
            {
                this._item = e.Items[0].ToLink();
                this._realItem = e.Items[0];
            }
            else if (e.Args[0].StartsWith("item:") && e.Args.Length > 1)
            {
                string[] tmp = e.Args[0].Substring(5).Split(':');
                if (tmp.Length == 3)
                {
                    try
                    {
                        int lowID = Convert.ToInt32(tmp[0]);
                        int highID = Convert.ToInt32(tmp[0]);
                        int ql = Convert.ToInt32(tmp[0]);
                        string name = HTML.UnescapeString(e.Words[1]);
                        this._realItem = new AoItem(name, lowID, highID, ql, null);
                        this._item = this._realItem.ToLink();
                    }
                    catch { }
                }
            }
            this._running = true;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Raffle");

            window.AppendLineBreak();
            window.AppendHighlight("Raffle for ");
            window.AppendRawString(this._item);

            window.AppendLineBreak(2);
            window.AppendNormalStart();
            window.AppendString("[");
            window.AppendBotCommand("Join", "raffle join");
            window.AppendString("] [");
            window.AppendBotCommand("Leave", "raffle leave");
            window.AppendString("]");

            string output =
                string.Format("{1}{2}{0} has started a raffle for {1}{3}{0} »» ",
                              bot.ColorHighlight, bot.ColorHeader,
                              e.Sender, this._item) + window.ToString();
            bot.SendPrivateChannelMessage( output );
            if ( bot.InOrganization )
                bot.SendOrganizationMessage( output );
        }

        private void OnRaffleStatusCommand(BotShell bot, CommandArgs e)
        {
            if (!this._running)
            {
                bot.SendReply(e, "There is no raffle currently running.");
                bot.SendReply(e, "To start a raffle, do " + bot.ColorHeader +
                              " !raffle start [item]");
                return;
            }
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Raffle");

                window.AppendLineBreak();
                window.AppendHighlight("Raffle for ");
                window.AppendRawString(this._item);

                if ( this._admin.IndexOf(e.Sender,0) < 0 )
                {
                    window.AppendLineBreak();
                    window.AppendNormalStart();
                    window.AppendString("[");
                    window.AppendBotCommand("Join", "raffle join");
                    window.AppendString("] [");
                    window.AppendBotCommand("Leave", "raffle leave");
                    window.AppendString("]");
                }
                if ( ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) ||
                     ( this._admin.IndexOf(e.Sender,0) >= 0 ) )
                {

                    bot.SendReply(e, "There are currently " +
                                  this._joined.Count +
                                  " users signed up for the raffle for " +
                                  this._item );

                    window.AppendLineBreak();
                    window.AppendLineBreak();
                    window.AppendString("[");
                    window.AppendBotCommand("End", "raffle stop");
                    window.AppendString("] [");
                    window.AppendBotCommand("Abort", "raffle abort");
                    window.AppendString("]");

                    window.AppendLineBreak();
                    window.AppendLineBreak();
                    window.AppendString( "There are currently " +
                                         this._joined.Count +
                                         " users signed up for this raffle." );

                }

                string output =
                    string.Format("{1}{2}{0} has started a raffle for {1}{3}{0} »» ",
                                  bot.ColorHighlight, bot.ColorHeader,
                                  this._admin, this._item) + window.ToString();
                bot.SendReply(e, output);

                return;
            }
        }

        private void OnRaffleAbortCommand(BotShell bot, CommandArgs e)
        {
            if ( ( this._admin.IndexOf(e.Sender,0) >= 0) ||
                 ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) )
            {
                if (!this._running)
                {
                    bot.SendReply(e, "There is no raffle to abort");
                    return;
                }
                this._running = false;
                this._item = null;
                this._realItem = null;
                this._admin = null;
                lock (this._joined)
                    this._joined.Clear();
                bot.SendPrivateChannelMessage(
                    bot.ColorHighlight +
                    HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) +
                    " has aborted the raffle" );
                if ( bot.InOrganization )
                    bot.SendOrganizationMessage(
                        bot.ColorHighlight +
                        HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) +
                        " has aborted the raffle" );
                bot.SendReply(e, "You aborted the raffle");
            }
            else
            {
                bot.SendReply(e, e.Sender + " was not the person who started the raffle and/or does not have sufficient privileges to stop the raffle.");
                return;
            }
        }

        private void OnRaffleJoinCommand(BotShell bot, CommandArgs e)
        {
            if (!this._running)
            {
                bot.SendReply(e, "There is no raffle to join");
                return;
            }
            string main = bot.Users.GetMain(e.Sender);
            lock (this._joined)
            {
                if (this._joined.ContainsKey(main))
                {
                    bot.SendReply(e, e.Sender +
                                  " has already joined the raffle." );
                    return;
                }
                this._joined.Add(main, e.Sender);
                bot.SendReply(e, e.Sender + " has joined the raffle." );
            }
        }

        private void OnRaffleLeaveCommand(BotShell bot, CommandArgs e)
        {
            string main = bot.Users.GetMain(e.Sender);
            lock (this._joined)
            {
                if (!this._joined.ContainsKey(main))
                {
                    bot.SendReply(e, e.Sender + " hasn't joined the raffle." );
                    return;
                }
                this._joined.Remove(main);
                bot.SendReply(e, e.Sender + " has left the raffle." );
            }
        }

        private void OnRaffleStopCommand(BotShell bot, CommandArgs e)
        {
            if ( ( this._admin.IndexOf(e.Sender,0) >= 0) ||
                 ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) )
            {
                if (!this._running)
                {
                    bot.SendReply(e, "There is no raffle to stop");
                    return;
                }
                lock (this._joined)
                {
                    bot.SendReply(e, e.Sender + " has ended the raffle");
                    if (this._joined.Count == 0)
                    {
                        bot.SendPrivateChannelMessage(bot.ColorHighlight + "The raffle has ended without any winners");
                        if ( bot.InOrganization )
                            bot.SendOrganizationMessage( bot.ColorHighlight + "The raffle has ended without any winners");
                    }
                    else
                    {
                        string[] keys = new string[this._joined.Keys.Count];
                        this._joined.Keys.CopyTo(keys, 0);
                        Random random = new Random();
                        int winner = random.Next(0, keys.Length);
                        bot.SendPrivateChannelMessage(bot.ColorHeader + this._joined[keys[winner]] + bot.ColorHighlight + " has won the raffle for " + bot.ColorHeader + this._item + ". Please see " + this._admin + " to receive your item(s)" );
                        if ( bot.InOrganization )
                            bot.SendOrganizationMessage( bot.ColorHeader + this._joined[keys[winner]] + bot.ColorHighlight + " has won the raffle for " + bot.ColorHeader + this._item + ". Please see " + this._admin + " to receive your item(s)" );  
                  }
                    this._running = false;
                    this._item = null;
                    this._realItem = null;
                    this._admin = null;
                    this._joined.Clear();
                }
            }
            else
            {
                bot.SendReply(e, e.Sender + " was not the person who started the raffle or does not have sufficient privileges to end the raffle.");
                return;
            }
        }

        private void OnRaidStatusCommand(BotShell bot, CommandArgs e)
        {
            
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Raid Status");
            window.AppendLineBreak();
            window.AppendNormalStart();

            if ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader )
            {

                window.AppendLineBreak();
                window.AppendHighlight( "Players awaiting approval:" );
                window.AppendLineBreak();
                foreach ( string player in this._raid_unapproved )
                {
                    window.AppendNormal( player + " [" );
                    window.AppendBotCommand( "Approve", "raidroll add " +
                                             player );
                    window.AppendNormal( "] [" );
                    window.AppendBotCommand( "Reject", "raidroll kick " +
                                             player );
                    window.AppendNormal( "]" );
                    window.AppendLineBreak();
                }
                window.AppendNormal( "[" );
                window.AppendBotCommand( "All", "raidroll add all" );
                window.AppendNormal( "]" );

                window.AppendLineBreak(2);
                window.AppendHighlight( "Players in raid loot distribution:" );
                window.AppendLineBreak();
                foreach ( string player in this._raid )
                {
                    window.AppendNormal( player + " [" );
                    window.AppendBotCommand( "Kick", "raidroll kick " +
                                             player );
                    window.AppendNormal( "]" );
                    window.AppendLineBreak();
                }
                
                window.AppendLineBreak(2);
                window.AppendHighlight( "Loot to be distributed:" );
                window.AppendLineBreak();
                for( int i=0; i<_raid_loot.Count; i++ )
                {
                    if ( _raid_loot[i].LowID > 0 )
                        window.AppendItem( _raid_loot[i].Name,
                                           _raid_loot[i].LowID,
                                           _raid_loot[i].HighID,
                                           _raid_loot[i].QL );
                    else
                    window.AppendString( _raid_loot[i].Name  );
                    window.AppendLineBreak();
                }

                window.AppendLineBreak(2);
                window.AppendHighlight( "Commands:" );
                window.AppendLineBreak();
                window.AppendNormal( "[" );
                window.AppendBotCommand( "Roll", "raidroll roll" );
                window.AppendNormal( "] " );
                if ( this._raid_unapproved.Count > 0 )
                {
                    window.AppendNormal( "[" );
                    window.AppendBotCommand( "Force Roll",
                                             "raidroll roll forced" );
                    window.AppendNormal( "] " );
                }
                window.AppendNormal( "[" );
                window.AppendBotCommand( "Clear",
                                         "raidroll clear" );
                window.AppendNormal( "] " );

            }
            else
            {
                string main = bot.Users.GetMain(e.Sender);

                if ( this._raid.Contains(main) )
                {
                    window.AppendString( "You are included in this raid. [" );
                    window.AppendBotCommand("Leave", "raidroll leave");
                    window.AppendString( "]" );
                }
                else if ( this._raid_unapproved.Contains(main) )
                {
                    window.AppendString( "You still require a raid leader's approval to be included in this raid. [" );
                    window.AppendBotCommand("Leave", "raidroll leave");
                    window.AppendString( "]" );
                }
                else
                {
                    window.AppendString( "You have not joined this raid.  [" );
                    window.AppendBotCommand("Join", "raidroll join");
                    window.AppendString( "]" );
                }

            }

            bot.SendReply(e, "Raid Status »» ", window);

        }

        private static void Shuffle( List<string> raid_list )
        {
            
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = raid_list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                string value = raid_list[k];
                raid_list[k] = raid_list[n];
                raid_list[n] = value;
            }

        }

        private static void ParseItems( string str,
                                        ref List<AoItem> _raid_loot )
        {
            while ( str.Length > 0 )
            {
                str = str.Trim();

                int idx = str.IndexOfAny(new char[]{'<', ',', ';'});
                if ( idx < 0 )
                    idx = str.Length;

                int lowID = 0;
                int highID = 0;
                int ql = 0;
                string name = string.Empty;
                AoItem item = null;
                if ( ( idx == 0 ) && ( str[idx] == '<' ) )
                {
                    int idx1 = str.IndexOf("itemref://");
                    if( idx1 < 0 )
                    {
                        str.Remove( 0, str.IndexOf( ">" ) );
                    }
                    else
                    {
                        int cursor = idx1 + 10;
                        try
                        {
                            lowID = Convert.ToInt32(
                                str.Substring( cursor,
                                               str.IndexOf( "/", cursor ) -
                                               cursor ) );
                        } catch { }


                        cursor = str.IndexOf( "/", cursor ) + 1;
                        try
                        {
                            highID = Convert.ToInt32(
                                str.Substring( cursor,
                                               str.IndexOf( "/", cursor ) -
                                               cursor ) );
                        } catch { }

                        cursor = str.IndexOf( "/", cursor ) + 1;
                        try
                        {
                            ql = Convert.ToInt32(
                                str.Substring( cursor,
                                               str.IndexOf( "\"", cursor ) -
                                               cursor ) );
                        } catch { }

                        cursor = str.IndexOf( ">", cursor ) + 1;
                        name = str.Substring( cursor,
                                              str.IndexOf( "</a>", cursor ) -
                                              cursor );

                        item = new AoItem( name, lowID, highID, ql, null );

                        str = str.Remove( idx, str.IndexOf( "</a>", cursor ) +
                                          4 - idx );
                    }
                }
                else
                {
                    name = HTML.UnescapeString(str.Substring( 0, idx ).Trim());
                    item = new AoItem( name, lowID, highID, ql, null );

                    if ( ( idx < str.Length ) && ( str[idx] != '<' ) )
                        str = str.Remove( 0, idx + 1 );
                    else if ( idx < str.Length )
                        str = str.Remove( 0, idx );
                    else
                        str = string.Empty;
                }

                _raid_loot.Add( item );
                
            }
        }

        private void OnRaidLootCommand(BotShell bot, CommandArgs e)
        {
            ParseItems( e.Words[0], ref _raid_loot );
            int count = _raid_loot.Count;
            bot.SendReply(e, count.ToString() + " items added." );
        }

        private void OnRaidRollCommand(BotShell bot, CommandArgs e)
        {
            bool verified = false;
            if ( ( e.Args.Length == 2 ) && ( e.Args[1] == "force" ) )
            {
                verified = true;
            }
            if ( ( this._raid_unapproved.Count > 0 ) && ( verified != true ) )
            {
                bot.SendReply(e, "There are unapproved members in the raid.  You can either kick then, approve them, or use \"raidroll roll force\" to complete this roll." );
                return;
            }

            List<string> raid_list = new List<string>();
            foreach ( string player in this._raid )
            {
                raid_list.Add( player );
            }
            Raffle.Shuffle(raid_list);

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Raid Loot Distribution: ");
            window.AppendLineBreak();
            window.AppendNormalStart();

            List<string> winners = new List<string>();
            for ( int i=0; i<Math.Min(_raid_loot.Count, raid_list.Count); i++ )
            {
                window.AppendString( raid_list[i] + ": "  );
                if ( _raid_loot[i].LowID > 0 )
                    window.AppendItem( _raid_loot[i].Name,
                                       _raid_loot[i].LowID,
                                       _raid_loot[i].HighID,
                                       _raid_loot[i].QL );
                else
                    window.AppendString( _raid_loot[i].Name );
                window.AppendLineBreak();

                winners.Add( raid_list[i] );
                _history_names.Add( raid_list[i] );
                _history_items.Add( _raid_loot[i] );
                
            }

            // move winners from raid to unapproved if specified by config
            if ( ItemsPerRoll == "one" )
            {
                for( int i=0; i<winners.Count; i++ )
                {
                    _raid.Remove(winners[i]);
                    _raid_unapproved.Add(winners[i]);
                }
            }

            _raid_loot.RemoveRange( 0, Math.Min(_raid_loot.Count,
                                                raid_list.Count) );

            string output =
                string.Format( "Results of roll »» ",
                               bot.ColorHighlight, bot.ColorHeader,
                               e.Sender, this._item) + window.ToString();

            if ( ( e.Type == CommandType.Tell ) &&
                 ( bot.PrivateChannel.List().Count == 0 ) )
                bot.SendReply( e, output );
            bot.SendPrivateChannelMessage( output );
            if ( bot.InOrganization )
                bot.SendOrganizationMessage( output );

        }

        private void OnRaidHistoryCommand(BotShell bot, CommandArgs e)
        {

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Raid Loot History: ");
            window.AppendLineBreak();
            window.AppendNormalStart();

            for( int i=0; i<_history_names.Count; i++ )
            {
                window.AppendString( _history_names[i] + ": "  );
                if ( _history_items[i].LowID > 0 )
                    window.AppendItem( _history_items[i].Name,
                                       _history_items[i].LowID,
                                       _history_items[i].HighID,
                                       _history_items[i].QL );
                else
                    window.AppendString( _history_items[i].Name );
                window.AppendLineBreak();
            }

            bot.SendReply(e, "Raid roll history »» ", window );

        }

        private void OnRaidJoinCommand(BotShell bot, CommandArgs e)
        {
            if ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader )
            {
                string main = bot.Users.GetMain(e.Sender);
                lock (this._raid)
                {
                    if ( this._raid.Contains(main) )
                    {
                        bot.SendReply(e, e.Sender +
                                      " is already included in this raid." );
                        return;
                    }
                    this._raid.Add( main );
                    bot.SendReply(e, e.Sender + " has been added to raid." );
                }

            }
            else
            {
                string main = bot.Users.GetMain(e.Sender);
                lock (this._raid_unapproved)
                {
                    if ( this._raid_unapproved.Contains(main) ||
                         this._raid.Contains(main) )
                    {
                        bot.SendReply(e, e.Sender +
                                      " is already included in this raid." );
                        return;
                    }
                    this._raid_unapproved.Add( main );
                    bot.SendReply(e, e.Sender + " will be added to raid pending raid leader approval." );
                }
            }

        }

        private void OnRaidLeaveCommand(BotShell bot, CommandArgs e)
        {
            string main = bot.Users.GetMain(e.Sender);
            if ( ! this._raid_unapproved.Contains(main) &&
                 ! this._raid.Contains(main))
            {
                bot.SendReply(e, e.Sender + " is not in this raid." );
                return;
            }
            if ( this._raid_unapproved.Contains(main) )
                lock (this._raid_unapproved)
                {
                    this._raid_unapproved.Remove(main);
                    bot.SendReply(e,  e.Sender + " has left the raid." );
                }
            else if ( this._raid.Contains(main) )
                lock (this._raid)
                {
                    this._raid.Remove(main);
                    bot.SendReply(e,  e.Sender + " has left the raid." );
                }
        }

        private void OnRaidAddCommand(BotShell bot, CommandArgs e)
        {

            List<string> added = new List<string>();
            List<string> not_added = new List<string>();

            lock (this._raid)
            {
                if ( ( e.Args.Length == 1 ) && ( e.Args[0] == "all" ) )
                {
                    foreach ( string player in this._raid_unapproved )
                    {
                        this._raid.Add( player );
                        added.Add( player );
                    }
                    lock (this._raid_unapproved)
                    {
                        this._raid_unapproved.Clear();
                    }

                }
                else

                    for ( int i=0; i<e.Args.Length; i++ )
                    {

                        string main =
                            bot.Users.GetMain(Format.UppercaseFirst(e.Args[i]));
                        if ( this._raid.Contains(main) )
                        {
                            not_added.Add(main);
                        }
                        else
                        {
                            if ( this._raid_unapproved.Contains(main) )
                                lock (this._raid_unapproved)
                                {
                                    this._raid_unapproved.Remove(main);
                                }
                            this._raid.Add( main );
                            added.Add( main );
                        }

                    }

            }

            if ( ( added.Count == 1 ) && ( not_added.Count == 1 ) )
                bot.SendReply(e, e.Args[0] + " has been added to the raid." );
            else if ( ( not_added.Count == 1 ) && ( added.Count == 0 ) )
                bot.SendReply(e, e.Args[0] + " was already in this raid." );
            else
            {
                                
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Results: ");
                window.AppendLineBreak();
                window.AppendNormalStart();

                for ( int i=0; i<added.Count; i++ )
                {
                    window.AppendString( added[i] + " was added to raid." );
                    window.AppendLineBreak();
                }
                for ( int i=0; i<not_added.Count; i++ )
                {
                    window.AppendString( not_added[i] +
                                         " is already in raid." );
                    window.AppendLineBreak();
                }

                bot.SendReply(e, "People added to raid »» ", window );

            }


        }

        private void OnRaidAutoAddCommand(BotShell bot, CommandArgs e)
        {
            try
            {
                // get org members
                string[] online = bot.FriendList.Online("notify");
                Dictionary<UInt32, Friend> list = bot.PrivateChannel.List();
                // get guest channel members
                List<string> guest_list = new List<string>();
                foreach (KeyValuePair<UInt32, Friend> user in list)
                    guest_list.Add(user.Value.User);
                string [] guests = guest_list.ToArray();

                List<string> added = new List<string>();

                lock (this._raid)
                {

                    for ( int i=0; i<online.Length; i++ )
                    {

                        string main =
                            bot.Users.GetMain(Format.UppercaseFirst(online[i]));
                        if ( ! this._raid.Contains(main) )
                        {
                            this._raid.Add( main );
                            added.Add( main );
                        }

                    }

                    for ( int i=0; i<guests.Length; i++ )
                    {

                        string main =
                            bot.Users.GetMain(Format.UppercaseFirst(guests[i]));
                        if ( ! this._raid.Contains(main) )
                        {
                            this._raid.Add( main );
                            added.Add( main );
                        }

                    }

                }

                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Added the following people to the raid: ");
                window.AppendLineBreak();
                window.AppendNormalStart();

                for ( int i=0; i<added.Count; i++ )
                {
                    window.AppendString( added[i] );
                    window.AppendLineBreak();
                }

                bot.SendReply(e, "People added to raid »» ", window );

            }
            catch
            {
                bot.SendReply( e, "An error has occured.  Are the Online and PrivateGroup plug-ins loaded?" );
            }
            
        }

        private void OnRaidKickCommand(BotShell bot, CommandArgs e)
        {

            List<string> removed = new List<string>();
            List<string> not_removed = new List<string>();

            for ( int i=0; i<e.Args.Length; i++ )
            {

                string main =
                    bot.Users.GetMain(Format.UppercaseFirst(e.Args[i]));

                if ( ! this._raid_unapproved.Contains(main) &&
                     ! this._raid.Contains(main) )
                {
                    not_removed.Add( main );
                }
                else if ( this._raid_unapproved.Contains(main) )
                    lock (this._raid_unapproved)
                    {
                        this._raid_unapproved.Remove(main);
                        removed.Add(main);
                    }
                else if ( this._raid.Contains(main) )
                    lock (this._raid)
                    {
                        this._raid.Remove(main);
                        removed.Add(main);
                    }
            }
            if ( ( removed.Count == 1 ) && ( not_removed.Count == 0 ) )
                bot.SendReply(e, e.Args[0] + " was removed from raid." );
            else if ( ( not_removed.Count == 1 ) &&  ( removed.Count == 0 ) )
                bot.SendReply(e, e.Args[0] + " was not in raid." );
            else
            {
                
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Results: ");
                window.AppendLineBreak();
                window.AppendNormalStart();

                for ( int i=0; i<removed.Count; i++ )
                {
                    window.AppendString( removed[i] + " was removed." );
                    window.AppendLineBreak();
                }
                for ( int i=0; i<not_removed.Count; i++ )
                {
                    window.AppendString( not_removed[i] + " was not in raid." );
                    window.AppendLineBreak();
                }

                bot.SendReply(e, "People removed from raid »» ", window );

            }
        }

        private void OnRaidClearCommand(BotShell bot, CommandArgs e)
        {
            lock (this._raid)
            {
                this._raid.Clear();
            }
            lock (this._raid_unapproved)
            {
                this._raid_unapproved.Clear();
            }
            lock (this._raid_loot)
            {
                this._raid_loot.Clear();
            }
            lock (this._history_names)
            {
                this._history_names.Clear();
            }
            lock (this._history_items)
            {
                this._history_items.Clear();
            }
            bot.SendReply(e, "Raid cleared by " + e.Sender ); 
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command.ToLower())
            {

            case "raffle":
                return "Shows the current raffle, if one has been started, and provides option to join or leave the raffle, as well as options to stop or abort the raffle if you are the person who started the raffle or a bot admin.\n" +
                    "Usage: /tell " + bot.Character + "raffle";

            case "raffle start":
                return "Start a raffle for an item.  Type or drop an item or reference to the start command. \n" +
                    "Usage: /tell " + bot.Character + "raffle start [item]";

            case "raffle join":
                return "Indicate to the bot that you wish to join the current raffle.\n" +
                    "Usage: /tell " + bot.Character + "raffle join";

            case "raffle leave":
                return "Indicate to the bot that you wish to leave the current raffle.\n" +
                    "Usage: /tell " + bot.Character + "raffle leave";

            case "raffle stop":
                return "Complete the current raffle by selecting a winner from the list of participants.\n" +
                    "Usage: /tell " + bot.Character + "raffle stop";

            case "raffle abort":
                return "Abort the current raffle without selecting a winner.\n" +
                    "Usage: /tell " + bot.Character + "raffle abort";

            case "raidroll":
                return "Display the current status of the participants of the current raid loot distribution.  Raid leaders will have additional options for management of the loot and participants.\n" +
                    "Usage: /tell " + bot.Character + "raidroll";

            case "raidroll join":
                return "Indicate to the bot that you wish to be included in the raid loot distribution.  You will not be included until approved by a raid leader.\n" +
                    "Usage: /tell " + bot.Character + "raidroll join";

            case "raidroll leave":
                return "Indicate to the bot that you are leaving the raid or do not wish to be included in the loot distribution.\n" +
                    "Usage: /tell " + bot.Character + "raidroll leave";

            case "raidroll add":
                return "Raid leaders may use this command to add individuals to be included in the loot distribution.  Raid leaders may also use this command to quickly add all unapproved names.\n" +
                    "Usage: /tell " + bot.Character + "raidroll add [name|all]";

            case "raidroll auto":
                return "Raid leaders may use this command to add individuals joined to the private channel and online in org (if this is an org bot) to the loot distribution.  This feature may save time for the raid leader of large raids.  Use this command to add everyone, then kick the players not participating in the raid.\n" +
                    "Usage: /tell " + bot.Character + "raidroll auto";

            case "raidroll kick":
                return "Raid leaders may use this command to remove individuals to from the loot distribution.\n" +
                    "Usage: /tell " + bot.Character + "raidroll kick [name]";

            case "raidroll loot":
                return "Raid leaders should use this command to add loot to be distributed.\n" +
                    "Usage: /tell " + bot.Character + "raidroll loot [items ... ]";
                
            case "raidroll roll":
                return "Raid leaders should use this command to distribute loot.  If the preference for loot is set to \"one\" then each winner will be moved from the raid to the unapproved list to prevent that player from winning a second item.  If there are players in the unapproved list, the bot will warn the raid leader.  To roll loot with people in the unapproved list, simply add the \"force\" argument.\n" +
                    "Usage: /tell " + bot.Character + "raidroll roll [force]";

            case "raidroll history":
                return "Use this command to view the winners of the raid loot.\n" +
                    "Usage: /tell " + bot.Character + "raidroll history";

            case "raidroll clear":
                return "Raid leaders should use this command to clear the raid.  This command will clear all particpants, all unapproved members, all loot, and the history.\n" +
                    "Usage: /tell " + bot.Character + "raidroll roll [force]";

            default:
                return null;

            }
        }

    }
}
