using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Web;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class NewRecruits : PluginBase
    {
        private Config _recruits;
        private Config _leavers;

        // number of days back to report
        private Int32 _days = 60;

        private Int32 _send_welcome = 0;
        private string _welcome_message = string.Empty;

        private BotShell _bot;

        public NewRecruits()
        {
            this.Name = "New recruits and leavers listing";
            this.InternalName = "llRecruits";
            this.Author = "Llie / Veremit";
            this.DefaultState = PluginState.Installed;
            this.Description = "Display list of recent recruits";
            this.Version = 151;
            this.Commands = new Command[] {
                new Command("recruits", true, UserLevel.Leader),
                new Command("leavers", true, UserLevel.Leader)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._bot = bot;

            this._recruits = new Config(bot.ToString(), "users");

            this._leavers = new Config(bot.ToString(), "leavers");
            this._leavers.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Left_Members (ID INTEGER PRIMARY KEY AUTOINCREMENT, Username VARCHAR(14), Reason VARCHAR(10), KickedBy VARCHAR(14), Left INTEGER)");
            bot.Events.ChannelMessageEvent += new ChannelMessageHandler(OnChannelMessageEvent);

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "nrndays", "Numbers of days back to report", this._days, 7, 14, 30, 60, 90);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "sendwelcome", "Send a welcome message to org and private group channels?", this._send_welcome, 0, 1 );
            bot.Configuration.Register(ConfigType.String, this.InternalName, "welcomemsg", "Set the welcome message for new recruits", this._welcome_message );
            LoadConfiguration(bot);
        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.ChannelMessageEvent -= new ChannelMessageHandler(OnChannelMessageEvent);
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            LoadConfiguration( bot );
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._days = bot.Configuration.GetInteger(this.InternalName, "nrndays", this._days);
            this._send_welcome = bot.Configuration.GetInteger(this.InternalName, "sendwelcome", this._send_welcome);
            this._welcome_message = bot.Configuration.GetString(this.InternalName, "welcomemsg", this._welcome_message);
        }

        private void SendWelcome( string newmember )
        {
            if ( this._send_welcome != 0 )
            {
                string message = "Welcome to " + this._bot.Organization;
                if ( ! string.IsNullOrEmpty(this._welcome_message) )
                    message = HttpUtility.HtmlDecode(this._welcome_message);

                while ( message.IndexOf("%u") > 0 )
                    message = message.Replace( "%u", newmember );

                while ( message.IndexOf("%o") > 0 )
                    message = message.Replace( "%o", this._bot.Organization );

                this._bot.SendOrganizationMessage(message);
                this._bot.SendPrivateChannelMessage(message);
            }
        }

        private bool AddMember( string member )
        {
            return this.AddMember( member, "System" );
        }

        private bool AddMember( string member, string officer )
        {
            if( this._bot.Users.AddUser(member, UserLevel.Member, officer) )
            {
                this._bot.FriendList.Add("notify", member);
                return true;
            }
            return false;
        }

        private bool RemoveMember( string member )
        {
            this._bot.Users.RemoveUser(member);
            this._bot.Users.RemoveAlt(member);
            this._bot.FriendList.Remove("notify", member);
            return true;
        }

        private bool AddLeaver( string username, string reason, string officer )
        {
            if ( reason == "kicked" )
                this._leavers.ExecuteNonQuery("INSERT INTO Left_Members (Username, Reason, KickedBy, Left) VALUES ('" + Format.UppercaseFirst(Config.EscapeString(username)) + "', 'kicked', '" + Format.UppercaseFirst(Config.EscapeString(officer)) + "', " + TimeStamp.Now + ")");
            else if ( reason == "left" )
                this._leavers.ExecuteNonQuery("INSERT INTO Left_Members (Username, Reason, Left) VALUES ('" + Format.UppercaseFirst(Config.EscapeString(username)) + "', 'left', " + TimeStamp.Now + ")");
            else if ( reason == "side" )
                this._leavers.ExecuteNonQuery("INSERT INTO Left_Members (Username, Reason, Left) VALUES ('" + Format.UppercaseFirst(Config.EscapeString(username)) + "', 'side', " + TimeStamp.Now + ")");
            else
                return false;
            return true;
        }

        private void OnChannelMessageEvent(BotShell bot, ChannelMessageArgs e)
        {
            string username = string.Empty;
            string officer = string.Empty;
            if ( e.IsDescrambled )
            {

                if (e.Descrambled.CategoryID == 508)
                {
                    switch (e.Descrambled.EntryID)
                    {

                    case 5146599:
                        // %s has joined your organization.
                        username = e.Descrambled.Arguments[0].Text;
                        this.AddMember( username );
                        SendWelcome( username );
                        break;

                    case 20908201:
                        // %s removed inactive character %s from your organization.
                        officer = e.Descrambled.Arguments[0].Text;
                        username = e.Descrambled.Arguments[1].Text;
                        this.AddLeaver( username, "kicked", officer );
                        this.RemoveMember( username );
                        break;

                    case 33183704:
                        // GM kicked %s from your organization.
                        officer = "GM";
                        username = e.Descrambled.Arguments[0].Text;
                        this.AddLeaver( username, "kicked", officer );
                        this.RemoveMember( username );
                        break;

                    case 33408158:
                        // %s, %s and %s kicked %s from the organization.
                        officer = "Consensus";
                        username = e.Descrambled.Arguments[3].Text;
                        this.AddLeaver( username, "kicked", officer );
                        this.RemoveMember( username );
                        break;

                    case 37093479:
                        // %s kicked %s from your organization.
                        officer = e.Descrambled.Arguments[0].Text;
                        username = e.Descrambled.Arguments[1].Text;
                        this.AddLeaver( username, "kicked", officer );
                        this.RemoveMember( username );
                        break;

                    case 45978487:
                        // %s just left your organization.
                        username = e.Descrambled.Arguments[0].Text;
                        this.AddLeaver( username, "left", string.Empty );
                        this.RemoveMember( username );
                        break;

                    case 147071208:
                        // GM removed character %s from your organization.
                        officer = "GM";
                        username = e.Descrambled.Arguments[0].Text;
                        this.AddLeaver( username, "kicked", officer );
                        this.RemoveMember( username );
                        break;

                    case 173558247:
                        // %s invited %s to your organization.
                        officer = e.Descrambled.Arguments[0].Text;
                        username = e.Descrambled.Arguments[1].Text;
                        if (bot.Bans.IsBanned(username))
                        {
                            bot.SendPrivateMessage( bot.GetUserID(officer), HTML.CreateColorString( "FF0000", "WARNING: " + username + " has been banned on this bot and you have just invited " + username + " into the organization!" ) );
                        }
                        this.AddMember( username, officer );
                        SendWelcome( username );
                        break;

                    }
                }

                else if (e.Descrambled.CategoryID == 501)
                {
                    switch (e.Descrambled.EntryID)
                    {

                    case 181448347:
                        // %s kicked from organization (alignment changed).
                        username = e.Descrambled.Arguments[0].Text;
                        this.AddLeaver( username, "side", string.Empty );
                        this.RemoveMember( username );
                        break;

                    }
                }

            }
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "recruits":
                this.OnRecruitsCommand(bot, e);
                break;

            case "leavers":
                this.OnLeaversCommand(bot, e);
                break;

            }
        }

        public void ReconcileLeavers()
        {
            Int64 backdate = TimeStamp.Now - ( this._days * 24 * 60 * 60 );

            List<string> recruit_list = new List<string>();
            List<Int32> recruit_times = new List<Int32>();
            string query_recruits = String.Format("SELECT Username, AddedBy, AddedOn FROM CORE_Members WHERE AddedOn >= {0} ORDER BY AddedOn ASC", Convert.ToString(backdate) );
            using (IDbCommand command = this._recruits.Connection.CreateCommand())
            {
                command.CommandText = query_recruits;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        if ( reader.GetString(1) != "System" )
                        {
                            recruit_list.Add(reader.GetString(0));
                            recruit_times.Add(reader.GetInt32(2));
                        }
                    }
                    catch { }
                }
                reader.Close();
            }

            List<string> leaver_list = new List<string>();
            List<Int32> leaver_times = new List<Int32>();
            string query_leavers = String.Format("SELECT Username, Reason, KickedBy, Left FROM Left_Members WHERE Left >= {0} ORDER BY Left ASC", Convert.ToString(backdate) );
            using (IDbCommand command = this._leavers.Connection.CreateCommand())
            {
                command.CommandText = query_leavers;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        leaver_list.Add(reader.GetString(0));
                        leaver_times.Add(reader.GetInt32(3));
                    }
                    catch { }
                }
                reader.Close();
            }

            bool[] lflag = new bool[leaver_list.Count];
            
            for ( int i = 0; i<leaver_list.Count; i++ )
                for ( int j = 0; j<recruit_list.Count; j++ )
                {
                    // leaver subsequently recruited
                    if( ( leaver_list[i] == recruit_list[j] ) &&
                        ( recruit_times[j] > leaver_times[i] ) )
                        lflag[i] = true;
                }

            for ( int i = 0; i<leaver_list.Count; i++ )
                if ( lflag[i] )
                {
                    string delete_command = "DELETE FROM Left_Members WHERE Username = \"" + leaver_list[i] + "\" AND Left = " + leaver_times[i];
                    this._leavers.ExecuteNonQuery( delete_command );
                }

        }

        public void OnRecruitsCommand(BotShell bot, CommandArgs e)
        {
            Int64 backdate = TimeStamp.Now - ( this._days * 24 * 60 * 60 );
            string query = String.Format("SELECT Username, AddedBy, AddedOn FROM CORE_Members WHERE AddedOn >= {0} ORDER BY AddedOn DESC", Convert.ToString(backdate) );

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("New recruits from the last " + Convert.ToString(this._days) + " days: " );
            window.AppendLineBreak();

            using (IDbCommand command = this._recruits.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        if ( reader.GetString(1) != "System" )
                        {
                            window.AppendHighlight( reader.GetString(0) );
                            window.AppendNormal(" was invited by ");
                            window.AppendHighlight( reader.GetString(1) );
                            window.AppendNormal(" on ");
                            window.AppendNormal( TimeStamp.ToDateTime( reader.GetInt32(2)).ToString("dd/MM/yyyy") );
                            window.AppendNormal(" at " + TimeStamp.ToDateTime( reader.GetInt32(2)).ToString("h:mm tt").ToLower() );
                            window.AppendLineBreak();
                        }
                    }
                    catch { }
                }
                reader.Close();
            }

            bot.SendReply(e, "New Recruits »» ", window);

        }

        public void OnLeaversCommand(BotShell bot, CommandArgs e)
        {

            ReconcileLeavers();

            Int64 backdate = TimeStamp.Now - ( this._days * 24 * 60 * 60 );
            string query = String.Format("SELECT Username, Reason, KickedBy, Left FROM Left_Members WHERE Left >= {0} ORDER BY Left DESC", Convert.ToString(backdate) );

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Left members from the last " + Convert.ToString(this._days) + " days: " );
            window.AppendLineBreak();

            using (IDbCommand command = this._leavers.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        window.AppendHighlight( reader.GetString(0) );
                        switch (reader.GetString(1))
                        {
                            case "kicked":
                                window.AppendNormal(" was kicked by ");
                                window.AppendHighlight(reader.GetString(2));
                                break;
                            case "side":
                                window.AppendNormal(" changed factions");
                                break;
                            case "left":
                                window.AppendNormal(" left the org");
                                break;
                            default:
                                window.AppendNormal(" disappeared");
                                break;
                        }
                        window.AppendNormal(" on ");
                        window.AppendNormal( TimeStamp.ToDateTime( reader.GetInt32(3)).ToString("dd/MM/yyyy") );
                        window.AppendNormal(" at " + TimeStamp.ToDateTime( reader.GetInt32(3)).ToString("h:mm tt").ToLower() );
                        window.AppendLineBreak();
                    }
                    catch { }
                }
                reader.Close();
            }

            bot.SendReply(e, "Leavers »» ", window);

        }

    }
}
