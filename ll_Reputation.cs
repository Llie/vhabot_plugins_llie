using System;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class Reputation : PluginBase
    {
        private Config _votes;

        public Reputation()
        {
            this.Name = "Player Reputation";
            this.InternalName = "llReputation";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;

            this.Commands = new Command[] {
                new Command("rep", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._votes = new Config(bot.ToString(), this.InternalName);

            this._votes.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS rep_votes (Username VARCHAR(14), Voter VARCHAR(14), Vote INTEGER, PRIMARY KEY ( Username, Voter ) )");

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            this.LoadConfiguration(bot);
        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
        }

        private void Score( string user, out Int32 score, out Int32 votes )
        {
            score = 0;
            votes = 0;
            string query = String.Format("SELECT SUM(Vote), COUNT(Voter) FROM rep_votes WHERE Username = '{0}';", Format.UppercaseFirst( user ) );
            using (IDbCommand command = this._votes.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        score = reader.GetInt32(0);
                        votes = reader.GetInt32(1);
                    }
                    catch
                    {
                        score = 0;
                        votes = 0;
                    }
                }
            }

        }

        private int Vote( string user, string sender, Int32 vote )
        {
            if ( vote > 0 )
                vote = 1;
            if ( vote < 0 )
                vote = -1;
            return this._votes.ExecuteNonQuery( String.Format( "REPLACE INTO rep_votes VALUES( '{0}', '{1}', {2} );", user, sender, vote ) );
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            RichTextWindow window = new RichTextWindow(bot);

            string user = string.Empty;
            Int32 score = 0;
            Int32 votes = 0;

            switch (e.Command)
            {

            case "rep":

                switch (e.Args.Length)
                {

                case 0:
                    // return usage
                    bot.SendReply(e, "Usage: !rep name");
                    return;

                case 1:
                    // return rep window
                    user = Format.UppercaseFirst(e.Args[0]);
                    if (bot.GetUserID(user) < 100)
                    {
                        bot.SendReply(e, "No such user: " +
                                      HTML.CreateColorString(bot.ColorHeaderHex,
                                                             user));
                        return;
                    }
                    Score( user, out score, out votes );
                    window.AppendTitle( user + "'s Reputation: " +
                                        score.ToString() + " ( " +
                                        votes.ToString() + " votes )" );
                    window.AppendLineBreak();
                    window.AppendNormal("Cast/Adjust Vote: [");
                    window.AppendBotCommand( "+", "rep " + user + " 1" );
                    window.AppendNormal("][");
                    window.AppendBotCommand( "-", "rep " + user + " -1" );
                    window.AppendNormal("]");
                    bot.SendReply( e, user + "'s Reputation: " +
                                   score.ToString() + " ( " + votes.ToString() +
                                   " votes) »» ", window );
                    return;

                case 2:
                    // cast vote
                    user = Format.UppercaseFirst(e.Args[0]);
                    Int32.TryParse( e.Args[1], out votes );
                    if ( Vote( user, Format.UppercaseFirst(e.Sender), votes )
                         > 0 )
                        bot.SendReply( e, "Your vote on " + user + " has been cast." );
                    else
                        bot.SendReply( e, "An error has occurred attepting to cast a vote on " + user );
                    return;

                }
                break;
            }
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "rep":
                return "Returns Reputation on status.\n" +
                    "usage: /tell " + bot.Character + " name";

            }
            return null;
        }
    }

}
