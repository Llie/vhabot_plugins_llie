using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AoLib.Utils;

// preferences for _store_max and _search_max (?)

// For an example PHP page that allows web-based search of the store
// implemented by this plug-in, please refer to this:

// https://bitbucket.org/Llie/llie_vhabot/raw/tip/Extra/html/store.php

namespace VhaBot.Plugins
{
    public class LlieStore : PluginBase
    {
        private Config _database;
        private string _table = string.Empty;
        private int _store_max = 21;
        private int _search_max = 100;

        public LlieStore()
        {
            this.Name = "Open Marketplace";
            this.InternalName = "LlieStore";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;
            this.Description = "Provides a simple marketplace for items.";
            this.Commands = new Command[] {
                new Command("store", true, UserLevel.Guest),
                new Command("store add", true, UserLevel.Guest),
                new Command("store remove", true, UserLevel.Guest),
                new Command("store stock", true, UserLevel.Guest),
                new Command("store show", true, UserLevel.Guest),
                new Command("store search", true, UserLevel.Guest),
                new Command("store test", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._table = bot.Character + "Store";            
            this._database = new Config(bot.ID, this.InternalName);
            string create_table = "CREATE TABLE IF NOT EXISTS " + this._table + "  ( lowid INTEGER NOT NULL, highid INTEGER NOT NULL, ql INTEGER NOT NULL, price INTEGER, quantity INTEGER, seller VARCHAR(14) NOT NULL, name VARCHAR(80), iconid INTEGER, PRIMARY KEY ( lowid, highid, ql, seller ) )";
            this._database.ExecuteNonQuery( create_table );
        }

        public override void OnUnload(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e )
        {
            // reformat Args if there is an itemref in the message
            int n_items = 0;
            string[] rArgs = ReformatArgs( e, ref n_items );
            int n_args = rArgs.Length - n_items;
            Int32 price = 0;
            Int32 quantity = 1;
            Int32 item_number = 0;

            switch (e.Command)
            {
            case "store":
                // Generic command... some smarts here to try to guess what
                // the user wants to do
                
                // No args, then display your own store with control options
                if ( e.Args.Length == 0 )
                    DisplayStore( bot, e, e.Sender );

                // if there are items, then add to your store:
                else if ( e.Items.Length > 0 )
                    StockStore( bot, e, rArgs );

                // user specified some arguments but it doesn't match one of
                // the sub-commands, so we try to figure out what they want
                else
                {
                    // user specified another user -- display that store
                    if ( bot.GetUserID(e.Args[0]) >= 100 )
                    {
                        DisplayStore( bot, e, e.Args[0] );
                        return;
                    }

                    bot.SendReply(e, "Error. Unknown store command.  <a href='chatcmd:///tell " + bot.Character + " !help store'>Click for help</a>" );
                }
                
                break;

            case "store add":

                // similar to store, but let's not allow any confusion
                if ( e.Items.Length > 0 )
                    StockStore( bot, e, rArgs );
                else
                {
                    bot.SendReply(e, "Correct usage: /tell " + bot.Character +
                                  " !store add item [price [quantity]] [second_item ...]  <a href='chatcmd:///tell " + bot.Character + " !help store add'>Click for help</a>");
                }
                
                break;

            case "store stock":

                // This command changes price and quantity for ONE ITEM.

                // all these combinations for this command are invalid
                if ( ( e.Items.Length > 1 ) ||
                     ( ( e.Items.Length == 0 ) &&
                       ( ( e.Args.Length < 2 ) || ( e.Args.Length > 3 ) ) ) ||
                     ( ( e.Items.Length == 1 ) &&
                       ( ( n_args < 1 ) || ( n_args > 2 ) ) ) )
                {
                    bot.SendReply(e, "Correct usage: /tell " +
                                  bot.Character +
                                  " !store stock item|item_number price [quantity] <a href='chatcmd:///tell " + bot.Character + " !help store stock'>Click for help</a>");
                    return;
                }

                // user dropped an item on the line, we need to look it up
                if ( e.Items.Length == 1 )
                {
                    price = Convert.ToInt32( rArgs[1] );
                    if ( n_args == 2 )
                        quantity = Convert.ToInt32( rArgs[2] );

                    ModifyItem( bot, e, e.Items[0].LowID, e.Items[0].QL,
                                price, quantity );
                }
                else
                {
                    item_number = Convert.ToInt32(e.Args[0]);
                    string[] store_items = GetUserStore( e.Sender );
                    string[] item_info = store_items[item_number-1].Split('|');

                    price = Convert.ToInt32(e.Args[1]);
                    if ( e.Args.Length == 3 )
                        quantity = Convert.ToInt32(e.Args[2]);

                    ModifyItem( bot, e, Convert.ToInt32(item_info[0]),
                                Convert.ToInt32(item_info[2]),
                                price, quantity );
                }
                break;

                
            case "store remove":

                // This removes the specified item.

                // We will do this by creating an item list from the
                // command-line such that we remove things correctly
                // if the user specifies arguments like "remove 3 4"
                int i = 0;
                int j=0;
                int iter=0;
                for ( j=0; j<n_args; j++ )
                {

                    if ( rArgs[j] == "all" )
                    {
                        // remove all items from store
                        string[] store_items = GetUserStore( e.Sender );
                        for ( i=0; i<store_items.Length; i++ )
                        {
                            string[] item_info = store_items[i].Split('|');
                            ModifyItem( bot, e, Convert.ToInt32(item_info[0]),
                                        Convert.ToInt32(item_info[2]), 0, -1 );
                        }
                        return;
                    }
                    else if ( rArgs[j].IndexOf( "<a" ) > 0 )
                    {
                        // user dropped an item on the line, look it up
                        ModifyItem( bot, e, e.Items[iter].LowID,
                                    e.Items[iter].QL, 0, -1 );
                    }
                    else
                    {
                        item_number = Convert.ToInt32(e.Args[j]);
                        string[] store_items = GetUserStore( e.Sender );
                        string[] item_info = store_items[item_number-1].Split('|');
                        ModifyItem( bot, e, Convert.ToInt32(item_info[0]),
                                    Convert.ToInt32(item_info[2]),
                                    0, -1 );
                    }
                }
                break;

            case "store show":
                // show specified user's store

                if ( e.Args.Length == 0 )
                    DisplayStore( bot, e, e.Sender );
                else if ( e.Args.Length == 1 )
                {
                    if ( bot.GetUserID(e.Args[0]) >= 100 )
                        DisplayStore( bot, e, e.Args[0] );
                    else
                        bot.SendReply(e, "Unknown user: " + e.Args[0] );
                }
                else
                    bot.SendReply(e, "Correct usage: /tell " +
                                  bot.Character + " !store show [username] <a href='chatcmd:///tell " + bot.Character + " help store show'>Click for help</a>");
                break;

            case "store search":

                // search database for provided string

                SearchStore( bot, e );

                break;
            }
        }

        private string[] ReformatArgs( CommandArgs e, ref int n_items )
        {
            string[] temp = new string[e.Args.Length];
            int i=0;
            int n=0;
            n_items = 0;
            while ( i < e.Args.Length )
            {
                if ( e.Args[i].IndexOf( "<a" ) < 0 )
                    temp[n++] = e.Args[i++];
                else if ( e.Args[i].IndexOf( "<a" ) >= 0 )
                {
                    temp[n] = e.Args[i++];
                    while ( e.Args[i].IndexOf( "</a>" ) < 0 )
                        temp[n] += " " + e.Args[i++];
                    temp[n] += " " + e.Args[i++];
                    n++;
                    n_items++;
                }
            }
            Array.Resize( ref temp, n );
            return temp;
        }

        private string SearchItem( AoItem item, string seller )
        {
            string itemstring = string.Empty;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM " + this._table +
                    " WHERE seller = '" + seller + "' AND lowid = " +
                    item.LowID + " AND highid = " + item.HighID +
                    " AND ql = " + item.QL + " ORDER BY lowid, ql";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    itemstring =
                        Convert.ToString(reader.GetInt32(0)) + "|" +  // lowid
                        Convert.ToString(reader.GetInt32(1)) + "|" +  // highid
                        Convert.ToString(reader.GetInt32(2)) + "|" +  // ql
                        Convert.ToString(reader.GetInt32(3)) + "|" +  // price
                        Convert.ToString(reader.GetInt32(4)) + "|" +  // qty
                        Convert.ToString(reader.GetString(5)) + "|" + // seller
                        Convert.ToString(reader.GetString(6)) + "|" + // name
                        Convert.ToString(reader.GetInt32(7));         // iconid
                }
                reader.Close();
            }
            return itemstring;
        }

        private void AddItem( AoItem item, string seller )
        {
            this. AddItem( item, seller, 0, 1 );
        }
        private void AddItem( AoItem item, string seller,
                              int price )
        {
            this. AddItem( item, seller, price, 1 );
        }
        private void AddItem( AoItem item, string seller,
                              int price, int qty )
        {
            string name = string.Empty;
            Int32 iconid = 0;
            LookUp( item.LowID, item.QL, ref iconid, ref name );

            // check if item is already in store
            string itemstring = SearchItem ( item, seller );
            if ( itemstring != string.Empty )
            {
                // change item properties and increment quantity
                string[] item_info = itemstring.Split('|');
                string update_command =
                    string.Format("UPDATE {0} SET price = {1}, quantity = {2} WHERE lowid = {3} AND highid = {4} AND ql = {5} AND seller = \"{6}\"",
                                  this._table, price,
                                  qty + Convert.ToInt32(item_info[4]) ,
                                  item.LowID, item.HighID, item.QL,
                                  item_info[5] );
                this._database.ExecuteNonQuery( update_command );
            }
            else
            {
                string insert_command =
                    string.Format("INSERT INTO {0} ( lowid, highid, ql, price, quantity, seller, name, iconid ) VALUES ( {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', {8} )",
                                  this._table, item.LowID, item.HighID, item.QL,
                                  price, qty, seller, name, iconid );
                this._database.ExecuteNonQuery( insert_command );
            }
        }

        private string[] GetUserStore( string seller )
        {
            string[] store_items = new string[this._store_max];
            int n = 0;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM " + this._table +
                    " WHERE seller = '" + seller + "' ORDER BY lowid, ql";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    store_items[n] =
                        Convert.ToString(reader.GetInt32(0)) + "|" +  // lowid
                        Convert.ToString(reader.GetInt32(1)) + "|" +  // highid
                        Convert.ToString(reader.GetInt32(2)) + "|" +  // ql
                        Convert.ToString(reader.GetInt32(3)) + "|" +  // price
                        Convert.ToString(reader.GetInt32(4)) + "|" +  // qty
                        Convert.ToString(reader.GetString(5)) + "|" + // seller
                        Convert.ToString(reader.GetString(6)) + "|" + // name
                        Convert.ToString(reader.GetInt32(7));         // iconid
                    n++;
                }
                reader.Close();
            }
            Array.Resize( ref store_items, n );
            return store_items;
        }

        private int LookUp( Int32 lowid, Int32 ql,
                            ref Int32 iconid, ref string name )
        {
            
            string xml = HTML.GetHtml( string.Format( "http://itemxml.xyphos.com/?id={0}&ql={1}", lowid, ql ), 60000 );

            if ( xml.IndexOf ( "<error>" ) >= 0 )
                return 0;

            string name_match = "<name>";
            name = xml.Substring( xml.IndexOf( name_match ) +
                                         name_match.Length );
            name = name.Remove( name.IndexOf( "<" ) );

            string icon_match = "name=\"Icon\"";
            string icon = xml.Substring( xml.IndexOf( icon_match ) +
                                         icon_match.Length );
            icon = icon.Remove( 0, icon.IndexOf( "value=\"" ) + 7 );
            icon = icon.Remove( icon.IndexOf( "\"" ) );

            iconid = Convert.ToInt32( icon );

            return 1;
        }

        private string PriceString( Int32 iprice )
        {
            string price_string = "accepting offers";
            if ( iprice > 0 )
            {
                double exp = Math.Log10( iprice );
                string mod = string.Empty;
                if ( exp >= 9 )
                {
                    iprice /= 1000000000; 
                    mod = "B";
                }
                else if ( exp >= 6 )
                {
                    iprice /= 1000000; 
                    mod = "M";
                }
                else if ( exp >= 3 )
                {
                    iprice /= 1000;
                    mod = "K";
                }
                price_string = Convert.ToString( iprice ) + mod;
            }
            return price_string;
        }
            
        private void AppendDBItem( string itemstring, ref RichTextWindow window,
                                   int n, int owner )
        {
            string[] item_info = itemstring.Split('|');

            string price_string = PriceString( Convert.ToInt32(item_info[3]) );

            // item number
            if ( ( n > 0 ) && ( owner > 0 ) )
                window.AppendHighlight( Convert.ToString(n) + ". " );

            // buy or shop management
            if ( owner == 0 )
            {
                string negotiated_price = string.Empty;
                if ( price_string == "accepting offers" )
                    negotiated_price = ". Please reply to negotiate a price.";
                else
                    negotiated_price = " for " + price_string + ".";
                window.AppendNormal( "[" );
                window.AppendCommand( "Buy", "/tell " + item_info[5] +
                                      " I would like to buy your QL " +
                                      Convert.ToInt32( item_info[2] ) + " " +
                                      item_info[6] + negotiated_price );
                window.AppendNormal( "]" );
            }
            else
            {
                int qty = -1;
                if ( Convert.ToInt32( item_info[4] ) > 1 )
                    qty = Convert.ToInt32( item_info[4] ) - 1;
                window.AppendNormal( "[" );
                window.AppendBotCommand( "Sold", "store stock " +
                                         Convert.ToString(n) +
                                         " " + item_info[3] +
                                         "  " + Convert.ToString(qty) );
                window.AppendNormal( "]" );
            }

            // icon 
            window.AppendIcon( Convert.ToInt32(item_info[7]) );

            // quantity (if user wishes it to be displayed)
            if ( Convert.ToInt32( item_info[4] ) > 0 )
                window.AppendNormal( " " + item_info[4] + " X " );

            // QL
            window.AppendNormal( "QL " + item_info[2] + " " );

            // item link
            window.AppendItem( item_info[6],
                               Convert.ToInt32( item_info[0] ),
                               Convert.ToInt32( item_info[1] ),
                               Convert.ToInt32( item_info[2] ) );

            // price
            window.AppendNormal( " (" + price_string + ")" );
            window.AppendLineBreak();

        }

        private void AppendDBItem( string itemstring, ref RichTextWindow window,
                                   int owner )
        {
            AppendDBItem( itemstring, ref window, 0, owner );
        }

        private void AppendDBItems( string[] store_items,
                                    ref RichTextWindow window, int owner )
        {
            int i=0;
            for( i=0; i<store_items.Length; i++ )
                AppendDBItem( store_items[i], ref window, i+1, owner );
        }

        private void DisplayStore(BotShell bot, CommandArgs e, string seller )
        {
            int owner = 0;
            if ( e.Sender == seller )
                owner = 1;
            string[] store_items = GetUserStore( seller );
            if ( store_items.Length == 0 )
            {
                bot.SendReply( e, seller +
                               " does not have any items to sell." );
                return;
            }
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle( bot.Character + " Marketplace" );
                window.AppendImage("GFX_GUI_FRIENDLIST_SPLITTER");
                window.AppendLineBreak();
                window.AppendHighlight( seller + "'s Store" );
                window.AppendLineBreak();
                window.AppendImage("GFX_GUI_FRIENDLIST_SPLITTER");
                window.AppendLineBreak();
                AppendDBItems( store_items, ref window, owner );
                if ( e.Sender == seller )
                {
                    window.AppendLineBreak();
                    window.AppendNormal( "For more information on how to manage your store ");
                    window.AppendBotCommand( "click here", "!help store" );
                    window.AppendNormal( "." );
                }
                bot.SendReply(e, "View " + seller + "'s Store »» ", window);
            }
        }

        private void StockStore(BotShell bot, CommandArgs e, string[] rArgs )
        {
            // we have some number of items specifed, and they may or may not
            // have prices and quantities...
            int iter = 0;
            Int32 price = 0;
            Int32 quantity = 1;
            int item_iter = 0;
            string[] store_items = GetUserStore( e.Sender );            
            int max_items = this._store_max - store_items.Length;
            if ( max_items <= 0 )
            {
                bot.SendReply(e, "Sorry, your store is full.  Please remove some items and try again.");
                return;
            }
            while ( ( iter < rArgs.Length ) && ( item_iter < max_items ) )
            {
                if ( rArgs[iter].IndexOf( "<a" ) >= 0 )
                {
                    price = 0;
                    quantity = 1;
                    if ( ( iter+1 < rArgs.Length ) &&
                         ( rArgs[iter+1].IndexOf( "<a" ) < 0 ) )
                        price = Convert.ToInt32(rArgs[++iter]);
                    if ( ( iter+1 < rArgs.Length ) &&
                         ( rArgs[iter+1].IndexOf( "<a" ) < 0 ) )
                        quantity = Convert.ToInt32(rArgs[++iter]);
                    AddItem( e.Items[item_iter], e.Sender, price, quantity );
                    string name = string.Empty;
                    Int32 iconid = 0;
                    if ( LookUp ( e.Items[item_iter].LowID,
                                  e.Items[item_iter].QL,
                                  ref iconid, ref name ) != 0 )
                        bot.SendReply( e, name + " added to your store. <a href='chatcmd:///tell " + bot.Character + " !store'>Click to view</a>" );
                    else
                        bot.SendReply( e, "Unknown item added to your store. <a href='chatcmd:///tell " + bot.Character + " !store'>Click to view</a>" );
                    item_iter++;
                    iter ++;
                }
            }
        }

        private void ModifyItem(BotShell bot, CommandArgs e,
                                int lowid, int ql,
                                Int32 price, Int32 quantity )
        {
            string[] store_items = GetUserStore( e.Sender );
            if ( store_items.Length == 0 ) 
            {
                bot.SendReply(e, "Error. No items in your store.");
                return;
            }

            int item_number = 0;
            int i;
            for( i=0; i<store_items.Length; i++ )
            {
                string[] tmp_info = store_items[i].Split('|');
                if ( ( Convert.ToInt32(tmp_info[0]) == lowid ) &&
                     ( Convert.ToInt32(tmp_info[2]) == ql ) )
                    item_number = i+1;
            }

            if ( ( item_number <= 0 ) ||
                 ( item_number > store_items.Length ) )
            {
                bot.SendReply(e, "Error. Specified item is not in your store.");
                return;
            }
            string[] item_info = store_items[item_number-1].Split('|');

            if ( quantity >= 0 )
            {
                string update_command =
                    string.Format("UPDATE {0} SET price = {1}, quantity = {2} WHERE lowid = {3} AND highid = {4} AND ql = {5} AND seller = \"{6}\"",
                                  this._table, price, quantity,
                                  Convert.ToInt32(item_info[0]),
                                  Convert.ToInt32(item_info[1]),
                                  Convert.ToInt32(item_info[2]),
                                  item_info[5]);
                this._database.ExecuteNonQuery( update_command );
                bot.SendReply(e, "Price (and quantity) set for " + item_info[6] + ". <a href='chatcmd:///tell " + bot.Character + " !store'>Click to view</a>" );
            }
            else
            {
                string delete_command =
                    string.Format("DELETE FROM {0} WHERE lowid = {1} AND highid = {2} AND ql = {3} AND seller = \"{4}\"",
                                  this._table,
                                  Convert.ToInt32(item_info[0]),
                                  Convert.ToInt32(item_info[1]),
                                  Convert.ToInt32(item_info[2]),
                                  item_info[5]);
                this._database.ExecuteNonQuery( delete_command );
                bot.SendReply(e,  item_info[6] +
                              " has been removed from store.  <a href='chatcmd:///tell " + bot.Character + " !store'>Click to iew</a>" );
            }

        }

        private void SearchStore( BotShell bot, CommandArgs e )
        {
            Int32 loql = 0;
            Int32 hiql = 0;
            string search_string = "%";
            int arg = 0;

            // get QL or QL range if specied
            try
            {
                loql = Convert.ToInt32(e.Args[arg]);
                if (loql <= 0 || loql >= 1000)
                {
                    bot.SendReply(e, "Quality level has to be between 0 and 999");
                    return;
                }
                arg++;
            }
            catch { }

            try
            {
                hiql = Convert.ToInt32(e.Args[arg]);
                if (hiql <= 0 || hiql >= 1000)
                {
                    bot.SendReply(e, "Quality level has to be between 0 and 999");
                    return;
                }
                arg++;
            }
            catch { }

            // compose search string

            int i = 0;
            for ( i = arg; i<e.Args.Length; i++ )
                search_string += e.Args[i] + "%";

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( bot.Character + " Marketplace" );
            window.AppendImage("GFX_GUI_FRIENDLIST_SPLITTER");
            window.AppendLineBreak();

            int n = 0;

            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                if ( ( loql == 0 ) && ( hiql == 0 ) )
                    command.CommandText =
                        string.Format("SELECT * FROM {0} WHERE name LIKE '{1}'",
                                      this._table, search_string );
                else if ( ( loql > 0 ) && ( hiql == 0 ) )
                    command.CommandText =
                        string.Format("SELECT * FROM {0} WHERE name LIKE '{1}' AND ql = {2}",
                                      this._table, search_string,
                                      Convert.ToString(loql) );
                else
                    command.CommandText =
                        string.Format("SELECT * FROM {0} WHERE name LIKE '{1}' AND ql BETWEEN {2} AND {3}",
                                      this._table, search_string,
                                      Convert.ToString(loql),
                                      Convert.ToString(hiql) );
                    
                IDataReader reader = command.ExecuteReader();
                while ( ( reader.Read() ) && ( n < this._search_max ) )
                {

                    string price_string = PriceString( reader.GetInt32(3) );
                    
                    // buy / view store
                    string negotiated_price = string.Empty;
                    if ( price_string == "accepting offers" )
                        negotiated_price = ". Please reply to negotiate a price.";
                    else
                        negotiated_price = " for " + price_string + ".";
                    window.AppendNormal( "[" );
                    window.AppendCommand( "Buy", "/tell " +
                                          reader.GetString(5) +
                                          " I would like to buy your QL " +
                                          Convert.ToString(reader.GetInt32(2)) +
                                          " " + reader.GetString(6) +
                                          negotiated_price );
                    window.AppendNormal( "]" );
                    window.AppendNormal( "[" );
                    window.AppendBotCommand( "More", "store show " +
                                          reader.GetString(5) );
                    window.AppendNormal( "]" );

                    // icon 
                    window.AppendIcon( reader.GetInt32(7) );

                    // quantity (if user wishes it to be displayed)
                    if ( reader.GetInt32(4) > 0 )
                        window.AppendNormal( " " +
                                             Convert.ToString(reader.GetInt32(4)) +
                                             " X " );

                    // QL
                    window.AppendNormal( "QL " +
                                         Convert.ToString(reader.GetInt32(2))
                                         + " " );

                    // item link
                    window.AppendItem( reader.GetString(6),
                                       reader.GetInt32(0),
                                       reader.GetInt32(1),
                                       reader.GetInt32(2) );

                    // price
                    window.AppendNormal( " (" + price_string + ")" );
                    window.AppendLineBreak();

                    n++;
                }
                reader.Close();
            }

            bot.SendReply(e,
                          HTML.CreateColorString(bot.ColorHeaderHex,
                                                 Convert.ToString(n)) +
                          " Results »» ", window);

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
            case "store":
                return "Without any arguments, this command will display your own store (if you have one).  If you drop an item to the store command, it will work the same as the &quot;store add&quot; command.  If you specify a player's name, then it will work the same as &quot;store show&quot;.  When you view your own store you will have the option to click and remove items from your store.  When you view someone else's store, you will be able to click on a link to send that player a message indicating that you would like to purchase an item.\n\n Other commands: \n<a href='chatcmd:///tell " + bot.Character + " !help store add'>store add</a>\n<a href='chatcmd:///tell " + bot.Character + " !help store remove'>store remove</a>\n<a href='chatcmd:///tell " + bot.Character + " !help store stock'>store stock</a>\n<a href='chatcmd:///tell " + bot.Character + " !help store show'>store show</a>\n<a href='chatcmd:///tell " + bot.Character + " !help store search'>store search</a>\n";

            case "store add":
                return "Add items to your store.  The basic usage is: \n\n<font color=#00FF00>store add item [price [quantity]] [item ...]</font>\n\nIf you do not specify a price, the store will indicate that you are willing to entertain offers on this item.  If you specify a quantity, you will indicate how many of the dropped item you have for sale.";

            case "store remove":
                return "Removes items from your store.  The basic usage is: \n\n<font color=#00FF00>store remove item_number|all</font>\n\nYou can obtain the item number for the item you would like to remove by viewing your own store.  If you use the word &quote;all&quote; instead of an item number, all the items in your store will be removed.";

            case "store stock":
                return "Modifies the price or quantity of items in your store.  The basic usage is: \n\n<font color=#00FF00>store stock item_number price [quantity]</font>\n\nIf you do not specify a quantity, the quantity will default to one.  If you set the quantity to zero, the item will not be removed from your store, instead the quantity will not be displayed.";

            case "store show":
                return "Displays the items in a named individual's store.  The basic usage is: \n\n<font color=#00FF00>store show player_name</font>";

            case "store search":
                return "Searches the store for a particular item.  The basic usage is: \n\n<font color=#00FF00>store search [lowql [highql]] search string</font>\n\nIf you specify a QL, then only items that match that exact QL will be displayed.  If you specify two QLs, then items that fall within the range of those QL's will be displayed.";

            }
            return null;
        }
    }
}
