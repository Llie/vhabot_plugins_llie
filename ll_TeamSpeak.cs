using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Timers;
using AoLib.Utils;

// Additional Installation instructions for TS version 3:

// In order for third-party apps to perform a serverinfo query to the
// new TS3 server, you must first give the "Guest" group permission to
// do so.  It can be done as follows via command line:
                        
// connect to your ts3 server (example here is using telnet, but you
// might use a different method):

// > telnet localhost 10011

// issue the following commands to the TS server

// login client_login_name=serveradmin client_login_password={your server admin password here}
// use sid={your virtual server id number here}
// servergroupaddperm sgid=1 permid=13 permvalue=1 permnegated=0 permskip=0
// servergroupaddperm sgid=1 permid=23 permvalue=1 permnegated=0 permskip=0

namespace VhaBot.Plugins
{
    public class TeamSpeak : PluginBase
    {
        private string Server = "localhost"; 
        private Int32 Port = 0;
        private Int32 VSID = 1;
        private Int32 TSVersion = 3;
        private string Password = "";

        private Int32 CommTimeout = 500;
        private Int32 CommWait = 10;
        private bool ChanList = false;

        private BotShell _bot;
        private System.Timers.Timer _welcome;

        public TeamSpeak()
        {
            this.Name = "TeamSpeak Server Status";
            this.InternalName = "llTeamSpeak";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 105;

            this.Commands = new Command[] {
                new Command("ts", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            if ( Type.GetType ("Mono.Runtime") != null )
                CommWait = 500;

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.String, this.InternalName, "server", "Teamspeak Server Address", this.Server);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "port", "Teamspeak Server Port", this.Port);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "version", "Teamspeak Server version", this.TSVersion, 2, 3);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "vsid", "Virtual server ID(TS3)/Chat port(TS2)", this.VSID);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "password", "Teamspeak Server Password (if needed)", this.Password);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "chanlist", "Query channel listing", this.ChanList );

            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "timeout", "Advanced: Server query timeout (milliseconds)", this.CommTimeout, 100, 500, 1000, 2000 );
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "wait", "Advanced: Server query time to wait for more data (milliseconds)", this.CommWait, 10, 100, 500, 1000, 2000 );

            this.LoadConfiguration(bot);

            bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);

            this._bot = bot;

            this._welcome = new System.Timers.Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;

        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= TSWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += TSWindow;
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
                this._bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            this._bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= TSWindow;
             bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this.Server = bot.Configuration.GetString(this.InternalName, "server", this.Server);
            this.Port = bot.Configuration.GetInteger(this.InternalName, "port", this.Port);
            this.TSVersion = bot.Configuration.GetInteger(this.InternalName, "version", this.TSVersion);
            this.VSID = bot.Configuration.GetInteger(this.InternalName, "vsid", this.VSID);
            this.Password = bot.Configuration.GetString(this.InternalName, "password", this.Password);
            this.ChanList = bot.Configuration.GetBoolean(this.InternalName, "chanlist", this.ChanList);

            this.CommTimeout = bot.Configuration.GetInteger(this.InternalName, "timeout", this.CommTimeout);
            this.CommWait = bot.Configuration.GetInteger(this.InternalName, "wait", this.CommWait);

        }

        private int ParseClients ( string window )
        {
            int idx0 = window.IndexOf( "Users Currently Online" );
            int num = -1;
            if ( idx0 >= 0 )
            {
                string substr = window.Substring( idx0 + 51 );
                int idx1 = substr.IndexOf( " " );
                substr = substr.Substring( 0, idx1 );
                Int32.TryParse( substr, out num );
            }
            return num;
        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return;
            RichTextWindow window = null;
            TSWindow( ref window, e.Sender );
            int num = ParseClients ( window.ToString() );
            if ( num >= 0 )
                bot.SendPrivateMessage(e.Sender, Convert.ToString(num) + " users on TeamSpeak »» " + window);
        }

        private void UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            RichTextWindow window = null;
            TSWindow( ref window, e.Sender );
            int num = ParseClients ( window.ToString() );
            if ( num >= 0 )
                bot.SendPrivateMessage(e.Sender, Convert.ToString(num) + " users on TeamSpeak »» " + window);
        }

        public List<string> GetResponseElements( string responseData,
                                                 string keyword )
        {
            List<string> elements = new List<string>();
            
            Int32 idx = 0;
            while ( responseData.IndexOf( keyword, idx) >= 0 )
            {
                idx = responseData.IndexOf( keyword, idx );
                string element = responseData.Substring( idx + keyword.Length );
                element = element.Substring( 0, element.IndexOf(" "));
                element = element.Replace( "\\s", " " );
                elements.Add( element );
                idx += keyword.Length + element.Length+1;
            }
            return elements;
        }

        private string GetResponseElement( string response, string keyword )
        {
            string value = "";
            if ( response.IndexOf(keyword) >= 0 )
            {
                value = response.Substring( response.IndexOf(keyword) +
                                            keyword.Length );
                if ( ( this.TSVersion == 3 ) && ( value.IndexOf(" ") > 0 ) )
                        value = value.Substring( 0, value.IndexOf(" "));
                else if ( ( this.TSVersion == 2 ) &&
                          ( value.IndexOf("\n") > 0 ) )
                    value = value.Substring( 0, value.IndexOf("\n")-1);
            }
            else
            {
                Console.WriteLine("[TeamSpeak] Error: Keyword: " + keyword + " not found!");
            }
            return value;
        }

        private Int32 GetResponseValue( string response, string keyword )
        {
            string value = "";
            if ( response.IndexOf(keyword) >= 0 )
            {
                value = response.Substring( response.IndexOf(keyword) +
                                            keyword.Length );
                if ( ( this.TSVersion == 3 ) && ( value.IndexOf(" ") > 0 ) )
                        value = value.Substring( 0, value.IndexOf(" "));
                else if ( ( this.TSVersion == 2 ) &&
                          ( value.IndexOf("\n") > 0 ) )
                    value = value.Substring( 0, value.IndexOf("\n"));
            }
            else
            {
                Console.WriteLine("[TeamSpeak] Error: Keyword: " + keyword + " not found!");
            }
            return Convert.ToInt32(value);
        }

        // private string GetResponseString( string response, string keyword
        //     Int32 protocol_version ) {
        //     string value = response.Substring(
        //         response.IndexOf(keyword) + keyword.Length );
        //     switch ( protocol_version )
        //     {
        //     case 2:
        //         // return everything between [ and ]
        //         value = value.Substring( 1, value.IndexOf("]") );
        //         break;
        //     case 3:
        //         // replace /s with spaces
        //         // handle [URL]?
        //         value = value.Substring( 0, value.IndexOf(" "));
        //         break;
        //     }
        //     return value
        // }

        private int QueryPort()
        {

            Int32 QueryPort = 0;
            if (this.Port > 0)
                QueryPort = Port;
            else
                switch ( TSVersion )
                {
                case 2:
                    QueryPort = 51234;
                    break;
                case 3:
                    QueryPort = 10011;
                    break;
                }

            return QueryPort;

        }

        private string ServerQuery( NetworkStream stream,
                                    string sendMessage )
        {
            Byte[] data = null;
            if ( !string.IsNullOrEmpty(sendMessage) )
            {
                data = System.Text.Encoding.ASCII.GetBytes(sendMessage);
                stream.Write( data, 0, data.Length);
            }
            data = new byte[4096];
            Int32 res = 0;

            if(stream.CanRead)
            {
                StringBuilder responseData = new StringBuilder();
                do
                {
                    res = stream.Read(data, 0, data.Length);
                    responseData.AppendFormat("{0}", Encoding.ASCII.GetString(data, 0, res));
                    // Network stream read doesn't always get all of
                    // TS response.  This slight pause causes the read
                    // to wait for more data
                    System.Threading.Thread.Sleep(CommWait);       
                }
                while(stream.DataAvailable);
                return responseData.ToString();
            }
            else
            {
                return string.Empty;
            }

        }

        private bool ResponseOk ( string responseData )
        {
            if( responseData.IndexOf( "msg=ok" ) >= 0 )
                return true;
            return false;
        }

        public int GetClients( out List<string> Clients )
        {
            TcpClient sockclient = new TcpClient( Server, QueryPort() );
            NetworkStream stream = sockclient.GetStream();
            stream.ReadTimeout = CommTimeout;
            string sendMessage = string.Empty;
            string responseData = string.Empty;

            if ( TSVersion == 3 )
            {

                // pull off initial TS response
                responseData = ServerQuery( stream, sendMessage );

                // set server ID
                sendMessage = "use " + this.VSID +  "\n";
                responseData = ServerQuery( stream, sendMessage );
                if ( ResponseOk( responseData ) )
                {
                    sendMessage = "clientlist\n";
                    responseData = ServerQuery( stream, sendMessage );
                    Clients = GetResponseElements( responseData, "client_nickname=" );
                }
                else
                    Clients = new List<string>();
            }
            else // TS 2
            {
                Clients = new List<string>();
                sendMessage = "pl\n";
                responseData = ServerQuery( stream, sendMessage );
                string[] list = responseData.Split('\n');
                for ( int i=1; i<list.Length+1; i++ )
                {
                    string[] line = list[i].Split('\t');
                    string nick = line[15].Replace( "\"", "" );
                    Clients.Add(nick);
                }
            }
            return Clients.Count;
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {
                case "ts":

                    try
                    {

                        RichTextWindow window = null;
                        TSWindow( ref window, e.Sender );
                        bot.SendReply(e, "TeamSpeak Server Info »» ", window);
                        
                    }
                    catch
                    {
                        bot.SendReply(e, "Cannot connect to Teamspeak server, please check your settings and try again or the Teamspeak server may be down.");
                        return;
                    }

                    break;
            }
        }

        public void TSWindow( ref RichTextWindow window, string user )
        {

            bool new_window = false;
            if ( window == null )
            {
                window = new RichTextWindow(_bot);
                window.AppendTitle("::::: Teamspeak Server Info :::::");
                new_window = true;
            }
            else
                window.AppendHeader( "Teamspeak Server Info" );
            window.AppendLineBreak();

            TcpClient sockclient = new TcpClient(Server, QueryPort());

            NetworkStream stream = sockclient.GetStream();
            stream.ReadTimeout = CommTimeout;

            string sendMessage = String.Empty;
            string responseData = String.Empty;

            // read initial response
            responseData = ServerQuery( stream, string.Empty );
            responseData = responseData.Trim();

            if ( ( TSVersion == 3 ) &&
                 ( responseData.IndexOf( "TS3" ) == 0 ) )
            {

                sendMessage = "use " + this.VSID +  "\n";
                responseData = ServerQuery( stream, sendMessage );
                if ( ! ResponseOk( responseData ) )
                {
                    window.AppendHighlight( "Server ID invalid.  Please check configuration.");
                    return;
                }

                // get channels (if available)
                Dictionary<string,string> chan_map =
                    new Dictionary<string,string>();
                if ( this.ChanList )
                {
                    sendMessage = "channellist\n";
                    responseData = ServerQuery( stream,
                                                sendMessage );
                    if ( ResponseOk( responseData ) )
                    {
                        List<string> channel_list =
                            GetResponseElements( responseData,
                                                 "channel_name=" );
                        List<string> channel_ids =
                            GetResponseElements( responseData,
                                                 "cid=" );
                        for( int i=0; i<channel_list.Count; i++ )
                            chan_map.Add( channel_ids[i],
                                          channel_list[i] );
                    }
                }

                sendMessage = "serverinfo\n";
                responseData = ServerQuery( stream, sendMessage );
                if ( ! ResponseOk( responseData ) &&
                     ( responseData.IndexOf( "insufficient\\sclient\\spermissions" )>=0) )
                {
                    window.AppendLineBreak();
                    window.AppendHighlight( "Connect to your TS server and issue the following commands:" );
                    window.AppendLineBreak(2);
                    window.AppendNormal( "login client_login_name=serveradmin client_login_password={your server admin password here}" );
                    window.AppendLineBreak();
                    window.AppendNormal( "use sid={your virtual server id number here}" );
                    window.AppendLineBreak();
                    window.AppendNormal( "servergroupaddperm sgid=1 permid=13 permvalue=1 permnegated=0 permskip=0" );
                    window.AppendNormal( "servergroupaddperm sgid=1 permid=23 permvalue=1 permnegated=0 permskip=0" );
                                
                    return;
                }
                else if ( ! ResponseOk( responseData ) )
                {
                    window.AppendHighlight( "Unexpected response to query.  Are you sure you are running a TS version 3?" );
                    return;
                }

                // does this server require a password?
                Int32 requires_password = GetResponseValue(
                    responseData, "virtualserver_flag_password=" );

                // get chat/UDP port
                string voice_port = GetResponseElement(
                    responseData, "virtualserver_port=" );

                // clients online -- counts this query as 1
                string clients_online = GetResponseElement(
                    responseData, "virtualserver_clientsonline=" );
                Int32 nclients = Convert.ToInt32(clients_online)-1;

                // max clients
                string max_clients = GetResponseElement(
                    responseData, "virtualserver_maxclients=" );

                // uptime in milliseconds
                Int32 uptime = GetResponseValue( responseData,
                                                 "virtualserver_uptime=");
                Int32 uptime_hours = Convert.ToInt32 (Math.Floor(uptime/3600000.0));
                Int32 uptime_minutes = Convert.ToInt32 (Math.Floor((uptime%3600000)/60000.0));
                double uptime_seconds = ((uptime%3600000)%60)/1000.0;

                // platform
                string platform = GetResponseElement(
                    responseData, "virtualserver_platform=" );

                // parse welcome message?
                // anything else we want?
                // virtualserver_name=

                // format output
                window.AppendLineBreak();
                window.AppendHighlight("Server: ");
                window.AppendNormal(this.Server + " (" + platform + ")" );
                window.AppendLineBreak();
                window.AppendHighlight("Port: ");
                window.AppendNormal(voice_port);
                window.AppendLineBreak();
                window.AppendHighlight("Server Version: ");
                window.AppendNormal(Convert.ToString(TSVersion));
                if ( requires_password == 1 )
                {
                    window.AppendLineBreak();
                    window.AppendHighlight("Password: ");
                    window.AppendNormal(Password);
                }
                window.AppendLineBreak();
                window.AppendHighlight("VirtualServer ID: ");
                window.AppendNormal(Convert.ToString(this.VSID));
                window.AppendLineBreak();
                window.AppendHighlight("Server Uptime: ");
                window.AppendNormal(Convert.ToString(uptime_hours) +
                                    ":" +
                                    Convert.ToString(uptime_minutes) +
                                    ":" +
                                    Convert.ToString(uptime_seconds));
                window.AppendLineBreak();
                window.AppendHighlight("Users Currently Online: ");
                window.AppendNormal(Convert.ToString( nclients ) + " out of " + max_clients );
                if ( nclients > 0 )
                {
                    window.AppendNormal( " ( Users: " );
                    // send query clientlist to get client list:
                    // clid=1 cid=1 client_database_id=2 client_nickname=Llie client_type=0|clid=2 cid=1 client_database_id=3 client_nickname=Unknown\sfrom\s127.0.0.1:52774 client_type=1
                    sendMessage = "clientlist\n";
                    responseData = ServerQuery( stream, sendMessage );
                    List<string> Clients = GetResponseElements(
                        responseData, "client_nickname=" );
                    List<string> Cid = GetResponseElements(
                        responseData, "cid=" );

                    for ( Int32 i=0; i<nclients+1; i++ )
                        if (Clients[i].IndexOf("Unknown from")!=0)
                        {
                            window.AppendNormal(Clients[i]);
                            if ( chan_map.ContainsKey(Cid[i]) )
                                window.AppendNormal( " - " + chan_map[Cid[i]] );
                            if ( i < nclients-1 )
                                window.AppendNormal(", ");
                        }
                    window.AppendNormal( " ) " );
                }
                window.AppendLineBreak(2);
                window.AppendNormal( "Visit " );
                window.AppendCommand( "http://www.teamspeak.com/?page=downloads", "/start http://www.teamspeak.com/?page=downloads" );
                window.AppendNormal( " to download a TeamSpeak client");

            }
            else if ( ( TSVersion == 2 ) &&
                      ( responseData.IndexOf( "[TS]" ) == 0 ) )
            {

                sendMessage = "sel " + this.VSID +  "\n";
                responseData = ServerQuery( stream, sendMessage );

                // probably should check for "OK" here

                sendMessage = "si\n";
                responseData = ServerQuery( stream, sendMessage );

                // does this server require a password?
                Int32 requires_password = GetResponseValue(
                    responseData, "server_password=" );

                // get chat/UDP port
                string voice_port = GetResponseElement(
                    responseData, "server_udpport=" );

                // current users
                string clients_online = GetResponseElement(
                    responseData, "server_currentusers=" );
                Int32 nclients = Convert.ToInt32(clients_online);

                // max clients
                string max_clients = GetResponseElement(
                    responseData, "server_maxusers=" );

                // uptime in seconds
                Int32 uptime = GetResponseValue( responseData,
                                                 "server_uptime=");
                Int32 uptime_hours = Convert.ToInt32 (Math.Floor(uptime/3600.0));
                Int32 uptime_minutes = Convert.ToInt32 (Math.Floor((uptime%3600)/60.0));
                Int32 uptime_seconds = (uptime%3600)%60;

                // platform
                string platform = GetResponseElement(
                    responseData, "server_platform=" );

                // format output
                window.AppendLineBreak();
                window.AppendHighlight("Server: ");
                window.AppendNormal(this.Server + " (" + platform + ")" );
                window.AppendLineBreak();
                window.AppendHighlight("Port: ");
                window.AppendNormal(voice_port);
                window.AppendLineBreak();
                window.AppendHighlight("Server Version: ");
                window.AppendNormal(Convert.ToString(TSVersion));
                if ( requires_password == 1 )
                {
                    window.AppendLineBreak();
                    window.AppendHighlight("Password: ");
                    window.AppendNormal(Password);
                }
                window.AppendLineBreak();
                window.AppendHighlight("Server Uptime: ");
                window.AppendNormal(Convert.ToString(uptime_hours) +
                                    ":" +
                                    Convert.ToString(uptime_minutes) +
                                    ":" +
                                    Convert.ToString(uptime_seconds));
                window.AppendLineBreak();
                window.AppendHighlight("Users Currently Online: ");
                window.AppendNormal(Convert.ToString( nclients ) + " out of " + max_clients );
                if ( nclients > 0 )
                {
                    window.AppendNormal( " ( Users: " );
                    // send query clientlist to get client list:
                    //p_idc_idpsbsprbrplpinglogintimeidletimecprivspprivspflagsipnickloginname
                    //17396223800249153782381110694420592117916"0.0.0.0""veremit2""veremit2"

                    sendMessage = "pl\n";
                    responseData = ServerQuery( stream, sendMessage );
                    Int32 i = 0;
                    // strip header line from response data
                    // responseData = responseData.Substring(
                    //     responseData.IndexOf("\n") + 1 );
                    string[] list = responseData.Split('\n');
                    for ( i=1; i<nclients+1; i++ )
                    {
                        string[] line = list[i].Split('\t');
                        string nick = line[15].Replace( "\"", "" );
                        window.AppendNormal(nick);
                        if ( i < nclients )
                            window.AppendNormal(", ");
                    }
                    window.AppendNormal( " ) " );
                }
                window.AppendLineBreak(2);
                window.AppendNormal( "Visit " );
                window.AppendCommand( "http://www.teamspeak.com/?page=downloads", "/start http://www.teamspeak.com/?page=downloads" );
                window.AppendNormal( " to download a TeamSpeak client");
                        
            }
            else
            {
                window.AppendHighlight("Error: Unable to connect to TeamSpeak Server");
            }

            stream.Close();
            sockclient.Close();            

            if ( ! new_window )
            {
                window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
                window.AppendLineBreak();
            }
            
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "ts":
                    return "Returns TeamSpeak server status.  (Note: If you think you have entered your TS server information correctly, but you are still getting \"Invalid Server ID\" errors, try increasing the communications timeout value in the configuration.)";
            }
            return null;
        }
    }

}
