using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class UniqueMobs : PluginBase
    {
        // private string Server = "unimob.dastof.com";
        // private string Server = "nano.byethost12.com/services";
        // private string Server = "nano.exofire.net/services";
        private string Server = string.Empty;
        private string filename = "data/uniques.xml";
        private string UrlTemplate = "http://{0}/query.php?{1}={2}";
        private int Max = 100;

        public UniqueMobs()
        {
            this.Name = "Unique Mobs Database Lookup";
            this.InternalName = "llUniqueMobs";
            this.Author = "Llie / Vhab";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("boss", true, UserLevel.Guest),
                new Command("umob", "boss"),
                new Command("loot", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "uniques.xml" );
        }
        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            string search_command = "";

            switch (e.Command)
            {
            case "boss":
                search_command = "search";
                break;

            case "loot":
                search_command = "loot";
                    break;
            }

            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: " + e.Command + " [search string]");
                return;
            }

            string exact_search = e.Words[0];
            string search = exact_search.ToLower();
            string xml;
            string result = string.Empty;
            MemoryStream stream = null;
            UniquesResults search_results = null;

            if ( ! File.Exists ( filename ) )
            {
                // no local unique mobs xml file -- query web instead

                string url = string.Format(this.UrlTemplate, this.Server,
                                           search_command,
                                           HttpUtility.UrlEncode(search));
                xml = HTML.GetHtml(url, 60000);
                if (string.IsNullOrEmpty(xml))
                {
                    bot.SendReply(e, "Unable to query the unique mobs database");
                    return;
                }

                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(UniquesResults));
                    search_results = (UniquesResults)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to query the unique mobs database";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            else
            {
                // read local unique mobs xml file

                using(StreamReader rdr = File.OpenText(filename))
                    xml = rdr.ReadToEnd();
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to read file: " + filename );
                    return;
                }

                UniquesResults all_umobs = null;
                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(UniquesResults));
                    all_umobs = (UniquesResults)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to parse the unique mob file";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                search_results = all_umobs;
                search_results.Version = all_umobs.Version;
                search_results.Max = Convert.ToString ( Max );
                search_results.Credits = all_umobs.Credits;

                Regex pattern = null;
                if ( search != string.Empty )
                    pattern = new Regex( ".*" + search.Replace( " ", ".*" ) +
                                         ".*", RegexOptions.IgnoreCase );

                bool is_match;
                Int32 i = 0;
                foreach ( UniqueMob mob in all_umobs.Mobs )
                {
                    is_match = true;
                    if( ( search_command == "search" ) &&
                        ! pattern.IsMatch( mob.name ) )
                        is_match = false;
                    else if ( search_command == "loot" )
                    {
                        is_match = false;
                        foreach (UItemsResults_Item item in mob.Items)
                            if ( pattern.IsMatch( item.Name ) )
                                is_match = true;
                    }
                    if ( is_match )
                        search_results.Mobs[i++] = mob;
                }
                Array.Resize( ref search_results.Mobs, i );
                search_results.Results = Convert.ToString( search_results.Mobs.Length );

            }

            Int32 dr = 0;
            if ( search_results.Mobs.Length > 1 )
                for ( int i=0; i<search_results.Mobs.Length; i++)
                    if ( search_results.Mobs[i].name == exact_search )
                        dr = i;

            if (search_results.Mobs == null || search_results.Mobs.Length == 0)
            {
                result = "No mobs were found.";

                List<string> other_commands = new List<string>();
                if ( bot.Plugins.IsLoaded("llAOUGuides") )
                    other_commands.Add("!locate");
                if ( bot.Plugins.IsLoaded("llNPCs") )
                    other_commands.Add("!npc");
                if ( bot.Plugins.IsLoaded("llWhereis") )
                    other_commands.Add("!whereis");
                if ( other_commands.Count > 0 )
                {
                    result = result + "  Perhaps the location you are looking for might be found in one of the other location databases this bot has access to.  You could try one of these other commands: ";
                    for ( int i=0; i<other_commands.Count; i++ )
                    {
                        result = result + other_commands[i];
                        if ( i < other_commands.Count - 1 )
                            result = result + ", ";
                    }
                }

            }
            else if( ( search_results.Mobs.Length == 1 ) ||
                     ( dr > 0 ) )
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Unique Mobs Database");
                if ( ! File.Exists ( filename ) )
                {
                    window.AppendHighlight("Server: ");
                    window.AppendNormal(this.Server);
                }
                window.AppendLineBreak();
                window.AppendHighlight("Version: ");
                window.AppendNormal(search_results.Version);
                window.AppendLineBreak();
                window.AppendHighlight("Search String: ");
                window.AppendNormal(search);
                window.AppendLineBreak();
                window.AppendHighlight("Results: ");
                window.AppendNormal(search_results.Mobs.Length.ToString() +
                                    " / " + search_results.Max);
                window.AppendLineBreak(2);
                window.AppendHeader("Search Results");
                UniqueMob mob = search_results.Mobs[dr];

                window.AppendHighlight(mob.name + " ");
                window.AppendNormalStart();
                window.AppendLineBreak();
                window.AppendString("    Level: " + mob.level);
                window.AppendLineBreak();
                if ( ( mob.pfid != "NA" ) && ( mob.coords != "?" ) )
                {
                    string coordstr = mob.coords;
                    coordstr = coordstr.Replace ( "x", " " );
                    window.AppendString("    ");
                    window.AppendCommand( "Location: " +
                                          mob.location +
                                          " (" + mob.coords + ")",
                                          "/waypoint " +
                                          coordstr + " " +
                                          mob.pfid );
                }
                else
                { 
                    window.AppendString("    Location: " +
                                        mob.location +
                                        " (" + mob.coords + ")");
                }
                window.AppendLineBreak();
                window.AppendString("    Drops: ");
                window.AppendLineBreak();
                foreach (UItemsResults_Item item in mob.Items)
                {
                    window.AppendHighlight(item.Name + " ");
                    window.AppendNormalStart();
                    window.AppendString("[");
                    window.AppendItem("QL " + item._ql, item.LowID,
                                      item.HighID, item.QL);
                    window.AppendString("] (" + item._droprate + "%)");
                    window.AppendColorEnd();
                    window.AppendLineBreak();
                }
                window.AppendLineBreak();
                window.AppendLineBreak();
                bot.SendReply(e, "Results »» ", window);
                return;
            }
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Unique Mobs Database");
                if ( ! File.Exists ( filename ) )
                {
                    window.AppendHighlight("Server: ");
                    window.AppendNormal(this.Server);
                }
                window.AppendLineBreak();
                window.AppendHighlight("Version: ");
                window.AppendNormal(search_results.Version);
                window.AppendLineBreak();
                window.AppendHighlight("Search String: ");
                window.AppendNormal(search);
                window.AppendLineBreak();
                Int32 i;
                if ( search_command == "loot" )
                    result = "The following bosses might drop the item you're interested in: ";
                else
                    result = "Do you mean one of these?: ";
                for ( i=0; i<search_results.Mobs.Length; i++)
                {
                    result = result + search_results.Mobs[i].name;
                    if ( i < search_results.Mobs.Length - 2 )
                        result = result + ", ";
                    else if ( i < search_results.Mobs.Length - 1 )
                        result = result + ", or ";
                    else
                        result = result + ".";
                    window.AppendNormal( "> " );
                    string clean_name =
                        Regex.Replace(search_results.Mobs[i].name, @"[^\w\s]",
                                      "");
                    window.AppendCommand( search_results.Mobs[i].name,
                                          "/tell " +  bot.Character +
                                          " boss " +
                                          clean_name );
                    window.AppendLineBreak();

                }
                bot.SendReply(e, result + "More Info »» ", window);
                return;
            }
            bot.SendReply(e, result);
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
            case "umob": 
            case "boss":
                return "Allows you to search the vhabot central unique mob database.\n" +
                "To search the items database use: /tell " + bot.Character + " " + command + " [partial mob name]\n" +
                "You don't need to specify the full name of the mob, you can use multiple words in order to find an item.\n" +
                "However, the different words are required to be in the right order to find a certain mob.\n" +
                "For example: 'supply smug' will work but 'smug supply' won't if you want to find Supplymaster Smug";

            case "loot":
                return "Allows you to search the vhabot central unique mob database by items dropped.\n" +
                    "To search the items database use: /tell " + bot.Character + " " + command + " [partial item name]\n" +
                    "You don't need to specify the full name of the item, you can use multiple words in order to find an item.\n" +
                    "However, the different words are required to be in the right order to find a certain item.\n" +
                    "For example: 'comb merc' will work but 'merc comb' won't if you want to find Combined Mercenary's armor";

            }
            return null;
        }
    }

    [XmlRoot("uniques")]
    public class UniquesResults
    {
        [XmlElement("version")]
        public string Version;
        [XmlElement("results")]
        public string Results;
        [XmlElement("max")]
        public string Max;
        [XmlElement("mob")]
        public UniqueMob[] Mobs;
        [XmlElement("credits")]
        public string Credits;
    }

    [XmlRoot("mob")]
    public class UniqueMob
    {
        [XmlElement("name")]
        public string name;
        [XmlElement("level")]
        public string level;
        [XmlElement("spawn_time")]
        public string spawn_time;
        [XmlElement("location")]
        public string location;
        [XmlElement("pfid")]
        public string pfid;
        [XmlElement("coords")]
        public string coords;
        [XmlElement("pf")]
        public string pf;
        [XmlElement("item")]
        public UItemsResults_Item[] Items;
        [XmlElement("info")]
        public string info;
    }

    public class UItemsResults_Item
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("lowid")]
        public string _lowid;
        [XmlAttribute("highid")]
        public string _highid;
        [XmlAttribute("ql")]
        public string _ql;
        [XmlAttribute("droprate")]
        public string _droprate;

        public int LowID { get { try { return Convert.ToInt32(this._lowid); } catch { return 0; } } }
        public int HighID { get { try { return Convert.ToInt32(this._highid); } catch { return 0; } } }
        public int QL { get { try { return Convert.ToInt32(this._ql); } catch { return 0; } } }

    }
}
