using System;
using System.Diagnostics;
using System.Timers;
using AoLib.Utils;

// Additional Installation instructions:

// In addition to loading this plugin please copy the "ventrilo_status"
// application from your Ventrilo installation into your VhaBot directory

namespace VhaBot.Plugins
{
    public class Ventrilo : PluginBase
    {
        private string Server = "localhost"; 
        private Int32 Port = 3784;
        private string Password = "";
        private BotShell _bot;
        private System.Timers.Timer _welcome;

        public Ventrilo()
        {
            this.Name = "Ventrilo Server Status";
            this.InternalName = "llVentrilo";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 105;

            this.Commands = new Command[] {
                new Command("vent", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot) {
            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "server", "Ventrilo Server Address", this.Server);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "Port", "Ventrilo Server Port", this.Port);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "password", "Ventrilo Server Password (if needed)", this.Password);
            this.LoadConfiguration(bot);

            bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);

            this._bot = bot;

            this._welcome = new System.Timers.Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;

        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= VentWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += VentWindow;
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
                this._bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            this._bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= VentWindow;
             bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this.Server = bot.Configuration.GetString(this.InternalName, "server", this.Server);
            this.Port = bot.Configuration.GetInteger(this.InternalName, "Port", this.Port);
            this.Password = bot.Configuration.GetString(this.InternalName, "password", this.Password);
        }

        private int ParseClients ( string window )
        {
            int idx0 = window.IndexOf( "Current Number of Clients" );
            int num = -1;
            if ( idx0 >= 0 )
            {
                string substr = window.Substring( idx0 + 55 );
                int idx1 = substr.IndexOf( "<" );
                substr = substr.Substring( 0, idx1 );
                Int32.TryParse( substr, out num );
            }
            return num;
        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return; 
            RichTextWindow window = null;
            VentWindow( ref window, e.Sender );
            int num = ParseClients ( window.ToString() );
            if ( num >= 0 )
                bot.SendPrivateMessage(e.Sender, Convert.ToString(num) + " users on Ventrilo »» " + window );
        }

        private void UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            RichTextWindow window = null;
            VentWindow( ref window, e.Sender );
            int num = ParseClients ( window.ToString() );
            if ( num >= 0 )
                bot.SendPrivateMessage(e.Sender, Convert.ToString(num) + " users on Ventrilo »» " + window );
        }

        private static string GetValue( string[] Responses, string Key )
        {
            for ( int i=0; i<Responses.Length; i++ )
            {
                if ( Responses[i].IndexOf(Key) == 0 )
                    return Responses[i].Substring(Responses[i].IndexOf( ":" ) +
                                                  1 );
            }
            return string.Empty;
        }

        public string[] GetStatus()
        {

            string output = string.Empty;
            string[] response = null;

            try
            {

                Process p = new Process();
                p.StartInfo.FileName = "ventrilo_status";
                p.StartInfo.Arguments = "-t" + Server + " -c2";
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();

                output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            
                response = output.Split( new char[] { '\n' } );

            }
            catch { }

            return response;
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {
                case "vent":

                    RichTextWindow window = null;
                    VentWindow( ref window, e.Sender );
                    bot.SendReply(e, "Ventrilo Server Info »» ", window);

                    break;
            }
        }

        public void VentWindow( ref RichTextWindow window, string user )
        {

            bool new_window = false;
            if ( window == null )
            {
                window = new RichTextWindow(_bot);
                window.AppendTitle("::::: Ventrilo Server Info :::::");
                new_window = true;
            }
            else
                window.AppendHeader( "Ventrilo Server Info" );
            window.AppendLineBreak();

            string[] response = this.GetStatus();
            if ( response == null )
            {
                window.AppendNormal( "Can not check Ventrilo server status.  Please insure that \"ventrilo_status\" program has been copied into your VhaBot directory.");
                return;
            }
                    
            if ( Ventrilo.GetValue( response, "ERROR" ) != string.Empty )
            {
                window.AppendNormal( "Cannot connect to Ventrilo server, please check your settings and try again or the Ventrilo server may be down.");
                return;
            }

            // format output
            window.AppendHighlight("Server: ");
            window.AppendNormal(this.Server + " (" +
                                Ventrilo.GetValue( response,
                                                   "PLATFORM" ) + " )");
            window.AppendLineBreak();
            window.AppendHighlight("Port: ");
            window.AppendNormal(Convert.ToString(this.Port));
            window.AppendLineBreak();
            window.AppendHighlight("Server Version: ");
            window.AppendNormal(Ventrilo.GetValue( response,
                                                   "VERSION" ));
            if ( Ventrilo.GetValue( response, "VERSION" ) == "1" )
            {
                window.AppendLineBreak();
                window.AppendHighlight("Password: ");
                window.AppendNormal(Password);
            }
            else if ( Ventrilo.GetValue( response, "VERSION" ) == "2" )
            {
                window.AppendHighlight("Per User Password is required. ");
                window.AppendNormal("Please contact the administrator of your Ventrilo server for a user name and password.");

            }
            window.AppendLineBreak();
            window.AppendHighlight("Server Uptime: ");
            window.AppendNormal(Ventrilo.GetValue( response,
                                                   "UPTIME" ));
            window.AppendLineBreak();
            window.AppendHighlight("Maximum Clients: ");
            window.AppendNormal(Ventrilo.GetValue( response,
                                                   "MAXCLIENTS" ));
            window.AppendLineBreak();
            window.AppendHighlight("Current Number of Clients: ");
            window.AppendNormal(Ventrilo.GetValue( response,
                                                   "CLIENTCOUNT" ));
            window.AppendLineBreak(2);
            window.AppendNormal( "Visit " );
            window.AppendCommand( "http://www.ventrilo.com/download.php", "/start http://www.ventrilo.com/download.php" );
            window.AppendNormal( " to download a Ventrilo client");
            window.AppendLineBreak(2);

            if ( ! new_window )
            {
                window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
                window.AppendLineBreak();
            }
            
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "vent":
                    return "Returns Ventrilo server status";
            }
            return null;
        }
    }

}
