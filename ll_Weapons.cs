using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.Web;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml;
using AoLib.Utils;

namespace VhaBot.Plugins
{

    public class AOWeapons : PluginBase
    {
        // Martial Arts item IDs
        static int[] ma_skill_cutoffs = { 1, 200, 1000, 1001,
                                          2000, 2001, 3000 };
        static int[] ma_fist_ids =  { 211352, 211353, 211354, 211357,
                                      211358, 211363, 211364 };
        static int[] ma_fist_min_dmg = { 3, 25, 90, 91, 203, 204, 425 };
        static int[] ma_fist_max_dmg = { 5, 60, 380, 381, 830, 831, 1280};
        static int[] ma_fist_crit = { 3, 50, 500, 501, 560, 561, 770 };

        static int[] shade_fist_ids =  { 211349, 211350, 211351, 211359,
                                         211360, 211365, 211366 };
        static int[] shade_fist_min_dmg =  { 3, 25, 55, 56, 130, 131, 280 };
        static int[] shade_fist_max_dmg = { 5, 60, 258, 259, 682, 683, 890 };
        static int[] shade_fist_crit = { 3, 50, 250, 251, 275, 276, 300 };

        static int[] gen_fist_ids =  { 43712, 144745, 43713, 211355,
                                       211356, 211361, 211362 };
        static int[] gen_fist_min_dmg = { 3, 25, 65, 66, 140, 141, 300 };
        static int[] gen_fist_max_dmg = { 5, 60, 280, 281, 715, 716, 990 };
        static int[] gen_fist_crit = { 3, 50, 500, 501, 605, 605, 630 };

        static int[] brawl_skill_cutoffs = { 1, 1000, 1001, 2000, 2001, 3000 };
        static int[] brawl_min_dmg = { 1, 100, 101, 170, 171, 235 };
        static int[] brawl_max_dmg = { 2, 500, 501, 850, 851, 1145 };
        static int[] brawl_crit = { 3, 500, 501, 600, 601, 725 };

        static int[] dimach_skill_cutoffs = { 1, 1000, 1001, 2000, 2001, 3000 };
        static int[] dimach_dmg = { 1, 2000, 2001, 2500, 2501, 2850};
        static int[] dimach_ma_recharge = { 1800, 1800, 1188, 600, 600, 300 };
        static int[] dimach_ma_dmg = { 1, 2000, 2001, 2340, 2341, 2550 };
        static int[] dimach_shade_recharge = { 300, 300, 300, 300, 240, 200 };
        static int[] dimach_shade_dmg = { 1, 920, 921, 1872, 1873, 2750 };
        static int[] dimach_shade_drain = { 70, 70, 70, 75, 75, 80 };
        static int[] dimach_keeper_heal = { 1, 3000, 3001, 10500, 10501, 30000 };

        static string[] special_attack_names = { "Brawl", "FastAttack",
                                                 "SneakAttack", "Dimach",
                                                 "FlingShot", "AimedShot",
                                                 "Burst", "FullAuto" };
        // AUNO Search strings
        static string[] special_attack_ids = { "special[33554432]",
                                               "special[262144]",
                                               "special[131072]",
                                               "special[67108864]",
                                               "special[4096]",
                                               "special[16384]",
                                               "special[2048]",
                                               "special[8192]" };
        static string[] special_attack_skill = { "skill[142]",
                                                 "skill[147]",
                                                 "skill[146]",
                                                 "skill[144]",
                                                 "skill[150]",
                                                 "skill[151]",
                                                 "skill[148]",
                                                 "skill[167]" };

        public AOWeapons()
        {
            this.Name = "Weapon Tools";
            this.InternalName = "llWeapons";
            this.Author = "Xyphos / Nogoal / Healnjoo / Imoutochan / Tyrence / Llie";
            this.DefaultState = PluginState.Installed;
            this.Dependencies = new string[] { "llXMLItems" };
            this.Version = 102;
            this.Commands = new Command[] {
                new Command("inits", true, UserLevel.Guest),
                new Command("dps", true, UserLevel.Guest),
                new Command("martialarts", true, UserLevel.Guest),
                new Command("ma","martialarts"),
                new Command("aimedshot", true, UserLevel.Guest),
                new Command("as", "aimedshot"),
                new Command("brawl", true, UserLevel.Guest),
                new Command("burst", true, UserLevel.Guest),
                new Command("dimach", true, UserLevel.Guest),
                new Command("fa", true, UserLevel.Guest),
                new Command("fastattack", true, UserLevel.Guest),
                new Command("flingshot", true, UserLevel.Guest),
                new Command("fling", "flingshot"),
                new Command("fullauto", true, UserLevel.Guest),
                new Command("weapon", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
        }

        public override void OnUnload(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "inits":
                DisplayInits ( bot, e );
                break;

            case "dps":
                DisplayDamage ( bot, e );
                break;

            case "martialarts":
                DisplayMartialArtsWeapon ( bot, e );
                break;

            case "aimedshot":
                DisplayAimedShot ( bot, e );
                break;

            case "brawl":
                DisplayBrawl ( bot, e );
                break;

            case "burst":
                DisplayBurst ( bot, e );
                break;

            case "dimach":
                DisplayDimach ( bot, e );
                break;

            case "fa":

                bool meantfullauto = false;
                bool meantfastattack = false;
                if ( e.Items.Length == 1 )
                {
                    Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                     e.Items[0].QL );
                    string can = string.Empty;
                    for ( int i=0; i<xmlitem.Attributes.Count; i++ )
                        if ( xmlitem.Attributes.Attribute[i].Stat == 30 )
                            can = xmlitem.Attributes.Attribute[i].Extra;
                    if ( can.IndexOf( "FullAuto" ) >= 0 )
                        meantfullauto = true;
                    if ( can.IndexOf( "FastAttack" ) >= 0 )
                        meantfastattack = true;
                }
                else if ( e.Args.Length == 3 )
                {
                    Item xmlitem =
                        XMLItems.GetItem( bot, e,
                                          Convert.ToInt32(e.Args[0]),
                                          Convert.ToInt32(e.Args[1]) );
                    string can = string.Empty;
                    for ( int i=0; i<xmlitem.Attributes.Count; i++ )
                        if ( xmlitem.Attributes.Attribute[i].Stat == 30 )
                            can = xmlitem.Attributes.Attribute[i].Extra;
                    if ( can.IndexOf( "FullAuto" ) >= 0 )
                        meantfullauto = true;
                    if ( can.IndexOf( "FastAttack" ) >= 0 )
                        meantfastattack = true;                    
                }
                else if ( e.Args.Length == 2 )
                {
                    meantfastattack = true;
                }
                else if ( e.Args.Length == 4 )
                {
                    meantfullauto = true;
                }

                if ( meantfastattack )
                    DisplayFastAttack ( bot, e );
                if ( meantfullauto )
                    DisplayFullAuto ( bot, e );
                break;

            case "fastattack":
                DisplayFastAttack ( bot, e );
                break;

            case "flingshot":
                DisplayFlingShot ( bot, e );
                break;

            case "fullauto":
                DisplayFullAuto ( bot, e );
                break;

            case "weapon":
                DisplayWeaponInfo ( bot, e );
                break;

            }

        }

        private void UsageMessages(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "inits":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [drop weapon here]" );
                break;

            case "dps":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref] [attack rating] [init] [special attack skills ...] [agg def] [add damage] [crit chance] [target ac]");
                break;

            case "martialarts":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [ma skill]");
                break;

            case "aimedshot":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref] [aimed shot skill]");
                break;

            case "brawl":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [brawl skill]");
                break;

            case "burst":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref] [burst skill]");
                break;

            case "dimach":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [dimach skill]");
                break;

            case "fa":
                bot.SendReply(e, "Correct Usage (varies by weapon): " +
                              e.Command + " [weapon ref] [skill]");
                break;

            case "fastattack":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref] [fast attack skill]");
                break;

            case "flingshot":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref] [fling shot skill]");
                break;

            case "fullauto":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref] [full auto skill]");
                break;

            case "weapon":
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [weapon ref]");
                break;

            }

        }
                                            
        private void DisplayMartialArtsWeapon ( BotShell bot, CommandArgs e )
        {
            if ( e.Args.Length != 1 )
            {
                UsageMessages( bot, e );
                return;
            }

            int ma_skill;

            try
            {
                ma_skill = Convert.ToInt32( e.Args[0] );
            }
            catch
            {
                UsageMessages( bot, e );
                return;
            }

            int minid = 0, maxid = 0, ql = 0;
            for ( int i=0; i<ma_skill_cutoffs.Length; i++ )
                if ( ma_skill >= ma_skill_cutoffs[i] )
                    minid = i;

            if ( ( ma_skill_cutoffs[minid] == ma_skill ) ||
                 ( minid == ma_skill_cutoffs.Length - 1 ) )
                maxid = minid;
            else
                maxid = minid+1;

            if ( ( ma_skill < 1000 ) || ( ma_skill == 3000 ) )
                ql = ma_skill / 2;
            else if ( ma_skill < 3000 )
                ql = ( ma_skill - ( ma_skill / 1000 ) * 1000 ) / 2;
            else
                ql = 500;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Martial Artist Damage");
            window.AppendLineBreak();

            int index = 0;
            if (ma_skill < 200) 
                index = 0;
            else if ( ma_skill < 1001) 
                index = 1;
            else if ( ma_skill < 2001) 
                index = 3;
            else if (ma_skill < 3001)
                index = 5;

            int fist_ql = (int)Math.Round( ma_skill / 2.0 );
            float speed = 1.25f;
            if (fist_ql <= 200) 
                speed = 1.25f;
            else if ( fist_ql <= 500)
                speed = 1.25f + (float)(0.2*((fist_ql-200)/300));
            else if ( fist_ql <= 1000)
                speed = 1.45f + (float)(0.2*((fist_ql-500)/500));
            else if ( fist_ql <= 1500)
                speed = 1.65f + (float)(0.2*((fist_ql-1000)/500));

            window.AppendHighlight( "Martial Arts Skill: " );
            window.AppendNormal ( Convert.ToString( ma_skill ) );
            window.AppendLineBreak();
            window.AppendHighlight( "Martial Arts Attack: " );
            window.AppendNormal ( Convert.ToString( speed ) );
            window.AppendLineBreak();
            window.AppendHighlight( "Martial Arts Recharge: " );
            window.AppendNormal ( Convert.ToString( speed ) );
            window.AppendLineBreak(2);

            int GenMinDamage = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                      ma_skill_cutoffs[index+1],
                                                      gen_fist_min_dmg[index],
                                                      gen_fist_min_dmg[index+1],
                                                      ma_skill );
            int GenMaxDamage = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                      ma_skill_cutoffs[index+1],
                                                      gen_fist_max_dmg[index],
                                                      gen_fist_max_dmg[index+1],
                                                      ma_skill );
            int GenCrit = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                 ma_skill_cutoffs[index+1],
                                                 gen_fist_crit[index],
                                                 gen_fist_crit[index+1],
                                                 ma_skill );

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "General" );
            window.AppendLineBreak();
            window.AppendHighlight( "Martial Arts Damage: " );
            window.AppendNormal( GenMinDamage.ToString() + "-" +
                                 GenMaxDamage.ToString() + "(" +
                                 GenCrit.ToString() + ")" );
            window.AppendLineBreak();
            AoItem gen_fist_item = new AoItem("Martial Arts Item",
                                              gen_fist_ids[minid],
                                              gen_fist_ids[maxid], ql,
                                              Convert.ToString(gen_fist_ids[minid]));
            window.AppendItem( gen_fist_item.Name,
                               gen_fist_item.LowID,
                               gen_fist_item.HighID,
                               gen_fist_item.QL );
            window.AppendNormal( " [" );
            window.AppendCommand( "DPS",
                                  "/tell " + bot.Character + " dps " +
                                  Convert.ToString( gen_fist_item.LowID ) +
                                  " " + Convert.ToString( gen_fist_item.QL ) );
            window.AppendNormal( "]" );
            window.AppendLineBreak(2);

            int MAMinDamage = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                     ma_skill_cutoffs[index+1],
                                                     ma_fist_min_dmg[index],
                                                     ma_fist_min_dmg[index+1],
                                                     ma_skill );
            int MAMaxDamage = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                     ma_skill_cutoffs[index+1],
                                                     ma_fist_max_dmg[index],
                                                     ma_fist_max_dmg[index+1],
                                                     ma_skill );
            int MACrit = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                ma_skill_cutoffs[index+1],
                                                ma_fist_crit[index],
                                                ma_fist_crit[index+1],
                                                ma_skill );

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "Martial Artist" );
            window.AppendLineBreak();
            window.AppendHighlight( "Martial Arts Damage: " );
            window.AppendNormal( MAMinDamage.ToString() + "-" +
                                 MAMaxDamage.ToString() + "(" +
                                 MACrit.ToString() + ")" );
            window.AppendLineBreak();
            AoItem ma_fist_item = new AoItem("Martial Arts Item",
                                             ma_fist_ids[minid],
                                             ma_fist_ids[maxid], ql,
                                             Convert.ToString(ma_fist_ids[minid]));
            window.AppendItem( ma_fist_item.Name,
                               ma_fist_item.LowID,
                               ma_fist_item.HighID,
                               ma_fist_item.QL );
            window.AppendNormal( " [" );
            window.AppendCommand( "DPS",
                                  "/tell " + bot.Character + " dps " +
                                  Convert.ToString( ma_fist_item.LowID ) +
                                  " " + Convert.ToString( ma_fist_item.QL ) );
            window.AppendNormal( "]" );
            window.AppendLineBreak(2);

            int ShadeMinDamage = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                        ma_skill_cutoffs[index+1],
                                                        shade_fist_min_dmg[index],
                                                        shade_fist_min_dmg[index+1],
                                                        ma_skill );
            int ShadeMaxDamage = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                        ma_skill_cutoffs[index+1],
                                                        shade_fist_max_dmg[index],
                                                        shade_fist_max_dmg[index+1],
                                                        ma_skill );
            int ShadeCrit = AOWeapons.Interpolate( ma_skill_cutoffs[index],
                                                   ma_skill_cutoffs[index+1],
                                                   shade_fist_crit[index],
                                                   shade_fist_crit[index+1],
                                                   ma_skill );

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "Shade" );
            window.AppendLineBreak();
            window.AppendHighlight( "Martial Arts Damage: " );
            window.AppendNormal( ShadeMinDamage.ToString() + "-" +
                                 ShadeMaxDamage.ToString() + "(" +
                                 ShadeCrit.ToString() + ")" );
            window.AppendLineBreak();
            AoItem shade_fist_item = new AoItem("Martial Arts Item",
                                                shade_fist_ids[minid],
                                                shade_fist_ids[maxid], ql,
                                                Convert.ToString(shade_fist_ids[minid]));
            window.AppendItem( shade_fist_item.Name,
                               shade_fist_item.LowID,
                               shade_fist_item.HighID,
                               shade_fist_item.QL );
            window.AppendNormal( " [" );
            window.AppendCommand( "DPS",
                                  "/tell " + bot.Character + " dps " +
                                  Convert.ToString( shade_fist_item.LowID ) +
                                  " " + Convert.ToString( shade_fist_item.QL ));
            window.AppendNormal( "]" );
            window.AppendLineBreak(2);

            window.AppendNormal( "Item look up is courtesy of Xyphos.  Interpolated damage ported from Budabot." );

            bot.SendReply(e, "Martial Arts Information »» ", window );

        }

        private void DisplayDamage ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length < 2 )
            {
                UsageMessages( bot, e );
                return;
            }

            int handle_arg = 0;
            Item xmlitem = null;

            if ( e.Items.Length == 1 )
                xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID, e.Items[0].QL );
            else if ( e.Items.Length == 0 )
            {
                // try item format [itemid ql] instead of a full reference
                xmlitem = XMLItems.GetItem( bot, e,
                                            Convert.ToInt32(e.Args[0]),
                                            Convert.ToInt32(e.Args[1]));
                handle_arg = 2;
            }

            RichTextWindow window = new RichTextWindow(bot);

            if ( xmlitem != null )
            {

                window.AppendTitle("Estimated Damage Calculator");
                window.AppendItem( xmlitem.Name,
                                   xmlitem.LowID.ID,
                                   xmlitem.HighID.ID,
                                   xmlitem.HighID.QL );
                window.AppendLineBreak();

                // look up all special attacks for this weapon
                string can = string.Empty;
                for ( int i=0; i<xmlitem.Attributes.Count; i++ )
                    if ( xmlitem.Attributes.Attribute[i].Stat == 30 )
                        can = xmlitem.Attributes.Attribute[i].Extra;

                string[] can_arr = can.Split(',');
                int[] has_special = new int[ special_attack_names.Length ];
                int[] special_skill = new int[ special_attack_names.Length ];

                // Identify special attacks for this weapon
                for ( int i=0; i<can_arr.Length; i++ )
                    for ( int j=0; j<special_attack_names.Length; j++ )
                        if ( can_arr[i].Trim() == special_attack_names[j] )
                            has_special[j] = 1;

                // scroll arguments past item
                if ( handle_arg == 0 )
                {
                    while( ( handle_arg < e.Args.Length ) &&
                           ( ! e.Args[handle_arg].EndsWith( "</a>",
                                                            System.StringComparison.CurrentCultureIgnoreCase ) ) )
                        handle_arg ++;
                    handle_arg ++;
                }

                int attack_rating, init, agg_def, add_damage,
                    crit_chance, target_ac;

                if ( handle_arg < e.Args.Length )
                {
                    attack_rating = Convert.ToInt32( e.Args[handle_arg] );
                    handle_arg ++;
                }
                else
                    attack_rating = 1000;

                if ( handle_arg < e.Args.Length )
                {
                    init = Convert.ToInt32( e.Args[handle_arg] );
                    handle_arg ++;
                }
                else
                    init = 1000;

                for ( int i=0; i<special_attack_names.Length; i++ )
                    if ( ( has_special[i] == 1 ) &&
                         ( handle_arg < e.Args.Length ) )
                    {
                        special_skill[i] = Convert.ToInt32( e.Args[handle_arg] );
                        handle_arg ++;
                    }
                    else
                        special_skill[i] = 1000;

                if ( handle_arg < e.Args.Length )
                {
                    agg_def = Convert.ToInt32( e.Args[handle_arg] );
                    handle_arg ++;
                }
                else
                    agg_def = 100;

                if ( handle_arg < e.Args.Length )
                {
                    add_damage = Convert.ToInt32( e.Args[handle_arg] );
                    handle_arg ++;
                }
                else
                    add_damage = 100;

                if ( handle_arg < e.Args.Length )
                {
                    crit_chance = Convert.ToInt32( e.Args[handle_arg] );
                    handle_arg ++;
                }
                else
                    crit_chance = 4;

                if ( handle_arg < e.Args.Length )
                    target_ac = Convert.ToInt32( e.Args[handle_arg] );
                else
                    target_ac = 10000;

                window.AppendLineBreak();
                window.AppendColorStart( "DD6600" );
                window.AppendString( "Using the following values:" );
                window.AppendColorEnd();
                window.AppendLineBreak();

                window.AppendHighlight(  "Attack Rating: " );
                window.AppendNormal( Convert.ToString( attack_rating ) );
                window.AppendLineBreak();

                window.AppendHighlight(  "Initiative: " );
                window.AppendNormal( Convert.ToString( init ) );
                window.AppendLineBreak();

                window.AppendHighlight(  "AGG/DEF slider: " );
                window.AppendNormal( Convert.ToString( agg_def ) );
                window.AppendNormal( " (100 = full agg; 0 = full def)" );
                window.AppendLineBreak();

                window.AppendHighlight(  "Initiative: " );
                window.AppendNormal( Convert.ToString( init ) );
                window.AppendLineBreak();

                window.AppendHighlight(  "Additional Damage: " );
                window.AppendNormal( Convert.ToString( add_damage ) );
                window.AppendLineBreak();

                window.AppendHighlight(  "% Chance to Crit: " );
                window.AppendNormal( Convert.ToString( crit_chance ) );
                window.AppendLineBreak();

                for ( int i=0; i<special_attack_names.Length; i++ )
                    if( has_special[i] == 1 )
                    {
                        window.AppendHighlight( special_attack_names[i] + ": " );
                        window.AppendNormal( Convert.ToString( special_skill[i] ) );
                        window.AppendLineBreak();
                    }

                window.AppendHighlight(  "Target AC: " );
                window.AppendNormal( Convert.ToString( target_ac ) );
                window.AppendLineBreak();

                int burst_cycle = 0, full_auto_cycle = 0;
                if ( has_special[6] == 1 ) // has burst
                    burst_cycle = xmlitem.Attributes.GetAttributeByName ( "BurstRecharge" );

                if ( has_special[7] == 1 ) // has full auto
                    full_auto_cycle = xmlitem.Attributes.GetAttributeByName ( "FullAutoRecharge" );

                // assemble url

                string url = "http://auno.org/ao/dmg.php?";

                url = url + "name=" + xmlitem.Name.Replace( " ", "+" );
                url = url + "&lowdam=" + Convert.ToString( xmlitem.Attributes.GetAttributeByName ( "MinDamage" ) );
                url = url + "&highdam=" + Convert.ToString( xmlitem.Attributes.GetAttributeByName ( "MaxDamage" ) );
                url = url + "&critmod=" + Convert.ToString( xmlitem.Attributes.GetAttributeByName ( "CriticalBonus" ) );
                url = url + "&atttime=" + Convert.ToString( xmlitem.Attributes.GetAttributeByName ( "AttackDelay" ) / 100.0 );
                url = url + "&rectime=" + Convert.ToString( xmlitem.Attributes.GetAttributeByName ( "RechargeDelay" ) / 100.0 );
                url = url + "&clip=" + Convert.ToString( xmlitem.Attributes.GetAttributeByName ( "MaxEnergy" ) );

                url = url + "&attrate=" + Convert.ToString( attack_rating );
                url = url + "&init=" + Convert.ToString( init );
                url = url + "&aggdef=" + Convert.ToString( agg_def );
                url = url + "&critchance=" + Convert.ToString( crit_chance );
                url = url + "&bonus=" + Convert.ToString( add_damage );
                url = url + "&armor=" + Convert.ToString( target_ac );

                for ( int i=0; i<special_attack_names.Length; i++ )
                    if( has_special[i] == 1 )
                    {
                        url = url + "&" + special_attack_ids[i] + "=1&" +
                            special_attack_skill[i] + "=" +
                            Convert.ToString( special_skill[i] );
                        if ( i == 6 )
                            url = url + "&skill[374]" +
                                Convert.ToString( burst_cycle );
                        if ( i == 7 )
                            url = url + "&skill[375]" +
                                Convert.ToString( full_auto_cycle );
                    }

                string html = HTML.GetHtml(url, 60000);
                if (string.IsNullOrEmpty(html))
                {
                    bot.SendReply(e, "Error: Unable to connect to AUNO.");
                    return;
                }

                string bstring = "<legend>Damage estimate</legend>";
                string estring = "</fieldset>";
                int bidx = html.IndexOf( bstring );
                if ( bidx < 0 )
                {
                    bot.SendReply(e, "Error: Unable to parse results.");
                    return;
                }
                html = html.Remove( 0, bidx + bstring.Length + 2 );
                int eidx = html.IndexOf( estring );
                if ( eidx < 0 )
                {
                    bot.SendReply(e, "Error: Unable to parse results.");
                    return;
                }
                html = html.Remove( eidx, html.Length - eidx );

                Regex rgx = new Regex("<[^>]+>");
                html = rgx.Replace( html, "");
                html = html.Replace( "\n\n", "\n");
                html = html.Replace( "      ", "");
                html = html.Replace( "about\n", "about");
                html = html.Replace( "normal,\n", "normal,");

                window.AppendLineBreak();
                window.AppendColorStart( "DD6600" );
                window.AppendString( "Estimated Damage:" );
                window.AppendColorEnd();
                window.AppendLineBreak();
                window.AppendString( html );

                window.AppendLineBreak();
                window.AppendColorStart( "DD6600" );
                window.AppendString( "Usage Notes:" );
                window.AppendColorEnd();
                window.AppendLineBreak();
                window.AppendNormal( "The full usage for the dps command for this weapon is specified as follows: " );
                window.AppendLineBreak();
                window.AppendHighlight( "dps [drop " + xmlitem.Name +
                                        " here] [Attack Rating] [Init] " );
                for ( int i=0; i<special_attack_names.Length; i++ )
                    if( has_special[i] == 1 )
                        window.AppendHighlight( "[" + special_attack_names[i]
                                                + "] " );
                window.AppendHighlight( "[Agg/Def] [Add Damage] " +
                                        "[Crit Chance] [Target AC]" ); 
                window.AppendLineBreak();
                window.AppendNormal( "If you do not specify one or more of the above values, default values will be used." );
                window.AppendLineBreak(2);

                window.AppendNormal( "Item look up is courtesy of Xyphos and damage calculator is courtesy of AUNO." );

                bot.SendReply( e, "Estimated damage for " + xmlitem.Name + " »» ", window );
            }
            else
                bot.SendReply( e, "Error looking up weapon in database." );

        }

        public static float InitNerf( float I )
        {
            // No nerf below 1200
            if ( I <= 1200.0f )
                return I;
            return 1200 + (I-1200.0f)/3.0f;
        }

        public void DisplayInits(BotShell bot, CommandArgs e)
        {
            if (e.Items.Length < 1) 
            {
                // Pattern did not match
                UsageMessages( bot, e );
                return; 
            }

            Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                             e.Items[0].QL );

            float A = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
            float R = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Inits Calculator");

            window.AppendLineBreak();
            window.AppendHighlight("Weapon: ");
            window.AppendItem( xmlitem.Name, e.Items[0].LowID,
                               e.Items[0].HighID, e.Items[0].QL);
            window.AppendLineBreak(2);

            window.AppendHighlight("Optimum AggDef Slider position: ");
            for ( int i=0; i<9; i++ )
            {
                float S = (200.0f/8.0f)*i-100.0f;
                float I = Math.Max( InitNerf( (A-1f-(S-75f)/100f)*600f ),
                                    InitNerf( (R-1f-(S-75f)/100f)*300f ) );

                char[] slider = new char[] { '[',
                                             '-','-','-','-','-','-','-',
                                             '|',
                                             '-','-','-','-','-','-','-',
                                             ']' };

                slider[(int)Math.Round(((i/9f)*(slider.Length-1)))+1] = 'I';

                string output = new string(slider);

                output +=  string.Format( " {0,3}",
                                          Math.Abs(Math.Round(S)).ToString() );
                if ( S < 0 )
                    output += "% Def ";
                else
                    output += "% Agg ";

                output += Math.Round(I).ToString();
                output += " init";
                window.AppendNormal( output );
                window.AppendLineBreak();
                
            }

            window.AppendLineBreak();
            window.AppendNormal( "Item look up is courtesy of Xyphos.   Initiative computation based on Xyphos' Init Calculator." );

            bot.SendReply(e, "Init Calculation  »» ", window);

            return;
        }

        // The following cap computations were ported from BudaBot.
        // Formulas can be found on official Anarchy Online Forums and
        // AO-Universe

		public static void AimedShotCap( float Attack, float Recharge,
                                         ref int Cap, ref int Skill )
        {
            Cap = (int)Math.Floor( Attack + 10 );
            Skill = (int)Math.Ceiling( (4000 * Recharge - 1100) / 3 );
        }

		public static void BurstCap( float Attack, float Recharge, float Cycle,
                                     ref int Cap, ref int Skill )
        {
            Cap = (int)Math.Round( Attack + 8 );
            Skill = (int)Math.Floor( ((Recharge*20)+(Cycle/100)-8) * 25 );
        }

		public static void FastAttackCap( float Attack, 
                                          ref int Cap, ref int Skill )
        {
            Cap = (int)( Attack + 5 );
            Skill = (int)((( Attack*16 ) - Cap ) * 100);
        }

		public static void FlingShotCap( float Attack, ref int Cap,
                                         ref int Skill )
        {
            Cap = (int)( Attack + 5 );
            Skill = (int)((( Attack*16 ) - Cap ) * 100);
        }

		public static void FullAutoCap( float Attack, float Recharge,
                                        float Cycle, ref int Cap,
                                        ref int Skill )
        {
            Cap = (int)Math.Floor( Attack + 10 );
            Skill = (int)Math.Floor( ((Recharge*40)+(Cycle/100)-11) * 25 );
        }

		private static int Interpolate( float x1, float x2,
                                        float y1, float y2, float x)
        {
            float result = (y2 - y1)/(x2 - x1) * (x - x1) + y1;
            result = (float)Math.Round(result);
            return (int)result;
        }

        private void DisplayAimedShot ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length < 2 )
            {
                UsageMessages( bot, e );
                return;
            }

            float Attack = 0;
            float Recharge = 0;
            float AimedShotSkill = 0;
            string Weapon = "your weapon";

            if ( e.Items.Length == 1 )
            {
                try
                {
                    Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                     e.Items[0].QL );
                    Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                    Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
                    Weapon = xmlitem.Name;
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
            }
            else if ( e.Items.Length == 0 )
            {

                int test_arg = 0;
                try
                {
                    test_arg =  Convert.ToInt32(e.Args[0]);
                }
                catch { }

                if ( test_arg > 100 )
                {
                    try
                    {
                        Item xmlitem=XMLItems.GetItem( bot, e,
                                                       Convert.ToInt32(e.Args[0]),
                                                       Convert.ToInt32(e.Args[1]));
                        Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                        Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
                        Weapon = xmlitem.Name;
                    }
                    catch
                    {
                        UsageMessages( bot, e );
                        return;
                    }
                }
                else 
                {
                    float.TryParse( e.Args[0], out Attack );
                    float.TryParse( e.Args[1], out Recharge );
                }
            }

            float.TryParse( e.Args[e.Args.Length-1], out AimedShotSkill );

            int Cap = 0;
            int Skill = 0;

            AOWeapons.AimedShotCap( Attack, Recharge, ref Cap, ref Skill );

            int AimedShotRecharge = (int)Math.Ceiling(( Recharge * 40 ) -
                                                      ( AimedShotSkill * 3 /
                                                        100 ) +
                                                      Attack - 1);
            if ( AimedShotRecharge < Cap )
                AimedShotRecharge = Cap;

            int AimedShotMultiplier = (int)Math.Round( AimedShotSkill / 95 );

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Aimed Shot Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Attack: " );
            window.AppendNormal( Attack.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Recharge: " );
            window.AppendNormal( Recharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Aimed Shot Skill: " );
            window.AppendNormal( AimedShotSkill.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Aimed Shot Multiplier: " );
            window.AppendNormal( "1-" + AimedShotMultiplier.ToString() );
            window.AppendLineBreak();
            window.AppendHighlight( "Aimed Shot Recharge: " );
            window.AppendNormal( AimedShotRecharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak(2);

            window.AppendNormal( "With " + Weapon +
                                 ", your Aimed Shot recharge will cap at " );
            window.AppendHighlight( Cap.ToString() );
            window.AppendNormal( " seconds.  You need " );
            window.AppendHighlight( Skill.ToString() );
            window.AppendNormal( " in Aimed Shot skill to cap your recharge." );

            window.AppendLineBreak(2);
            window.AppendNormal( "Aimed Shot computation was ported from BudaBot." );

            bot.SendReply(e, "Aimed Shot Information »» ", window);

        }
                                            
        private void DisplayBrawl ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length != 1 )
            {
                UsageMessages( bot, e );
                return;
            }

            float BrawlSkill = 0;
            float.TryParse( e.Args[0], out BrawlSkill );

            int index = 0;
            if ( BrawlSkill < 1001 )
                index = 0;
            else if ( BrawlSkill < 2001 )
                index = 2;
            else if ( BrawlSkill < 3001 )
                index = 4;

            int MinDamage = AOWeapons.Interpolate( brawl_skill_cutoffs[index],
                                                   brawl_skill_cutoffs[index+1],
                                                   brawl_min_dmg[index],
                                                   brawl_min_dmg[index+1],
                                                   BrawlSkill );
            int MaxDamage = AOWeapons.Interpolate( brawl_skill_cutoffs[index],
                                                   brawl_skill_cutoffs[index+1],
                                                   brawl_max_dmg[index],
                                                   brawl_max_dmg[index+1],
                                                   BrawlSkill );
            int CritMultiplier = AOWeapons.Interpolate( brawl_skill_cutoffs[index],
                                                        brawl_skill_cutoffs[index+1],
                                                        brawl_crit[index],
                                                        brawl_crit[index+1],
                                                        BrawlSkill );

            int StunChance = 10;
            if ( BrawlSkill >= 1000)
                StunChance = 30;

            int StunDuration = 3;
            if ( BrawlSkill > 2000 )
                StunDuration = 4;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Brawl Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Brawl Skill: " );
            window.AppendNormal( BrawlSkill.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Brawl Recharge: " );
            window.AppendNormal( "15" );
            window.AppendHighlight( " seconds (constant)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Brawl Damage: " );
            window.AppendNormal( MinDamage.ToString() + "-" +
                                 MaxDamage.ToString() + "(" +
                                 CritMultiplier.ToString() + ")" );
            window.AppendLineBreak();
            window.AppendHighlight( "Stun Chance: " );
            window.AppendNormal( StunChance.ToString() );
            window.AppendLineBreak();
            window.AppendHighlight( "Stun Duration: " );
            window.AppendNormal( StunDuration.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak(2);

            window.AppendNormal( "Brawl computation is courtesy of Imoutochan and was ported from BudaBot." );

            bot.SendReply(e, "Brawl Information »» ", window);

        }

        private void DisplayBurst ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length < 2 )
            {
                UsageMessages( bot, e );
                return;
            }

            float Attack = 0;
            float Recharge = 0;
            float BurstCycle = 0;
            float BurstSkill = 0;
            string Weapon = "your weapon";

            if ( e.Items.Length == 1 )
            {
                try
                {
                    Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                     e.Items[0].QL );
                    Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                    Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
                    BurstCycle = xmlitem.Attributes.GetAttributeByName("BurstRecharge");
                    Weapon = xmlitem.Name;
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
            }
            else if ( e.Items.Length == 0 )
            {

                int test_arg = 0;
                try
                {
                    test_arg =  Convert.ToInt32(e.Args[0]);
                }
                catch { }

                if ( test_arg > 100 )
                {
                    try
                    {
                        Item xmlitem=XMLItems.GetItem( bot, e,
                                                       Convert.ToInt32(e.Args[0]),
                                                       Convert.ToInt32(e.Args[1]));
                        Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                        Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
                        BurstCycle = xmlitem.Attributes.GetAttributeByName("BurstRecharge");
                        Weapon = xmlitem.Name;
                    }
                    catch
                    {
                        UsageMessages( bot, e );
                        return;
                    }
                }
                else 
                {
                    float.TryParse( e.Args[0], out Attack );
                    float.TryParse( e.Args[1], out Recharge );
                    float.TryParse( e.Args[2], out BurstCycle );
                }
            }

            float.TryParse( e.Args[e.Args.Length-1], out BurstSkill );

            int Cap = 0;
            int Skill = 0;

            AOWeapons. BurstCap( Attack, Recharge, BurstCycle,
                                 ref Cap, ref  Skill );

            int BurstRecharge = (int)Math.Floor(( Recharge * 20 ) +
                                                ( BurstCycle / 100) -
                                                ( BurstSkill / 25 ) +
                                                Attack);
            if ( BurstRecharge < Cap )
                BurstRecharge = Cap;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Burst Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Attack: " );
            window.AppendNormal( Attack.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Recharge: " );
            window.AppendNormal( Recharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Burst Skill: " );
            window.AppendNormal( BurstSkill.ToString() );
            window.AppendLineBreak();
            window.AppendHighlight( "Burst Cycle: " );
            window.AppendNormal( BurstCycle.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Burst Recharge: " );
            window.AppendNormal( BurstRecharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak(2);

            window.AppendNormal( "With " + Weapon +
                                 ", your Burst recharge will cap at " );
            window.AppendHighlight( Cap.ToString() );
            window.AppendNormal( " seconds.  You need " );
            window.AppendHighlight( Skill.ToString() );
            window.AppendNormal( " in Burst skill to cap your recharge." );
            window.AppendLineBreak(2);

            window.AppendNormal( "Burst computation was ported from BudaBot." );

            bot.SendReply(e, "Burst Information »» ", window);

        }

        private void DisplayDimach ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length != 1 )
            {
                UsageMessages( bot, e );
                return;
            }

            float DimachSkill = 0;
            float.TryParse( e.Args[0], out DimachSkill );

            int index = 0;
            if ( DimachSkill < 1001 )
                index = 0;
            else if ( DimachSkill < 2001 )
                index = 2;
            else if ( DimachSkill < 3001 )
                index = 4;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Dimach Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Dimach Skill: " );
            window.AppendNormal( DimachSkill.ToString() );
            window.AppendLineBreak(2);

            int GenDamage = AOWeapons.Interpolate( dimach_skill_cutoffs[index],
                                                   dimach_skill_cutoffs[index+1],
                                                   dimach_dmg[index],
                                                   dimach_dmg[index+1],
                                                   DimachSkill );

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "General" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Recharge: " );
            window.AppendNormal( "1" );
            window.AppendHighlight( " hour (constant)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Damage: " );
            window.AppendNormal( GenDamage.ToString() + "-" +
                                 GenDamage.ToString() + "(1)" );
            window.AppendLineBreak(2);

            int MADamage = AOWeapons.Interpolate( dimach_skill_cutoffs[index],
                                                  dimach_skill_cutoffs[index+1],
                                                  dimach_ma_dmg[index],
                                                  dimach_ma_dmg[index+1],
                                                  DimachSkill );
            int MARecharge = AOWeapons.Interpolate( dimach_skill_cutoffs[index],
                                                    dimach_skill_cutoffs[index+1],
                                                    dimach_ma_recharge[index],
                                                    dimach_ma_recharge[index+1],
                                                    DimachSkill );
            MARecharge /= 60;

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "Martial Artist" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Recharge: " );
            window.AppendNormal( MARecharge.ToString() );
            window.AppendHighlight( " minutes" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Damage: " );
            window.AppendNormal( MADamage.ToString() + "-" +
                                 MADamage.ToString() + "(1)" );
            window.AppendLineBreak(2);

            int ShadeDamage=AOWeapons.Interpolate(dimach_skill_cutoffs[index],
                                                  dimach_skill_cutoffs[index+1],
                                                  dimach_shade_dmg[index],
                                                  dimach_shade_dmg[index+1],
                                                  DimachSkill );
            int ShadeRecharge=AOWeapons.Interpolate(dimach_skill_cutoffs[index],
                                                    dimach_skill_cutoffs[index+1],
                                                    dimach_shade_recharge[index],
                                                    dimach_shade_recharge[index+1],
                                                    DimachSkill );
            int ShadeDrain=AOWeapons.Interpolate(dimach_skill_cutoffs[index],
                                                 dimach_skill_cutoffs[index+1],
                                                 dimach_shade_drain[index],
                                                 dimach_shade_drain[index+1],
                                                 DimachSkill );

            ShadeRecharge /= 60;

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "Shade" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Recharge: " );
            window.AppendNormal( ShadeRecharge.ToString() );
            window.AppendHighlight( " minutes" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Damage: " );
            window.AppendNormal( ShadeDamage.ToString() + "-" +
                                 ShadeDamage.ToString() + "(1)" );
            window.AppendLineBreak();
            window.AppendHighlight( "HP Drain: " );
            window.AppendNormal( ShadeDrain.ToString() );
            window.AppendLineBreak(2);

            int KeeperHeal=AOWeapons.Interpolate(dimach_skill_cutoffs[index],
                                                 dimach_skill_cutoffs[index+1],
                                                 dimach_keeper_heal[index],
                                                 dimach_keeper_heal[index+1],
                                                 DimachSkill );

            window.AppendHighlight( "Profession: " );
            window.AppendNormal( "Keeper" );
            window.AppendLineBreak();
            window.AppendHighlight( "Dimach Recharge: " );
            window.AppendNormal( "1" );
            window.AppendHighlight( " hour (constant)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Self Heal: " );
            window.AppendNormal( KeeperHeal.ToString() );
            window.AppendLineBreak(2);

            window.AppendNormal( "Dimach computation is courtesy of Imoutochan and was ported from BudaBot." );

            bot.SendReply(e, "Dimach Information »» ", window);

        }

        private void DisplayFastAttack ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length < 2 )
            {
                UsageMessages( bot, e );
                return;
            }

            float Attack = 0;
            float FastAttackSkill = 0;
            string Weapon = "your weapon";

            if ( e.Items.Length == 1 )
            {
                try
                {
                    Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                     e.Items[0].QL );
                    Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                    Weapon = xmlitem.Name;
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
            }
            else if ( e.Items.Length == 0 )
            {

                int test_arg = 0;
                try
                {
                    test_arg =  Convert.ToInt32(e.Args[0]);
                }
                catch { }

                if ( test_arg > 100 )
                {
                    try
                    {
                        Item xmlitem=XMLItems.GetItem( bot, e,
                                                       Convert.ToInt32(e.Args[0]),
                                                       Convert.ToInt32(e.Args[1]));
                        Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                        Weapon = xmlitem.Name;
                    }
                    catch
                    {
                        UsageMessages( bot, e );
                        return;
                    }
                }
                else 
                {
                    float.TryParse( e.Args[0], out Attack );
                }
            }

            float.TryParse( e.Args[e.Args.Length-1], out FastAttackSkill );

            int Cap = 0;
            int Skill = 0;

            AOWeapons.FastAttackCap( Attack, ref Cap, ref Skill );

            int FastAttackRecharge = (int)Math.Round(( Attack * 16 ) -
                                                     ( FastAttackSkill / 100));
            if ( FastAttackRecharge < Cap )
                FastAttackRecharge = Cap;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Fast Attack Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Attack: " );
            window.AppendNormal( Attack.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Fast Attack Skill: " );
            window.AppendNormal( FastAttackSkill.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Fast Attack Recharge: " );
            window.AppendNormal( FastAttackRecharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak(2);

            window.AppendNormal( "With " + Weapon +
                                 ", your Fast Attack recharge will cap at " );
            window.AppendHighlight( Cap.ToString() );
            window.AppendNormal( " seconds.  You need " );
            window.AppendHighlight( Skill.ToString() );
            window.AppendNormal( " in Fast Attack skill to cap your recharge." );
            window.AppendLineBreak(2);

            window.AppendNormal( "Fast Attack computation was ported from BudaBot." );

            bot.SendReply(e, "Fast Attack Information »» ", window);

        }

        private void DisplayFlingShot ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length < 2 )
            {
                UsageMessages( bot, e );
                return;
            }

            float Attack = 0;
            float FlingShotSkill = 0;
            string Weapon = "your weapon";

            if ( e.Items.Length == 1 )
            {
                try
                {
                    Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                     e.Items[0].QL );
                    Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                    Weapon = xmlitem.Name;
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
            }
            else if ( e.Items.Length == 0 )
            {

                int test_arg = 0;
                try
                {
                    test_arg =  Convert.ToInt32(e.Args[0]);
                }
                catch { }

                if ( test_arg > 100 )
                {
                    try
                    {
                        Item xmlitem=XMLItems.GetItem( bot, e,
                                                       Convert.ToInt32(e.Args[0]),
                                                       Convert.ToInt32(e.Args[1]));
                        Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                        Weapon = xmlitem.Name;
                    }
                    catch
                    {
                        UsageMessages( bot, e );
                        return;
                    }
                }
                else 
                {
                    float.TryParse( e.Args[0], out Attack );
                }
            }

            float.TryParse( e.Args[e.Args.Length-1], out FlingShotSkill );

            int Cap = 0;
            int Skill = 0;

            AOWeapons.FlingShotCap( Attack, ref Cap, ref Skill );

            int FlingShotRecharge = (int)Math.Round(( Attack * 16 ) -
                                                     ( FlingShotSkill / 100));
            if ( FlingShotRecharge < Cap )
                FlingShotRecharge = Cap;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Fling Shot Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Attack: " );
            window.AppendNormal( Attack.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Fling Shot Skill: " );
            window.AppendNormal( FlingShotSkill.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Fling Shot Recharge: " );
            window.AppendNormal( FlingShotRecharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak(2);

            window.AppendNormal( "With " + Weapon +
                                 ", your Fling Shot recharge will cap at " );
            window.AppendHighlight( Cap.ToString() );
            window.AppendNormal( " seconds.  You need " );
            window.AppendHighlight( Skill.ToString() );
            window.AppendNormal( " in Fling Shot skill to cap your recharge." );
            window.AppendLineBreak(2);

            window.AppendNormal( "Fling Shot computation was ported from BudaBot." );

            bot.SendReply(e, "Fling Shot Information »» ", window);

        }

        private void DisplayFullAuto ( BotShell bot, CommandArgs e )
        {

            if ( e.Args.Length < 2 )
            {
                UsageMessages( bot, e );
                return;
            }

            float Attack = 0;
            float Recharge = 0;
            float FullAutoCycle = 0;
            float FullAutoSkill = 0;
            string Weapon = "your weapon";

            if ( e.Items.Length == 1 )
            {
                try
                {
                    Item xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                     e.Items[0].QL );
                    Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                    Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
                    FullAutoCycle = xmlitem.Attributes.GetAttributeByName("FullAutoRecharge");
                    Weapon = xmlitem.Name;
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
            }
            else if ( e.Items.Length == 0 )
            {

                int test_arg = 0;
                try
                {
                    test_arg =  Convert.ToInt32(e.Args[0]);
                }
                catch { }

                if ( test_arg > 100 )
                {
                    try
                    {
                        Item xmlitem=XMLItems.GetItem( bot, e,
                                                       Convert.ToInt32(e.Args[0]),
                                                       Convert.ToInt32(e.Args[1]));
                        Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
                        Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
                        FullAutoCycle = xmlitem.Attributes.GetAttributeByName("FullAutoRecharge");
                        Weapon = xmlitem.Name;
                    }
                    catch
                    {
                        UsageMessages( bot, e );
                        return;
                    }
                }
                else 
                {
                    float.TryParse( e.Args[0], out Attack );
                    float.TryParse( e.Args[1], out Recharge );
                    float.TryParse( e.Args[2], out FullAutoCycle );
                }
            }

            float.TryParse( e.Args[e.Args.Length-1], out FullAutoSkill );

            int Cap = 0;
            int Skill = 0;

            AOWeapons. FullAutoCap( Attack, Recharge, FullAutoCycle,
                                    ref Cap, ref Skill );

            int FullAutoRecharge = (int)Math.Round(( Recharge * 40 ) +
                                                   ( FullAutoCycle / 100) -
                                                   ( FullAutoSkill / 25 ) +
                                                   Math.Round( Attack - 1 ));

            if ( FullAutoRecharge < Cap )
                FullAutoRecharge = Cap;

            int MaxBullets = 5 + (int)Math.Floor( FullAutoSkill / 100 );

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Full Auto Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Attack: " );
            window.AppendNormal( Attack.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Recharge: " );
            window.AppendNormal( Recharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Full Auto Skill: " );
            window.AppendNormal( FullAutoSkill.ToString() );
            window.AppendLineBreak();
            window.AppendHighlight( "Full Auto Cycle: " );
            window.AppendNormal( FullAutoCycle.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Full Auto Recharge: " );
            window.AppendNormal( FullAutoRecharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Maximum Number of Bullets: " );
            window.AppendNormal( MaxBullets.ToString() );
            window.AppendLineBreak(2);

            window.AppendNormal( "With " + Weapon +
                                 ", your Full Auto recharge will cap at " );
            window.AppendHighlight( Cap.ToString() );
            window.AppendNormal( " seconds.  You need " );
            window.AppendHighlight( Skill.ToString() );
            window.AppendNormal( " in FullAuto skill to cap your recharge. " );
            window.AppendLineBreak(1);
            window.AppendNormal( "From 0 to 10K damage, the bullet damage is unchanged." );
            window.AppendLineBreak(1);
            window.AppendNormal( "From 10K to 11.5K damage, each bullet damage is halved." );
            window.AppendLineBreak(1);
            window.AppendNormal( "From 11K to 15K damage, each bullet damage is halved again." );
            window.AppendNormal( "15K is the damage cap." );
            window.AppendLineBreak(2);

            window.AppendNormal( "Full Auto computation was ported from BudaBot." );

            bot.SendReply(e, "Full Auto Information »» ", window);

        }

        private void DisplayWeaponInfo ( BotShell bot, CommandArgs e )
        {

            Item xmlitem = null;
            int QL = 0;
            if ( e.Items.Length == 1 )
            {
                try
                {
                    xmlitem = XMLItems.GetItem( bot, e, e.Items[0].LowID,
                                                e.Items[0].QL );
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
                QL = e.Items[0].QL;
            }
            else if ( e.Items.Length == 0 )
            {
                try
                {
                    xmlitem=XMLItems.GetItem( bot, e,
                                              Convert.ToInt32(e.Args[0]),
                                              Convert.ToInt32(e.Args[1]));
                }
                catch
                {
                    UsageMessages( bot, e );
                    return;
                }
                QL = Convert.ToInt32(e.Args[1]);
            }

            if ( xmlitem == null )
            {
                bot.SendReply(e, "Error: Unable to parse weapon.]1");
                return;
            }

            float Attack = xmlitem.Attributes.GetAttributeByName("AttackDelay") / 100.0f;
            float Recharge = xmlitem.Attributes.GetAttributeByName("RechargeDelay") / 100.0f;
            int MinDamage =  (int)xmlitem.Attributes.GetAttributeByName("MinDamage");
            int MaxDamage =  (int)xmlitem.Attributes.GetAttributeByName("MaxDamage");
            int CritBonus =  (int)xmlitem.Attributes.GetAttributeByName("CriticalBonus");
            int MBS =  (int)xmlitem.Attributes.GetAttributeByName("AMSCap");

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Weapon Information");
            window.AppendLineBreak();

            window.AppendHighlight( "Weapon: " );
            window.AppendNormal( xmlitem.Name );
            window.AppendHighlight( " (QL " );
            window.AppendNormal( QL.ToString() );
            window.AppendHighlight( ")" );
            window.AppendLineBreak(2);

            window.AppendHighlight( "Attack: " );
            window.AppendNormal( Attack.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Recharge: " );
            window.AppendNormal( Recharge.ToString() );
            window.AppendHighlight( " second(s)" );
            window.AppendLineBreak();
            window.AppendHighlight( "Damage: " );
            window.AppendNormal( MinDamage.ToString() + "-" +
                                 MaxDamage.ToString() + "(" +
                                 CritBonus.ToString() + ")" );
            window.AppendLineBreak();
            window.AppendHighlight( "Attack Rating Cap: " );
            window.AppendNormal( MBS.ToString() );
            window.AppendLineBreak(2);

            window.AppendHighlight( "In order for this weapon not to be over-equipped" );
            window.AppendLineBreak(1);
            window.AppendHighlight( "you need have at least the following skills:" );
            window.AppendLineBreak();
            foreach ( ActionEl action in xmlitem.Actions.Action )
                if ( action.Name == "ToWield" )
                    foreach ( RequirementEl requirement in
                              action.Requirements.Requirement )
                        if ( ( requirement.Target.Name == "Self" ) &&
                             ( requirement.Stat.Name != "Profession" ) &&
                             ( requirement.Op.Name == "GreaterThan" ) )
                            
                        {
                            window.AppendHighlight( requirement.Stat.Name  +
                                                    ": " );
                            int skill = (int)Math.Round(requirement.Value.Num *
                                                        .8);
                            window.AppendNormal( skill.ToString() );
                            window.AppendLineBreak();
                        }
            window.AppendLineBreak();

            string can_str = string.Empty;
            for ( int i=0; i<xmlitem.Attributes.Count; i++ )
                if ( xmlitem.Attributes.Attribute[i].Stat == 30 )
                    can_str = xmlitem.Attributes.Attribute[i].Extra;
            string[] can = can_str.Split(',');

            int Cap = 0;
            int Skill = -1;
            int Cycle = -1;
            window.AppendHighlight( "In order to cap the recharge time of each of the following attacks" );
            window.AppendLineBreak(1);
            window.AppendHighlight( "you need have at least the following skills:" );
            window.AppendLineBreak();

            foreach ( string c in can )
            {

                switch ( c.Trim() )
                {
                case "AimedShot":
                    AimedShotCap( Attack, Recharge, ref Cap, ref Skill );
                    window.AppendHighlight( "Aimed Shot: " );
                    break;

                case "Brawl":
                    window.AppendHighlight( "Brawl: " );
                    Skill = 0;
                    Cap = 15;
                    break;

                case "Burst":
                    Cycle = xmlitem.Attributes.GetAttributeByName("BurstRecharge");
                    BurstCap( Attack, Recharge, Cycle, ref Cap, ref Skill );
                    window.AppendHighlight( "Burst: " );
                    break;

                case "FastAttack":
                    FastAttackCap( Attack, ref Cap, ref Skill );
                    window.AppendHighlight( "Fast Attack: " );
                    break;

                case "FlingShot":
                    FlingShotCap( Attack, ref Cap, ref Skill );
                    window.AppendHighlight( "Fling Shot: " );
                    break;

                case "FullAuto":
                    Cycle = xmlitem.Attributes.GetAttributeByName("FullAutoRecharge");
                    FullAutoCap( Attack, Recharge, Cycle, ref Cap, ref Skill );
                    window.AppendHighlight( "Full Auto: " );
                    break;

                case "SneakAttack":
                    window.AppendHighlight( "Sneak Attack: " );
                    Skill = 0;
                    Cap = 40;
                    break;

                case "Dimach":
                    window.AppendHighlight( "Dimach (not MA or Shade): " );
                    Skill = 0;
                    Cap = 3600;
                    break;

                default:
                    Skill = -1;
                    Cap = -1;
                    break;
                }

                if ( Skill > 0 )
                {
                    window.AppendNormal( Skill.ToString() );
                    window.AppendHighlight( " to cap at " );
                    window.AppendNormal( Cap.ToString() );
                    window.AppendHighlight( " second(s)" );
                    window.AppendLineBreak();
                }
                else if ( Skill == 0 )
                {
                    window.AppendHighlight( " cap is fixed at " );
                    window.AppendNormal( Cap.ToString() );
                    window.AppendHighlight( " second(s)" );
                    window.AppendLineBreak();
                }
            }
            window.AppendLineBreak();

            window.AppendCommand( "View estimated damage for " + xmlitem.Name,
                                  "/tell " + bot.Character + " dps " +
                                  Convert.ToString( xmlitem.LowID.ID ) +
                                  " " + Convert.ToString( QL ) );
            window.AppendLineBreak(2);

            window.AppendNormal( "This function inspired by SpecialAttack command by Tyrence in BudaBot." );

            bot.SendReply(e, "Weapon Information »» ", window);

        }

        public override string OnHelp( BotShell bot, string command )
        {
            switch (command)
            {

            case "inits":
                return "Calculates a dozen init calculations for a given weapon " +
                    "and displays the optimal AGG-DEF slider position.\n\n" +
                    "The inits calculations are done via public interface with the " +
                    "Xyphos Items Database (http://www.xyphos.com) and are displayed in game.\n\n" +
                    "Usage: /tell " + bot.Character +
                    " inits [drop weapon in chat]\n";

            case "dps":
                return "Display estimated damage using AUNO's damage calculator for the specified weapon.\n" +
                    "Usage: /tell " + bot.Character +
                    " dps [weapon ref] [weapon ref] [attack rating] [init] [special attack skills ...] [agg def] [add damage] [crit chance] [target ac]";

            case "ma":
                return "Display the item reference for the Martial Artists' bare fist weapon given a Martial Arts skill.\n" +
                    "Usage: /tell " + bot.Character + " ma [Martial Arts Skill]";

            case "aimedshot":
                return "Displays information about Aimed Shot for a specified weapon given your skill.\n" +
                    "Usage: /tell " + bot.Character + " as [Weapon Ref] [Aimed Shot Skill]" +
                    "or: /tell " + bot.Character + " as [Weapon Item ID] [QL] [Aimed Shot Skill]" +
                    "or: /tell " + bot.Character + " as [Weapon Attack Speed] [Weapon Recharge Speed] [Aimed Shot Skill]";

            case "brawl":
                return "Displays information about Brawl given your skill.\n" +
                    "Usage: /tell " + bot.Character + " brawl [Brawl Skill]";

            case "burst":
                return "Displays information about Burst for a specific weapon given your skill.\n" +
                    "Usage: /tell " + bot.Character + " burst [Weapon Reference] [Burst Skill]\n" +
                    "or: /tell " + bot.Character + " burst [Weapon Item ID] [QL] [Burst Skill]\n" +
                    "or: /tell " + bot.Character + " burst [Weapon Attack Speed] [Weapon Recharge Speed] [Weapon Burst Cycle] [Burst Skill]\n";

            case "dimach":
                return "Displays information about dimach given your skill.\n" +
                    "Usage: /tell " + bot.Character + " dimach [Dimach Skill]"; 

            case "fa":
                return "Attempts to display information about Fast Attack or Full Auto for a specific weapon given your skill.\n" +
                    "Usage: /tell " + bot.Character + " fa [Weapon Reference] [Skill]\n" +
                    "or: /tell " + bot.Character + " fa [Weapon Item ID] [QL] [Skill]\n" +
                    "or: /tell " + bot.Character + " fa [Weapon Attack Speed] [Fast Attack Skill]\n" +
                    "or: /tell " + bot.Character + " fa [Weapon Attack Speed] [Weapon Recharge Speed] [Full Auto Cycle] [Full Auto Skill]\n";

            case "fastattack":
                return "Displays information about Fast Attack for a specific weapon given your skill.\n" +
                    "Usage: /tell " + bot.Character + " fastattack [Weapon Reference] [Fast Attack Skill]\n" +
                    "or: /tell " + bot.Character + " fastattack [Weapon Item ID] [QL] [Fast Attack Skill]\n" +
                    "or: /tell " + bot.Character + " fastattack [Weapon Attack Speed] [Fast Attack Skill]\n";

            case "flingshot":
                return "Displays information about Fling Shot for a specific weapon given your skill.\n" +
                    "Usage: /tell " + bot.Character + " flingshot [Weapon Reference] [Fling Shot Skill]\n" +
                    "or: /tell " + bot.Character + " flingshot [Weapon Item ID] [QL] [Fling Shot Skill]\n" +
                    "or: /tell " + bot.Character + " flingshot [Weapon Attack Speed] [Fling Shot Skill]\n";

            case "fullauto":
                return "Displays information about Full Auto for a specific weapon given your skill.\n" +
                    "Usage: /tell " + bot.Character + " fullauto [Weapon Reference] [Full Auto Skill]\n" +
                    "or: /tell " + bot.Character + " fullauto [Weapon Item ID] [QL] [Full Auto Skill]\n" +
                    "or: /tell " + bot.Character + " fullauto [Weapon Attack Speed] [Weapon Recharge Speed] [Weapon Full Auto Cycle] [Full Auto Skill]\n";

            case "weapon":
                return "Displays information about skills needed for a specified weapon.\n" +
                    "Usage: /tell " + bot.Character + " weapon [Weapon Reference]\n" +
                    "or: /tell " + bot.Character + " weapon [Weapon Item ID] [QL]\n";
            }

            return null;

        }

    }

    [XmlRoot("inits")]
    public class InitsResults
    {
        [XmlElement("version")]
        public string Version;

        [XmlElement("lowid")]
        public int LowID;

        [XmlElement("highid")]
        public int HighID;

        [XmlElement("ql")]
        public int QL;

        [XmlElement("name")]
        public string Name;

        [XmlElement("inits")] 
        public InitsResults_Init[] Inits;
    }

    public class InitsResults_Init
    {
        [XmlAttribute("slider")]
        public string _Slider;

        [XmlAttribute("percent")]
        public string _Percent;

        [XmlAttribute("init")]
        public string _Init;

        public string Slider { get { return this._Slider; } }
        public string Percent { get { return this._Percent; } }
        public string Init { get { return this._Init; } }
    }

}
