using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using AoLib.Utils;

// for an example of how to embed or append these contents to an
// existing web page please refer to this:

// https://bitbucket.org/Llie/llie_vhabot/raw/tip/Extra/html/online.html

namespace VhaBot.Plugins
{
    public class WebGateway : PluginBase
    {

        private HttpListener _httpListener;
        private Thread listenThread;
        private BotShell bot;
        private Int32 listenPort = 8081;

        private string[] webQueries;

        private Config eventDatabase;

        public WebGateway()
        {
            this.Name = "Web Gateway";
            this.InternalName = "llWebGateway";
            this.Author = "Llie / Veremit";
            this.Version = 110;
            this.Description = "Provides a Web service to allow various bot services to be queried from a web server.";
            this.DefaultState = PluginState.Installed;

            this.Commands = new Command[] {
                new Command("webserver", true, UserLevel.Admin)
            };

            this. webQueries = new string[3] {
                "Online", "Calendar", "Events" };

        }

        public override void OnLoad(BotShell bot)
        {
            this.bot = bot;

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "port", "Web query service listen port", this.listenPort );
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "autostart", "Auto-Connect", false);
            this.listenPort = bot.Configuration.GetInteger(this.InternalName, "port", this.listenPort);

            System.Threading.ThreadPool.SetMaxThreads(20, 20);
            System.Threading.ThreadPool.SetMinThreads(5, 5);

            if (bot.Configuration.GetBoolean(this.InternalName, "autostart", false))
                this.StartListener();

            if ( bot.Plugins.IsLoaded("llEvents") )
                eventDatabase = new Config(bot.ID, "llEvents");

        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);

            this.StopListener();
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.listenPort = bot.Configuration.GetInteger(this.InternalName, "port", this.listenPort);
        }

        private void StartListener()
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("[WebGateway] Cannot use the HttpListener class.");
                return;
            }
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
        }

        private void ListenForClients()
        {
            try
            {
                this._httpListener = new HttpListener();
                this._httpListener.Prefixes.Add("http://*:" + Convert.ToString(this.listenPort) + "/");
                this._httpListener.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[WebGateway] Unable to start HttpListener. Please check your settings.");
                Console.WriteLine("[WebGateway] Exception: ", ex.ToString());
                bot.Configuration.Set(ConfigType.Boolean, this.InternalName, "autostart", false);
                return;
            }
            IAsyncResult result = this._httpListener.BeginGetContext(new AsyncCallback(WebRequestCallback), this._httpListener);
            result.AsyncWaitHandle.WaitOne();
        }

        private void StopListener()
        {
            if (this._httpListener != null)
            {
                this._httpListener.Close();
                this._httpListener = null;
            }
        }

        private void WebRequestCallback(IAsyncResult result)
        {
            if (this._httpListener == null) { return; }

            HttpListenerContext context = this._httpListener.EndGetContext(result);
            this._httpListener.BeginGetContext(new AsyncCallback(WebRequestCallback), this._httpListener);

            ThreadPool.QueueUserWorkItem(ProcessRequest, context);
        }

        private void ProcessRequest(object listenerContext)
        {
            try
            {
                HttpListenerContext context = (HttpListenerContext)listenerContext;
                StringBuilder responseMsg = new StringBuilder();
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.ContentType = "text/html";

                // Console.WriteLine("[WebGateway] Requested: " + context.Request.Url);

                if ( context.Request.RawUrl == "/" )
                    AppendSupportedCommands( ref responseMsg );
                else if ( ! HandleURI( context.Request.RawUrl,
                                       ref responseMsg ) )
                    AppendSupportedCommands( ref responseMsg );

                byte[] buffer = Encoding.ASCII.GetBytes(responseMsg.ToString());
                context.Response.ContentLength64 = buffer.Length;
                using (Stream s = context.Response.OutputStream)
                {
                    s.Write(buffer, 0, buffer.Length);
                    s.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[WebGateway] Request error: " + ex);
            }
        }

        private void AppendSupportedCommands( ref StringBuilder responseMsg )
        {
            responseMsg.Append("VhaBot Supported Commands:<br>\n");
            foreach ( string command in this.webQueries )
                responseMsg.Append("<li><a href=\"/" + command + "\">" +
                                   command + "</a><br>\n");
        }
                              
        private bool HandleURI( string RawUrl, ref StringBuilder responseMsg )
        {
            String ParsedURL = RawUrl.Substring( 1 );
            if ( ParsedURL.Contains( "?" ) )
                ParsedURL = ParsedURL.Substring( 0, ParsedURL.IndexOf( "?" ) );
            int index = Array.IndexOf( this.webQueries, ParsedURL );
            if ( index > -1 )
            {
                switch ( index )
                {

                case 0:
                    // Online
                    AppendOnline( ref responseMsg );
                    break;

                case 1:
                    // Calendar
                    AppendCalendar( ref responseMsg );
                    break;

                case 2:
                    // Events
                    AppendEvents( ref responseMsg, RawUrl );
                    break;

                }
                return true;
            }
            else
                return false;
        }

        private void AppendOnline( ref StringBuilder responseMsg )
        {

            // compile list of people on-line
            responseMsg.Append("<DIV ID=vh_online_container>\n");

            try
            {

                // online
                string[] online = bot.FriendList.Online("notify");
                if (online.Length > 0)
                    responseMsg.Append(AppendUsers(bot, "Currently Online:", online));

                // private channel
                Dictionary<UInt32, Friend> list = bot.PrivateChannel.List();
                List<string> guests = new List<string>();
                foreach (KeyValuePair<UInt32, Friend> user in list)
                    guests.Add(user.Value.User);
                if (guests.Count > 0)
                    responseMsg.Append(AppendUsers(bot, "On Guest Channel:", guests.ToArray()));

                // teamspeak
                if ( bot.Plugins.IsLoaded("llTeamSpeak") )
                {
                    List<string> tsclients = null;
                    ((TeamSpeak)bot.Plugins.GetPlugin("llTeamSpeak")).GetClients( out tsclients );
                    if ( tsclients.Count > 0)
                        responseMsg.Append(AppendOther(bot, "On Teamspeak:", "http://teamspeak.com/media_repository/files/mediakit_2013/03_button/ts_Button_66x66_01.png", tsclients.ToArray()));
                            
                }

            }
            catch ( Exception ex )
            {
                responseMsg.Append("Bot Error.<br>" + ex);
                responseMsg.Append("(Possibly cause by dependent plug-ins not loaded.");
            }
            responseMsg.Append("</DIV>\n");
        }

        private void AppendCalendar( ref StringBuilder responseMsg )
        {

            responseMsg.Append("\n<DIV id=vh_calendar_listing style=\"clear: both;\">\n");
            if ( bot.Plugins.IsLoaded("llEvents") &&
                 ( this.eventDatabase != null ) )
            {
                DateTime Now = DateTime.UtcNow;

                // create an array of 2 months
                int ndays = DateTime.DaysInMonth( Now.Year, Now.Month );
                DateTime NextMonth = Now.AddMonths( 1 );
                ndays += DateTime.DaysInMonth( NextMonth.Year,
                                               NextMonth.Month );

                Dictionary<DateTime,bool> TwoMonth =
                    new Dictionary<DateTime,bool>();
                for ( int i=0; i<ndays; i++ )
                {
                    int day = i+1;
                    int month = Now.Month;
                    int year = Now.Year;
                    if ( i >= DateTime.DaysInMonth( Now.Year, Now.Month ) )
                    {
                        day -= DateTime.DaysInMonth( Now.Year, Now.Month );
                        month += 1;
                        if ( month > 12 )
                        {
                            month --;
                            year ++;
                        }
                    }
                    TwoMonth[new DateTime( year, month, day )] = false;
                }

                string NowDateSpec = string.Format(
                "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute );

                using ( IDbCommand command =
                        this.eventDatabase.Connection.CreateCommand() )
                {
                    command.CommandText =
                        string.Format(
                            "SELECT * FROM events WHERE EventDateTime > '{0}'",
                            NowDateSpec );

                    IDataReader reader = command.ExecuteReader();
                    while ( reader.Read() )
                    {
                        string DateSpec = reader.GetString(1);
                        DateTime EventTime;
                        try
                        {
                            EventTime =
                                DateTime.ParseExact( DateSpec,
                                                     "M/d/yyyy h:mm:ss tt",
                                                     CultureInfo.InvariantCulture );
                            DateTime StripTime = new DateTime( EventTime.Year,
                                                               EventTime.Month,
                                                               EventTime.Day );
                            TwoMonth[StripTime] = true;
                        }
                        catch { }
                    }
                }

                responseMsg.Append( "<table id=vh_calendar_table style=\"clear: both;\">\n" );
                for ( int i=0; i<ndays; i++ )
                {
                    int day = i+1;
                    int month = Now.Month;
                    int year = Now.Year;
                    if ( i >= DateTime.DaysInMonth( Now.Year, Now.Month ) )
                    {
                        day -= DateTime.DaysInMonth( Now.Year, Now.Month );
                        month += 1;
                        if ( month > 12 )
                        {
                            month --;
                            year ++;
                        }
                    }
                    DateTime calDay = new DateTime( year, month, day );
                    if ( day == 1 )
                    {
                        // append month and day of week padding
                        string monthName = new DateTime( year, month, day).ToString("MMM", CultureInfo.InvariantCulture);
                        if ( month > Now.Month )
                            responseMsg.Append( "</tr>" );
                        responseMsg.Append( "<th colspan=7 id=vh_calendar_heading style=\"clear: both; text-align:center;\">" + monthName + "</th>" );
                        responseMsg.Append( "<tr id=vh_calendar_row><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Sun</td><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Mon</td><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Tue</td><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Wed</td><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Thu</td><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Fri</td><td id=vh_calendar_cell style=\"clear: both; text-align:center;\">Sat</td>" );
                        responseMsg.Append( "<tr id=vh_calendar_row>" );
                        if ( calDay.DayOfWeek > DayOfWeek.Sunday )
                            responseMsg.Append( "<td colspan=" + ((int)calDay.DayOfWeek).ToString() + "></td>" );
                    }
                    if ( !TwoMonth[calDay] )
                        responseMsg.Append( "<td id=vh_calendar_cell style=\"clear: both; text-align:center;\">" + day.ToString() + "</td>" );
                    else
                        responseMsg.Append( "<td id=vh_calendar_cell style=\"clear: both; text-align:center;\"><a href=\"/Events?" + calDay.Year + "+" + calDay.Month + "+" + calDay.Day + "\">" + day.ToString() + "</a></td>" );
                    if ( calDay.DayOfWeek == DayOfWeek.Saturday )
                        responseMsg.Append( "<tr id=vh_calendar_row>" );
                }
                responseMsg.Append( "</table>\n" );                

            }
            else
            {
                responseMsg.Append("\nEvent plug-in is not loaded.");
            }
            responseMsg.Append("\n</DIV>\n");
            
        }

        private void AppendEvents( ref StringBuilder responseMsg,
                                   string RawUrl )
        {
            responseMsg.Append("\n<DIV id=vh_calendar_listing style=\"clear: both;\">\n");
            if ( bot.Plugins.IsLoaded("llEvents") &&
                 ( this.eventDatabase != null ) )
            {

                DateTime SearchDate = DateTime.UtcNow;
                bool SpecifiedDate = false;
                if ( RawUrl.Contains( "?" ) )
                {
                    SpecifiedDate = true;
                    RawUrl = RawUrl.Substring( RawUrl.IndexOf( '?' ) + 1 );
                    SearchDate = DateTime.ParseExact( RawUrl, "yyyy+M+d", CultureInfo.InvariantCulture );
                }

                string DateSpecBeg = string.Format(
                    "{0}/{1,2:D2}/{2,2:D2} {3,2:D2}:{4,2:D2}:00",
                    SearchDate.Year, SearchDate.Month, SearchDate.Day,
                    SearchDate.Hour, SearchDate.Minute );
                string DateSpecEnd = string.Format(
                    "{0}/{1,2:D2}/{2,2:D2} 23:59:59",
                    SearchDate.Year, SearchDate.Month, SearchDate.Day );

                using ( IDbCommand command =
                        this.eventDatabase.Connection.CreateCommand() )
                {

                    if ( SpecifiedDate )
                    {
                        responseMsg.Append( "Events on " + SearchDate.Month +
                                            "/" + SearchDate.Day + "/" +
                                            SearchDate.Year + ": <br>" );
                        command.CommandText = string.Format("SELECT * FROM events WHERE EventDateTime >= '{0}' AND EventDateTime <= '{1}'", DateSpecBeg, DateSpecEnd );
                    }
                    else
                    {
                        responseMsg.Append( "Upcoming Events: <br>" );
                        command.CommandText = string.Format( "SELECT * FROM events WHERE EventDateTime > '{0}'", DateSpecBeg);
                    }

                    IDataReader reader = command.ExecuteReader();
                    while ( reader.Read() )
                    {
                        responseMsg.Append( "<div id=vh_online_item style=\"clear: both;\">" +
                                            reader.GetString(1) + " GMT: " +
                                            reader.GetString(0) + "</div>" );
                    }
                }
            }
            else
            {
                responseMsg.Append("\nEvent plug-in is not loaded.");
            }
            responseMsg.Append("\n</DIV>\n");
           
        }

        public string AppendUsers(BotShell bot, string category, string[] users)
        {
            if (users.Length == 0)
                return String.Empty;

            StringBuilder response = new StringBuilder();

            response.Append("\n<DIV id=vh_online_listing style=\"clear: both;\">\n");
            response.Append( "<hr size=1>" + category );
            response.Append("</DIV><br>\n");

            foreach (string user in users)
            {
                if (string.IsNullOrEmpty(user))
                    continue;

                response.Append("\n<DIV id=vh_online_listing style=\"clear: both;\">\n");
                WhoisResult whois = XML.GetWhois(user, bot.Dimension);
                if (whois != null && whois.Stats != null)
                {
                    // append user image
                    string hostfix = whois.SmallPictureURL;
                    hostfix = hostfix.Replace ("www.anarchy-online.com", "people.anarchy-online.com");

                    response.Append("<div style=\"float: left; padding: 5px;\"><img src=\"");
                    response.Append(hostfix + "\"  align=\"left\" ></div>\n");

                    // append (clickable?) name
                    // the <div> here causes user info to line up next to image
                    response.Append("<div  style=\"float:left;\">" + whois.Name.ToString() + "<br>\n");

                    // append level (ai level)
                    response.AppendFormat("Level {0} ", whois.Stats.Level);
                    if (whois.Stats.DefenderLevel > 0) { }
                        response.AppendFormat("({0}) ", whois.Stats.DefenderLevel);

                    // append stats
                    response.Append(whois.Stats.Gender + " " + whois.Stats.Breed + " ");

                    // append profession
                    response.Append(whois.Stats.Faction + " " + whois.Stats.Profession + "<br>\n");

                    // append rank and organization
                    if ( whois.Organization != null )
                        response.Append(whois.Organization.ToString());
                    else
                        response.Append("(Not in an organization)");

                    // draw a horizontal rule between listings and closes info div
                    response.Append("\n<hr size=1></div>\n");

                }

                response.Append("</DIV><br>\n");
            }
            return response.ToString();
        }

        public string AppendOther(BotShell bot, string category, string icon, string[] users)
        {
            if (users.Length == 0)
                return String.Empty;

            StringBuilder response = new StringBuilder();

            response.Append("\n<DIV id=vh_online_listing style=\"clear: both;\">\n");
            response.Append( "<hr size=1>" + category );
            response.Append("</DIV><br>\n");

            foreach (string user in users)
            {
                if ( string.IsNullOrEmpty(user) ||
                     ( user.IndexOf("Unknown from") >=0 ) )
                    continue;
                response.Append("\n<DIV id=vh_online_listing style=\"clear: both;\">\n");
                response.Append("<div style=\"float: left; padding: 5px;\"><img src=\"" + icon + "\"  align=\"left\" ></div>\n");
                response.Append("<div  style=\"float:left;\">" + user + "<br></div>\n");                
                response.Append("</DIV><br>\n");
            }

            return response.ToString();

        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            if ( e.Args.Length == 0 )
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("::::: Web Query Module :::::");

                window.AppendLineBreak();
                window.AppendHighlight( "Status: " );
                if ( ( _httpListener != null ) && _httpListener.IsListening )
                {
                    window.AppendNormal( "Started [" );
                    window.AppendBotCommand ( "Stop", "webserver stop" );
                    window.AppendNormal( "]" );
                }
                else
                {
                    window.AppendNormal( "Stopped [" );
                    window.AppendBotCommand ( "Start", "webserver start" );
                    window.AppendNormal( "]" );
                }

                bot.SendReply(e, "Web Query Status »» ", window);

            }
            else switch (e.Args[0])
            {

            case "start":
                this.StartListener();
                bot.SendReply(e, "Starting WebGateway");
                break;

            case "stop":
                bot.SendReply(e, "Stopping WebGateway");
                this.StopListener();
                break;

            }

        }

		public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "webserver":
                return "Starts or stops the webserver service.\n\n" +
                    "Usage: /tell " + bot.Character +
                    " webserver [start|stop]";

            }

            return null;
        }

    }
}
