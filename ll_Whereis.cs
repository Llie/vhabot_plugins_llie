using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class Whereis : PluginBase
    {
        // private string Server = "nano.byethost12.com/services";
        // private string Server = "nano.exofire.net/services";
        private string Server = string.Empty;
        private string filename = "data/whereis.xml";
        private string UrlTemplate = "http://{0}/whereis.php?{1}={2}";
        private int Max = 100;

        public Whereis()
        {
            this.Name = "Whereis Database Lookup";
            this.InternalName = "llWhereis";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("whereis", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "whereis.xml" );
        }
        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            string search_command = "search";

            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: " + e.Command +
                              " [search string]");
                return;
            }

            if ( (e.Args.Length == 1) &&
                 ( e.Args[0].ToLower() == e.Sender.ToLower() ) )
            {
                string[] replies = new string[] {
                    "Aren't you the clever one?",
                    "Have you tried with both hands?",
                    "You are quite lost.",
                    "Try Shift-F9.",
                    "How would I know?"
                };
                Random random = new Random();
                bot.SendReply(e, replies[random.Next( replies.Length )] );
                return;                
            }

            string exact_search = e.Words[0];
            string search = exact_search.ToLower();
            string xml;
            string result = string.Empty;
            MemoryStream stream = null;
            WhereisResult search_results = null;

            if ( ! File.Exists ( filename ) )
            {

                string url = string.Format(this.UrlTemplate, this.Server,
                                           search_command,
                                           HttpUtility.UrlEncode(search));
                xml = HTML.GetHtml(url, 60000);
                if (string.IsNullOrEmpty(xml))
                {
                    bot.SendReply(e, "Unable to query the whereis database");
                    return;
                }

                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(WhereisResult));
                    search_results = (WhereisResult)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to query the whereis database";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            else
            {
                
                // search local file
                using(StreamReader rdr = File.OpenText(filename))
                    xml = rdr.ReadToEnd();
                if (string.IsNullOrEmpty(xml))
                {
                    Console.WriteLine( "Unable to read file: " + filename );
                    return;
                }
                
                WhereisResult all_locs = null;
                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    XmlSerializer serializer = new XmlSerializer(typeof(WhereisResult));
                    all_locs = (WhereisResult)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    result = "Unable to parse the whereis file";
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                search_results = all_locs;
                search_results.Version = all_locs.Version;
                search_results.Max = Convert.ToString ( Max );
                search_results.Credits = all_locs.Credits;

                Int32 i = 0;
                foreach ( Location loc in all_locs.Locations)
                {
                    if ( ( loc.name.ToLower().IndexOf( search ) >= 0 ) ||
                         ( loc.aka.ToLower().IndexOf( search ) >= 0 ) )
                        search_results.Locations[i++] = loc;
                }
                Array.Resize( ref search_results.Locations, i );
                search_results.Results = Convert.ToString( search_results.Locations.Length );

            }

            Int32 dr = 0;
            if ( search_results.Locations.Length > 1 )
                for ( int i=0; i<search_results.Locations.Length; i++)
                    if ( search_results.Locations[i].name == exact_search )
                        dr = i;

            if ( (search_results.Locations == null) ||
                 (search_results.Locations.Length == 0) )
            {
                result = "No matching locations were found.";
                
                List<string> other_commands = new List<string>();
                if ( bot.Plugins.IsLoaded("llAOUGuides") )
                    other_commands.Add("!locate");
                if ( bot.Plugins.IsLoaded("llNPCs") )
                    other_commands.Add("!npc");
                if ( bot.Plugins.IsLoaded("llUniqueMobs") )
                    other_commands.Add("!boss");
                if ( other_commands.Count > 0 )
                {
                    result = result + "  Perhaps the location you are looking for might be found in one of the other location databases this bot has access to.  You could try one of these other commands: ";
                    for ( int i=0; i<other_commands.Count; i++ )
                    {
                        result = result + other_commands[i];
                        if ( i < other_commands.Count - 1 )
                            result = result + ", ";
                    }
                }
            }
            else if ( ( search_results.Locations.Length == 1 ) ||
                      ( dr > 0 ) )
            {
                    
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Whereis Database");
                window.AppendHighlight("Server: ");
                if ( this.Server != string.Empty )
                    window.AppendNormal(this.Server);
                else
                    window.AppendNormal("local");
                window.AppendLineBreak();
                window.AppendHighlight("Version: ");
                window.AppendNormal(search_results.Version);
                window.AppendLineBreak();
                window.AppendHighlight("Search String: ");
                window.AppendNormal(search);
                window.AppendLineBreak(2);
                window.AppendHighlight(search_results.Locations[dr].name);
                int pfid = 0;
                Int32.TryParse ( search_results.Locations[dr].pfid, out pfid );
                if ( ( pfid > 0 ) &&
                     ( search_results.Locations[dr].coords != "na" ) )
                {
                    string coordstr = search_results.Locations[dr].coords;
                    coordstr = coordstr.Replace ( "x", " " );
                    window.AppendCommand( " (" + search_results.Locations[dr].coords + ")",
                                          "/waypoint " +
                                          coordstr + " " +
                                          search_results.Locations[dr].pfid );
                }
                else if ( search_results.Locations[dr].coords != "na" )
                { 
                    window.AppendString( " (" +
                                         search_results.Locations[dr].coords +
                                         ")" );
                }
                window.AppendLineBreak();
                window.AppendHighlight("Additional info: ");
                window.AppendLineBreak();
                window.AppendNormal( search_results.Locations[dr].info );
                bot.SendReply(e, search_results.Locations[dr].info +
                              " More Info »» ", window);
                return;
            }
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Whereis Database");
                window.AppendHighlight("Server: ");
                if ( this.Server != string.Empty )
                    window.AppendNormal(this.Server);
                else
                    window.AppendNormal("local");
                window.AppendLineBreak();
                window.AppendHighlight("Version: ");
                window.AppendNormal(search_results.Version);
                window.AppendLineBreak();
                window.AppendHighlight("Search String: ");
                window.AppendNormal(search);
                window.AppendLineBreak(2);
                Int32 i;
                result = "Do you mean one of these?: ";
                for ( i=0; i<search_results.Locations.Length; i++)
                {
                    result = result + search_results.Locations[i].name;
                    if ( i < search_results.Locations.Length - 2 )
                        result = result + ", ";
                    else if ( i < search_results.Locations.Length - 1 )
                        result = result + ", or ";
                    else
                        result = result + ".";
                    window.AppendNormal( "> " );
                    window.AppendCommand( search_results.Locations[i].name,
                                          "/tell " +  bot.Character +
                                          " whereis " +
                                          search_results.Locations[i].name);
                    window.AppendLineBreak();
                }
                bot.SendReply(e, result + " More Info »» ", window);
                return;

            }
            bot.SendReply(e, result);
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
            case "whereis":
                return "Allows you to search the central whereis database.\n" +
                    "To search the whereis database by name use: /tell " + bot.Character + " whereis [partial name]\n" +
                    "You don't need to specify the full name of the location, you can use multiple words in order to find a location.\n" +
                    "However, the different words are required to be in the right order to find a certain locations.\n" +
                    "For example: 'herc linc' will work but 'linc herc' won't if you want to find Dr. Hercules Lincoln";
            }
            return null;
        }
    }

    [XmlRoot("whereis")]
    public class WhereisResult
    {
        [XmlElement("version")]
        public string Version;
        [XmlElement("results")]
        public string Results;
        [XmlElement("max")]
        public string Max;
        [XmlElement("location")]
        public Location[] Locations;
        [XmlElement("credits")]
        public string Credits;
    }

    [XmlRoot("Location")]
    public class Location
    {
        [XmlElement("name")]
        public string name;
        [XmlElement("aka")]
        public string aka;
        [XmlElement("pfid")]
        public string pfid;
        [XmlElement("coords")]
        public string coords;
        [XmlElement("info")]
        public string info;
    }
}
