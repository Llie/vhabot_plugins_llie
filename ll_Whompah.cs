using System;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class WhompahRoute : PluginBase
    {
	    private static Playfields PFs;
        private static Whompahs WPs;

        public WhompahRoute()
        {
            this.Name = "Whompah Routing Guide";
            this.InternalName = "llWhompahs";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;
            this.Commands = new Command[] {
                new Command("whompah", true, UserLevel.Guest),
                new Command("whompa","whompah"),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Updater.Update( "pfid.xml" );
            Updater.Update( "whompah.xml" );

            PFs = PluginShared.GetPlayfields();
            WPs = (Whompahs)PluginShared.ParseXml(typeof(Whompahs),
                                                  "whompah.xml");
        }

        public int index ( string city )
        {
            int i=0, j=0;
            // construct some regexes
            string tmp = "^";
            for(j=0;j<city.Length;j++)
            {
                if ( Char.IsLetter(city[j]) )
                {
                    tmp += "[";
                    tmp += city.ToLower()[j];
                    tmp += city.ToUpper()[j];
                    tmp += "]";
                }
                else if ( Char.IsDigit( city[j] ) )
                    tmp += city[j];
                else
                    tmp += ".*";
            }
            tmp += ".*";
            Regex lss = new Regex ( tmp );
        
            tmp = String.Empty;
            for(j=0;j<city.Length;j++)
            {
                if ( j > 0 )
                    tmp += "[ (-][";
                else
                    tmp += "^[";
                tmp += city.ToLower()[j];
                tmp += city.ToUpper()[j];
                tmp += "].*";
            }
            Regex abb = new Regex ( tmp, RegexOptions.Multiline );

            // city
            for( i=0; i<WPs.List.Length; i++ )
                if ( lss.IsMatch( WPs.List[i].City ) )
                    return i;

            // zone match
            for( i=0; i<WPs.List.Length; i++ )
                if ( lss.IsMatch (PluginShared.GetPFName(WPs.List[i].Zone,
                                      PFs ) ) )
                    return i;

            // abbreviation matching
            for( i=0; i<WPs.List.Length; i++ )
                if ( ( WPs.List[i].City.IndexOf( " " ) >= 0 ) &&
                     abb.IsMatch( WPs.List[i].City ) )
                    return i;

            return -1;
        }

        public override void OnUnload(BotShell bot) { }

        public int path ( string start, string destination, ref string[] route )
        {
            return path ( start, destination, ref route, true  );
        }

        public int path ( string start, string destination, ref string[] path,
                          bool grid )
        {
            Whompahs network = (Whompahs)PluginShared.ParseXml(typeof(Whompahs),
                                                               "whompah.xml");

            if ( grid == false )
            {
                Whompahs network_nogrid = new Whompahs();
                network_nogrid.List = new Whompah[network.List.Length-1];
                int l = 0;
                foreach( Whompah w in network.List )
                    if ( w.City != "Grid" )
                    {
                        network_nogrid.List[l] = new Whompah();
                        network_nogrid.List[l].City = w.City;
                        network_nogrid.List[l].Faction = w.Faction;
                        network_nogrid.List[l].Zone = w.Zone;
                        network_nogrid.List[l].Connections = new Connections();
                        if ( Array.IndexOf(w.Connections.Cities, "Grid") >= 0 )
                            network_nogrid.List[l].Connections.Cities =
                                new string[w.Connections.Cities.Length-1];
                        else
                            network_nogrid.List[l].Connections.Cities =
                                new string[w.Connections.Cities.Length];
                        int m = 0;
                        foreach ( string c in w.Connections.Cities )
                            if ( c != "Grid" )
                                network_nogrid.List[l].Connections.Cities[m++] =
                                    c;
                        l++;
                    }
                network = network_nogrid;
            }

            Int32 distance = 0;

            // validate input
            if ( index(start) < 0 )
                return 0;
            else
                start = network.List[index(start)].City;
            if ( index(destination) < 0 )
                return 0;
            else
                destination = network.List[index(destination)].City;

            int i=0, j=0, done=0;
            for( i=0; i<network.List.Length; i++ )
                network.List[i].Zone = -1;

            network.List[index(start)].Zone = 0;

            while ( ( done == 0 ) && ( distance < network.List.Length ) )
            {
                for( i=0; i<network.List.Length; i++ )
                    if ( network.List[i].Zone < 0 )
                    {
                        for( j=0; j<network.List[i].Connections.Cities.Length; j++ )
                            if ( network.List[index(network.List[i].Connections.Cities[j])].Zone == distance )
                            {
                                network.List[i].Zone = distance + 1;
                                if ( network.List[i].City == destination )
                                    done = 1;
                            }
                    }
                distance ++;
            }

            path[0] = destination;
            for( i=1; i<distance; i++ )
                for ( j=0; j<network.List[index(path[i-1])].Connections.Cities.Length;
                      j++)
                    if(network.List[index(network.List[index(path[i-1])].Connections.Cities[j])].Zone == distance - i )
                        path[i] = network.List[index(network.List[index(path[i-1])].Connections.Cities[j])].City;
            path[distance] = start;

            return distance;
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Whom-Pah Routing Information");
            window.AppendLineBreak();
            if ( ( e.Args.Length < 1 ) || ( e.Args.Length > 2 ) )
            {
                window.AppendHighlight("Cities with Whom-Pahs: ");
                window.AppendLineBreak();
                int i;
                for ( i = 0; i<WPs.List.Length; i++ )
                {
                    window.AppendCommand( WPs.List[i].City, "/tell " +
                        bot.Character + " whompah " +
                                          WPs.List[i].City.Replace( " ", "-") );
                    window.AppendNormal ( " (" );
                    switch ( WPs.List[i].Faction )
                    {
                    case "Clan":
                        window.AppendColorString( "FF8040", "Clan"  );
                        break;
                    case "Omni":
                        window.AppendColorString( "2B60DE", "Omni" );
                        break;
                    case "Neutral":
                        window.AppendColorString( "008000", "Neutral" );
                        break;
                    }
                    window.AppendNormal ( ")" );
                    window.AppendColorEnd();
                    window.AppendLineBreak();
                }
                window.AppendLineBreak();
                window.AppendCommand( "Click here for more help", "/tell " +
                                      bot.Character + " help whompah" );

                bot.SendReply(e, "Correct Usage: whompah [from] [to] »» " +
                              window );

                return;
            }
            else if (e.Args.Length == 1)
            {
                string src = e.Args[0];
                if ( index(e.Args[0]) < 0 )
                {
                    window.AppendHighlight( "Unknown location: " );
                    window.AppendNormal( e.Args[0] );
                    window.AppendHighlight( ", or the specified location does not have a whom-pah" );
                    window.AppendLineBreak();
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here list of cities with Whom-Pahs", "/tell " +
                                          bot.Character + " whompah" );
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here for more help", "/tell " +
                                          bot.Character + " help whompah" );

                }
                else
                {
                    src = WPs.List[index(e.Args[0])].City;
                    window.AppendHighlight( "Whom-Pah at: " );
                    window.AppendNormal( src );
                    window.AppendHighlight( " connects to: " );
                    window.AppendLineBreak();
                    int i;
                    for(i=0;
                        i<WPs.List[index(e.Args[0])].Connections.Cities.Length;
                        i++ )
                    {
                        string cc=WPs.List[index(e.Args[0])].Connections.Cities[i];
                        window.AppendCommand( cc,
                            "/tell " + bot.Character + " whompah " +
                            cc.Replace( " ", "-") );
                        window.AppendNormal ( " (" );
                        switch ( WPs.List[index(cc)].Faction )
                        {
                        case "Clan":
                            window.AppendColorString( "FF8040", "Clan"  );
                            break;
                        case "Omni":
                            window.AppendColorString( "2B60DE", "Omni" );
                            break;
                        case "Neutral":
                            window.AppendColorString( "008000", "Neutral" );
                            break;
                        }
                        window.AppendNormal ( ")" );
                        window.AppendColorEnd();
                        window.AppendLineBreak();
                    }
                    window.AppendLineBreak();
                    window.AppendNormal ( "Specify a second city to compute a route." );
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here for more help", "/tell " +
                                          bot.Character + " help whompah" );

                }

                bot.SendReply(e, "Whom-Pah information for " + src + " »» " +
                              window );
                return;
                
            }
            else if (e.Args.Length == 2)
            {
                string src = e.Args[0];
                string des = e.Args[1];
                if ( index(e.Args[0]) < 0 )
                {
                    window.AppendHighlight( "Unknown location: " );
                    window.AppendNormal( e.Args[0] );
                    window.AppendHighlight( ", or the specified location does not have a whom-pah" );
                    window.AppendLineBreak();
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here list of cities with Whom-Pahs", "/tell " +
                                          bot.Character + " whompah" );
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here for more help", "/tell " +
                                          bot.Character + " help whompah" );
                }
                else if ( index(e.Args[1]) < 0 )
                {
                    window.AppendHighlight( "Unknown location: " );
                    window.AppendNormal( e.Args[1] );
                    window.AppendHighlight( ", or  the specified location does not have a whom-pah" );
                    window.AppendLineBreak();
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here list of cities with Whom-Pahs", "/tell " +
                                          bot.Character + " whompah" );
                    window.AppendLineBreak();
                    window.AppendCommand( "Click here for more help", "/tell " +
                                          bot.Character + " help whompah" );
                }
                else
                {
                    src = WPs.List[index(e.Args[0])].City;
                    des = WPs.List[index(e.Args[1])].City;
                    window.AppendHighlight( "Whom-Pah route from " );
                    window.AppendNormal( src );
                    window.AppendHighlight( " to " );
                    window.AppendNormal( des );
                    window.AppendLineBreak(2);

                    string[] nodes = new string[WPs.List.Length];
                    int distance = path( e.Args[0], e.Args[1], ref nodes );
                    if ( distance == 0 )
                    {
                        bot.SendReply(e, "Error finding whompah route.  Please try different specifying cities." );
                        return;
                    }
                    int i=0;
                    bool UsedGrid = false;
                    for(i=distance; i>=0; i-- )
                    {
                        window.AppendNormal( Convert.ToString(distance-i+1) +
                                             ". " );
                        window.AppendNormal( nodes[i] );
                        if ( nodes[i] == "Grid" )
                            UsedGrid = true;
                        window.AppendNormal ( " (" );
                        switch ( WPs.List[index(nodes[i])].Faction )
                        {
                        case "Clan":
                            window.AppendColorString( "FF8040", "Clan"  );
                            break;
                        case "Omni":
                            window.AppendColorString( "2B60DE", "Omni" );
                            break;
                        case "Neutral":
                            window.AppendColorString( "008000", "Neutral" );
                            break;
                        }
                        window.AppendColorEnd();
                        window.AppendNormal( ")" );
                        window.AppendLineBreak();
                    }

                    if ( UsedGrid == true )
                    {
                        distance = path( e.Args[0], e.Args[1], ref nodes,
                                         false );
                        window.AppendLineBreak();
                        window.AppendHighlight( "Alternate route from " );
                        window.AppendNormal( src );
                        window.AppendHighlight( " to " );
                        window.AppendNormal( des );
                        window.AppendHighlight( " without using Grid" );
                        window.AppendLineBreak(2);
                        for(i=distance; i>=0; i-- )
                        {
                            window.AppendNormal( Convert.ToString(distance-i+1) +
                                                 ". " );
                            window.AppendNormal( nodes[i] );
                            if ( nodes[i] == "Grid" )
                                UsedGrid = true;
                            window.AppendNormal ( " (" );
                            switch ( WPs.List[index(nodes[i])].Faction )
                            {
                            case "Clan":
                                window.AppendColorString( "FF8040", "Clan"  );
                                break;
                            case "Omni":
                                window.AppendColorString( "2B60DE", "Omni" );
                                break;
                            case "Neutral":
                                window.AppendColorString( "008000", "Neutral" );
                                break;
                            }
                            window.AppendColorEnd();
                            window.AppendNormal( ")" );
                            window.AppendLineBreak();
                        }
                    }
                }
                window.AppendLineBreak();
                
                bot.SendReply(e, "Whom-Pah route from " + src +
                              " to " + des + " »» " + window );
                return;
            }

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "whompah":
                    return "Usage: \tell " + bot.Character +
                        " whompa [origin] [destination]\n\n" +
                        "Attempt to provide directions for the shortest " +
                        "whom-pah route to a given destination.\n\nEnter " +
                        "multi-word city names by replacing spaces with " +
                        "dashes or use an abbreviation for the city name.  " +
                        "For instance, to enter &quot;Omni Trade&quot;, " +
                        "use &quot;omni-trade&quot; instead.  Or to enter " +
                        "&quot;Old Athen&quot; enter &quot;OA&quot; " +
                        "or &quot;Old-Athen&quot; instead.\n\n" +
                        "If both origin and destination are omitted, then " +
                        "a listing of all whom-pahs is returned.  If only " +
                        "the origin is specified, then a listing of " +
                        "destinations that are connected to that origin " +
                        "is listed.\n\n" +
                        "The Jobe Marketplace whompah is not included in "+
                        "this search.\n\n" +
                        "I make no guarantees that the bot won't attempt to " +
                        "route you through an opposing facton's city to get " +
                        "you to your destination.";
            }
            return null;
        }
    }

    [XmlRoot("whompahs")]
    public class Whompahs
    {
        [XmlElement("whompah")]
        public Whompah[] List;
    }

    [XmlRoot("whompah")]
    public class Whompah
    {
        [XmlElement("city")]
        public string City;
        [XmlElement("faction")]
        public string Faction;
        [XmlElement("zone")]
        public Int32 Zone;
        [XmlElement("connections")]
        public Connections Connections;
    }

    [XmlRoot("connections")]
    public class Connections
    {
        [XmlElement("city")]
        public string[] Cities;
    }

}
