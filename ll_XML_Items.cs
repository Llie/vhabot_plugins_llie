using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using AoLib.Utils;

namespace VhaBot.Plugins
{

    public class XMLItems : PluginBase
    {
        static string AOURL="http://itemxml.xyphos.com/?id={0}";
        private int NonPageSize = 3;

        public XMLItems()
        {
            this.Name = "Xyphos Items Lookup";
            this.InternalName = "llXMLItems";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("value", true, UserLevel.Guest),
                new Command("itemid", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot) { }

        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "value":
                DisplayValue ( bot, e );
                break;

            case "itemid":
                DisplayItem ( bot, e );
                break;

            }

        }

        static public Item GetItem( int id )
        {
            string url = string.Format(AOURL,Convert.ToString(id));            

            Item item = null;
            
            string xml = new System.Net.WebClient().DownloadString(url);
            if (string.IsNullOrEmpty(xml))
            {
                return item;
            }

            // clean up known HTML from XML to prevent deserialization errors
            xml = xml.Replace( "<br />", "&lt;br /&gt;" );

            MemoryStream stream = null;

            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(Item));
                item = (Item)serializer.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                return item;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return item;
        }

        static public Item GetItem( BotShell bot, CommandArgs e, int id )
        {
            return GetItem( bot, e, id, 0 );
        }

        static public Item GetItem( BotShell bot, CommandArgs e, int id,
                                    int ql )
        {
            string url = string.Format(AOURL,Convert.ToString(id));

            if ( ql > 0 )
                url = url + "&ql=" + Convert.ToString(ql);

            string xml = string.Empty;

            Item item = null;
            
            xml = new System.Net.WebClient().DownloadString(url);
            if (string.IsNullOrEmpty(xml))
            {
                bot.SendReply( e, "Unable to connect to Xyphos.");
                return item;
            }

            // clean up known HTML from XML to prevent deserialization errors
            xml = xml.Replace( "<br />", "&lt;br /&gt;" );

            MemoryStream stream = null;

            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(Item));
                item = (Item)serializer.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                bot.SendReply( e, "Unable parse item " + Convert.ToString(id)
                               + " from Xyphos.");
                return item;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return item;
        }

        private void DisplayValue ( BotShell bot, CommandArgs e )
        {

            if ( e.Items.Length < 1 )
            {
                bot.SendReply(e, "Correct Usage: value [[comp-lit] [clan|training|omni|trader]] [item item ...]");
                return;
            }

            float cl = 250.0f;
            float shop = 1.0f;
            string shoptype = "clan";
            int argparse = 0;

            try
            {
                if ( Convert.ToSingle( e.Args[argparse] ) > 0.0f )
                    cl = Convert.ToSingle( e.Args[argparse++] );
            }
            catch
            {
            }
            
            switch ( e.Args[argparse] )
            {
            case "training":
                shop = 1.05f;
                shoptype = "training ground";
                break;

            case "omni":
                shop = 1.5f;
                shoptype = "omni";
                break;

            case "trader":
                shop = 1.75f;
                shoptype = "trader";
                break;
            }

            float cl_factor = Convert.ToSingle ( Math.Floor( cl / 40.0f ) );

            RichTextWindow window = new RichTextWindow(bot);
            if ( e.Items.Length > NonPageSize )
            {
                window.AppendTitle("Items Value Lookup");
                window.AppendHighlight("Comp Lit: ");
                window.AppendNormal( Convert.ToString(cl) );
                window.AppendLineBreak();
                window.AppendHighlight("Shop Type: ");
                window.AppendNormal( shoptype );
                window.AppendLineBreak(2);
            }
            else
            {
                window.AppendLineBreak();
            }

            foreach (AoItem item in e.Items)
            {
                Item xmlitem = GetItem( bot, e, item.HighID, item.QL );
                float value=xmlitem.Attributes.GetAttributeByName ( "Value" ) *
                    shop * ( 100 + cl_factor ) / 2500;
                if ( e.Items.Length <= NonPageSize )
                    bot.SendReply( e, xmlitem.Name + " value is " +
                                   Convert.ToString ( value ) + " credits at " +
                                   shoptype + " shop terminal." );
                else
                {
                    window.AppendNormal( xmlitem.Name + ": " +
                                         Convert.ToString ( value ) +
                                         " credits." );
                    window.AppendLineBreak();
                }
            }

            if ( e.Items.Length > NonPageSize )
                bot.SendReply(e, " Results »» ", window );

        }

        private void DisplayItem ( BotShell bot, CommandArgs e )
        {
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: item [itemid]");
                return;
            }

            Item xmlitem = GetItem( bot, e, Convert.ToInt32( e.Args[0] ) );

            if ( xmlitem != null )
                bot.SendReply( e, e.Args[0] + " is " + xmlitem.Name );
        }

        public override string OnHelp(BotShell bot, string command)
        {

            switch (command)
            {

            case "value":
                return "Look up the estimated value of the specified item if sold to trader/omni/clan shop.\n";

            case "itemid":
                return "Look up the itemid of the specified item.\n";

            }

            return null;

        }

    }
    
    [XmlRoot("website")]
    public class WebSiteEl
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("link")]
        public string Link;
    }

    [XmlRoot("patches")]
    public class PatchesEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("patch")]
        public PatchEl[] Patch;
    }

    [XmlRoot("patch")]
    public class PatchEl
    {
        [XmlAttribute("num")]
        public int Num;
        [XmlAttribute("version")]
        public string Version;
    }

    [XmlRoot("attributes")]
    public class AttributesEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("attribute")]
        public AttributeEl[] Attribute;

        public Int32 GetAttributeByName ( string name )
        {
            for( int i=0; i<Count; i++ )
            {
                if ( Attribute[i].Name == name )
                    return Attribute[i].Value;
            }
            return 0;
        }

        public string GetAttributeExtraByName ( string name )
        {
            for( int i=0; i<Count; i++ )
            {
                if ( Attribute[i].Name == name )
                    return Attribute[i].Extra;
            }
            return string.Empty;
        }
    }

    [XmlRoot("attribute")]
    public class AttributeEl
    {
        [XmlAttribute("stat")]
        public int Stat;
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("value")]
        public Int32 Value;
        [XmlAttribute("extra")]
        public string Extra;
    }

    [XmlRoot("attacks")]
    public class AttacksEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("attack")]
        public AttackEl[] Attack;
    }

    [XmlRoot("attack")]
    public class AttackEl
    {
        [XmlAttribute("stat")]
        public int Stat;
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("percent")]
        public float Percent;
    }

    [XmlRoot("defenses")]
    public class DefensesEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("defense")]
        public DefenseEl[] Defense;
    }

    [XmlRoot("defense")]
    public class DefenseEl
    {
        [XmlAttribute("stat")]
        public int Stat;
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("percent")]
        public float Percent;
    }

    [XmlRoot("actions")]
    public class ActionsEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("action")]
        public ActionEl[] Action;
    }

    [XmlRoot("action")]
    public class ActionEl
    {
        [XmlAttribute("num")]
        public int Num;
        [XmlAttribute("name")]
        public string Name;
        [XmlElement("requirements")]
        public RequirementsEl Requirements;
    }

    [XmlRoot("requirements")]
    public class RequirementsEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("requirement")]
        public RequirementEl[] Requirement;
    }

    [XmlRoot("requirement")]
    public class RequirementEl
    {
        [XmlElement("target")]
        public NumNameEl Target;
        [XmlElement("stat")]
        public NumNameEl Stat;
        [XmlElement("op")]
        public NumNameEl Op;
        [XmlElement("value")]
        public NumNameEl Value;
        [XmlElement("subop")]
        public NumNameEl SubOp;
    }

    public class NumNameEl
    {
        [XmlAttribute("num")]
        public int Num;
        [XmlAttribute("name")]
        public string Name;
    }

    public class IDQLEl
    {
        [XmlAttribute("id")]
        public Int32 ID;
        [XmlAttribute("ql")]
        public int QL;
    }

    [XmlRoot("events")]
    public class EventsEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("event")]
        public EventEl[] Event;

        public EventEl GetEventByName ( string name )
        {
            for( int i=0; i<Count; i++ )
            {
                if ( Event[i].Name == name )
                    return Event[i];
            }
            return null;
        }

    }

    [XmlRoot("event")]
    public class EventEl
    {
        [XmlAttribute("num")]
        public int Num;
        [XmlAttribute("name")]
        public string Name;
        [XmlElement("functions")]
        public FunctionsEl Functions;
    }

    [XmlRoot("functions")]
    public class FunctionsEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("function")]
        public FunctionEl[] Function;
    }

    [XmlRoot("function")]
    public class FunctionEl
    {
        [XmlElement("target")]
        public NumNameEl[] Target;
        [XmlElement("func")]
        public FuncEl Func;
        [XmlElement("parameters")]
        public ParametersEl Parameters;
    }

    [XmlRoot("param")]
    public class ParametersEl
    {
        [XmlAttribute("count")]
        public int Count;
        [XmlElement("param")]
        public string[] Param;
    }

    [XmlRoot("func")]
    public class FuncEl
    {
        [XmlAttribute("num")]
        public int Num;
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("iterations")]
        public int Iterations;
        [XmlAttribute("delay")]
        public int Delay;
        [XmlAttribute("timespan")]
        public string Timespan;
    }

    [XmlRoot("item")]
    public class Item
    {
        [XmlElement("website")]
        public WebSiteEl Website;
        [XmlElement("name")]
        public string Name;
        [XmlElement("description")]
        public string Description;
        [XmlElement("version")]
        public string Version;
        [XmlElement("low")]
        public IDQLEl LowID;
        [XmlElement("high")]
        public IDQLEl HighID;        
        [XmlElement("patches")]
        public PatchesEl Patches;        
        [XmlElement("attributes")]
        public AttributesEl Attributes;        
        [XmlElement("attacks")]
        public AttacksEl Attacks;        
        [XmlElement("defenses")]
        public DefensesEl Defenses;        
        [XmlElement("actions")]
        public ActionsEl Actions;
        [XmlElement("events")]
        public EventsEl Events;
    }

}
